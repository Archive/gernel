#include <gernel.h>
#include <gernel_canvas_section.h>
#include <gernel_canvas_text.h>
#include <X11/Xlib.h>
#include <gdk/gdkprivate.h>

void gernel_canvas_section_show(
		GernelCanvasSection *canvas_section
		);

void gernel_canvas_section_hide(
		GernelCanvasSection *canvas_section
		);

static guint32 gernel_canvas_section_get_foreground_color(
		GernelCanvasSection *canvas_section
		);

static guint32 gernel_canvas_section_get_background_color(
		GernelCanvasSection *canvas_section
		);

static void gernel_canvas_section_init(
		GernelCanvasSection *canvas_section
		);

static void gernel_canvas_section_class_init(
		GernelCanvasSectionClass *klass
		);

static void gernel_canvas_section_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void gernel_canvas_section_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static gchar* gernel_canvas_section_get_text(
		GernelCanvasSection *canvas_section
		);

static void gernel_canvas_section_set_group(
		GernelCanvasSection *canvas_section,
		GtkWidget *canvas
		);

static GnomeCanvasGroupClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_CANVAS,
	ARG_TYPE,
	ARG_BACKGROUND_COLOR,
	ARG_FOREGROUND_COLOR,
	ARG_TEXT,
	ARG_WIDTH,
	ARG_HEIGHT,
	ARG_FONTSET	};

GtkType gernel_canvas_section_get_type(	void	)
/*	Return type identifier for type GernelCanvasSection	*/
{
	static GtkType	canvas_section_type = 0;

	if (!canvas_section_type) {

		static const GtkTypeInfo	canvas_section_info = {
			"GernelCanvasSection",
			sizeof(GernelCanvasSection),
			sizeof(GernelCanvasSectionClass),
			(GtkClassInitFunc)gernel_canvas_section_class_init,
			(GtkObjectInitFunc)gernel_canvas_section_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		canvas_section_type = gtk_type_unique(
				gnome_canvas_group_get_type(),
				&canvas_section_info
				);
	}

	return canvas_section_type;
}

GernelCanvasSection* gernel_canvas_section_new(
		enum GernelCanvasSectionType type,
		GtkWidget *canvas
		)
/*	Create an instance of GernelCanvasSection	*/
{
	GtkObject	*canvas_section;

	g_return_val_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0, NULL);
	g_return_val_if_fail(canvas != NULL, NULL);
	g_return_val_if_fail(GNOME_IS_CANVAS(canvas), NULL);

	canvas_section = gtk_object_new(gernel_canvas_section_get_type(),
					"canvas", canvas,
					"type", type,
					NULL	);

	return GERNEL_CANVAS_SECTION(canvas_section);
}

static void gernel_canvas_section_init(	GernelCanvasSection *canvas_section )
/*	Initialize a GernelCanvasSection	*/
{
	canvas_section->type = GERNEL_CANVAS_SECTION_TYPE_0;
	canvas_section->background = NULL;
	canvas_section->foreground = NULL;
	canvas_section->foreground_color = 0;
	canvas_section->background_color = 0;
	canvas_section->text = NULL;
	canvas_section->group = NULL;
	canvas_section->height = 0;
	canvas_section->width = 0;
	canvas_section->x = 0;
	canvas_section->y = 0;
	canvas_section->visible = TRUE;
}

static void gernel_canvas_section_class_init(	GernelCanvasSectionClass *klass)
/*	Initialize the GernelCanvasSectionClass	*/
{
	GtkObjectClass		*object_class;
	GnomeCanvasItemClass	*canvas_item_class;

	object_class = (GtkObjectClass*)klass;
	canvas_item_class = (GnomeCanvasItemClass*)klass;

	parent_class = gtk_type_class(GNOME_TYPE_CANVAS_GROUP);

	gtk_object_add_arg_type(	"GernelCanvasSection::width",
					GTK_TYPE_INT,
					GTK_ARG_READWRITE,
					ARG_WIDTH	);

	gtk_object_add_arg_type(	"GernelCanvasSection::height",
					GTK_TYPE_INT,
					GTK_ARG_READABLE,
					ARG_HEIGHT	);

	gtk_object_add_arg_type(	"GernelCanvasSection::canvas",
					GTK_TYPE_WIDGET,
					GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT,
					ARG_CANVAS	);

	gtk_object_add_arg_type(	"GernelCanvasSection::type",
					GTK_TYPE_ENUM,
					GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT,
					ARG_TYPE	);

	gtk_object_add_arg_type(	"GernelCanvasSection::fontset",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_FONTSET	);

	gtk_object_add_arg_type(	"GernelCanvasSection::background_color",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_BACKGROUND_COLOR	);

	gtk_object_add_arg_type(	"GernelCanvasSection::foreground_color",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_FOREGROUND_COLOR	);

	gtk_object_add_arg_type(	"GernelCanvasSection::text",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_TEXT	);

	object_class->get_arg = gernel_canvas_section_get_arg;
	object_class->set_arg = gernel_canvas_section_set_arg;
}

static void gernel_canvas_section_set_type(
		GernelCanvasSection *canvas_section,
		enum GernelCanvasSectionType type
		)
/*	Set the type of canvas_section	*/
{
	GnomeCanvasItem		*foreground, *background, *canvas_item;
	GnomeCanvasGroup	*canvas_group;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));
	g_return_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0);

	canvas_group = canvas_section->group;
	canvas_item = GNOME_CANVAS_ITEM(canvas_section);
	gnome_canvas_item_construct(canvas_item, canvas_group, NULL, NULL);
	canvas_section->type = type;
	canvas_group = GNOME_CANVAS_GROUP(canvas_section);

	switch (type) {
		GernelCanvasText	*canvas_text;

		case GERNEL_CANVAS_SECTION_TYPE_PROMPT:
		case GERNEL_CANVAS_SECTION_TYPE_HEADER:
		case GERNEL_CANVAS_SECTION_TYPE_TITLE:

			background = gnome_canvas_item_new(
					canvas_group,
					gnome_canvas_rect_get_type(),
					NULL
					);

			canvas_section->background = background;
			canvas_text = gernel_canvas_text_new(canvas_group);
			foreground = GNOME_CANVAS_ITEM(canvas_text);
			canvas_section->foreground = foreground;
			break;
		case GERNEL_CANVAS_SECTION_TYPE_BACKGROUND:
		case GERNEL_CANVAS_SECTION_TYPE_TAIL:

			background = gnome_canvas_item_new(
					canvas_group,
					gnome_canvas_rect_get_type(),
					NULL
					);

			canvas_section->background = background;
			break;
		case GERNEL_CANVAS_SECTION_TYPE_TEXT:
			canvas_text = gernel_canvas_text_new(canvas_group);
			foreground = GNOME_CANVAS_ITEM(canvas_text);
			canvas_section->foreground = foreground;
			break;
		default:
			g_assert_not_reached();
			break;
	}
}

void gernel_canvas_section_set_text(	GernelCanvasSection *canvas_section, 
					gchar *text	)
/*	Set the foreground text of canvas_section	*/
{
	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));
	g_return_if_fail(canvas_section->foreground != NULL);

	canvas_section->text = text;

	gnome_canvas_item_set(	canvas_section->foreground, 
				"text", text, 
				NULL	);
}

static gchar* gernel_canvas_section_get_text(
		GernelCanvasSection *canvas_section
		)
/*	Get the text canvas_section displays	*/
{
	g_return_val_if_fail(canvas_section, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), NULL);

	return canvas_section->text;
}

void gernel_canvas_section_set_fontset(	GernelCanvasSection *canvas_section,
					gchar *fontset	)
/*	Set the fontset of canvas_section	*/
{
	GernelCanvasText	*canvas_text;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));
	g_return_if_fail(fontset != NULL);
	g_return_if_fail(canvas_section->foreground != NULL);

	canvas_text = GERNEL_CANVAS_TEXT(canvas_section->foreground);
	gernel_canvas_text_set_fontset(canvas_text, fontset);
}

void gernel_canvas_section_set_font(	GernelCanvasSection *canvas_section,
					GdkFont *font	)
/*	Set the font to be used by canvas_section	*/
{
	XFontStruct	**font_structs;
	gchar		*fontset, **font_names;
	GdkFontPrivate	*private;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));
	g_return_if_fail(font != NULL);

	if (canvas_section->foreground == NULL) return;

	private = (GdkFontPrivate*)font;
	XFontsOfFontSet(private->xfont, &font_structs, &font_names);

	if (font->type == GDK_FONT_FONTSET) {
		fontset = G_CHAR(private->names->data);
	} else
		fontset = g_strconcat(	G_CHAR(private->names->data),
					",",
					G_CHAR(private->names->data),
					",",
					G_CHAR(private->names->data),
					NULL	);

	gernel_canvas_section_set_fontset(canvas_section, fontset);
}

void gernel_canvas_section_set_foreground_color(
		GernelCanvasSection *canvas_section,
		guint32 color
		)
/*	Set the foreground color of canvas_section	*/
{
	GernelCanvasText	*canvas_text;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	if (canvas_section->foreground == NULL) return;
	canvas_text = GERNEL_CANVAS_TEXT(canvas_section->foreground);
	gernel_canvas_text_set_color(canvas_text, color);
	canvas_section->foreground_color = color;
}

void gernel_canvas_section_set_background_color(
		GernelCanvasSection *canvas_section,
		guint32 color
		)
/*	Set the foreground color of canvas_section	*/
{
	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	canvas_section->background_color = color;

	if (	(canvas_section->type != GERNEL_CANVAS_SECTION_TYPE_PROMPT) &
		(canvas_section->type != GERNEL_CANVAS_SECTION_TYPE_TEXT) )

		gnome_canvas_item_set(	canvas_section->background, 
					"fill_color_rgba", color,
					NULL	);
}

static guint32 gernel_canvas_section_get_foreground_color(
		GernelCanvasSection *canvas_section
		)
/*	Get the color of canvas_section's foreground	*/
{
	g_return_val_if_fail(canvas_section != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), 0);

	return canvas_section->foreground_color;
}

static guint32 gernel_canvas_section_get_background_color(
		GernelCanvasSection *canvas_section
		)
/*	Get the color of canvas_section's background	*/
{
	g_return_val_if_fail(canvas_section != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), 0);

	return canvas_section->background_color;
}

static gchar* gernel_canvas_section_get_fontset(
		GernelCanvasSection *canvas_section
		)
/*	Get fontset of canvas_section	*/
{
	GernelCanvasText	*canvas_text;

	g_return_val_if_fail(canvas_section != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), NULL);

	canvas_text = GERNEL_CANVAS_TEXT(canvas_section->foreground);

	return gernel_canvas_text_get_fontset(canvas_text);
}

static void gernel_canvas_section_set_group(
		GernelCanvasSection *canvas_section,
		GtkWidget *widget
		)
/*	Set the rendering base for canvas_section	*/
{
	GnomeCanvas	*canvas;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GNOME_IS_CANVAS(widget));

	canvas = GNOME_CANVAS(widget);

	canvas_section->group = GNOME_CANVAS_GROUP(gnome_canvas_root(canvas));
}

void gernel_canvas_section_set_width(	GernelCanvasSection *canvas_section,
					gint width	)
/*	Set width of canvas_section	*/
{
	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	canvas_section->width = width;

	if (canvas_section->foreground != NULL) {
		GernelCanvasText	*canvas_text;

		canvas_text = GERNEL_CANVAS_TEXT(canvas_section->foreground);
		gernel_canvas_text_set_width(canvas_text, width);
	}

	if (canvas_section->background != NULL) {
		GnomeCanvasItem	*canvas_item;

		canvas_item = GNOME_CANVAS_ITEM(canvas_section->background);

		gnome_canvas_item_set(	canvas_item,
					"x1", 0.0,
					"x2", (double)width,
					NULL	);

	}

/*	gernel_canvas_section_set_height(canvas_section);	*/
}

void gernel_canvas_section_set_height(	GernelCanvasSection *canvas_section )
/*	Recompute the height needed by canvas_section and apply it to its
	elements	*/
{
	gint	height;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	height = 0;

	if (canvas_section->foreground != NULL) {
		GernelCanvasText	*canvas_text;

		canvas_text = GERNEL_CANVAS_TEXT(canvas_section->foreground);
		height += gernel_canvas_text_get_height(canvas_text);
	} 

	if (canvas_section->background != NULL) {
		double		y2;
		GnomeCanvasItem	*canvas_item;

		canvas_item = GNOME_CANVAS_ITEM(canvas_section->background);

		y2 = (double)MAX(height, GNOME_PAD);
		gnome_canvas_item_set(canvas_item, "y1", 0.0, "y2", y2, NULL);
	}

	canvas_section->height = MAX(height, GNOME_PAD);
}

void gernel_canvas_section_set_height_custom(
		GernelCanvasSection *canvas_section,
		gint height
		)
/*	Force height of canvas_section to 'height'	*/
{
	GnomeCanvasItem	*canvas_item;
	double		y2;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	canvas_item = GNOME_CANVAS_ITEM(canvas_section->background);
	y2 = (double)height;

	gnome_canvas_item_set(canvas_item, "y1", 0.0, "y2", y2, NULL);
}

gint gernel_canvas_section_get_width(	GernelCanvasSection *canvas_section )
/*	Get width of canvas_section	*/
{
	g_return_val_if_fail(canvas_section != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), 0);

	return canvas_section->width;
}

gint gernel_canvas_section_get_height(	GernelCanvasSection *canvas_section )
/*	Get height of canvas_section; type dependant	*/
{
	g_return_val_if_fail(canvas_section != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), 0);

	if (gernel_canvas_section_is_visible(canvas_section))
		return canvas_section->height;
	else
		return 0;
}

gint gernel_canvas_section_get_y(	GernelCanvasSection *canvas_section )
/*	Get vertical start point of canvas_section	*/
{
	g_return_val_if_fail(canvas_section != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), 0);

	return canvas_section->y;
}

void gernel_canvas_section_set_x(	GernelCanvasSection *canvas_section,
					gint x	)
/*	Set horizontal start point of canvas_section	*/
{
	GnomeCanvasItem	*canvas_item;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	canvas_item = GNOME_CANVAS_ITEM(canvas_section);
	gnome_canvas_item_set(canvas_item, "x", (double)x, NULL);
}

void gernel_canvas_section_set_y(	GernelCanvasSection *canvas_section,
					gint y	)
/*	Set vertical start point of canvas_section	*/
{
	GnomeCanvasItem	*canvas_item;

	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	canvas_item = GNOME_CANVAS_ITEM(canvas_section);
	canvas_section->y = y;
	gnome_canvas_item_set(canvas_item, "y", (double)y, NULL);
}

static void gernel_canvas_section_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelCanvasSection	*canvas_section;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(object));

	canvas_section = GERNEL_CANVAS_SECTION(object);

	switch (arg_id) {
		GtkWidget	*widget;
		gchar		*str;
		gint		width;

		case ARG_CANVAS:
			widget = GTK_WIDGET(GTK_VALUE_OBJECT(*arg));
			gernel_canvas_section_set_group(canvas_section, widget);
			break;
		case ARG_TYPE:

			gernel_canvas_section_set_type(	canvas_section, 
							GTK_VALUE_ENUM(*arg) );

			break;
		case ARG_FONTSET:
			str = GTK_VALUE_STRING(*arg);
			gernel_canvas_section_set_fontset(canvas_section, str);
			break;
		case ARG_BACKGROUND_COLOR:

			gernel_canvas_section_set_background_color(
					canvas_section,
					GTK_VALUE_UINT(*arg)
					);

			break;
		case ARG_FOREGROUND_COLOR:

			gernel_canvas_section_set_foreground_color(
					canvas_section,
					GTK_VALUE_UINT(*arg)
					);

			break;
		case ARG_TEXT:

			gernel_canvas_section_set_text(	canvas_section, 
							GTK_VALUE_STRING(*arg));

			break;
		case ARG_WIDTH:
			width = GTK_VALUE_INT(*arg);
			gernel_canvas_section_set_width(canvas_section, width);
			break;
		case ARG_HEIGHT:	/*	Fall Through	*/
		default:
			break;
	}
}

static void gernel_canvas_section_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelCanvasSection	*canvas_section;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(object));

	canvas_section = GERNEL_CANVAS_SECTION(object);

	switch (arg_id) {
		gchar	*str;
		guint32	value;
		gint	width, height;

		case ARG_FONTSET:

			str = gernel_canvas_section_get_fontset(
					canvas_section
					);

			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_BACKGROUND_COLOR:

			value = gernel_canvas_section_get_background_color(
					canvas_section
					);

			GTK_VALUE_UINT(*arg) = value;
			break;
		case ARG_FOREGROUND_COLOR:

			value = gernel_canvas_section_get_foreground_color(
					canvas_section
					);

			GTK_VALUE_UINT(*arg) = value;
			break;
		case ARG_TEXT:
			str = gernel_canvas_section_get_text(canvas_section);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_WIDTH:
			width = gernel_canvas_section_get_width(canvas_section);
			GTK_VALUE_INT(*arg) = width;
			break;
		case ARG_HEIGHT:
			height=gernel_canvas_section_get_height(canvas_section);
			GTK_VALUE_INT(*arg) = height;
			break;
		case ARG_TYPE:	/*	Fall Through	*/
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

void gernel_canvas_section_hide(	GernelCanvasSection *canvas_section )
/*	Hide canvas_section	*/
{
	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	gnome_canvas_item_hide(GNOME_CANVAS_ITEM(canvas_section));
	canvas_section->visible = FALSE;
}

void gernel_canvas_section_show(	GernelCanvasSection *canvas_section )
/*	Show canvas_section	*/
{
	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	gnome_canvas_item_show(GNOME_CANVAS_ITEM(canvas_section));
	canvas_section->visible = TRUE;
}

gboolean gernel_canvas_section_is_visible(
		GernelCanvasSection *canvas_section
		)
/*	Check if canvas_section is currently visible	*/
{
	g_return_val_if_fail(canvas_section != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section), FALSE);

	return canvas_section->visible;
}
