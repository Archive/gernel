#include <gernel.h>
#include <gernel_canvas_menu.h>

static void gernel_canvas_menu_init(	GernelCanvasMenu *canvas_menu	);
static void gernel_canvas_menu_class_init(	GernelCanvasMenuClass *klass );
static void gernel_canvas_menu_preferences(	GernelCanvasMenu *canvas_menu );

static void gernel_canvas_menu_construct(	GernelCanvasMenu *canvas_menu,
						GtkWidget *app	);

static GernelCanvasMenuClass	*parent_class = NULL;

enum {	PREFERENCES,
	LAST_SIGNAL	};

static guint canvas_menu_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_canvas_menu_get_type(	void	)
/*	Return type identifier for type GernelCanvasMenu	*/
{
	static GtkType	canvas_menu_type = 0;

	if (!canvas_menu_type) {

		static const GtkTypeInfo	canvas_menu_info = {
				"GernelCanvasMenu",
				sizeof(GernelCanvasMenu),
				sizeof(GernelCanvasMenuClass),
				(GtkClassInitFunc)gernel_canvas_menu_class_init,
				(GtkObjectInitFunc)gernel_canvas_menu_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		canvas_menu_type = gtk_type_unique(	gtk_menu_get_type(),
							&canvas_menu_info );
	}

	return canvas_menu_type;
}

GtkWidget* gernel_canvas_menu_new(	GtkWidget *app	)
/*	Create an instance of GernelCanvasMenu	*/
{
	GernelCanvasMenu	*canvas_menu;

	g_return_val_if_fail(app != NULL, NULL);
	g_return_val_if_fail(GNOME_IS_APP(app), NULL);

	canvas_menu = gtk_type_new(gernel_canvas_menu_get_type());
	gernel_canvas_menu_construct(canvas_menu, app);

	return GTK_WIDGET(canvas_menu);
}

static void gernel_canvas_menu_init(	GernelCanvasMenu *canvas_menu	)
/*	Initialize the GernelCanvasMenu	*/
{
	canvas_menu->preferences = NULL;
	canvas_menu->uiinfo = NULL;
}

static void gernel_canvas_menu_class_init(	GernelCanvasMenuClass *klass )
/*	Initialize the GernelCanvasMenuClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkMenuShellClass	*menu_shell_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	menu_shell_class = (GtkMenuShellClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_MENU);

	function_offset = GTK_SIGNAL_OFFSET(GernelCanvasMenuClass, preferences);

	canvas_menu_signals[PREFERENCES] =

			gtk_signal_new(	"preferences",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class, 
					canvas_menu_signals, 
					LAST_SIGNAL	);

	klass->preferences = NULL;
}

static void gernel_canvas_menu_construct(	GernelCanvasMenu *canvas_menu,
						GtkWidget *app	)
/*	Fill the menu for the requirements of the Gernel's text viewer	*/
{
	GnomeUIInfo menu[] = {	{	GNOME_APP_UI_ITEM,
					_("_Preferences..."),
					_("Configure the Text Viewer"),
					NULL,
					NULL,
					NULL,
					GNOME_APP_PIXMAP_STOCK,
					GNOME_STOCK_MENU_PREF,
					'p',
					GDK_CONTROL_MASK	},

					GNOMEUIINFO_END	};

	g_return_if_fail(canvas_menu != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_MENU(canvas_menu));
	g_return_if_fail(app != NULL);
	g_return_if_fail(GNOME_IS_APP(app));

	gnome_app_fill_menu(	GTK_MENU_SHELL(canvas_menu), 
				menu, 
				GNOME_APP(app)->accel_group, 
				TRUE, 
				0	);

	gnome_app_install_menu_hints(GNOME_APP(app), menu);
	canvas_menu->uiinfo = menu;
	canvas_menu->preferences = menu[0].widget;

	gtk_signal_connect_object(	GTK_OBJECT(canvas_menu->preferences),
					"activate",
					gernel_canvas_menu_preferences,
					GTK_OBJECT(canvas_menu)	);
}

static void gernel_canvas_menu_preferences(	GernelCanvasMenu *canvas_menu )
/*	Emit the 'preferences' signal	*/
{
	g_return_if_fail(canvas_menu != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_MENU(canvas_menu));

	gtk_signal_emit(	GTK_OBJECT(canvas_menu), 
				canvas_menu_signals[PREFERENCES]	);
}

void gernel_canvas_menu_popup(	GernelCanvasMenu *canvas_menu,
				guint32 time	)
/*	Popup canvas_menu	*/
{
	g_return_if_fail(canvas_menu != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_MENU(canvas_menu));

	gtk_menu_popup(GTK_MENU(canvas_menu), NULL, NULL, NULL, NULL, 3, time);
}
