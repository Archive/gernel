#define GERNEL_TYPE_DEPENDENCY	(gernel_dependency_get_type())

#define GERNEL_DEPENDENCY(obj) \
	(GTK_CHECK_CAST(	(obj), \
				GERNEL_TYPE_DEPENDENCY, \
				GernelDependency	))

#define GERNEL_DEPENDENCY_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_DEPENDENCY, \
				GernelDependencyClass	))

#define GERNEL_IS_DEPENDENCY(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_DEPENDENCY))

#define GERNEL_IS_DEPENDENCY_CLASS(klass), \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_DEPENDENCY))

typedef struct _GernelDependency	GernelDependency;
typedef struct _GernelDependencyClass	GernelDependencyClass;

typedef enum {	GERNEL_DEPENDENCY_MODE_DEPENDANTS,
		GERNEL_DEPENDENCY_MODE_PREREQUISITES	} GernelDependencyMode;

struct _GernelDependency{
	GnomeDialog		dialog;

	GernelDependencyMode	mode;
	/*	Depending on GernelDependencyMode, this is either a tree or a 
		list widget	*/
	GtkWidget		*viewer;
};

struct _GernelDependencyClass {
	GnomeDialogClass	parent_class;
};

GtkType		gernel_dependency_get_type(	void	);

GtkWidget*	gernel_dependency_new(	GernelDependencyMode mode,
					GtkWidget *var_item	);

void		gernel_dependency_set_title(	GernelDependency *dependency,
						gchar *title	);

void		gernel_dependency_add_info(	GernelDependency *dependency,
						gchar *info	);
