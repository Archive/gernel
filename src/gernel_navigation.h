#include <gernel_scrolled_window.h>

#define	GERNEL_TYPE_NAVIGATION	(gernel_navigation_get_type())

#define	GERNEL_NAVIGATION(obj) \
	(GTK_CHECK_CAST(	(obj), \
				GERNEL_TYPE_NAVIGATION, \
				GernelNavigation	))

#define	GERNEL_NAVIGATION_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_NAVIGATION, \
				GernelNavigation	))

#define	GERNEL_IS_NAVIGATION(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_NAVIGATION))

#define GERNEL_IS_NAVIGATION_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass, GERNEL_TYPE_NAVIGATION))

typedef struct _GernelNavigation	GernelNavigation;
typedef struct _GernelNavigationClass	GernelNavigationClass;

struct _GernelNavigation {
	GernelScrolledWindow	scrolled_window;

	gchar			*title, *local_name;
	GtkWidget		*tree;

	gchar			*base_directory, *contents_path, 
				*template_path, *navigation_path;

	GSList			*history, *future;
	GCache			*cache;

	/*	If TRUE, hide all variables whose preconditions are not met;
		otherwise, they will be shown yet be insensitive	*/
	gboolean		hide_unmet;
};

struct _GernelNavigationClass {
	GernelScrolledWindowClass	parent_class;

	void (*dependants)		(	GernelNavigation *navigation,
						gboolean flag	);

	void (*prerequisites)		(	GernelNavigation *navigation,
						gboolean flag	);

	void (*selected)		(	GernelNavigation *navigation,
						GtkWidget *var_item	);

	void (*unselected)		(	GernelNavigation *navigation );

	void (*updated)			(	GernelNavigation *navigation,
						gchar *message1,
						gchar *message2	);

	void (*done_parsing)		(	GernelNavigation *navigation );

	void (*history)			(	GernelNavigation *navigation,
						guint history,
						guint future	);

	void (*up_possible)		(	GernelNavigation *navigation,
						gboolean flag	);

	void (*down_possible)		(	GernelNavigation *navigation,
						gboolean flag	);

	void (*next_possible)		(	GernelNavigation *navigation,
						gboolean flag	);

	void (*previous_possible)	(	GernelNavigation *navigation,
						gboolean flag	);

	void (*title)			(	GernelNavigation *navigation,
						gchar *title	);

	void (*view_changed)		(	GernelNavigation *navigation,
						gchar *description,
						gchar *name,
						gchar *text	);
};

GtkType		gernel_navigation_get_type(	void	);
GtkWidget*	gernel_navigation_new(	gchar *name	);

void		gernel_navigation_parse(	GernelNavigation *navigation,
						GdkWindow *window	);

GtkWidget*	gernel_navigation_get_selection(GernelNavigation *navigation );

void		gernel_navigation_read_defaults(
		GernelNavigation *navigation,
		GSList *lines
		);

void		gernel_navigation_set_base_directory(
		GernelNavigation *navigation,
		gchar *directory
		);

void		gernel_navigation_set_template_path(
		GernelNavigation *navigation,
		gchar *path
		);

void		gernel_navigation_set_contents_path(
		GernelNavigation *navigation,
		gchar *path
		);

void		gernel_navigation_set_navigation_path(
		GernelNavigation *navigation,
		gchar *path
		);

void		gernel_navigation_search(	GernelNavigation *navigation );
void		gernel_navigation_up(	GernelNavigation *navigation	);
void		gernel_navigation_down(	GernelNavigation *navigation	);
void		gernel_navigation_next(	GernelNavigation *navigation	);
void		gernel_navigation_previous(	GernelNavigation *navigation );
void		gernel_navigation_back(	GernelNavigation *navigation	);
void		gernel_navigation_forward(	GernelNavigation *navigation );
void		gernel_navigation_prerequisites(GernelNavigation *navigation );
void		gernel_navigation_dependants(	GernelNavigation *navigation );

void		gernel_navigation_save_as(	GernelNavigation *navigation,
						gchar *path	);
