#define GERNEL_TYPE_CANVAS_TEXT	(gernel_canvas_text_get_type ())

#define GERNEL_CANVAS_TEXT(obj)	\
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_CANVAS_TEXT, GernelCanvasText))

#define GERNEL_CANVAS_TEXT_CLASS(klass)	\
	(GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_CANVAS_TEXT, \
				GernelCanvasTextClass	))

#define GERNEL_IS_CANVAS_TEXT(obj)	(GTK_CHECK_TYPE ((obj), \
					GERNEL_TYPE_CANVAS_TEXT))

#define GERNEL_IS_CANVAS_TEXT_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass),\
						GNOME_TYPE_CANVAS_TEXT))


typedef struct _GernelCanvasText	GernelCanvasText;
typedef struct _GernelCanvasTextClass	GernelCanvasTextClass;

struct _GernelCanvasText {
	GnomeIconTextItem	item;

	gchar		*text;
	gchar		*fontset;
	GdkGC		*gc;
	guint32		color;
	gboolean	color_change_pending;

	/*	width is the width actually needed by item	*/
	double		width, height, x, y, right_margin, bottom_margin;
};

struct _GernelCanvasTextClass {
	GnomeIconTextItemClass parent_class;
};

GtkType 		gernel_canvas_text_get_type(	void	);
GernelCanvasText*	gernel_canvas_text_new(	GnomeCanvasGroup *group	);
gint gernel_canvas_text_get_height(	GernelCanvasText *canvas_text	);

void gernel_canvas_text_set_width(	GernelCanvasText *canvas_text,
					gint width	);

void gernel_canvas_text_set_color(	GernelCanvasText *canvas_text,
					guint32 color	);

gchar* gernel_canvas_text_get_fontset(	GernelCanvasText *canvas_text	);

void gernel_canvas_text_set_fontset(	GernelCanvasText *canvas_text,
					gchar *fontset	);

void gernel_canvas_text_set_x(	GernelCanvasText *canvas_text,
				gint x	);

void gernel_canvas_text_set_y(	GernelCanvasText *canvas_text,
				gint y	);
