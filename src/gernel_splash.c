#include <gernel.h>
#include <gernel_splash.h>
#include <gernel.watermark.xpm>

#define	FONT	"-adobe-helvetica-medium-r-normal-*-*-100-*-*-p-*-iso8859-1*"
#define	TITLE	"-adobe-helvetica-bold-r-normal-*-*-140-*-*-p-*-iso8859-1*"

#define GERNEL_SPLASH_PROGRESS_INTERVAL	20
#define	GERNEL_SPLASH_PROGRESS_STEP	1

static void gernel_splash_init(	GernelSplash *splash	);
static void gernel_splash_class_init(	GernelSplashClass *klass	);
static void gernel_splash_construct(	GernelSplash *splash	);
static void gernel_splash_destroy(	GtkObject *splash	);

static void gernel_splash_update_status(	GernelSplash *splash,
						GtkWidget *statusbar,
						gchar *message	);

static gboolean gernel_splash_update_progress(	GernelSplash *splash	);

static GtkWindowClass	*parent_class = NULL;

GtkType gernel_splash_get_type(	void	)
/*	Return type identifier for type GernelSplash	*/
{
	static GtkType	splash_type = 0;

	if (!splash_type) {

		static const GtkTypeInfo	splash_info = {
				"GernelSplash",
				sizeof(GernelSplash),
				sizeof(GernelSplashClass),
				(GtkClassInitFunc)gernel_splash_class_init,
				(GtkObjectInitFunc)gernel_splash_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		splash_type = gtk_type_unique(	gtk_window_get_type(), 
						&splash_info	);
	}

	return splash_type;
}

GtkWidget* gernel_splash_new(	void	)
/*	Create and instance of GernelSplash	*/
{
	GernelSplash	*splash;

g_print("gernel_splash_new:	FIXME: window changes size when texts gets too long\n");
	splash = gtk_type_new(gernel_splash_get_type());
	gernel_splash_construct(splash);
	gernel_splash_update(splash, _(COPYRIGHT), NULL);

	return GTK_WIDGET(splash);
}

GtkWidget* gernel_splash_new_with_title(	gchar *title	)
/*	Create an instance of GernelSplash with title set	*/
{
	GtkWidget	*splash;

	g_return_val_if_fail(title != NULL, NULL);

	splash = gernel_splash_new();
	gtk_window_set_title(GTK_WINDOW(splash), title);

	return splash;
}

static void gernel_splash_init(	GernelSplash *splash	)
/*	Initialize the GernelSplash	*/
{
	splash->vbox = NULL;
	splash->major_status = NULL;
	splash->minor_status = NULL;
	splash->progress_bar = NULL;
	splash->timeout = 0;
}

static void gernel_splash_class_init(	GernelSplashClass *klass	)
/*	Initialize the GernelSplashClass	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_WINDOW);

	object_class->destroy = gernel_splash_destroy;
}

static void gernel_splash_construct(	GernelSplash *splash	)
/*	Construct the splash window for the Gernel	*/
{
	GtkWindow	*window;

	GtkWidget	*vbox, *image, *label, *status, *progress_bar, 
			*event_box;

	GtkStatusbar	*statusbar;
	GtkStyle	*style;
	GdkColor	white = { 0, 0xffff, 0xffff, 0xffff };

	g_return_if_fail(splash != NULL);
	g_return_if_fail(GERNEL_IS_SPLASH(splash));

	window = GTK_WINDOW(splash);
	window->type = GTK_WINDOW_DIALOG;
	gtk_window_set_wmclass(window, "FIXME", "FIXME2");
	gtk_window_set_position(window, GTK_WIN_POS_CENTER);
	gtk_window_set_policy(window, FALSE, TRUE, FALSE);
	style = gtk_widget_get_default_style();
	style->bg[GTK_STATE_NORMAL] = white;
	gtk_widget_set_style(GTK_WIDGET(splash), style);

	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(splash), vbox);
	splash->vbox = vbox;
	gtk_widget_show(vbox);

	image = gnome_pixmap_new_from_xpm_d(gernel_watermark_xpm);
	gtk_box_pack_start(GTK_BOX(vbox), image, FALSE, TRUE, 0);
	gtk_widget_show(image);

	label = gtk_label_new(_(PROGRAM));
	style = gtk_style_new();
	style->font = gdk_fontset_load(TITLE);
	gtk_widget_set_style(label, style);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), label);
	gtk_widget_show(label);

	status = gtk_statusbar_new();
	style = gtk_widget_get_default_style();
	style->font = gdk_fontset_load(FONT);
	splash->major_status = status;
	statusbar = GTK_STATUSBAR(status);
	gtk_widget_set_style(statusbar->label, style);
	gtk_misc_set_alignment(GTK_MISC(statusbar->label), 0.5, 0.5);
	gtk_frame_set_shadow_type(GTK_FRAME(statusbar->frame), GTK_SHADOW_NONE);

	gtk_box_pack_start(	GTK_BOX(vbox), 
				status, 
				FALSE, 
				FALSE, 
				GNOME_PAD_SMALL	);

	gtk_widget_show(status);

	/*	The bottom statusbar should have the default style, so we need
		to set up an event box for it	*/
	event_box = gtk_event_box_new();
	gtk_box_pack_start(GTK_BOX(vbox), event_box, FALSE, FALSE, 0);
	gtk_widget_show(event_box);

	status = gtk_statusbar_new();
	style = gtk_widget_get_default_style();
	gtk_widget_set_style(GTK_STATUSBAR(status)->label, style);
	splash->minor_status = status;
	gtk_container_add(GTK_CONTAINER(event_box), status);
	gtk_widget_show(status);

	progress_bar = gtk_progress_bar_new();
	gtk_progress_set_activity_mode(GTK_PROGRESS(progress_bar), TRUE);
	splash->progress_bar = progress_bar;
	gtk_box_pack_start_defaults(GTK_BOX(vbox), progress_bar);
	gtk_widget_show(progress_bar);
}

void gernel_splash_start(	GernelSplash *splash	)
/*	Start activity mode of the splash screen	*/
{
	GtkFunction	function;
	guint		timeout;

	g_return_if_fail(splash != NULL);
	g_return_if_fail(GERNEL_IS_SPLASH(splash));

	function = (GtkFunction)gernel_splash_update_progress;

	timeout = gtk_timeout_add(	GERNEL_SPLASH_PROGRESS_INTERVAL, 
					function, 
					splash	);

	splash->timeout = timeout;
}

void gernel_splash_stop(	GernelSplash *splash	)
/*	Stop activity mode of the splash screen	*/
{
	g_return_if_fail(splash != NULL);
	g_return_if_fail(GERNEL_IS_SPLASH(splash));

	gtk_timeout_remove(splash->timeout);
}

void gernel_splash_update(	GernelSplash *splash, 
				gchar *major,
				gchar *minor	)
/*	Update function for the statusbar and progressbar of splash; major and
	minor are messages to display	*/
{
	g_return_if_fail(splash != NULL);
	g_return_if_fail(GERNEL_IS_SPLASH(splash));
	g_return_if_fail(major || minor);

	gernel_splash_update_status(splash, splash->major_status, major);
	gernel_splash_update_status(splash, splash->minor_status, minor);
}

static void gernel_splash_update_status(	GernelSplash *splash,
						GtkWidget *statusbar,
						gchar *message	)
/*	Update function for the statusbar of splash	*/
{
	g_return_if_fail(splash != NULL);
	g_return_if_fail(GERNEL_IS_SPLASH(splash));
	g_return_if_fail(statusbar != NULL);
	g_return_if_fail(GTK_IS_STATUSBAR(statusbar));

	if (message) gtk_statusbar_push(GTK_STATUSBAR(statusbar), 1, message);
}

static gboolean gernel_splash_update_progress(	GernelSplash *splash	)
/*	Update function for the progress bar of splash	*/
{
	GtkAdjustment	*adjustment;
	GtkProgress	*progress;
	gfloat		value;

	g_return_val_if_fail(splash != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_SPLASH(splash), FALSE);

	progress = GTK_PROGRESS(splash->progress_bar);
	adjustment = progress->adjustment;
	value = gtk_progress_get_value(progress) + GERNEL_SPLASH_PROGRESS_STEP;
	if (value > adjustment->upper) value = adjustment->lower;
	gtk_progress_set_value(progress, value);

	return TRUE;
}

static void gernel_splash_destroy(	GtkObject *object	)
/*	Destroy handler for GernelSplash	*/
{
	GernelSplash	*splash;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_SPLASH(object));

	splash = GERNEL_SPLASH(object);

	gernel_splash_stop(splash);
	GTK_OBJECT_CLASS(parent_class)->destroy(object);
}
