#include <gernel.h>
#include <gernel.yes.xpm>
#include <gernel.mod.xpm>
#include <gernel.no.xpm>
#include <gernel_var_item_icon_set.h>
#include <gernel_var_item.h>

static void gernel_var_item_icon_set_init(
		GernelVarItemIconSet *var_item_icon_set
		);

static void gernel_var_item_icon_set_class_init(
		GernelVarItemIconSetClass *klass
		);

static void gernel_var_item_icon_set_base_init(
		GernelVarItemIconSetClass *klass
		);

static void gernel_var_item_icon_set_construct(
		GernelVarItemIconSet *var_item_icon_set,
		enum GernelVarItemIconSetType type,
		GSList *var_items
		);

static void gernel_var_item_icon_set_construct_icon(
		GernelVarItemIconSet *var_item_icon_set,
		enum GernelVarItemIconSetValue value,
		GdkPixmap *data,
		GdkBitmap *mask,
		GSList *var_items
		);

static void gernel_var_item_icon_set_construct_label(
		GernelVarItemIconSet *var_item_icon_set,
		GernelVarItem *var_item
		);

static void gernel_var_item_icon_set_construct_value(
		GernelVarItemIconSet *var_item_icon_set,
		GtkWidget *var_item
		);

static GtkHBoxClass	*parent_class = NULL;

static GdkPixmap	*yes = NULL, *module = NULL, *no = NULL;
static GdkBitmap	*yes_mask = NULL, *module_mask = NULL, *no_mask = NULL;

/*	window used for initializing the pixmap data	*/
static GdkWindow	*pixmap_window = NULL;

GtkType gernel_var_item_icon_set_get_type(	void	)
/*	Return type identifier for type GernelVarItemIconSet	*/
{
	static GtkType		var_item_icon_set_type = 0;

	if (!var_item_icon_set_type) {

		static const GtkTypeInfo	var_item_icon_set_info = {
			"GernelVarItemIconSet",
			sizeof(GernelVarItemIconSet),
			sizeof(GernelVarItemIconSetClass),
			(GtkClassInitFunc)gernel_var_item_icon_set_class_init,
			(GtkObjectInitFunc)gernel_var_item_icon_set_init,
			NULL,
			NULL,
			(GtkClassInitFunc)gernel_var_item_icon_set_base_init
		};

		var_item_icon_set_type = gtk_type_unique(
				gtk_hbox_get_type(), 
				&var_item_icon_set_info
				);
	}

	return var_item_icon_set_type;
}

static void gernel_var_item_icon_set_base_init(
		GernelVarItemIconSetClass *klass
		)
/*	Generate the pixmap data necessary for creating the actual images; put
	here so the data is generated only once	*/
{
	g_return_if_fail(pixmap_window != NULL);

return;
	yes = gdk_pixmap_create_from_xpm_d(	pixmap_window, 
						&yes_mask, 
						NULL,
						gernel_yes_xpm	);

	module = gdk_pixmap_create_from_xpm_d(	pixmap_window,
						&module_mask,
						NULL,
						gernel_mod_xpm	);

	no = gdk_pixmap_create_from_xpm_d(	pixmap_window,
						&no_mask,
						NULL,
						gernel_no_xpm	);
}

GtkWidget* gernel_var_item_icon_set_new(
		GdkWindow *window,
		enum GernelVarItemIconSetType type,
		GSList *var_items
		)
/*	Create an instance of GernelVarItemIconSet	*/
{
	GernelVarItemIconSet	*var_item_icon_set;

	g_return_val_if_fail(window != NULL, NULL);
	g_return_val_if_fail(type != GERNEL_VAR_ITEM_ICON_SET_TYPE_0, NULL);

	pixmap_window = window;
	var_item_icon_set = gtk_type_new(gernel_var_item_icon_set_get_type());
	gernel_var_item_icon_set_construct(var_item_icon_set, type, var_items);

	return GTK_WIDGET(var_item_icon_set);
}

static void gernel_var_item_icon_set_init(
		GernelVarItemIconSet *var_item_icon_set
		)
/*	Initialize the GernelVarItemIconSet	*/
{
	var_item_icon_set->yes = NULL;
	var_item_icon_set->module = NULL;
	var_item_icon_set->no = NULL;

	var_item_icon_set->labels = g_array_new(	FALSE, 
							FALSE, 
							sizeof(GtkLabel*) );
}

static void gernel_var_item_icon_set_class_init(
		GernelVarItemIconSetClass *klass
		)
/*	Initialize the GernelVarItemIconClass	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBoxClass		*box_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	box_class = (GtkBoxClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_HBOX);
}

static void gernel_var_item_icon_set_construct(
		GernelVarItemIconSet *var_item_icon_set,
		enum GernelVarItemIconSetType type,
		GSList *var_items
		)
/*	Fill the var_item_icon_set with icons	*/
{
	GtkBox	*box;

	g_return_if_fail(var_item_icon_set != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM_ICON_SET(var_item_icon_set));

	box = GTK_BOX(var_item_icon_set);
	gtk_box_set_homogeneous(box, FALSE);
	gtk_box_set_spacing(box, 0);

	switch (type) {
		GSList *slist;

		case GERNEL_VAR_ITEM_ICON_SET_TYPE_VALUE:
			g_return_if_fail(var_items != NULL);

			gernel_var_item_icon_set_construct_value(
					var_item_icon_set,
					GTK_WIDGET(var_items->data)
					);

			break;
		case GERNEL_VAR_ITEM_ICON_SET_TYPE_CHOICE:
			g_return_if_fail(var_items != NULL);

			for (slist = var_items; slist; slist = slist->next)

				gernel_var_item_icon_set_construct_label(
						var_item_icon_set,
						GERNEL_VAR_ITEM(slist->data)
						);

			break;
		case GERNEL_VAR_ITEM_ICON_SET_TYPE_TRISTATE:

			gernel_var_item_icon_set_construct_icon(
					var_item_icon_set,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_NO,
					no,
					no_mask,
					NULL
					);

			gernel_var_item_icon_set_construct_icon(
					var_item_icon_set,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_MODULE,
					module,
					module_mask,
					NULL
					);

			gernel_var_item_icon_set_construct_icon(
					var_item_icon_set,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_YES,
					yes,
					yes_mask,
					NULL
					);

			break;
		case GERNEL_VAR_ITEM_ICON_SET_TYPE_BOOL:

			gernel_var_item_icon_set_construct_icon(
					var_item_icon_set,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_YES,
					yes,
					yes_mask,
					NULL
					);

			gernel_var_item_icon_set_construct_icon(
					var_item_icon_set,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_NO,
					no,
					no_mask,
					NULL
					);

			break;
		default:
			g_assert_not_reached();
			break;
	}
}

static void gernel_var_item_icon_set_construct_icon(
		GernelVarItemIconSet *var_item_icon_set,
		enum GernelVarItemIconSetValue value,
		GdkPixmap *data,
		GdkBitmap *mask,
		GSList *var_items
		)
/*	Generate pixmap from data and mask and pack it into the icon_set */
{
	GtkWidget	*widget;
	GtkBox		*box;

	g_return_if_fail(var_item_icon_set != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM_ICON_SET(var_item_icon_set));

	widget = NULL;

	switch (value) {
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_YES:
			widget = gnome_pixmap_new_from_xpm_d(gernel_yes_xpm);
			var_item_icon_set->yes = widget; 
			break;
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_MODULE:
			widget = gnome_pixmap_new_from_xpm_d(gernel_mod_xpm);
			var_item_icon_set->module = widget;
			break;
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_NO:
			widget = gnome_pixmap_new_from_xpm_d(gernel_no_xpm);
			var_item_icon_set->no = widget;
			break;
		default:
			g_assert_not_reached();
			break;
	}

	box = GTK_BOX(var_item_icon_set);
	gtk_box_pack_start(box, widget, FALSE, FALSE, 0);
}

void gernel_var_item_icon_set_set_value(
		GernelVarItemIconSet *var_item_icon_set, 
		enum GernelVarItemIconSetValue type,
		gint value,
		gchar *string
		)
/*	Show the icon specified by type	and value	*/
{
	g_return_if_fail(var_item_icon_set != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM_ICON_SET(var_item_icon_set));

	switch (type) {
		GtkWidget	*label;
		GtkLabel	*lab;
		gint		i;
		GArray		*labels;
		gchar		*tmp;

		case GERNEL_VAR_ITEM_ICON_SET_VALUE_0:
			break;
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_STRING:
			labels = var_item_icon_set->labels;

			for (i = 0; i < labels->len; i++) {
				label = g_array_index(labels, GtkWidget*, i);
				gtk_widget_hide(label);
			}

			label = g_array_index(labels, GtkWidget*, value);
			gtk_widget_show(label);
			break;
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_YES:
			gtk_widget_show(var_item_icon_set->yes);

			if (var_item_icon_set->module != NULL)
				gtk_widget_hide(var_item_icon_set->module);

			gtk_widget_hide(var_item_icon_set->no);
			break;
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_MODULE:
			gtk_widget_hide(var_item_icon_set->yes);
			gtk_widget_show(var_item_icon_set->module);
			gtk_widget_hide(var_item_icon_set->no);
			break;
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_NO:
			gtk_widget_hide(var_item_icon_set->yes);

			if (var_item_icon_set->module != NULL)
				gtk_widget_hide(var_item_icon_set->module);

			gtk_widget_show(var_item_icon_set->no);
			break;
		case GERNEL_VAR_ITEM_ICON_SET_VALUE_VALUE:
			lab = GTK_LABEL(var_item_icon_set->label);
			tmp = g_strconcat("[", string, "]", NULL);
			gtk_label_set_text(lab, tmp);
			g_free(tmp);
			break;
		default:
			break;
	}
}

static void gernel_var_item_icon_set_construct_value(
		GernelVarItemIconSet *var_item_icon_set,
		GtkWidget *item
		)
/*	Construct a new label for var_item_icon_set from var_item's value */
{
	GtkWidget	*label;
	GtkBox		*box;
	gchar		*string, *value;
	GernelVarItem	*var_item;

	g_return_if_fail(var_item_icon_set != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM_ICON_SET(var_item_icon_set));
	g_return_if_fail(item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(item));

	box = GTK_BOX(var_item_icon_set);
	var_item = GERNEL_VAR_ITEM(item);

	value = gernel_var_item_get_value(var_item);
	string = g_strconcat("[", value, "]", NULL);
	label = gtk_label_new(string);
	g_free(string);

	if (var_item->fontset) {
		GdkFont		*font;
		GtkStyle	*style;

		font = gdk_fontset_load(var_item->fontset);
		style = gtk_style_copy(gtk_widget_get_style(label));
		style->font = font;
		gtk_widget_set_style(label, style);
	}

	var_item_icon_set->label = label;
	gtk_box_pack_start(box, label, FALSE, FALSE, GNOME_PAD_SMALL);
	gtk_widget_show(label);
}

static void gernel_var_item_icon_set_construct_label(
		GernelVarItemIconSet *var_item_icon_set,
		GernelVarItem *var_item
		)
/*	Construct a new label for var_item_icon_set	*/
{
	GtkWidget	*label;
	GArray		*labels;
	GString		*pattern;
	GtkBox		*box;
	gint		i;

	g_return_if_fail(var_item_icon_set != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM_ICON_SET(var_item_icon_set));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(var_item->description != NULL);

	labels = var_item_icon_set->labels;
	box = GTK_BOX(var_item_icon_set);

	label = gtk_label_new(var_item->description);

	if (var_item->fontset) {
		GdkFont		*font;
		GtkStyle	*style;

		font = gdk_fontset_load(var_item->fontset);
		style = gtk_style_copy(gtk_widget_get_style(label));
		style->font = font;
		gtk_widget_set_style(label, style);
	}

	pattern = g_string_new(NULL);

	for (i = 0; i < strlen(var_item->description); i++) 
		pattern = g_string_append_c(pattern, '_');

	gtk_label_set_pattern(GTK_LABEL(label), pattern->str);
	g_string_free(pattern, TRUE);
	var_item_icon_set->labels = g_array_append_val(labels, label);
	gtk_box_pack_start(box, label, FALSE, FALSE, GNOME_PAD_SMALL);
}
