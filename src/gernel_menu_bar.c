#include <gernel.h>
#include <gernel_menu_bar.h>
#include <gernel_file_selection.h>

#define	GERNEL_MENU_BAR_OFFSET(obj) \
	GTK_SIGNAL_OFFSET(GernelMenuBarClass, obj)

static void gernel_menu_bar_init(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_class_init(	GernelMenuBarClass *klass	);
static void gernel_menu_bar_construct(	GernelMenuBar *menu_bar,
					GtkWidget *window	);

static void gernel_menu_bar_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_menu_bar_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_menu_bar_install_menu_hints(	GernelMenuBar *menu_bar, 
						GtkWidget *app	);

static void gernel_menu_bar_directory_selection(
		GernelMenuBar *menu_bar
		);

static void gernel_menu_bar_save(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_save_as(	GernelMenuBar *menu_bar	);

static void gernel_menu_bar_directory(	GernelMenuBar *menu_bar,
					gchar *directory	);

static void gernel_menu_bar_class_add_signals(	GernelMenuBarClass *klass, 
						gchar *signal,
						guint index, 
						guint offset,
						...	);

static void gernel_menu_bar_class_add_signal(	GernelMenuBarClass *klass, 
						gchar *signal,
						guint index, 
						guint offset	);

static void gernel_menu_bar_about(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_reset(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_previous(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_next(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_up(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_down(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_back(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_forward(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_find(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_prerequisites(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_dependants(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_preferences(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_compile(	GernelMenuBar *menu_bar	);
static void gernel_menu_bar_new_window(	GernelMenuBar *menu_bar	);

static void gernel_menu_bar_connect_multiple(	GernelMenuBar *menu_bar,
						GtkWidget *widget,
						GtkSignalFunc function,
						...	);

static GernelMenuBarClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_DEPENDANTS,
	ARG_PREREQUISITES	};

enum {	DIRECTORY,
	NEXT,
	PREVIOUS,
	UP,
	DOWN,
	BACK,
	FORWARD,
	FIND,
	PREREQUISITES,
	DEPENDANTS,
	PREFERENCES,
	COMPILE,
	NEW_WINDOW,
	SAVE,
	SAVE_AS,
	LAST_SIGNAL	};

static guint menu_bar_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_menu_bar_get_type(	void	)
/*	Return type identifier for type GernelMenuBar	*/
{
	static GtkType	menu_bar_type = 0;

	if (!menu_bar_type) {

		static const GtkTypeInfo	menu_bar_info = {
				"GernelMenuBar",
				sizeof(GernelMenuBar),
				sizeof(GernelMenuBarClass),
				(GtkClassInitFunc)gernel_menu_bar_class_init,
				(GtkObjectInitFunc)gernel_menu_bar_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		menu_bar_type = gtk_type_unique(gtk_menu_bar_get_type(),
						&menu_bar_info	);
	}

	return menu_bar_type;
}

GtkWidget* gernel_menu_bar_new(	GtkWidget *window	)
/*	Create an instance of GernelMenuBar	*/
{
	GernelMenuBar	*menu_bar;

	g_return_val_if_fail(window != NULL, NULL);
	g_return_val_if_fail(GNOME_IS_APP(window), NULL);

	menu_bar = gtk_type_new(gernel_menu_bar_get_type());
	gernel_menu_bar_construct(menu_bar, window);

	return GTK_WIDGET(menu_bar);
}

static void gernel_menu_bar_init(	GernelMenuBar *menu_bar	)
/*	Initialize the GernelMenuBar	*/
{
	menu_bar->new_window = NULL;
	menu_bar->dependants = NULL;
	menu_bar->prerequisites = NULL;
	menu_bar->template = NULL;
	menu_bar->back = NULL;
	menu_bar->forward = NULL;
	menu_bar->next = NULL;
	menu_bar->previous = NULL;
	menu_bar->reload = NULL;
	menu_bar->find = NULL;
	menu_bar->uiinfo = NULL;
	menu_bar->load_directory = NULL;
	menu_bar->about = NULL;
	menu_bar->preferences = NULL;
}

static void gernel_menu_bar_class_init(	GernelMenuBarClass *klass	)
/*	Initialize the GernelMenuBarClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_MENU_BAR);

	gtk_object_add_arg_type(	"GernelMenuBar::dependants",
					GTK_TYPE_BOOL,
					GTK_ARG_READWRITE,
					ARG_DEPENDANTS	);

	gtk_object_add_arg_type(	"GernelMenuBar::prerequisites",
					GTK_TYPE_BOOL,
					GTK_ARG_READWRITE,
					ARG_PREREQUISITES	);

	object_class->get_arg = gernel_menu_bar_get_arg;
	object_class->set_arg = gernel_menu_bar_set_arg;

	gernel_menu_bar_class_add_signals(
		klass,
		"find", FIND, GERNEL_MENU_BAR_OFFSET(find),
		"compile", COMPILE, GERNEL_MENU_BAR_OFFSET(compile),
		"new_window", NEW_WINDOW, GERNEL_MENU_BAR_OFFSET(new_window),
		"save", SAVE, GERNEL_MENU_BAR_OFFSET(save),
		"save_as", SAVE_AS, GERNEL_MENU_BAR_OFFSET(save_as),
		"preferences", PREFERENCES, GERNEL_MENU_BAR_OFFSET(preferences),
		"prerequisites", PREREQUISITES, GERNEL_MENU_BAR_OFFSET(prerequisites),
		"dependants", DEPENDANTS, GERNEL_MENU_BAR_OFFSET(dependants),
		"next", NEXT, GERNEL_MENU_BAR_OFFSET(next),
		"previous", PREVIOUS, GERNEL_MENU_BAR_OFFSET(previous),
		"up", UP, GERNEL_MENU_BAR_OFFSET(up),
		"down", DOWN, GERNEL_MENU_BAR_OFFSET(down),
		"back", BACK, GERNEL_MENU_BAR_OFFSET(back),
		"forward", FORWARD, GERNEL_MENU_BAR_OFFSET(forward),
		NULL
		);

	function_offset = GTK_SIGNAL_OFFSET(GernelMenuBarClass, directory);

	menu_bar_signals[DIRECTORY] =

			gtk_signal_new(	"directory",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__STRING,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_STRING	);

	gtk_object_class_add_signals(	object_class, 
					menu_bar_signals, 
					LAST_SIGNAL	);

	klass->preferences = NULL;
	klass->directory = NULL;
	klass->next = NULL;
	klass->previous = NULL;
	klass->down = NULL;
	klass->back = NULL;
	klass->forward = NULL;
	klass->find = NULL;
	klass->compile = NULL;
	klass->new_window = NULL;
	klass->save = NULL;
	klass->save_as = NULL;
}

static void gernel_menu_bar_class_add_signals(	GernelMenuBarClass *klass,
						gchar *signal,
						guint index,
						guint offset,
						...	)
/*	Create new signals for klass	*/
{
	va_list		args;
	guint		current_index, current_offset;
	gchar		*current_signal;

	va_start(args, offset);
	current_index = index;
	current_signal = signal;
	current_offset = offset;

	do {
		gernel_menu_bar_class_add_signal(	klass,
							current_signal,
							current_index,
							current_offset	);

		if ((current_signal = va_arg(args, gchar*)) != NULL) {
			current_index = va_arg(args, guint);
			current_offset = va_arg(args, guint);
		}
	} while (current_signal != NULL);

	va_end(args);
}

static void gernel_menu_bar_class_add_signal(	GernelMenuBarClass *klass, 
						gchar *signal,
						guint index, 
						guint offset	)
/*	Create a new signal for klass with menu_signals index, name 'signal'
	and offset generated using 'field'	*/
{
	GtkObjectClass	*object_class;

	object_class = (GtkObjectClass*)klass;

	menu_bar_signals[index] =

			gtk_signal_new(	signal,
					GTK_RUN_FIRST,
					object_class->type,
					offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);
}

static void gernel_menu_bar_construct(	GernelMenuBar *menu_bar,
					GtkWidget *window	)
/*	Fill the menu for the requirements of the Gernel	*/
{
	GnomeUIInfo file_menu[] = {

		/*	0	*/
		GNOMEUIINFO_MENU_NEW_WINDOW_ITEM(NULL, NULL),

		/*	1	*/
		GNOMEUIINFO_SEPARATOR,

		/*	2	*/
		{	GNOME_APP_UI_ITEM,
			_("_Load..."),
			_("Load configuration from kernel source directory"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_OPEN,
			'l',
			GDK_CONTROL_MASK	},

		/*	3	*/
		{	GNOME_APP_UI_ITEM,
			_("Load _Template..."),
			_("Load different template from file"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_OPEN,
			'l',
			GDK_CONTROL_MASK	},

		/*	4	*/
		GNOMEUIINFO_MENU_SAVE_ITEM(NULL, NULL),

		/*	5	*/
		GNOMEUIINFO_MENU_SAVE_AS_ITEM(NULL, NULL),

		/*	5	*/
		GNOMEUIINFO_SEPARATOR,

		/*	6	*/
		GNOMEUIINFO_MENU_CLOSE_WINDOW_ITEM(close_window, window),

		/*	7	*/
		GNOMEUIINFO_MENU_EXIT_ITEM(gtk_main_quit, NULL),
		GNOMEUIINFO_END
	};

	GnomeUIInfo edit_menu[] = {
		/*	0	*/
		GNOMEUIINFO_MENU_FIND_ITEM(NULL, NULL),
		GNOMEUIINFO_END
	};

	GnomeUIInfo attributes_menu[] = {

		{	GNOME_APP_UI_ITEM,
			_("_Prerequisites..."),
			_("Show prerequisites for this variable"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_NONE,
			NULL,
			'p',
			GDK_CONTROL_MASK	},

		{	GNOME_APP_UI_ITEM,
			_("_Dependants..."),
			_("Show variables that depend on this variable"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_NONE,
			NULL,
			'd',
			GDK_CONTROL_MASK	},

		GNOMEUIINFO_END
	};

/*	GnomeUIInfo back_menu[] = {
		GNOMEUIINFO_END
	};	*/

	GnomeUIInfo view_menu[] = {

		/*	0	*/
		{	GNOME_APP_UI_ITEM,
			_("Back"),
			_("Move back in history"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_BACK,
			'b',
			GDK_CONTROL_MASK	},

/*		{	GNOME_APP_UI_SUBTREE,
			_("Back"),
			_("Move back in history"),
			back_menu,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_BACK,
			'b',
			GDK_CONTROL_MASK	},	*/

		/*	1	*/
		{	GNOME_APP_UI_ITEM,
			_("Forward"),
			_("Move forward in history"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_FORWARD,
			'f',
			GDK_CONTROL_MASK	},

		/*	2	*/
		GNOMEUIINFO_SEPARATOR,

		/*	3	*/
		{	GNOME_APP_UI_ITEM,
			_("Previous"),
			_("Go to previous item"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_BACK,
			'p',
			GDK_CONTROL_MASK	},

		/*	4	*/
		{	GNOME_APP_UI_ITEM,
			_("Next"),
			_("Go to next item"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_FORWARD,
			'n',
			GDK_CONTROL_MASK	},

		/*	5	*/
		{	GNOME_APP_UI_ITEM,
			_("Up"),
			_("Go to item on higher level"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_UP,
			'u',
			GDK_CONTROL_MASK	},

		/*	6	*/
		{	GNOME_APP_UI_ITEM,
			_("Down"),
			_("Go to item on lower level"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_DOWN,
			'd',
			GDK_CONTROL_MASK	},

		/*	7	*/
		GNOMEUIINFO_SEPARATOR,

		/*	8	*/
		{	GNOME_APP_UI_ITEM,
			_("_Reload"),
			_("Reload Current Configuration"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_REFRESH,
			'r',
			GDK_CONTROL_MASK	},

		/*	9	*/
		GNOMEUIINFO_SEPARATOR,

		/*	10	*/
		{	GNOME_APP_UI_SUBTREE,
			_("_Attributes"),
			_("View attributes of currently selected item"),
			attributes_menu,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_NONE,
			NULL,
			'a',
			GDK_CONTROL_MASK	},

		GNOMEUIINFO_END
	};

	GnomeUIInfo settings_menu[] = {
		GNOMEUIINFO_MENU_PREFERENCES_ITEM(NULL, NULL),
		GNOMEUIINFO_END
	};

	GnomeUIInfo actions_menu[] = {

		{	GNOME_APP_UI_ITEM,
			_("_Compile Kernel"),
			_("Compile the Kernel with the new settings"),
			NULL,
			NULL,
			NULL,
			GNOME_APP_PIXMAP_STOCK,
			GNOME_STOCK_MENU_EXEC,
			'c',
			GDK_CONTROL_MASK	},

		GNOMEUIINFO_END
	};

	GnomeUIInfo help_menu[] = {
		GNOMEUIINFO_MENU_ABOUT_ITEM(NULL, NULL),
		GNOMEUIINFO_END
	};

	GnomeUIInfo menubar[] = {
		GNOMEUIINFO_MENU_FILE_TREE(file_menu),
		GNOMEUIINFO_MENU_EDIT_TREE(edit_menu),
		GNOMEUIINFO_MENU_VIEW_TREE(view_menu),
		GNOMEUIINFO_MENU_SETTINGS_TREE(settings_menu),

		{	GNOME_APP_UI_SUBTREE, 
			_("_Actions"), 
			NULL, 
			&actions_menu, 
			NULL, 
			NULL, 
			GNOME_APP_PIXMAP_NONE, 
			NULL, 
			0, 
			0, 
			NULL	},

		GNOMEUIINFO_MENU_HELP_TREE(help_menu),
		GNOMEUIINFO_END
	};

	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));
	g_return_if_fail(window != NULL);
	g_return_if_fail(GNOME_IS_APP(window));

	gnome_app_fill_menu(	GTK_MENU_SHELL(menu_bar), 
				menubar, 
				GNOME_APP(window)->accel_group,
				TRUE,
				0	);

	menu_bar->uiinfo = menubar;
	menu_bar->dependants = attributes_menu[1].widget;
	menu_bar->prerequisites = attributes_menu[0].widget;
	menu_bar->load_directory = file_menu[2].widget;
	menu_bar->new_window = file_menu[0].widget;
	menu_bar->template = file_menu[3].widget;
	menu_bar->save = file_menu[4].widget;
	menu_bar->save_as = file_menu[5].widget;
	menu_bar->find = edit_menu[0].widget;
	menu_bar->preferences = settings_menu[0].widget;
	menu_bar->back = view_menu[0].widget;
	menu_bar->forward = view_menu[1].widget;
	menu_bar->next = view_menu[4].widget;
	menu_bar->up = view_menu[5].widget;
	menu_bar->down = view_menu[6].widget;
	menu_bar->previous = view_menu[3].widget;
	menu_bar->reload = view_menu[8].widget;
	menu_bar->about = help_menu[0].widget;
	menu_bar->compile = actions_menu[0].widget;
	gnome_app_set_menus(GNOME_APP(window), GTK_MENU_BAR(menu_bar));
	gernel_menu_bar_install_menu_hints(menu_bar, window);

	gernel_menu_bar_connect_multiple(
		menu_bar,
		menu_bar->new_window, gernel_menu_bar_new_window,
		menu_bar->load_directory, gernel_menu_bar_directory_selection,
		menu_bar->save, gernel_menu_bar_save,
		menu_bar->save_as, gernel_menu_bar_save_as,
		menu_bar->back, gernel_menu_bar_back,
		menu_bar->forward, gernel_menu_bar_forward,
		menu_bar->up, gernel_menu_bar_up,
		menu_bar->down, gernel_menu_bar_down,
		menu_bar->next, gernel_menu_bar_next,
		menu_bar->previous, gernel_menu_bar_previous,
		menu_bar->find, gernel_menu_bar_find,
		menu_bar->prerequisites, gernel_menu_bar_prerequisites,
		menu_bar->dependants, gernel_menu_bar_dependants,
		menu_bar->about, gernel_menu_bar_about,
		menu_bar->preferences, gernel_menu_bar_preferences,
		menu_bar->compile, gernel_menu_bar_compile,
		NULL
	);

	gernel_menu_bar_reset(menu_bar);
}

static void gernel_menu_bar_find(	GernelMenuBar *menu_bar	)
/*	Emit the 'find' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[FIND]);
}

static void gernel_menu_bar_up(	GernelMenuBar *menu_bar	)
/*	Emit the 'up' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[UP]);
}

static void gernel_menu_bar_down(	GernelMenuBar *menu_bar	)
/*	Emit the 'down' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[DOWN]);
}

static void gernel_menu_bar_next(	GernelMenuBar *menu_bar	)
/*	Emit the 'next' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[NEXT]);
}

static void gernel_menu_bar_previous(	GernelMenuBar *menu_bar	)
/*	Emit the 'previous' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[PREVIOUS]);
}

static void gernel_menu_bar_reset(	GernelMenuBar *menu_bar	)
/*	Reset sensitivity of menu_bar's compononents	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->dependants, FALSE);
	gtk_widget_set_sensitive(menu_bar->prerequisites, FALSE);
	gtk_widget_set_sensitive(menu_bar->template, FALSE);
	gtk_widget_set_sensitive(menu_bar->back, FALSE);
	gtk_widget_set_sensitive(menu_bar->forward, FALSE);
	gtk_widget_set_sensitive(menu_bar->next, FALSE);
	gtk_widget_set_sensitive(menu_bar->save, FALSE);
	gtk_widget_set_sensitive(menu_bar->save_as, FALSE);
	gtk_widget_set_sensitive(menu_bar->up, FALSE);
	gtk_widget_set_sensitive(menu_bar->down, FALSE);
	gtk_widget_set_sensitive(menu_bar->previous, FALSE);
	gtk_widget_set_sensitive(menu_bar->reload, FALSE);
}

static void gernel_menu_bar_directory_selection(	GernelMenuBar *menu_bar)
/*	Pop up a dialog to let the user choose a new kernel path	*/
{
	GtkWidget	*file_selection;

	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	file_selection = gernel_file_selection_new(_("Choose kernel path"));

	gtk_signal_connect_object(	GTK_OBJECT(file_selection),
					"ok",
					gernel_menu_bar_directory,
					GTK_OBJECT(menu_bar)	);

	gtk_widget_show(file_selection);
}

static void gernel_menu_bar_save(	GernelMenuBar *menu_bar	)
/*	Emit the 'save' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[SAVE]);
}

static void gernel_menu_bar_save_as(	GernelMenuBar *menu_bar	)
/*	Emit the 'save as' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

g_print("gernel_menu_bar_save_as:	invoked\n");
	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[SAVE_AS]);
}

static void gernel_menu_bar_directory(	GernelMenuBar *menu_bar,
					gchar *directory	)
/*	Emit the 'directory' signal; the signal is actually just passed on from
	the kernel path selection dialog	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));
	g_return_if_fail(directory != NULL);

	gtk_signal_emit(	GTK_OBJECT(menu_bar), 
				menu_bar_signals[DIRECTORY], 
				directory	);
}

static void gernel_menu_bar_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelMenuBar	*menu_bar;

	menu_bar = GERNEL_MENU_BAR(object);

	switch (arg_id) {
		gboolean	flag;

		case ARG_DEPENDANTS:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_menu_bar_set_dependants(menu_bar, flag);
			break;
		case ARG_PREREQUISITES:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_menu_bar_set_prerequisites(menu_bar, flag);
			break;
		default:
			g_assert_not_reached();
			break;
	}
}

static void gernel_menu_bar_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	g_warning("gernel_menu_bar_get_arg:	not implemented\n");
}

void gernel_menu_bar_set_dependants(	GernelMenuBar *menu_bar,
					gboolean flag	)
/*	Set sensitivity of menu_bar's dependants-widget	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));
	g_return_if_fail(menu_bar->dependants);
	g_return_if_fail(GTK_IS_WIDGET(menu_bar->dependants));

	gtk_widget_set_sensitive(menu_bar->dependants, flag);
}

void gernel_menu_bar_set_save_as(	GernelMenuBar *menu_bar,
					gboolean flag	)
/*	Set sensitivity of menu_bar's save-as widget	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->save_as, flag);
}

void gernel_menu_bar_set_prerequisites(	GernelMenuBar *menu_bar,
					gboolean flag	)
/*	Set sensitivity of menu_bar's prerequisites-widget	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->prerequisites, flag);
}

void gernel_menu_bar_set_history(	GernelMenuBar *menu_bar,
					guint history,
					guint future	)
/*	Set sensitivity of menu_bar's history- and future widgets	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->back, history > 1);
	gtk_widget_set_sensitive(menu_bar->forward, future > 0);
}

void gernel_menu_bar_set_up(	GernelMenuBar *menu_bar,
				gboolean flag	)
/*	Set sensitivity of menu_bar's 'up' button	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->up, flag);
}

void gernel_menu_bar_set_down(	GernelMenuBar *menu_bar,
				gboolean flag	)
/*	Set sensitivity of menu_bar's 'down' button	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->down, flag);
}

void gernel_menu_bar_set_next(	GernelMenuBar *menu_bar,
				gboolean flag	)
/*	Set sensitivity of menu_bar's 'next' button	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->next, flag);
}

void gernel_menu_bar_set_previous(	GernelMenuBar *menu_bar,
					gboolean flag	)
/*	Set sensitivity of menu_bar's 'previous' button	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_widget_set_sensitive(menu_bar->previous, flag);
}

static void gernel_menu_bar_install_menu_hints(	GernelMenuBar *menu_bar, 
						GtkWidget *app	)
/*	Install menu hints for app	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));
	g_return_if_fail(app != NULL);
	g_return_if_fail(GNOME_IS_APP(app));
	g_return_if_fail(menu_bar->uiinfo != NULL);

	gnome_app_install_menu_hints(GNOME_APP(app), menu_bar->uiinfo);
}

static void gernel_menu_bar_back(	GernelMenuBar *menu_bar	)
/*	Emit the 'back' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[BACK]);
}

static void gernel_menu_bar_forward(	GernelMenuBar *menu_bar	)
/*	Emit the 'forward' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[FORWARD]);
}

static void gernel_menu_bar_prerequisites(	GernelMenuBar *menu_bar	)
/*	Emit the 'prerequisites' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[PREREQUISITES]);
}

static void gernel_menu_bar_dependants(	GernelMenuBar *menu_bar	)
/*	Emit the 'dependants' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[DEPENDANTS]);
}

static void gernel_menu_bar_preferences(	GernelMenuBar *menu_bar	)
/*	Emit the 'preferences' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[PREFERENCES]);
}

static void gernel_menu_bar_about(	GernelMenuBar *menu_bar	)
/*	Show an about popup window	*/
{
	GtkWidget	*window;
	const gchar	*authors[] = { AUTHOR, NULL };

	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	window = gnome_about_new(	PROGRAM,
					VERSION,
					COPYRIGHT,
					authors,
					DESCRIPTION,
					LOGO	);

	gtk_widget_show(window);
}

static void gernel_menu_bar_connect_multiple(	GernelMenuBar *menu_bar,
						GtkWidget *widget,
						GtkSignalFunc function,
						...	)
/*	Connect multple signal handlers; widget is emitting object, function is
	signal function to be passed, menu_bar is object to be passed	*/
{
	va_list		args;
	GtkSignalFunc	func;
	GtkObject	*bar;
	GtkWidget	*object;

	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));

	bar = GTK_OBJECT(menu_bar);
	va_start(args, function);
	object = widget;
	func = function;

	do {
		GtkObject	*subject;

		subject = GTK_OBJECT(object);
		gtk_signal_connect_object(subject, "activate", func, bar);
		object = va_arg(args, GtkWidget*);
		if (object != NULL) func = va_arg(args, GtkSignalFunc);
	} while (object);

	va_end(args);
}

static void gernel_menu_bar_new_window(	GernelMenuBar *menu_bar	)
/*	Emit the 'new_window' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[NEW_WINDOW]);
}

static void gernel_menu_bar_compile(	GernelMenuBar *menu_bar	)
/*	Emit the 'compile' signal	*/
{
	g_return_if_fail(menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(menu_bar));

	gtk_signal_emit(GTK_OBJECT(menu_bar), menu_bar_signals[COMPILE]);
}
