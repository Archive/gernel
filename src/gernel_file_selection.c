#include <gernel.h>
#include <gernel_file_selection.h>

static void gernel_file_selection_save_path(
		GernelFileSelection *file_selection,
		gchar *path
		);

static void gernel_file_selection_set_local_name(
		GernelFileSelection *file_selection,
		gchar *local_name
		);

static void gernel_file_selection_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void gernel_file_selection_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void	gernel_file_selection_init(
		GernelFileSelection *file_selection
		);

static void	gernel_file_selection_class_init(
		GernelFileSelectionClass *klass
		);

static void gernel_file_selection_ok(	GernelFileSelection *file_selection );

static GtkFileSelectionClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_LOCAL_NAME,
	ARG_PATH	};

enum {	OK,
	LAST_SIGNAL	};

static guint file_selection_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_file_selection_get_type(	void	)
/*	Return type identifier for type GernelFileSelection	*/
{
	static GtkType	file_selection_type = 0;

	if (!file_selection_type) {

		static const GtkTypeInfo	file_selection_info = {
			"GernelFileSelection",
			sizeof(GernelFileSelection),
			sizeof(GernelFileSelectionClass),
			(GtkClassInitFunc)gernel_file_selection_class_init,
			(GtkObjectInitFunc)gernel_file_selection_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		file_selection_type = gtk_type_unique(	GTK_TYPE_FILE_SELECTION,
							&file_selection_info );
	}

	return file_selection_type;
}

GtkWidget* gernel_file_selection_new(	gchar *local_name	)
/*	Create an instance of GernelFileSelection	*/
{
	GtkWidget	*file_selection;

	g_return_val_if_fail(local_name != NULL, NULL);
g_print("gernel_file_selection_new:	FIXME: g_get_current_dir() should be freed when no longer needed\n");
g_print("gernel_file_selection_new:	FIXME: when variable is set for the first time, the widget does not enter the complete path but displays the last directory component in the file entry\n");

	file_selection = erty_widget_new(
			gernel_file_selection_get_type(),
			local_name,
			"path", g_get_current_dir(),
			NULL
			);

	return GTK_WIDGET(file_selection);
}

static void gernel_file_selection_init(	GernelFileSelection *file_selection )
/*	Initialize a GernelFileSelection dialog	*/
{
	GtkWidget		*button;
	GtkFileSelection	*selection;

	g_return_if_fail(file_selection != NULL);
	g_return_if_fail(GERNEL_IS_FILE_SELECTION(file_selection));

	selection = GTK_FILE_SELECTION(file_selection);

	button = selection->ok_button;

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					gernel_file_selection_ok,
					GTK_OBJECT(file_selection)	);

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					GTK_SIGNAL_FUNC(gtk_widget_destroy),
					GTK_OBJECT(file_selection)	);

	button = selection->cancel_button;

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					GTK_SIGNAL_FUNC(gtk_widget_destroy),
					GTK_OBJECT(file_selection)	);
}

static void gernel_file_selection_class_init(	GernelFileSelectionClass *klass)
/*	Initialize the GernelFileSelection class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkWindowClass		*window_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	window_class = (GtkWindowClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_FILE_SELECTION);

	gtk_object_add_arg_type(	"GernelFileSelection::local_name",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_LOCAL_NAME	);

	erty_arg_add("GernelFileSelection::path", GTK_TYPE_STRING, ARG_PATH);

	function_offset = GTK_SIGNAL_OFFSET(GernelFileSelectionClass, ok);

	file_selection_signals[OK] =

			gtk_signal_new(	"ok",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__STRING,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_STRING	);

	gtk_object_class_add_signals(	object_class,
					file_selection_signals,
					LAST_SIGNAL	);

	object_class->get_arg = gernel_file_selection_get_arg;
	object_class->set_arg = gernel_file_selection_set_arg;
	klass->ok = gernel_file_selection_save_path;
}

static void gernel_file_selection_ok(	GernelFileSelection *file_selection )
/*	Emit the 'ok' signal and send the selected path	*/
{
	gchar			*path;
	GtkFileSelection	*selection;

	g_return_if_fail(file_selection);
	g_return_if_fail(GERNEL_IS_FILE_SELECTION(file_selection));

	selection = GTK_FILE_SELECTION(file_selection);
	path = g_strdup(gtk_file_selection_get_filename(selection));
	gtk_widget_hide(GTK_WIDGET(file_selection));

	gtk_signal_emit(	GTK_OBJECT(file_selection), 
				file_selection_signals[OK], 
				path	);
}

static void gernel_file_selection_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Argument getting	*/
{
	GernelFileSelection	*file_selection;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_FILE_SELECTION(object));

	file_selection = GERNEL_FILE_SELECTION(object);

	switch (arg_id) {
		case ARG_LOCAL_NAME:
			GTK_VALUE_STRING(*arg) = file_selection->local_name;
			break;
		case ARG_PATH:
			GTK_VALUE_STRING(*arg) = 
		gtk_file_selection_get_filename(GTK_FILE_SELECTION(object));
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void gernel_file_selection_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Argument setting	*/
{
	GernelFileSelection	*file_selection;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_FILE_SELECTION(object));

	file_selection = GERNEL_FILE_SELECTION(object);

	switch (arg_id) {
		gchar	*str;

		case ARG_LOCAL_NAME:
			str = GTK_VALUE_STRING(*arg);

			gernel_file_selection_set_local_name(	file_selection, 
								str	);

			break;
		case ARG_PATH:
			str = GTK_VALUE_STRING(*arg);
g_print("gernel_file_selection_set_arg:	str == %s\n", str);
			gtk_file_selection_set_filename(GTK_FILE_SELECTION(object), str);
		default:
			break;
	}
}

static void gernel_file_selection_set_local_name(
		GernelFileSelection *file_selection,
		gchar *local_name
		)
/*	Set the object-wise name of file_selection; needed by Liberty	*/
{
	g_return_if_fail(file_selection != NULL);
	g_return_if_fail(GERNEL_IS_FILE_SELECTION(file_selection));

	if (file_selection->local_name != NULL) 
		g_free(file_selection->local_name);

	file_selection->local_name = local_name;
}

static void gernel_file_selection_save_path(
		GernelFileSelection *file_selection,
		gchar *path
		)
/*	Remember 'path'	*/
{
	g_return_if_fail(file_selection != NULL);
	g_return_if_fail(GERNEL_IS_FILE_SELECTION(file_selection));
	g_return_if_fail(path != NULL);
	g_return_if_fail(g_file_exists(path));

	erty_save(GTK_WIDGET(file_selection), "path");
}
