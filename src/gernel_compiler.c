#include <gernel.h>
#include <gernel_compiler.h>
#include <gernel_password.h>

static void gernel_compiler_set_local_name(	GernelCompiler *compiler,
						gchar *name	);

static gchar* gernel_compiler_get_local_name(	GernelCompiler *compiler );
static void gernel_compiler_init(	GernelCompiler *compiler	);
static void gernel_compiler_class_init(	GernelCompilerClass *klass	);
static void gernel_compiler_construct(	GernelCompiler *compiler	);
static void gernel_compiler_configure(	GernelCompiler *compiler	);
static void gernel_compiler_start(	GernelCompiler *compiler	);
static GtkWidget* gernel_compiler_preferences(	GernelCompiler *compiler );
static void gernel_compiler_construct_password(	GernelCompiler *compiler );

static GtkWidget* gernel_compiler_password_preferences(
		GernelCompiler *compiler
		);

static void gernel_compiler_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_compiler_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

enum {	ARG_0,
	ARG_LOCAL_NAME,
	ARG_PREFERENCES	};

enum {	CONFIGURE,
	LAST_SIGNAL	};

static guint compiler_signals[LAST_SIGNAL] = { 0 };

static GnomeDialogClass	*parent_class = NULL;

GtkType gernel_compiler_get_type(	void	)
/*	Return type identifier for type GernelCompiler	*/
{
	static GtkType	compiler_type = 0;

	if (!compiler_type) {

		static const GtkTypeInfo	compiler_info = {
				"GernelCompiler",
				sizeof(GernelCompiler),
				sizeof(GernelCompilerClass),
				(GtkClassInitFunc)gernel_compiler_class_init,
				(GtkObjectInitFunc)gernel_compiler_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		compiler_type = gtk_type_unique(gnome_dialog_get_type(),
						&compiler_info	);
	}

	return compiler_type;
}

GtkWidget* gernel_compiler_new(	gchar *name	)
/*	Create an instance of GernelCompiler	*/
{
	GernelCompiler	*compiler;
	GtkWidget	*widget;

	g_return_val_if_fail(name != NULL, NULL);

	widget = erty_widget_new(gernel_compiler_get_type(), name, NULL);
	compiler = GERNEL_COMPILER(widget);
	gernel_compiler_set_local_name(compiler, name);
	gernel_compiler_construct(compiler);

	return widget;
}

static void gernel_compiler_init(	GernelCompiler *compiler	)
/*	Initialize the GernelCompiler	*/
{
	compiler->password = NULL;
}

static void gernel_compiler_class_init(	GernelCompilerClass *klass	)
/*	Initialize the GernelCompilerClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkWindowClass		*window_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	window_class = (GtkWindowClass*)klass;

	parent_class = gtk_type_class(GNOME_TYPE_DIALOG);

	gtk_object_add_arg_type(	"GernelCompiler::preferences",
					GTK_TYPE_WIDGET,
					GTK_ARG_READABLE,
					ARG_PREFERENCES	);

	gtk_object_add_arg_type(	"GernelCompiler::local_name",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_LOCAL_NAME	);

	object_class->get_arg = gernel_compiler_get_arg;
	object_class->set_arg = gernel_compiler_set_arg;

	function_offset = GTK_SIGNAL_OFFSET(GernelCompilerClass, configure);

	compiler_signals[CONFIGURE] =

			gtk_signal_new(	"configure",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class, 
					compiler_signals, 
					LAST_SIGNAL	);

	klass->configure = NULL;
}

static void gernel_compiler_construct(	GernelCompiler *compiler	)
/*	Construct the compiler dialog	*/
{
	GnomeDialog	*dialog;

	g_return_if_fail(compiler != NULL);
	g_return_if_fail(GERNEL_IS_COMPILER(compiler));

	dialog = GNOME_DIALOG(compiler);

	gnome_dialog_append_button_with_pixmap(	dialog, 
						_("Compile"), 
						GNOME_STOCK_PIXMAP_EXEC	);

	gnome_dialog_append_buttons(	dialog,
					GNOME_STOCK_PIXMAP_PROPERTIES,
					GNOME_STOCK_PIXMAP_CLOSE,
					GNOME_STOCK_PIXMAP_HELP,
					NULL	);


	gnome_dialog_button_connect_object(	dialog,
						0,
						gernel_compiler_start,
						(gpointer)compiler	);

	gnome_dialog_button_connect_object(	dialog,
						1,
						gernel_compiler_configure,
						(gpointer)compiler	);

	gnome_dialog_button_connect_object(	dialog,
						2,
						gnome_dialog_close,
						(gpointer)dialog	);

	gnome_dialog_set_default(dialog, 0);
	gnome_dialog_close_hides(dialog, TRUE);

	gernel_compiler_construct_password(compiler);
}

static void gernel_compiler_construct_password(	GernelCompiler *compiler )
/*	Construct the password dialog for this compiler dialog	*/
{
	GtkWidget	*password;

	g_return_if_fail(compiler != NULL);
	g_return_if_fail(GERNEL_IS_COMPILER(compiler));

	password = gernel_password_new();
	compiler->password = password;
}

static void gernel_compiler_start(	GernelCompiler *compiler	)
/*	Start compiling the kernel	*/
{
	g_return_if_fail(compiler != NULL);
	g_return_if_fail(GERNEL_IS_COMPILER(compiler));

	gtk_widget_show(compiler->password);
}

static void gernel_compiler_configure(	GernelCompiler *compiler	)
/*	Show compiler's property page in GernelPreferences dialog	*/
{
	g_return_if_fail(compiler != NULL);
	g_return_if_fail(GERNEL_IS_COMPILER(compiler));

	gtk_signal_emit(GTK_OBJECT(compiler), compiler_signals[CONFIGURE]);
}

static GtkWidget* gernel_compiler_preferences(	GernelCompiler *compiler )
/*	Assemble a page to be used in a Gnome property box	*/
{
	GtkWidget		*page, *section;
	GtkContainer		*container;

	g_return_val_if_fail(compiler != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_COMPILER(compiler), NULL);

	page = erty_preference_page_new();
	container = GTK_CONTAINER(page);
	section = erty_preference_section_new(_("Targets"));
	gtk_container_add(container, section);
	section = gernel_compiler_password_preferences(compiler);
	gtk_container_add(container, section);

	return page;
}

static GtkWidget* gernel_compiler_password_preferences(
		GernelCompiler *compiler
		)
/*	Wrapper to get compiler's password dialog preferences	*/
{
	GtkWidget	*widget;
	GtkArg		args[1];

	g_return_val_if_fail(compiler != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_COMPILER(compiler), NULL);

	args[0].name = g_strconcat(
			gtk_widget_get_name(compiler->password),
			"::preferences", 
			NULL
			);

	gtk_object_getv(GTK_OBJECT(compiler->password), 1, args);
	widget = GTK_WIDGET(GTK_VALUE_OBJECT(args[0]));

	return widget;
}

static void gernel_compiler_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelCompiler	*compiler;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_COMPILER(object));

	compiler = GERNEL_COMPILER(object);

	switch (arg_id) {
		GtkWidget	*widget;
		gchar		*string;

		case ARG_LOCAL_NAME:
			string = gernel_compiler_get_local_name(compiler);
			GTK_VALUE_STRING(*arg) = string;
			break;
		case ARG_PREFERENCES:
			widget = gernel_compiler_preferences(compiler);
			GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(widget);
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void gernel_compiler_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelCompiler	*compiler;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_COMPILER(object));

	compiler = GERNEL_COMPILER(object);

	switch (arg_id) {
		gchar	*string;

		case ARG_LOCAL_NAME:
			string = GTK_VALUE_STRING(*arg);
			gernel_compiler_set_local_name(compiler, string);
			break;
		case ARG_PREFERENCES:	/*	Fall Through	*/
		default:
			break;
	}
}

static void gernel_compiler_set_local_name(	GernelCompiler *compiler,
						gchar *name	)
/*	Set the local name of compiler to name	*/
{
	g_return_if_fail(compiler != NULL);
	g_return_if_fail(GERNEL_IS_COMPILER(compiler));
	g_return_if_fail(name != NULL);

	compiler->local_name = name;
}

static gchar* gernel_compiler_get_local_name(	GernelCompiler *compiler )
/*	Get the local name of compiler	*/
{
	g_return_val_if_fail(compiler != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_COMPILER(compiler), NULL);

	return compiler->local_name;
}
