#include <gernel.h>
#include <gernel_dock.h>
#include <gernel_dock_item.h>

static void	gernel_dock_class_init(	GernelDockClass *klass	);
static void	gernel_dock_child_detached(	GernelDock *dock	);
static void	gernel_dock_child_attached(	GernelDock *dock	);
static void	gernel_dock_width_customizable(	GernelDock *dock	);
static void 	gernel_dock_construct(	GernelDock *dock	);

static void	gernel_dock_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void	gernel_dock_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void	gernel_dock_size_allocate(	GtkWidget *widget,
						GtkAllocation *allocation );

static void	gernel_dock_size_request(	GtkWidget *widget, 
						GtkRequisition *requisition );

static void gernel_dock_init(	GernelDock *dock	);

static void gernel_dock_add(	GtkContainer *container,
				GtkWidget *widget	);

static GtkWidget* gernel_dock_get_item(	GernelDock *dock	);

static void gernel_dock_set_vscrolling(	GernelDock *dock,
					GtkPolicyType policy_type	);

enum {	WIDTH_CUSTOMIZABLE,
	CHILD_ATTACHED,
	CHILD_DETACHED,
	LAST_SIGNAL	};

enum {	ARG_0,
	ARG_WIDTH_CUSTOMIZABLE	};

static guint dock_signals[LAST_SIGNAL] = { 0 };

static GtkHBoxClass	*parent_class = NULL;

static void gernel_dock_class_init(	GernelDockClass *klass	)
/*	Initializes the GernelDock class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBoxClass		*box_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	box_class = (GtkBoxClass*)klass;

	parent_class = gtk_type_class(gtk_hbox_get_type());

	gtk_object_add_arg_type(	"GernelDock::width_customizable",
					GTK_TYPE_BOOL,
					GTK_ARG_READABLE,
					ARG_WIDTH_CUSTOMIZABLE	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelDockClass, 
						width_customizable	);

	dock_signals[WIDTH_CUSTOMIZABLE] =

			gtk_signal_new(	"width_customizable",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__BOOL,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_BOOL	);

	function_offset = GTK_SIGNAL_OFFSET(GernelDockClass, child_attached);

	dock_signals[CHILD_ATTACHED] = gtk_signal_new(	"child_attached",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelDockClass, child_detached);

	dock_signals[CHILD_DETACHED] = gtk_signal_new(	"child_detached",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	gtk_object_class_add_signals(object_class, dock_signals, LAST_SIGNAL);

	object_class->get_arg = gernel_dock_get_arg;
	object_class->set_arg = gernel_dock_set_arg;

	widget_class->size_allocate = gernel_dock_size_allocate;
	widget_class->size_request = gernel_dock_size_request;

	container_class->add = gernel_dock_add;

	klass->width_customizable = NULL;
	klass->child_attached = gernel_dock_width_customizable;
	klass->child_detached = gernel_dock_width_customizable;
}

static void gernel_dock_width_customizable(	GernelDock *dock	)
/*	Emit the 'width_customizable' signal	*/
{
	GnomeDockItem	*dock_item;

	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	dock_item = GNOME_DOCK_ITEM(dock->dock_item);

	gtk_signal_emit(	GTK_OBJECT(dock), 
				dock_signals[WIDTH_CUSTOMIZABLE], 
				!dock_item->is_floating	);
}

GtkType gernel_dock_get_type(	void	)
/*	Return type identifier for type GernelDock	*/
{
	static GtkType	dock_type = 0;

	if (!dock_type) {

		static const	GtkTypeInfo dock_info = {
			"GernelDock",
			sizeof(GernelDock),
			sizeof(GernelDockClass),
			(GtkClassInitFunc)gernel_dock_class_init,
			(GtkObjectInitFunc)gernel_dock_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
			};

		dock_type = gtk_type_unique(gtk_hbox_get_type(), &dock_info);
	}

	return dock_type;
}

static void gernel_dock_init( GernelDock *dock	)
{
	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	dock->dock = NULL;
	dock->dock_item = NULL;
	dock->widget = NULL;
	dock->detachable = TRUE;
}

GernelDock* gernel_dock_new(	void	)
/*	Create an instance of GernelDock	*/
{
	GernelDock	*dock;

	dock = gtk_type_new(gernel_dock_get_type());
	gernel_dock_construct(dock);

	return dock;
}

static void gernel_dock_construct(	GernelDock *dock	)
/*	Set up the dock	*/
{
	GtkWidget	*widget;

	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	widget = gnome_dock_new();
	dock->dock = widget;
	gtk_box_pack_start(GTK_BOX(dock), widget, FALSE, FALSE, 0);
	gtk_widget_show(widget);
}

static void gernel_dock_size_allocate(	GtkWidget *widget,
					GtkAllocation *allocation )
/*	Replacement for gnome_dock_size_allocate(); forces child to resize to 
	allocation	*/
{
	GernelDock	*dock;
	GtkWidget	*dock_item;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(widget));

	dock = GERNEL_DOCK(widget);

	if (GNOME_DOCK(dock->dock)->floating_children != NULL) {
		dock_item = GNOME_DOCK(dock->dock)->floating_children->data;
	} else if (dock->detachable)
		dock_item = GNOME_DOCK(dock->dock)->left_bands->data;
	else
		dock_item = dock->widget;

	gtk_widget_size_allocate(dock_item, allocation);
}

static void gernel_dock_add(	GtkContainer *container,
				GtkWidget *widget	)
/*	Generic add function for GernelDock	*/
{
	GnomeDock	*dock;
	GtkWidget	*item;
	GnomeDockItem	*dock_item;
	GtkObject	*object;

	g_return_if_fail(container != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(container));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_WIDGET(widget));

	dock = GNOME_DOCK(GERNEL_DOCK(container)->dock);

	item = gernel_dock_item_new();
	gtk_container_add(GTK_CONTAINER(item), widget);
	object = GTK_OBJECT(item);

	gtk_signal_connect_object(	object,
					"attached",
					gernel_dock_child_attached,
					GTK_OBJECT(container)	);

	gtk_signal_connect_object(	object,
					"detached",
					gernel_dock_child_detached,
					GTK_OBJECT(container)	);

	gtk_signal_connect_object(	object,
					"show",
					gtk_widget_show,
					GTK_OBJECT(container)	);

	gtk_signal_connect_object(	object,
					"hide",
					gtk_widget_hide,
					GTK_OBJECT(container)	);

	dock_item = GNOME_DOCK_ITEM(item);
	gnome_dock_add_item(dock, dock_item, GNOME_DOCK_LEFT, 0, 0, 0, TRUE);
	GERNEL_DOCK(container)->dock_item = item;

	GERNEL_DOCK(container)->widget = 
		gernel_dock_item_get_child(GERNEL_DOCK_ITEM(dock_item));

	gtk_widget_show(item);
}

static void gernel_dock_child_attached(	GernelDock *dock	)
/*	Emit the 'child_attached' signal	*/
{
	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	gtk_signal_emit(GTK_OBJECT(dock), dock_signals[CHILD_ATTACHED]);
}

static void gernel_dock_child_detached(	GernelDock *dock	)
/*	Emit the 'child_detached' signal	*/
{
	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	gtk_signal_emit(GTK_OBJECT(dock), dock_signals[CHILD_DETACHED]);
}

static void gernel_dock_size_request(	GtkWidget *widget, 
					GtkRequisition *requisition	)
/*	size requisition, dependant of widget's visibility	*/
{
	GnomeDockItem	*dock_item;
	GernelDock	*dock;

	dock = GERNEL_DOCK(widget);
	dock_item = GNOME_DOCK_ITEM(gernel_dock_get_item(dock));

/*FIXME:	get floating flag by using arg system (encapsulation of 
		GernelDockItem	*/

	/*	If the dock item is floating, don't let the container of the
		dock adjust to its requisition	*/
	if (dock_item->is_floating) {
		GtkRequisition	child_requisition;

		gtk_widget_size_request(	GTK_WIDGET(dock_item), 
						&child_requisition	);
	} else if (dock->detachable)
		gtk_widget_size_request(GTK_WIDGET(dock_item), requisition);
	else 
		gtk_widget_size_request(dock->widget, requisition);
}

static void gernel_dock_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelDock	*dock;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(object));

	dock = GERNEL_DOCK(object);

	switch (arg_id) {
		GnomeDockItem	*dock_item;

		case ARG_WIDTH_CUSTOMIZABLE:
			if (dock->dock_item == NULL) return;
			dock_item = GNOME_DOCK_ITEM(dock->dock_item);
			GTK_VALUE_BOOL(*arg) = !dock_item->is_floating;
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void gernel_dock_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelDock	*dock;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(object));

	dock = GERNEL_DOCK(object);

	switch (arg_id) {
		case ARG_WIDTH_CUSTOMIZABLE:	/*	Fall Through	*/
		default:
			break;
	}
}

void gernel_dock_set_hscrolling(	GernelDock *dock,
					GtkPolicyType policy_type	)
/*	Set scrolling type of dock's child	*/
{
	GtkWidget	*dock_item;

	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	dock_item = gernel_dock_get_item(dock);

	gernel_dock_item_set_hscrolling(	GERNEL_DOCK_ITEM(dock_item), 
						policy_type	);
}

static void gernel_dock_set_vscrolling(	GernelDock *dock,
					GtkPolicyType policy_type	)
/*	Set vertical scrolling type of dock's child	*/
{
	GtkWidget	*dock_item;

	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	dock_item = gernel_dock_get_item(dock);

	gernel_dock_item_set_vscrolling(	GERNEL_DOCK_ITEM(dock_item),
						policy_type	);
}

static GtkWidget* gernel_dock_get_item(	GernelDock *dock	)
/*	Get the GernelDockItem contained by dock	*/
{
	g_return_val_if_fail(dock != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_DOCK(dock), NULL);
	g_return_val_if_fail(dock->dock_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_DOCK_ITEM(dock->dock_item), NULL);

	return dock->dock_item;
}

void gernel_dock_set_detachable(	GernelDock *dock,
					gboolean flag	)
/*	Tell dock's dock_item whether to be detachable by user	*/
{
	GtkWidget	*dock_item, *widget;

	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

g_print("gernel_dock_set_detachable:	FIXME: think about how dock->detachable is initialized (shouldn't it be influenced by the canvas?\n");
	if (flag == dock->detachable) return;

	dock->detachable = flag;
	widget = dock->widget;
	gtk_widget_ref(widget);
	gtk_container_remove(GTK_CONTAINER(widget->parent), widget);

	if (flag)
		gtk_container_add(GTK_CONTAINER(dock->dock_item), widget);
	else
		gtk_box_pack_start(GTK_BOX(dock), widget, FALSE, FALSE, 0);

	gtk_widget_unref(widget);

	dock_item = gernel_dock_get_item(dock);
	gernel_dock_item_set_detachable(GERNEL_DOCK_ITEM(dock_item), flag);
}

void gernel_dock_no_vscrolling(	GernelDock *dock	)
/*	Tell dock's dock_item to switch off vertical scrolling, thereby 
	adjusting its size to contents	*/
{
	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	gernel_dock_set_vscrolling(dock, GTK_POLICY_NEVER);
}

void gernel_dock_no_hscrolling(	GernelDock *dock	)
/*	Tell dock's dock_item to switch off horizontal scrolling, thereby,
	adjusting its size to contents	*/
{
	g_return_if_fail(dock != NULL);
	g_return_if_fail(GERNEL_IS_DOCK(dock));

	gernel_dock_set_hscrolling(dock, GTK_POLICY_NEVER);
}
