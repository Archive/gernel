#define	GERNEL_TYPE_VAR_ITEM_ICON_SET	(gernel_var_item_icon_set_get_type())

#define	GERNEL_VAR_ITEM_ICON_SET(obj) \
	(GTK_CHECK_CAST(	(obj), \
				GERNEL_TYPE_VAR_ITEM_ICON_SET, \
				GernelVarItemIconSet)	)

#define GERNEL_VAR_ITEM_ICON_SET_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_VAR_ITEM_ICON_SET, \
				GernelVarItemIconSet	))

#define GERNEL_IS_VAR_ITEM_ICON_SET(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_VAR_ITEM_ICON_SET))

#define GERNELE_IS_VAR_ITEM_ICON_SET_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_VAR_ITEM_ICON_SET))

typedef struct _GernelVarItemIconSet		GernelVarItemIconSet;
typedef struct _GernelVarItemIconSetClass	GernelVarItemIconSetClass;

enum GernelVarItemIconSetType {	GERNEL_VAR_ITEM_ICON_SET_TYPE_0,
				GERNEL_VAR_ITEM_ICON_SET_TYPE_VALUE,
				GERNEL_VAR_ITEM_ICON_SET_TYPE_CHOICE,
				GERNEL_VAR_ITEM_ICON_SET_TYPE_TRISTATE,
				GERNEL_VAR_ITEM_ICON_SET_TYPE_BOOL	};

enum GernelVarItemIconSetValue {	GERNEL_VAR_ITEM_ICON_SET_VALUE_0,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_STRING,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_VALUE,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_YES,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_MODULE,
					GERNEL_VAR_ITEM_ICON_SET_VALUE_NO };

struct _GernelVarItemIconSet {
	GtkVBox		hbox;

	/*	Allow direct access when changing a variable's value visually */
	GtkWidget	*yes, *module, *no;

	/*	An array of GtkLabel widgets that allows for fast access when
		changing the value of a CHOICE GernelVarItem	*/
	GArray		*labels;

	/*	Used for type value	*/
	GtkWidget	*label;
};

struct _GernelVarItemIconSetClass {
	GtkHBoxClass	parent_class;
};

GtkType		gernel_var_item_icon_set_get_type(	void	);

GtkWidget*	gernel_var_item_icon_set_new(
		GdkWindow *window,
		enum GernelVarItemIconSetType type,
		GSList *var_items
		);

void		gernel_var_item_icon_set_set_value(
		GernelVarItemIconSet *var_item_icon_set,
		enum GernelVarItemIconSetValue type,
		gint value,
		gchar *string
		);
