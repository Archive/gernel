#define	GERNEL_TYPE_TOOLBAR	(gernel_toolbar_get_type())

#define	GERNEL_TOOLBAR(obj) \
	(GTK_CHECK_CAST(	(obj), \
				GERNEL_TYPE_TOOLBAR, \
				GernelToolbar	))

#define GERNEL_TOOLBAR_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_TOOLBAR, \
				GernelToolbar	))

#define	GERNEL_IS_TOOLBAR(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_TOOLBAR))

#define GERNEL_IS_TOOLBAR_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_TOOLBAR))

typedef struct _GernelToolbar		GernelToolbar;
typedef struct _GernelToolbarClass	GernelToolbarClass;

struct _GernelToolbar {
	GtkToolbar	toolbar;

	GtkWidget	*next, *find, *up, *down, *previous, *back, *forward,
			*close;
};

struct _GernelToolbarClass {
	GtkToolbarClass	parent_class;

	void (*up)	(	GernelToolbar *toolbar	);
	void (*down)	(	GernelToolbar *toolbar	);
	void (*next)	(	GernelToolbar *toolbar	);
	void (*previous)(	GernelToolbar *toolbar	);
	void (*back)	(	GernelToolbar *toolbar	);
	void (*forward)	(	GernelToolbar *toolbar	);
	void (*find)	(	GernelToolbar *toolbar	);
	void (*close)	(	GernelToolbar *toolbar	);
};

GtkType		gernel_toolbar_get_type(	void	);
GtkWidget*	gernel_toolbar_new(	void	);

void		gernel_toolbar_set_history(	GernelToolbar *toolbar,
						guint history,
						guint future	);

void		gernel_toolbar_set_up(	GernelToolbar *toolbar,
					gboolean flag	);

void		gernel_toolbar_set_down(	GernelToolbar *toolbar,
						gboolean flag	);

void		gernel_toolbar_set_next(	GernelToolbar *toolbar,
						gboolean flag	);

void		gernel_toolbar_set_previous(	GernelToolbar *toolbar,
						gboolean flag	);

void		gernel_toolbar_set_find(	GernelToolbar *toolbar	);
