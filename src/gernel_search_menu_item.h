#define GERNEL_TYPE_SEARCH_MENU_ITEM	(gernel_search_menu_item_get_type())

#define	GERNEL_SEARCH_MENU_ITEM(obj) \
	GTK_CHECK_CAST(obj, GERNEL_TYPE_SEARCH_MENU_ITEM, GernelSearchMenuItem)

#define	GERNEL_SEARCH_MENU_ITEM_CLASS(klass) \
	GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_SEARCH_ITEM, \
				GernelSearchMenuItemClass	)

#define	GERNEL_IS_SEARCH_MENU_ITEM(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_SEARCH_MENU_ITEM))

#define GERNEL_IS_SEARCH_MENU_ITEM_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE((klass, GERNEL_TYPE_SEARCH_MENU_ITEM))

typedef struct _GernelSearchMenuItem		GernelSearchMenuItem;
typedef struct _GernelSearchMenuItemClass	GernelSearchMenuItemClass;

struct _GernelSearchMenuItem {
	GtkMenuItem	menu_item;

	guint		identifier;
};

struct _GernelSearchMenuItemClass {
	GtkMenuItemClass	parent_class;
};

GtkType		gernel_search_menu_item_get_type(	void	);
GtkWidget*	gernel_search_menu_item_new(	void	);

GtkWidget*	gernel_search_menu_item_new_from_data(	gchar *label,
							guint identifier );

void		gernel_search_menu_item_set_label(
		GernelSearchMenuItem *search_menu_item,
		gchar *label
		);

void		gernel_search_menu_item_set_identifier(
		GernelSearchMenuItem *search_menu_item,
		guint identifier
		);

gint		gernel_search_menu_item_get_identifier(
		GernelSearchMenuItem *search_menu_item
		);
