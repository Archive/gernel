#include <gernel.h>
#include <gernel_app.h>
#include <gernel_paned.h>
#include <gernel_canvas.h>
#include <gernel_dock.h>
#include <gernel_menu_bar.h>
#include <gernel_toolbar.h>
#include <gernel_setter.h>
#include <gernel_search.h>
#include <gernel_compiler.h>
#include <gernel_navigation.h>
#include <gernel_splash.h>
#include <gernel_file_selection.h>
#include <libgnomeui/gnome-window-icon.h>

#define	GERNEL_APP_DEFAULT_WIDTH	500
#define	GERNEL_APP_DEFAULT_HEIGHT	500

enum GernelAppComponent {	GERNEL_APP_COMPONENT_0,
				GERNEL_APP_COMPONENT_SEARCH,
				GERNEL_APP_COMPONENT_COMPILER,
				GERNEL_APP_COMPONENT_SETTER,
				GERNEL_APP_COMPONENT_NAVIGATION,
				GERNEL_APP_COMPONENT_TEXT	};

static void gernel_app_save_as(	GernelApp *app	);
static void gernel_app_init(	GernelApp *app	);
static void gernel_app_class_init(	GernelAppClass *klass	);
static void gernel_app_construct_compiler(	GernelApp *app	);
static void gernel_app_construct_statusbar(	GernelApp *app	);
static void gernel_app_construct_menubar(	GernelApp *app	);
static void gernel_app_construct_toolbar(	GernelApp *app	);
static void gernel_app_construct_setter(	GernelApp *app	);
static void gernel_app_construct_contents(	GernelApp *app	);
static void gernel_app_construct_handlers(	GernelApp *app	);
static void gernel_app_construct_contents_paned(	GernelApp *app	);
static void gernel_app_close(	GernelApp *app	);
static gchar* gernel_app_get_machine_type(	void	);
static GtkWidget* gernel_app_preferences(	GernelApp *app	);

static void gernel_app_construct_canvas(	GernelApp *app, 
						GtkWidget *paned	);

static void gernel_app_construct_navigation(	GernelApp *app,
						GtkWidget *paned	);

static void gernel_app_connect_multiple(	GtkObject *object,
						gchar *signal,
						GtkSignalFunc function,
						GtkWidget *user_data,
						...	);

static void gernel_app_find(	GernelApp *app	);
static void gernel_app_configure_search(	GernelApp *app	);
static void gernel_app_configure_canvas(	GernelApp *app	);

static void gernel_app_new_window(	GernelApp *app	);

static void gernel_app_configure(	GernelApp *app,
					enum GernelAppComponent component );

static GnomeAppClass	*parent_class = NULL;

enum {	CLOSE,
	LAST_SIGNAL	};

static guint app_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_app_get_type(	void	)
/*	Return type identifier for type GernelApp	*/
{
	static GtkType	app_type = 0;

	if (!app_type) {

		static const GtkTypeInfo	app_info = {
				"GernelApp",
				sizeof(GernelApp),
				sizeof(GernelAppClass),
				(GtkClassInitFunc)gernel_app_class_init,
				(GtkObjectInitFunc)gernel_app_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		app_type = gtk_type_unique(gnome_app_get_type(), &app_info);
	}

	return app_type;
}

GtkWidget* gernel_app_new(	void	)
/*	Create an instance of GernelApp	*/
{
	return gtk_type_new(gernel_app_get_type());
}

static void gernel_app_init(	GernelApp *app	)
/*	Initialize the GernelApp	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	gnome_app_construct(GNOME_APP(app), PACKAGE, PROGRAM);

	/*	FIXME:	remember default width and height	*/
	gtk_window_set_default_size(	GTK_WINDOW(app), 
					GERNEL_APP_DEFAULT_WIDTH, 
					GERNEL_APP_DEFAULT_HEIGHT	);

	gernel_app_construct_statusbar(app);
	gernel_app_construct_toolbar(app);
	gernel_app_construct_setter(app);
	gernel_app_construct_contents(app);
/*	gernel_app_construct_search(app);	*/
	gernel_app_construct_compiler(app);
	gernel_app_construct_menubar(app);
	gernel_app_construct_handlers(app);
}


static void gernel_app_class_init(	GernelAppClass *klass	)
/*	Initialize the GernelAppClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;

	object_class = (GtkObjectClass*)klass;
	parent_class = gtk_type_class(GNOME_TYPE_APP);
	function_offset = GTK_SIGNAL_OFFSET(GernelAppClass, close);

	app_signals[CLOSE] = gtk_signal_new(	"close",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__NONE,
						GTK_TYPE_NONE,
						0	);

	gtk_object_class_add_signals(object_class, app_signals, LAST_SIGNAL);

	klass->close = NULL;
}

void gernel_app_set_splash(	GernelApp *app, 
				GtkWidget *splash	)
/*	Tell app to send its status information to splash	*/
{
	guint	handler;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));
	g_return_if_fail(splash != NULL);
	g_return_if_fail(GERNEL_IS_SPLASH(splash));

	handler = gtk_signal_connect_object(	GTK_OBJECT(app->navigation), 
						"updated",
						gernel_splash_update,
						GTK_OBJECT(splash) );

	app->splash_handler = handler;
}

void gernel_app_remove_splash(	GernelApp *app	)
/*	Tell app that splash no longer exists	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	gtk_signal_disconnect(GTK_OBJECT(app->navigation), app->splash_handler);
}

static void gernel_app_construct_statusbar(	GernelApp *app	)
/*	Construct the appbar of app	*/
{
	GtkWidget	*statusbar;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	statusbar = gnome_appbar_new(TRUE, TRUE, GNOME_PREFERENCES_NEVER);
	gnome_app_set_statusbar(GNOME_APP(app), statusbar);
}

static void gernel_app_construct_menubar(	GernelApp *app	)
/*	Construct the menubar of app	*/
{
	GtkWidget	*menu_bar;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	menu_bar = gernel_menu_bar_new(GTK_WIDGET(app));
	app->menu_bar = menu_bar;
}

static void gernel_app_construct_toolbar(	GernelApp *app	)
/*	Construct the toolbar of app	*/
{
	GtkWidget	*toolbar;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	toolbar = gernel_toolbar_new();
	gnome_app_set_toolbar(GNOME_APP(app), GTK_TOOLBAR(toolbar));
	app->toolbar = toolbar;
}

static void gernel_app_construct_setter(	GernelApp *app	)
/*	Construct the setter of app	*/
{
	GtkWidget	*setter;
	GnomeDock	*dock;
	GnomeDockItem	*dock_item;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	setter = gernel_setter_new(_("Variable Setter"));
	app->setter = setter;
	dock = GNOME_DOCK(GNOME_APP(app)->dock);
	dock_item = GNOME_DOCK_ITEM(setter);
	gnome_dock_add_item(dock, dock_item, GNOME_DOCK_TOP, 1, 0, 0, TRUE);
g_print("gernel_app_construct_setter:	FIXME: GernelSetter should be derived from GtkToolbar. This would allow to use gnome_app_add_toolbar() which seems to be the only way of adding special dock items to GnomeApp without breaking dock_item position remembering. Look into gnumeric_toolbar_new() for inspiration.\n");
	gtk_widget_show(setter);
}

static void gernel_app_construct_contents(	GernelApp *app	)
/*	Construct the paned of app	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	gernel_app_construct_contents_paned(app);
}

static void gernel_app_construct_contents_paned(	GernelApp *app	)
/*	Fill contents of app with paned	*/
{
	GtkWidget	*paned;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	paned = gernel_paned_new();	
	gnome_app_set_contents(GNOME_APP(app), paned);
	gtk_widget_show(paned);

	gernel_app_construct_navigation(app, paned);
	gernel_app_construct_canvas(app, paned);

	gernel_paned_set_position(	GERNEL_PANED(paned), 
					GERNEL_APP_DEFAULT_WIDTH / 2	);
}

static void gernel_app_construct_navigation(	GernelApp *app,
						GtkWidget *paned	)
/*	Construct the navigation for app's contents, add to paned	*/
{
	GtkWidget	*navigation, *setter, *menu_bar, *toolbar;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));
/*	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));	*/

	setter = app->setter;
	menu_bar = app->menu_bar;
	toolbar = app->toolbar;

	navigation = gernel_navigation_new(_("Navigation"));
	app->navigation = navigation;
	gtk_container_add(GTK_CONTAINER(paned), navigation);
	gtk_widget_show(navigation);
}

void gernel_app_set_path(	GernelApp *app, 
				gchar *path	)
/*	Set the kernel source directory path	*/
{
	GernelNavigation	*navigation;
	gchar			*tmp, *machine;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));
	g_return_if_fail(path != NULL);

	navigation = GERNEL_NAVIGATION(app->navigation);
	gernel_navigation_set_base_directory(navigation, path);

	tmp = g_strconcat(	DOCUMENTATION_DIRNAME,
				G_DIR_SEPARATOR_S,
				CONFIGURE_HELP_FILENAME,
				NULL	);

	gernel_navigation_set_contents_path(navigation, tmp);

	tmp = g_strconcat(	ARCH_DIRNAME,
				G_DIR_SEPARATOR_S,
				machine = gernel_app_get_machine_type(),
				G_DIR_SEPARATOR_S,
				CONFIG_IN_FILENAME,
				NULL	);

	gernel_navigation_set_navigation_path(navigation, tmp);

	tmp = g_strconcat(	ARCH_DIRNAME,
				G_DIR_SEPARATOR_S,
				machine,
				G_DIR_SEPARATOR_S,
				DEFCONFIG_FILENAME,
				NULL	);

	gernel_navigation_set_template_path(navigation, tmp);
	g_free(machine);
}

static void gernel_app_construct_handlers(	GernelApp *app	)
/*	Set up all the signal handlers between app's components	*/
{
	GtkWidget	*navigation, *setter, *menu_bar, *toolbar, *canvas;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	g_return_if_fail(app->navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(app->navigation));
	g_return_if_fail(app->setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(app->setter));
	g_return_if_fail(app->menu_bar != NULL);
	g_return_if_fail(GERNEL_IS_MENU_BAR(app->menu_bar));
	g_return_if_fail(app->toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(app->toolbar));
	g_return_if_fail(app->canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(app->canvas));

	navigation = app->navigation;
	setter = app->setter;
	menu_bar = app->menu_bar;
	toolbar = app->toolbar;
	canvas = app->canvas;

	gernel_app_connect_multiple(
		GTK_OBJECT(navigation),
		"selected", gernel_setter_update, setter,
		"unselected", gernel_setter_reset, setter,
		"dependants", gernel_menu_bar_set_dependants, menu_bar,
		"prerequisites", gernel_menu_bar_set_prerequisites, menu_bar,
		"history", gernel_menu_bar_set_history, menu_bar,
		"history", gernel_toolbar_set_history, toolbar,
		"up_possible", gernel_menu_bar_set_up, menu_bar,
		"down_possible", gernel_menu_bar_set_down, menu_bar,
		"next_possible", gernel_menu_bar_set_next, menu_bar,
		"previous_possible", gernel_menu_bar_set_previous, menu_bar,
		"up_possible", gernel_toolbar_set_up, toolbar,
		"down_possible", gernel_toolbar_set_down, toolbar,
		"next_possible", gernel_toolbar_set_next, toolbar,
		"previous_possible", gernel_toolbar_set_previous, toolbar,
		"done_parsing", gernel_toolbar_set_find, toolbar,
		"done_parsing", gernel_menu_bar_set_save_as, menu_bar,
		"title", gtk_window_set_title, app,
		"view_changed", gernel_canvas_update_contents, canvas,
		NULL
		);

	gernel_app_connect_multiple(
		GTK_OBJECT(menu_bar),
		"directory", gernel_navigation_set_base_directory, navigation,
		"directory", gernel_app_set_path, app,
		"prerequisites", gernel_navigation_prerequisites, navigation,
		"dependants", gernel_navigation_dependants, navigation,
		"directory", gernel_navigation_parse, navigation,
		"directory", gernel_app_construct_search, app,
		"find", gernel_app_find, app,
		"up", gernel_navigation_up, navigation,
		"down", gernel_navigation_down, navigation,
		"next", gernel_navigation_next, navigation,
		"previous", gernel_navigation_previous, navigation,
		"back", gernel_navigation_back, navigation,
		"forward", gernel_navigation_forward, navigation,
		"preferences", gernel_app_preferences, app,
		"compile", gtk_widget_show, app->compiler,
		"new_window", gernel_app_new_window, app,
/*		"save", gernel_navigation_save, app->navigation,	*/
		"save_as", gernel_app_save_as, app,
		NULL
		);

	gernel_app_connect_multiple(
			GTK_OBJECT(toolbar),
			"find", gernel_app_find, GTK_WIDGET(app),
			"back", gernel_navigation_back, navigation,
			"forward", gernel_navigation_forward, navigation,
			"up", gernel_navigation_up, navigation,
			"down", gernel_navigation_down, navigation,
			"next", gernel_navigation_next, navigation,
			"previous", gernel_navigation_previous, navigation,
			"close", gernel_app_close, app,
			NULL
			);

	gernel_app_connect_multiple(
		GTK_OBJECT(setter),
		"view_changed", gernel_canvas_update_contents, canvas,
		NULL
		);

	gtk_signal_connect_object(	GTK_OBJECT(app),
					"delete_event",
					gtk_main_quit,
					GTK_OBJECT(app)	);
}

static void gernel_app_find(	GernelApp *app	)
/*	Show the search dialog	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));
	g_return_if_fail(app->search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(app->search));

	gtk_widget_show(app->search);
}

static void gernel_app_new_window(	GernelApp *app	)
/*	Open a new Gernel window	*/
{
	GtkWidget	*window;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	window = gernel_app_new();
	gtk_widget_show(window);
}

static void gernel_app_save_as(	GernelApp *app	)
/*	Open a 'save' dialog	*/
{
	GtkWidget	*dialog;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	dialog = gernel_file_selection_new(_("Save configuration as file"));

	gtk_signal_connect_object(	GTK_OBJECT(dialog),
					"ok",
					gernel_navigation_save_as,
					GTK_OBJECT(app->navigation)	);

	gtk_widget_show(dialog);
}

static void gernel_app_construct_canvas(	GernelApp *app, 
						GtkWidget *paned	)
/*	Create the canvas	*/
{
	GtkWidget	*canvas, *dock;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	dock = GTK_WIDGET(gernel_dock_new());

	gtk_container_add(GTK_CONTAINER(paned), dock);

	canvas = gernel_canvas_new(	_("Text Viewer"),
					GERNEL_CANVAS_ELEMENT_TITLE, 
					GERNEL_CANVAS_ELEMENT_PROMPT,
					GERNEL_CANVAS_ELEMENT_HEADER,
					GERNEL_CANVAS_ELEMENT_TEXT,
					GERNEL_CANVAS_ELEMENT_TAIL,
					GERNEL_CANVAS_ELEMENT_0	);

	gernel_canvas_set_contents(	GERNEL_CANVAS(canvas),
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL	);

	gtk_container_add(GTK_CONTAINER(dock), canvas);
	gernel_dock_set_hscrolling(GERNEL_DOCK(dock), GTK_POLICY_NEVER);

	gernel_app_connect_multiple(
			GTK_OBJECT(canvas),
			"detachable", gernel_dock_set_detachable, dock,
			"configure", gernel_app_configure_canvas, app,
			NULL
			);

	gtk_signal_connect(	GTK_OBJECT(canvas),
				"menu",
				gernel_canvas_construct_menu,
				app	);

	gernel_app_connect_multiple(
		GTK_OBJECT(dock),
		"child_detached", gernel_dock_no_vscrolling, dock,
		"child_detached", gernel_dock_no_hscrolling, dock,
		"child_detached", gernel_paned_set_position_right,paned,
		"show", gtk_widget_queue_resize, paned,
		"child_detached", gtk_widget_hide, dock,
		NULL
		);

	app->canvas = canvas;
}

void gernel_app_construct_search(	GernelApp *app	)
/*	Set up the search dialog	*/
{
	GtkWidget		*search;
	GernelNavigation	*navigation;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));
	g_return_if_fail(app->navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(app->navigation));

	if (app->search != NULL) gtk_widget_destroy(app->search);
	navigation = GERNEL_NAVIGATION(app->navigation);

	search = gernel_search_new_with_tree(	_("Search Dialog"), 
						navigation->tree	);
	app->search = search;

	gtk_signal_connect_object(	GTK_OBJECT(search),
					"configure",
					gernel_app_configure_search,
					GTK_OBJECT(app)	);
}

static void gernel_app_connect_multiple(	GtkObject *object,
						gchar *signal,
						GtkSignalFunc function,
						GtkWidget *user_data,
						...	)
/*	Connect multple signal handlers; widget is emitting object, function is
	signal function to be passed, menu_bar is object to be passed	*/
{
	va_list		args;
	GtkSignalFunc	func;
	GtkObject	*data;
	gchar		*sig;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GTK_IS_OBJECT(object));
	g_return_if_fail(user_data != NULL);
	g_return_if_fail(GTK_IS_WIDGET(user_data));

	sig = signal;
	func = function;
	data = GTK_OBJECT(user_data);

	va_start(args, user_data);

	do {
		gtk_signal_connect_object(object, sig, func, data);
		sig = va_arg(args, gchar*);

		if (sig != NULL) {
			func = va_arg(args, GtkSignalFunc);
			data = va_arg(args, GtkObject*);
		}

	} while (sig);

	va_end(args);
}

static gchar* gernel_app_get_machine_type(	void	)
{
	struct utsname	machine;
	gchar	*result;

	gchar	*pc = "i386", *sun1 = "sun4u", *sun2 = "sparc64", *arm = "arm",
		*arm2 = "sa110";

	uname(&machine);

	if (	(strlen(machine.machine) == strlen(pc)) && 
		(	(pc[0] == machine.machine[0]) &
			(pc[2] == machine.machine[2]) &
			(pc[3] == machine.machine[3])	)	)

		result = pc;
	else if (match(sun1, machine.machine)) 
		result = sun2;
	else if (	(match0(machine.machine, arm)) | 
			(match(machine.machine, arm2))	)
		result = arm;
	else 
		result = machine.machine;

	return g_strdup(result);
}

static GtkWidget* gernel_app_preferences(	GernelApp *app	)
/*	The main preferences dialog	*/
{
	GtkWidget	*dialog;

	g_return_val_if_fail(app != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_APP(app), NULL);
g_print("gernel_app_preference:	FIXME: make sure search dialog etc. are set up at this point\n");
/*g_return_val_if_fail(app->navigation != NULL, NULL);
g_return_val_if_fail(app->canvas != NULL, NULL);
g_return_val_if_fail(app->search != NULL, NULL);
g_return_val_if_fail(app->compiler != NULL, NULL);
g_return_val_if_fail(app->setter != NULL, NULL);*/

	dialog = erty_preferences_new();

	erty_preferences_add_from_widgets(	ERTY_PREFERENCES(dialog),
						app->navigation, NULL,
						app->canvas, NULL,
						app->search, NULL,
						app->compiler, NULL,
						app->setter, NULL,
						NULL	);

	gnome_window_icon_set_from_file(	GTK_WINDOW(dialog), 
						gnome_pixmap_file(LOGO)	);

	gtk_widget_show_all(dialog);

	return dialog;
}

static void gernel_app_configure_search(	GernelApp *app	)
/*	Show the preferences dialog with GernelSearch page on top	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	gernel_app_configure(app, GERNEL_APP_COMPONENT_SEARCH);
}

static void gernel_app_configure_canvas(	GernelApp *app	)
/*	Show the preferences dialog with GernelCanvas page on top	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	gernel_app_configure(app, GERNEL_APP_COMPONENT_TEXT);
}

static void gernel_app_configure_compiler(	GernelApp *app	)
/*	Show the preferences dialog with GernelCompiler page on top	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	gernel_app_configure(app, GERNEL_APP_COMPONENT_COMPILER);
}

static void gernel_app_configure(	GernelApp *app,
					enum GernelAppComponent component )
/*	Show the property dialog with page specified by component on top */
{
	GtkWidget		*dialog;
	gpointer		data;

g_print("gernel_app_configure:	FIXME: connect 'configure' signals of preference widgets (canvas, search etc) internally in Erty\n");
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));
	g_return_if_fail(component != GERNEL_APP_COMPONENT_0);

	dialog = gernel_app_preferences(app);
	data = GINT_TO_POINTER(component);
	erty_preferences_show_page(ERTY_PREFERENCES(dialog), data);
}

static void gernel_app_construct_compiler(	GernelApp *app	)
/*	Construct the compile dialog	*/
{
	GtkWidget	*compiler;

	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	compiler = gernel_compiler_new(_("Compile Dialog"));
	app->compiler = compiler;

	gtk_signal_connect_object(	GTK_OBJECT(compiler),
					"configure",
					gernel_app_configure_compiler,
					GTK_OBJECT(app)	);
}

static void gernel_app_close(	GernelApp *app	)
/*	Emit the 'close' signal	*/
{
	g_return_if_fail(app != NULL);
	g_return_if_fail(GERNEL_IS_APP(app));

	gtk_signal_emit(GTK_OBJECT(app), app_signals[CLOSE]);
}
