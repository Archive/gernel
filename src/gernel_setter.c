#include <gernel.h>
#include <gernel_setter.h>
#include <gernel_setter_button.h>
#include <gernel_var_menu_item.h>
#include <gernel_var_item.h>

static void gernel_setter_set_local_name(	GernelSetter *setter,
						gchar *name	);

static gchar* gernel_setter_get_local_name(	GernelSetter *setter	);
static void gernel_setter_init(	GernelSetter *setter	);
static void gernel_setter_class_init(	GernelSetterClass *klass	);
static void gernel_setter_construct(	GernelSetter *setter	);
static GtkWidget* gernel_setter_preferences(	GernelSetter *setter	);

static void gernel_setter_show_setter(	GernelSetter *setter,
					gboolean flag	);

static void gernel_setter_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_setter_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_setter_connect_multiple(	GtkWidget *widget,
					GtkObject *obj,
					GtkSignalFunc func,
					...	);

static void gernel_setter_construct_frame(	GtkWidget *parent,
						gboolean fill,
						GtkWidget *child,
						gint spacing,
						gboolean show	);

static void gernel_setter_update_display_value(	GernelSetter *setter,
						GtkWidget *var_item	);

static void gernel_setter_update_connects_default(	GernelSetter *setter,
							GtkWidget *var_item );

static void gernel_setter_update_sensitivity(	GernelSetter *setter,
						GtkWidget *var_item	);

static void gernel_setter_update_connects(	GernelSetter *setter,
						GtkWidget *var_item	);

static void gernel_setter_update_view(	GernelSetter *setter,
					GtkWidget *var_item	);

static void gernel_setter_update_menu(	GernelSetter *setter,
					GernelVarItem *var_item	);

static void gernel_setter_view_changed(	GernelSetter *setter,
					GernelVarItem *var_item	);

static GnomeDockItemClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_LOCAL_NAME,
	ARG_SHOW_SETTER,
	ARG_PREFERENCES	};

enum {	VIEW_CHANGED,
	LAST_SIGNAL	};

static guint setter_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_setter_get_type(	void	)
/*	Return type identifier for type GernelSetter	*/
{
	static GtkType	setter_type = 0;

	if (!setter_type) {

		static const GtkTypeInfo	setter_info = {
			"GernelSetter",
			sizeof(GernelSetter),
			sizeof(GernelSetterClass),
			(GtkClassInitFunc)gernel_setter_class_init,
			(GtkObjectInitFunc)gernel_setter_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		setter_type = gtk_type_unique(	gnome_dock_item_get_type(), 
						&setter_info	);
	}

	return setter_type;
}

GtkWidget* gernel_setter_new(	gchar *name	)
/*	Create an instance of GernelSetter	*/
{
	GernelSetter	*setter;
	GtkWidget	*widget;

	g_return_val_if_fail(name != NULL, NULL);

	widget = erty_widget_new(	gernel_setter_get_type(),
					name,
					"show_setter", TRUE,
					NULL	);

	setter = GERNEL_SETTER(widget);
	gernel_setter_set_local_name(setter, name);
	gernel_setter_construct(setter);

	return widget;
}

static void gernel_setter_init(	GernelSetter *setter	)
/*	Initialize the GernelSetter	*/
{
	setter->menu = NULL;
	setter->yes = NULL;
	setter->module = NULL;
	setter->no = NULL;
	setter->value = NULL;
}

static void gernel_setter_class_init(	GernelSetterClass *klass	)
/*	Initialize the GernelSetterClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;

	parent_class = gtk_type_class(GNOME_TYPE_DOCK_ITEM);

	gtk_object_add_arg_type("GernelSetter::preferences",
				GTK_TYPE_WIDGET,
				GTK_ARG_READABLE,
				ARG_PREFERENCES	);

	gtk_object_add_arg_type("GernelSetter::local_name",
				GTK_TYPE_STRING,
				GTK_ARG_READWRITE,
				ARG_LOCAL_NAME	);

	gtk_object_add_arg_type("GernelSetter::show_setter",
				GTK_TYPE_BOOL,
				GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT,
				ARG_SHOW_SETTER	);

	function_offset = GTK_SIGNAL_OFFSET(GernelSetterClass, view_changed);

	setter_signals[VIEW_CHANGED] =

		gtk_signal_new(	"view_changed",
				GTK_RUN_FIRST,
				object_class->type,
				function_offset,
				gtk_marshal_NONE__POINTER_POINTER_POINTER,
				GTK_TYPE_NONE,
				3,
				GTK_TYPE_STRING,
				GTK_TYPE_STRING,
				GTK_TYPE_STRING	);

	gtk_object_class_add_signals(	object_class, 
					setter_signals, 
					LAST_SIGNAL	);

	object_class->get_arg = gernel_setter_get_arg;
	object_class->set_arg = gernel_setter_set_arg;

	klass->view_changed = NULL;
}

static void gernel_setter_construct(	GernelSetter *setter	)
/*	Fill the setter with some widgets	*/
{
	GtkWidget		*hbox, *menu, *button, *icon, *entry, *vbox, 
				*label, *yes_button;

	GSList			*group;
	GtkContainer		*container;
	GnomeDockItemBehavior	behavior;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));

	container = GTK_CONTAINER(setter);

	gtk_container_set_border_width(container, GNOME_PAD_SMALL);

	behavior =	GNOME_DOCK_ITEM_BEH_EXCLUSIVE | 
			GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL;

	GNOME_DOCK_ITEM(container)->behavior = behavior;

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_container_add(container, hbox);
	gtk_widget_show(hbox);

	/*	Menu	*/
	menu = gtk_option_menu_new();
	setter->menu = menu;
	gernel_setter_construct_frame(hbox, TRUE, menu, 0, FALSE);

	/*	Yes button	*/
	yes_button = gernel_setter_button_new();
	setter->yes = yes_button;
	gernel_setter_construct_frame(hbox, FALSE, yes_button, 0, FALSE);

	icon = gnome_stock_pixmap_widget(NULL, GNOME_STOCK_BUTTON_APPLY);
	gtk_container_add(GTK_CONTAINER(yes_button), icon);
	gtk_widget_show(icon);

	/*	Module button	*/
	group = gtk_radio_button_group(GTK_RADIO_BUTTON(yes_button));
	button = gernel_setter_button_new();
	gernel_setter_button_set_group(GERNEL_SETTER_BUTTON(button), group);
	setter->module = button;
	gernel_setter_construct_frame(hbox, FALSE, button, 0, FALSE);

	icon = gnome_stock_pixmap_widget(NULL, GNOME_STOCK_MENU_ATTACH);
	gtk_container_add(GTK_CONTAINER(button), icon);
	gtk_widget_show(icon);

	/*	No button	*/
	group = gtk_radio_button_group(GTK_RADIO_BUTTON(yes_button));
	button = gernel_setter_button_new();
	gernel_setter_button_set_group(GERNEL_SETTER_BUTTON(button), group);
	setter->no = button;
	gernel_setter_construct_frame(hbox, FALSE, button, 0, FALSE);

	icon = gnome_stock_pixmap_widget(NULL, GNOME_STOCK_BUTTON_CLOSE);
	gtk_container_add(GTK_CONTAINER(button), icon);
	gtk_widget_show(icon);

	/*	Value entry	*/
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD_SMALL);
	gernel_setter_construct_frame(hbox, FALSE, vbox, 0, FALSE);

	entry = gnome_entry_new(NULL);
	setter->value = entry;
	gtk_box_pack_start_defaults(GTK_BOX(vbox), entry);
	gtk_widget_show(entry);

	/*	Label	*/
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);
	gernel_setter_construct_frame(hbox, TRUE, vbox, GNOME_PAD_SMALL, TRUE);

	label = gtk_label_new(_("[Nothing selected]"));
	setter->label = label;
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), label);
	gtk_widget_show(label);
}

static void gernel_setter_construct_frame(	GtkWidget *parent,
						gboolean fill,
						GtkWidget *child,
						gint spacing,
						gboolean show	)
/*	Create a frame and insert parent and child as specified	*/
{
	GtkWidget	*frame;
	GtkBox		*box;

	g_return_if_fail(parent != NULL);
	g_return_if_fail(GTK_IS_BOX(parent));
	g_return_if_fail(child != NULL);
	g_return_if_fail(GTK_IS_WIDGET(child));

	box = GTK_BOX(parent);

	frame = gtk_frame_new(NULL);
	gtk_box_pack_start(box, frame, fill, fill, spacing);
	if (show) gtk_widget_show(frame);

	gtk_container_add(GTK_CONTAINER(frame), child);
	gtk_widget_show(child);
}

void gernel_setter_reset(	GernelSetter *setter	)
/*	Reset the setter to defaults	*/
{
	GtkWidget	*frame;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));

	gtk_widget_hide(setter->yes->parent);
	gtk_widget_hide(setter->module->parent);
	gtk_widget_hide(setter->no->parent);
	gtk_widget_hide(setter->menu->parent);

	frame = gtk_widget_get_ancestor(setter->value, GTK_TYPE_FRAME) ;
	gtk_widget_hide(frame);

	gtk_label_set_text(GTK_LABEL(setter->label), _("[Nothing selected]"));
}

void gernel_setter_update(	GernelSetter *setter,
				GtkWidget *var_item	)
/*	Update the shown widgets according to var_item's properties	*/
{
	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gernel_setter_update_sensitivity(setter, var_item);
	gernel_setter_update_connects(setter, var_item);
	gernel_setter_update_display_value(setter, var_item);
	gernel_setter_update_view(setter, var_item);
}

static void gernel_setter_update_display_value(	GernelSetter *setter,
						GtkWidget *item	)
/*	Make the setter's components display the value var_item	*/
{
	GtkWidget	*entry, *button;
	GtkToggleButton	*toggle_button;
	gchar		*value, *description;
	GernelVarItem	*var_item;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(item));

	var_item = GERNEL_VAR_ITEM(item);

	entry = gnome_entry_gtk_entry(GNOME_ENTRY(setter->value));
	value = gernel_var_item_get_value(var_item);
	button = NULL;

	switch (var_item->type) {
		case GERNEL_VAR_ITEM_TYPE_HEADLINE:
		case GERNEL_VAR_ITEM_TYPE_CHOICE:
			break;
		case GERNEL_VAR_ITEM_TYPE_HEX:
		case GERNEL_VAR_ITEM_TYPE_INT:
		case GERNEL_VAR_ITEM_TYPE_STRING:
			gtk_entry_set_text(GTK_ENTRY(entry), value);
			break;
		default:
			if (match(value, CONFIG_IN_YES))
				button = setter->yes;
			else if (match(value, CONFIG_IN_MOD))
				button = setter->module;
			else if (match(value, CONFIG_IN_NO))
				button = setter->no;
			else
				g_assert_not_reached();

			toggle_button = GTK_TOGGLE_BUTTON(button);
			gtk_toggle_button_set_active(toggle_button, TRUE);
			break;
	}

	if ((description = gernel_var_item_get_description(var_item)) != NULL)
		gtk_label_set_text(GTK_LABEL(setter->label), description);
}

static void gernel_setter_update_connects(	GernelSetter *setter,
						GtkWidget *item	)
/*	Update signal handlers of setter's components	*/
{
	GtkWidget	*entry;
	GernelVarItem	*var_item;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(item));

	var_item = GERNEL_VAR_ITEM(item);

	entry = gnome_entry_gtk_entry(GNOME_ENTRY(setter->value));

	switch (var_item->type) {
		case GERNEL_VAR_ITEM_TYPE_HEADLINE:
		case GERNEL_VAR_ITEM_TYPE_CHOICE:
			break;
		case GERNEL_VAR_ITEM_TYPE_HEX:
		case GERNEL_VAR_ITEM_TYPE_INT:
		case GERNEL_VAR_ITEM_TYPE_STRING:

			gtk_signal_handlers_destroy(GTK_OBJECT(entry));

			gtk_signal_connect(	GTK_OBJECT(entry),
						"changed",
						gernel_var_item_update_by_entry,
						var_item	);

			break;
		default:
			gernel_setter_update_connects_default(setter, item);
	}
}

static void gernel_setter_update_connects_default(	GernelSetter *setter,
							GtkWidget *item)
/*	Update signal handlers for var_items with type TRIPLE or BOOL and the
	like	*/
{
	GernelVarItem	*var_item;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(item));

	var_item = GERNEL_VAR_ITEM(item);

	gtk_signal_handlers_destroy(GTK_OBJECT(setter->yes));
	gtk_signal_handlers_destroy(GTK_OBJECT(setter->module));
	gtk_signal_handlers_destroy(GTK_OBJECT(setter->no));

	gernel_setter_connect_multiple(	setter->yes,
					GTK_OBJECT(var_item),
					gernel_var_item_set_true, 
					gernel_var_item_icon_sync,
					gernel_var_item_deps_sync,
					gernel_var_item_no_previous,
					gernel_var_item_allow_yes_deps,
					NULL	);

	gernel_setter_connect_multiple(	setter->module, 
					GTK_OBJECT(var_item),
					gernel_var_item_set_mod, 
					gernel_var_item_icon_sync,
					gernel_var_item_deps_sync,
					gernel_var_item_no_previous,
					gernel_var_item_allow_yes_deps,
					NULL	);

	gernel_setter_connect_multiple(	setter->no, 
					GTK_OBJECT(var_item),
					gernel_var_item_set_false, 
					gernel_var_item_icon_sync,
					gernel_var_item_deps_sync,
					gernel_var_item_no_previous,
					NULL	);
}

static void gernel_setter_update_view(	GernelSetter *setter,
					GtkWidget *item	)
/*	Show/hide the components of the setter	*/
{
	GtkWidget	*yes, *module, *no, *menu, *value;
	GernelVarItem	*var_item;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(item));

	var_item = GERNEL_VAR_ITEM(item);

	yes = setter->yes->parent;
	module = setter->module->parent;
	no = setter->no->parent;
	value = gtk_widget_get_ancestor(setter->value, GTK_TYPE_FRAME);
	menu = setter->menu->parent;

	switch (var_item->type) {
		case GERNEL_VAR_ITEM_TYPE_CHOICE:
			gtk_widget_hide(yes);
			gtk_widget_hide(module);
			gtk_widget_hide(no);
			gtk_widget_hide(value);
			gernel_setter_update_menu(setter, var_item);
			gtk_widget_show(menu);
			break;
		case GERNEL_VAR_ITEM_TYPE_DEP_MBOOL:
		case GERNEL_VAR_ITEM_TYPE_DEP_BOOL:
		case GERNEL_VAR_ITEM_TYPE_BOOL:
			gtk_widget_show(yes);
			gtk_widget_hide(module);
			gtk_widget_show(no);
			gtk_widget_hide(value);
			gtk_widget_hide(menu);
			break;
		case GERNEL_VAR_ITEM_TYPE_DEP_HEX:
		case GERNEL_VAR_ITEM_TYPE_STRING:
		case GERNEL_VAR_ITEM_TYPE_INT:
		case GERNEL_VAR_ITEM_TYPE_HEX:
			gtk_widget_hide(yes);
			gtk_widget_hide(module);
			gtk_widget_hide(no);
			gtk_widget_show(value);
			gtk_widget_hide(menu);
			break;
		case GERNEL_VAR_ITEM_TYPE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE:
			gtk_widget_show(yes);
			gtk_widget_show(module);
			gtk_widget_show(no);
			gtk_widget_hide(value);
			gtk_widget_hide(menu);
			break;
		default:
			gtk_widget_hide(yes);
			gtk_widget_hide(module);
			gtk_widget_hide(no);
			gtk_widget_hide(value);
			gtk_widget_hide(menu);
			break;
	};
}

static void gernel_setter_update_sensitivity(	GernelSetter *setter,
						GtkWidget *item	)
/*	Sensitize setter's widgets, depending on whether condition of var_item
	is met	*/
{
	gboolean	sensitivity;
	GtkWidget	*widget;
	GernelVarItem	*var_item;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(item));

	var_item = GERNEL_VAR_ITEM(item);

	sensitivity = gernel_var_item_meets_condition(var_item->condition);

	widget = setter->yes;
	gtk_widget_set_sensitive(widget, sensitivity?var_item->allow_yes:FALSE);
	widget = setter->module;
	gtk_widget_set_sensitive(widget, sensitivity?var_item->allow_mod:FALSE);
	widget = setter->no;
	gtk_widget_set_sensitive(widget, sensitivity);
	widget = setter->value;
	gtk_widget_set_sensitive(widget, sensitivity);
	widget = setter->menu;
	gtk_widget_set_sensitive(widget, sensitivity);
}

static void gernel_setter_update_menu(	GernelSetter *setter,
					GernelVarItem *var_item	)
/*	Update the menu of the setter from CHOICE variable var_item	*/
{
	GtkWidget	*menu;
	GSList		*entry;
	gint		i;
	GtkOptionMenu	*option_menu;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gtk_option_menu_remove_menu(GTK_OPTION_MENU(setter->menu));
	menu = gtk_menu_new();
	option_menu = GTK_OPTION_MENU(setter->menu);
	gtk_option_menu_set_menu(option_menu, menu);

	for (	entry = var_item->option_entries, i = 0; 
		entry; 
		entry = entry->next, i++	) {

		GernelVarItem	*this_var;
		GtkWidget	*item, *var;
		gchar		*descr;

		g_return_if_fail(entry != NULL);
		g_return_if_fail(entry->data != NULL);
		g_return_if_fail(GTK_IS_WIDGET(entry->data));
		g_return_if_fail(GERNEL_IS_VAR_ITEM(entry->data));

		this_var = GERNEL_VAR_ITEM(entry->data);
		var = GTK_WIDGET(entry->data);
		descr = gernel_var_item_get_description(this_var);
		item = gernel_var_menu_item_new_with_label(var, descr);
		GTK_WIDGET(this_var)->parent = GTK_WIDGET(var_item)->parent;

		gtk_signal_connect_object(	GTK_OBJECT(item),
						"activate",
						gernel_var_item_set_false_all,
						GTK_OBJECT(var_item)	);

		gtk_signal_connect_object(	GTK_OBJECT(item),
						"activate",
						gernel_var_item_set_true,
						GTK_OBJECT(this_var)	);

		gtk_signal_connect_object(	GTK_OBJECT(item),
						"activate",
						gernel_var_item_deps_sync_all,
						GTK_OBJECT(var_item)	);

		gtk_signal_connect_object(	GTK_OBJECT(item),
						"activate",
						gernel_var_item_icon_sync,
						GTK_OBJECT(var_item)	);

		gtk_signal_connect_object(	GTK_OBJECT(item),
						"current",
						gernel_setter_view_changed,
						GTK_OBJECT(setter)	);
						
		gtk_menu_append(GTK_MENU(menu), item);

		if (match(gernel_var_item_get_value(this_var), CONFIG_IN_YES))
			gtk_option_menu_set_history(option_menu, i);

		gtk_widget_show(item);
	}

	gtk_widget_show(menu);
}

static void gernel_setter_view_changed(	GernelSetter *setter,
					GernelVarItem *var_item	)
/*	Emit the 'view_changed' signal	*/
{
	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gtk_signal_emit(	GTK_OBJECT(setter),
				setter_signals[VIEW_CHANGED],
				gernel_var_item_get_description(var_item),
				gernel_var_item_get_name(var_item),
				gernel_var_item_get_text(var_item)	);
}

static void gernel_setter_connect_multiple(	GtkWidget *widget,
						GtkObject *obj,
						GtkSignalFunc func,
						...	)
/*	connect and add new handlers to widget	*/
{
	va_list		args;
	GtkObject	*subject, *object;
	GtkSignalFunc	runtime;

	va_start(args, func);
	subject = GTK_OBJECT(widget);
	object = GTK_OBJECT(obj);

	runtime = func;

	do {
		gtk_signal_connect_object(subject, "activate", runtime, object);
		runtime = va_arg(args, GtkSignalFunc);
	} while (runtime);

	va_end(args);
}

static void gernel_setter_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelSetter	*setter;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(object));

	setter = GERNEL_SETTER(object);

	switch (arg_id) {
		GtkWidget	*widget;
		gchar		*string;

		case ARG_LOCAL_NAME:
			string = gernel_setter_get_local_name(setter);
			GTK_VALUE_STRING(*arg) = string;
			break;
		case ARG_SHOW_SETTER:
			GTK_VALUE_BOOL(*arg) = GTK_WIDGET_VISIBLE(setter);
			break;
		case ARG_PREFERENCES:
			widget = gernel_setter_preferences(setter);
			GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(widget);
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void gernel_setter_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implemts argument setter for this object	*/
{
	GernelSetter	*setter;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(object));

	setter = GERNEL_SETTER(object);

	switch (arg_id) {
		gboolean	value;
		gchar		*string;

		case ARG_LOCAL_NAME:
			string = GTK_VALUE_STRING(*arg);
			gernel_setter_set_local_name(setter, string);
			break;
		case ARG_SHOW_SETTER:
			value = GTK_VALUE_BOOL(*arg);
			gernel_setter_show_setter(setter, value);
			break;
		case ARG_PREFERENCES:	/*	Fall Through	*/
		default:
			break;
	}
}

static GtkWidget* gernel_setter_preferences(	GernelSetter *setter	)
/*	Assemble a page to be used in a Gnome property box	*/
{
	GtkWidget	*page, *section;

	g_return_val_if_fail(setter != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_SETTER(setter), NULL);

	page = erty_preference_page_new();
	section = erty_preference_section_new(_("General"));

	erty_preference_section_add(
			ERTY_PREFERENCE_SECTION(section),
			GTK_WIDGET(setter),
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Show Setter"), 
			ARG_SHOW_SETTER
			);

	gtk_container_add(GTK_CONTAINER(page), section);

	return page;
}

static void gernel_setter_show_setter(	GernelSetter *setter,
					gboolean flag	)
/*	Show/hide setter according to flag	*/
{
	GtkWidget	*widget;

	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));

	widget = GTK_WIDGET(setter);

	if (flag)
		gtk_widget_show(widget);
	else
		gtk_widget_hide(widget);

	gtk_widget_queue_resize(widget->parent);
}

static void gernel_setter_set_local_name(	GernelSetter *setter,
						gchar *name	)
/*	Set the local name of setter	*/
{
	g_return_if_fail(setter != NULL);
	g_return_if_fail(GERNEL_IS_SETTER(setter));
	g_return_if_fail(name != NULL);

	setter->local_name = name;
}

static gchar* gernel_setter_get_local_name(	GernelSetter *setter	)
/*	Get the local name of setter	*/
{
	g_return_val_if_fail(setter != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_SETTER(setter), NULL);

	return setter->local_name;
}
