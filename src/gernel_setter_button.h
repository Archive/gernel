#define	GERNEL_TYPE_SETTER_BUTTON	(gernel_setter_button_get_type())

#define	GERNEL_SETTER_BUTTON(obj) \
	(GTK_CHECK_CAST(	(obj), \
				GERNEL_TYPE_SETTER_BUTTON, \
				GernelSetterButton	))

#define	GERNEL_SETTER_BUTTON_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_SETTER_BUTTON, \
				GernelSetterButton	))

#define	GERNEL_IS_SETTER_BUTTON(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_SETTER_BUTTON))

#define GERNEL_IS_SETTER_BUTTON_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass, GERNEL_TYPE_SETTER_BUTTON))

typedef struct _GernelSetterButton	GernelSetterButton;
typedef struct _GernelSetterButtonClass	GernelSetterButtonClass;

struct _GernelSetterButton {
	GtkRadioButton	radio_button;
};

struct _GernelSetterButtonClass {
	GtkRadioButtonClass	parent_class;

	void (*activate)	(	GernelSetterButton *setter_button );
	void (*deactivate)	(	GernelSetterButton *setter_button );
};

GtkType		gernel_setter_button_get_type(	void	);
GtkWidget*	gernel_setter_button_new(	void	);

void		gernel_setter_button_set_group(
		GernelSetterButton *setter_button,
		GSList *group
		);
