#include <gernel.h>
#include <gernel_setter_button.h>

static void gernel_setter_button_init(	GernelSetterButton *setter_button );

static void gernel_setter_button_class_init(
		GernelSetterButtonClass *klass
		);

static void gernel_setter_button_toggled(	GtkToggleButton *setter_button);

static void gernel_setter_button_activate(
		GernelSetterButton *setter_button
		);

static void gernel_setter_button_deactivate(
		GernelSetterButton *setter_button
		);

static GtkRadioButtonClass *parent_class = NULL;

enum {	ACTIVATE,
	DEACTIVATE,
	LAST_SIGNAL	};

static guint setter_button_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_setter_button_get_type(	void	)
/*	Return type identifier for type GernelSetterButton	*/
{
	static GtkType	setter_button_type = 0 ;

	if (!setter_button_type) {

		static const GtkTypeInfo	setter_button_info = {
			"GernelSetterButton",
			sizeof(GernelSetterButton),
			sizeof(GernelSetterButtonClass),
			(GtkClassInitFunc)gernel_setter_button_class_init,
			(GtkObjectInitFunc)gernel_setter_button_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		setter_button_type = gtk_type_unique(
				gtk_radio_button_get_type(), 
				&setter_button_info
				);
	}

	return setter_button_type;
}

GtkWidget* gernel_setter_button_new(	void	)
/*	Create an instance of GernelSetterButton	*/
{
	GernelSetterButton	*setter_button;

	setter_button = gtk_type_new(gernel_setter_button_get_type());

	return GTK_WIDGET(setter_button);
}

static void gernel_setter_button_init(	GernelSetterButton *setter_button )
/*	Initialize the GernelSetterButton	*/
{
}

static void gernel_setter_button_class_init(	GernelSetterButtonClass *klass )
/*	Initialize the GernelSetterButtonClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkButtonClass		*button_class;
	GtkToggleButtonClass	*toggle_button_class;
	GtkCheckButtonClass	*check_button_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	button_class = (GtkButtonClass*)klass;
	toggle_button_class = (GtkToggleButtonClass*)klass;
	check_button_class = (GtkCheckButtonClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_RADIO_BUTTON);

	function_offset = GTK_SIGNAL_OFFSET(GernelSetterButtonClass, activate);

	setter_button_signals[ACTIVATE] =

			gtk_signal_new(	"activate",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelSetterButtonClass, 
						deactivate	);

	setter_button_signals[DEACTIVATE] =

			gtk_signal_new(	"deactivate",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class, 
					setter_button_signals, 
					LAST_SIGNAL	);

	toggle_button_class->toggled = gernel_setter_button_toggled;

	klass->activate = NULL;
	klass->deactivate = NULL;
}

void gernel_setter_button_set_group(	GernelSetterButton *setter_button,
					GSList *group	)
/*	Set the group setter_button belongs to	*/
{
	g_return_if_fail(setter_button != NULL);
	g_return_if_fail(GERNEL_IS_SETTER_BUTTON(setter_button));
	g_return_if_fail(group != NULL);

	gtk_radio_button_set_group(GTK_RADIO_BUTTON(setter_button), group);
}

static void gernel_setter_button_toggled(	GtkToggleButton *toggle_button )
/*	Emit the 'toggled' signal	*/
{
	GernelSetterButton	*setter_button;

	g_return_if_fail(toggle_button != NULL);
	g_return_if_fail(GERNEL_IS_SETTER_BUTTON(toggle_button));

	setter_button = GERNEL_SETTER_BUTTON(toggle_button);

	if (gtk_toggle_button_get_active(toggle_button))
		gernel_setter_button_activate(setter_button);
	else
		gernel_setter_button_deactivate(setter_button);
}

static void gernel_setter_button_activate(
		GernelSetterButton *setter_button
		)
/*	Emit the 'activate' signal	*/
{
	g_return_if_fail(setter_button != NULL);
	g_return_if_fail(GERNEL_IS_SETTER_BUTTON(setter_button));


	gtk_signal_emit(	GTK_OBJECT(setter_button),
				setter_button_signals[ACTIVATE]	);
}

static void gernel_setter_button_deactivate(
		GernelSetterButton *setter_button
		)
/*	Emit the 'deactivate' signal	*/
{
	g_return_if_fail(setter_button != NULL);
	g_return_if_fail(GERNEL_IS_SETTER_BUTTON(setter_button));

	gtk_signal_emit(	GTK_OBJECT(setter_button),
				setter_button_signals[DEACTIVATE]	);
}
