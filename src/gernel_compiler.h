#define	GERNEL_TYPE_COMPILER	(gernel_compiler_get_type())

#define	GERNEL_COMPILER(obj) \
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_COMPILER, GernelCompiler))

#define GERNEL_COMPILER_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass), GERNEL_TYPE_COMPILER, GernelCompiler))

#define GERNEL_IS_COMPILER(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_COMPILER))

#define GERNEL_IS_COMPILER_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass, GERNEL_TYPE_COMPILER))

typedef struct _GernelCompiler		GernelCompiler;
typedef struct _GernelCompilerClass	GernelCompilerClass;

struct _GernelCompiler {
	GnomeDialog	dialog;

	gchar		*local_name;
	GtkWidget	*password;
};

struct _GernelCompilerClass {
	GnomeDialogClass	parent_class;

	void (*configure)	(	GernelCompiler *compiler	);
};

GtkType		gernel_compiler_get_type(	void	);
GtkWidget*	gernel_compiler_new(	gchar *name	);
