#include <gernel.h>
#include <gernel_var_menu_item.h>
#include <gernel_var_item.h>

static void gernel_var_menu_item_init(	GernelVarMenuItem *var_menu_item );
static void gernel_var_menu_item_class_init(	GernelVarMenuItemClass *klass );

static void gernel_var_menu_item_current(
		GernelVarMenuItem *var_menu_item,
		GernelVarItem *var_item
		);

static void gernel_var_menu_item_set_label(
					GernelVarMenuItem *var_menu_item,
					gchar *label
					);

static GtkMenuItemClass	*parent_class = NULL;

enum {	CURRENT,
	LAST_SIGNAL	};

static guint var_menu_item_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_var_menu_item_get_type(	void	)
/*	Retrun type identifier for type GernelVarMenuItem	*/
{
	static GtkType	var_menu_item_type = 0;

	if (!var_menu_item_type) {

		static const GtkTypeInfo	var_menu_item_info = {
			"GernelVarMenuItem",
			sizeof(GernelVarMenuItem),
			sizeof(GernelVarMenuItemClass),
			(GtkClassInitFunc)gernel_var_menu_item_class_init,
			(GtkObjectInitFunc)gernel_var_menu_item_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		var_menu_item_type = gtk_type_unique(
				gtk_menu_item_get_type(),
				&var_menu_item_info
				);
	}

	return var_menu_item_type;
}

GtkWidget* gernel_var_menu_item_new(	GtkWidget *var_item	)
/*	Create an instance of GernelVarMenuItem	*/
{
	GernelVarMenuItem	*var_menu_item;

	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	var_menu_item = gtk_type_new(gernel_var_menu_item_get_type());
	var_menu_item->var_item = var_item;

	gtk_signal_connect(	GTK_OBJECT(var_menu_item),
				"activate",
				gernel_var_menu_item_current,
				var_item	);

	return GTK_WIDGET(var_menu_item);
}

GtkWidget* gernel_var_menu_item_new_with_label(	GtkWidget *var_item,
						gchar *label	)
/*	Create a GernelVarMenuItem with a label	*/
{
	GtkWidget	*var_menu_item;

	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(label != NULL, NULL);

	var_menu_item = gernel_var_menu_item_new(var_item);

	gernel_var_menu_item_set_label(	GERNEL_VAR_MENU_ITEM(var_menu_item), 
					label	);

	return var_menu_item;
}

static void gernel_var_menu_item_set_label(
					GernelVarMenuItem *var_menu_item,
					gchar *label
					)
/*	Set label of var_menu_item	*/
{
	GtkWidget	*accel_label;

	g_return_if_fail(var_menu_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_MENU_ITEM(var_menu_item));
	g_return_if_fail(label != NULL);

	accel_label = gtk_accel_label_new(label);
	gtk_misc_set_alignment(GTK_MISC(accel_label), 0.0, 0.5);

	gtk_container_add(GTK_CONTAINER(var_menu_item), accel_label);

	gtk_accel_label_set_accel_widget(	GTK_ACCEL_LABEL(accel_label), 
						GTK_WIDGET(var_menu_item) );

	gtk_widget_show(accel_label);
}

static void gernel_var_menu_item_class_init(
		GernelVarMenuItemClass *klass
		)
/*	Initialize the GernelVarMenuItemClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkItemClass		*item_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)bin_class;
	item_class = (GtkItemClass*)item_class;

	parent_class = gtk_type_class(GTK_TYPE_MENU_ITEM);

	function_offset = GTK_SIGNAL_OFFSET(GernelVarMenuItemClass, current);

	var_menu_item_signals[CURRENT] = 

		gtk_signal_new(	"current",
				GTK_RUN_FIRST,
				object_class->type,
				function_offset,
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE,
				1,
				GTK_TYPE_POINTER	);

	gtk_object_class_add_signals(	object_class,
					var_menu_item_signals,
					LAST_SIGNAL	);

	klass->current = NULL;
}

static void gernel_var_menu_item_init(	GernelVarMenuItem *var_menu_item )
/*	Initialize var_menu_Item	*/
{
	var_menu_item->var_item = NULL;
}

static void gernel_var_menu_item_current(
		GernelVarMenuItem *var_menu_item,
		GernelVarItem *var_item
		)
/*	Emit the 'current' signal	*/
{
	g_return_if_fail(var_menu_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_MENU_ITEM(var_menu_item));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gtk_signal_emit(	GTK_OBJECT(var_menu_item),
				var_menu_item_signals[CURRENT],
				var_item	);
}
