#define GERNEL_TYPE_PANED	(gernel_paned_get_type())

#define GERNEL_PANED(obj)	\
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_PANED, GernelPaned))

#define GERNEL_PANED_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_CHECK_PANED, \
				GernelPanedClass)	)

#define GERNEL_IS_PANED(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_PANED))

#define GERNEL_IS_PANED_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_PANED))

typedef struct _GernelPaned		GernelPaned;
typedef struct _GernelPanedClass	GernelPanedClass;

struct _GernelPaned {
	GtkHPaned	hpaned;

	gint		handle_position;
};

struct _GernelPanedClass {
	GtkHPanedClass	parent_class;
};

GtkType		gernel_paned_get_type(	void	);
GtkWidget*	gernel_paned_new(	void	);

void		gernel_paned_set_position(	GernelPaned *paned,
						gint handle_position	);

void		gernel_paned_set_position_right(	GernelPaned *paned );
