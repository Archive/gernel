#define GERNEL_TYPE_DOCK_ITEM	(gernel_dock_item_get_type())

#define GERNEL_DOCK_ITEM(obj)	(GTK_CHECK_CAST(	obj, \
							GERNEL_TYPE_DOCK_ITEM, \
							GernelDockItem	))

#define GERNEL_DOCK_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST( \
	(klass), GERNEL_TYPE_DOCK_ITEM, GernelDockItemClass)\
	)

#define GERNEL_IS_DOCK_ITEM(obj) (GTK_CHECK_TYPE((obj), GERNEL_TYPE_DOCK_ITEM))

#define GERNEL_IS_DOCK_ITEM_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_DOCK_ITEM))

typedef struct _GernelDockItem		GernelDockItem;
typedef struct _GernelDockItemClass	GernelDockItemClass;

struct _GernelDockItem {
	GnomeDockItem	dock;

	GtkWidget	*scrolled_window;
	gboolean	floating;
};

struct _GernelDockItemClass {
	GnomeDockItemClass	parent_class;

	void (*attached)	(	GernelDockItem *dock_item	);
	void (*detached)	(	GernelDockItem *dock_item	);
	void (*double_click)	(	GernelDockItem *dock_item	);
};

GtkType		gernel_dock_item_get_type(	void	);
GtkWidget*	gernel_dock_item_new(	void	);

void		gernel_dock_item_set_detachable(GernelDockItem *dock_item,
						gboolean flag	);

void		gernel_dock_item_set_hscrolling(GernelDockItem *dock_item,
						GtkPolicyType policy_type );

void		gernel_dock_item_set_vscrolling(GernelDockItem *dock_item,
						GtkPolicyType policy_type );

GtkWidget*	gernel_dock_item_get_child(	GernelDockItem *dock_item );
