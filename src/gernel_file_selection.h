#define	GERNEL_TYPE_FILE_SELECTION	(gernel_file_selection_get_type())

#define	GERNEL_FILE_SELECTION(obj) \
	GTK_CHECK_CAST(obj, GERNEL_TYPE_FILE_SELECTION, GernelFileSelection)

#define	GERNEL_FILE_SELECTION_CLASS(klass) \
	GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_FILE_SELECTION, \
				GernelFileSelectionClass	)

#define GERNEL_IS_FILE_SELECTION(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_FILE_SELECTION))

#define GERNEL_IS_FILE_SELECTION_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE(klass, GERNEL_TYPE_FILE_SELECTION)

typedef struct _GernelFileSelection		GernelFileSelection;
typedef struct _GernelFileSelectionClass	GernelFileSelectionClass;

struct _GernelFileSelection {
	GtkFileSelection	file_selection;

	gchar			*local_name;
};

struct _GernelFileSelectionClass {
	GtkFileSelectionClass	parent_class;

	void (*ok)		(	GernelFileSelection *file_selection,
					gchar *path	);
};

GtkType		gernel_file_selection_get_type(	void	);
GtkWidget*	gernel_file_selection_new(	gchar *local_name	);
