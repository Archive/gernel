#include <gernel.h>
#include <gernel_search.h>
#include <gernel_search_menu.h>
#include <gernel_list.h>
#include <gernel_tree.h>
#include <gernel_var_item.h>

enum GernelSearchMenuType {	GERNEL_SEARCH_MENU_TYPE_0,
				GERNEL_SEARCH_MENU_TYPE_ATTRIBUTES,
				GERNEL_SEARCH_MENU_TYPE_SORT_ORDER,
				GERNEL_SEARCH_MENU_TYPE_MODES	};

enum GernelSearchCheckType {	GERNEL_SEARCH_CHECK_TYPE_0,
				GERNEL_SEARCH_CHECK_TYPE_CASE,
				GERNEL_SEARCH_CHECK_TYPE_PHRASE	};

enum GernelSearchCase {	GERNEL_SEARCH_CASE_0,
			GERNEL_SEARCH_CASE_SENSITIVE,
			GERNEL_SEARCH_CASE_INSENSITIVE	};

enum GernelSearchPhrase {	GERNEL_SEARCH_PHRASE_0,
				GERNEL_SEARCH_PHRASE_SINGLE,
				GERNEL_SEARCH_PHRASE_MULTIPLE	};

enum GernelSearchSortOrder {	GERNEL_SEARCH_SORT_ORDER_0,
				GERNEL_SEARCH_SORT_ORDER_ASCENDING,
				GERNEL_SEARCH_SORT_ORDER_DESCENDING	};

enum GernelSearchAttribute {	GERNEL_SEARCH_ATTRIBUTE_0,
				GERNEL_SEARCH_ATTRIBUTE_NAME,
				GERNEL_SEARCH_ATTRIBUTE_DESCR,
				GERNEL_SEARCH_ATTRIBUTE_TEXT,
				GERNEL_SEARCH_ATTRIBUTE_SEPARATOR,
				GERNEL_SEARCH_ATTRIBUTE_ALL	};

enum GernelSearchMode {	GERNEL_SEARCH_MODE_0,
			GERNEL_SEARCH_MODE_CONTAINS,
			GERNEL_SEARCH_MODE_MATCHES,
			GERNEL_SEARCH_MODE_STARTS,
			GERNEL_SEARCH_MODE_ENDS	};

enum GernelSearchLabelAlignment {	GERNEL_SEARCH_LABEL_LEFT,
					GERNEL_SEARCH_LABEL_RIGHT	};

static void gernel_search_set_local_name(	GernelSearch *search,
						gchar *name	);

static gchar* gernel_search_get_local_name(	GernelSearch *search	);
static void	gernel_search_init(	GernelSearch *search	);
static void	gernel_search_class_init(	GernelSearchClass *klass );
static void	gernel_search_construct(	GernelSearch *search	);
static GtkWidget* gernel_search_preferences(	GernelSearch*search	);
static void	gernel_search_configure(	GernelSearch *search	);

static void	gernel_search_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void	gernel_search_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_search_remove_duplicates(	GernelSearch *search	);

static GSList* gernel_search_foreach(	GernelSearch *search,
					GernelTree *tree,
					GernelVarItemSearchFunc function, 
					gpointer data	);

static void gernel_search_show_results(	GernelSearch *search	);

static void gernel_search_construct_list(	GernelSearch *search,
						GtkWidget *container	);

static void gernel_search_for_word(	GernelSearch *search,
					gchar *keyword,
					enum GernelSearchMode mode,
					enum GernelSearchCase sensitivity );

static void gernel_search_real_search(	GernelSearch *search,
					GernelVarItemSearchFunc function,
					gchar *pattern	);

static void gernel_search_show_descriptions(	GernelSearch *search,
						gboolean show	);

static void gernel_search_show_names(	GernelSearch *search,
					gboolean show	);

static void gernel_search_set_sort_order(	GernelSearch *search,
						GtkWidget *menu	);

static void gernel_search_construct_entry(	GernelSearch *search,
						GtkWidget *container	);

static void gernel_search_construct_label(	GernelSearch *search,
						GtkWidget *container,
						gchar *string	);

static void gernel_search_construct_menu(
		GernelSearch *search,
		GtkWidget *container,
		enum GernelSearchMenuType type,
		guint arg,
		gchar *label,
		...
		);

static void gernel_search_construct_separator(	GernelSearch *search,
						GtkWidget *container	);

static void gernel_search_start(	GernelSearch *search	);

static void gernel_search_names(	GernelSearch *search,
					gchar *keyword,
					enum GernelSearchMode search_mode,
					enum GernelSearchCase sensitivity );

static void gernel_search_construct_check_button(
		GernelSearch *search, 
		GtkWidget *container,
		enum GernelSearchCheckType check_type,
		gchar *label
		);

static void gernel_search_descriptions(	GernelSearch *search,
					gchar *keyword,
					enum GernelSearchMode search_mode,
					enum GernelSearchCase sensitivity );

static void gernel_search_texts(	GernelSearch *search,
					gchar *keyword,
					enum GernelSearchMode search_mode,
					enum GernelSearchCase sensitivity );

static enum GernelSearchCase gernel_search_get_case(	GernelSearch *search );
static enum GernelSearchCase gernel_search_get_phrase(	GernelSearch *search );
static enum GernelSearchMode gernel_search_get_mode(	GernelSearch *search );

static enum GernelSearchAttribute gernel_search_get_attribute(
		GernelSearch *search
		);

static gchar* gernel_search_get_keyword(	GernelSearch *search	);

static GnomeDialogClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_LOCAL_NAME,
	ARG_SHOW_DESCRIPTIONS,
	ARG_SHOW_NAMES,
	ARG_PREFERENCES	};

enum {	CONFIGURE,
	LAST_SIGNAL	};

static guint search_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_search_get_type(	void	)
/*	Return type identifier for type GernelSearch	*/
{
	static GtkType	search_type = 0;

	if (!search_type) {

		static const GtkTypeInfo	search_info = {
				"GernelSearch",
				sizeof(GernelSearch),
				sizeof(GernelSearchClass),
				(GtkClassInitFunc)gernel_search_class_init,
				(GtkObjectInitFunc)gernel_search_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		search_type = gtk_type_unique(	GNOME_TYPE_DIALOG, 
						&search_info	);
	}

	return search_type;
}

GtkWidget* gernel_search_new(	gchar *name	)
/*	Create an instance of GernelSearch	*/
{
	GernelSearch	*search;
	GtkWidget	*widget;

	g_return_val_if_fail(name != NULL, NULL);

	widget = erty_widget_new(	gernel_search_get_type(),
					name,
					"show_descriptions", TRUE,
					"show_names", FALSE,
					NULL	);

	search = GERNEL_SEARCH(widget);
	gernel_search_set_local_name(search, name);
	gernel_search_construct(search);

	return widget;
}

GtkWidget* gernel_search_new_with_tree(	gchar *name,
					GtkWidget *tree	)
/*	Creates an instance of GernelSearch complete with tree to search*/
{
	GtkWidget	*search;

	g_return_val_if_fail(name != NULL, NULL);
	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);

	search = gernel_search_new(name);
	gernel_search_set_tree(GERNEL_SEARCH(search), tree);

	return search;
}

void gernel_search_set_tree(	GernelSearch *search,
				GtkWidget *tree	)
/*	Sets tree field of search	*/
{
	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	search->tree = tree;
}

static void gernel_search_init(	GernelSearch *search	)
/*	Initialize a GernelSearch dialog	*/
{
	search->tree = NULL;
	search->attribute_menu = NULL;
	search->mode_menu = NULL;
	search->case_check = NULL;
	search->phrase_check = NULL;
	search->sort_order_menu = NULL;
	search->show_descriptions = TRUE;
	search->show_names = TRUE;
}

static void gernel_search_class_init(	GernelSearchClass *klass	)
/*	Initialize the GernelSearch class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkWindowClass		*window_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	window_class = (GtkWindowClass*)klass;

	parent_class = gtk_type_class(GNOME_TYPE_DIALOG);

	gtk_object_add_arg_type("GernelSearch::preferences",
				GTK_TYPE_WIDGET,
				GTK_ARG_READABLE,
				ARG_PREFERENCES	);

	gtk_object_add_arg_type("GernelSearch::local_name",
				GTK_TYPE_STRING,
				GTK_ARG_READWRITE,
				ARG_LOCAL_NAME	);

	erty_arg_add_multiple(
		"GernelSearch",
		"show_descriptions", GTK_TYPE_BOOL, ARG_SHOW_DESCRIPTIONS,
		"show_names", GTK_TYPE_BOOL, ARG_SHOW_NAMES,
		NULL
		);

	object_class->get_arg = gernel_search_get_arg;
	object_class->set_arg = gernel_search_set_arg;

	function_offset = GTK_SIGNAL_OFFSET(GernelSearchClass, configure);

	search_signals[CONFIGURE] = gtk_signal_new(	"configure",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	gtk_object_class_add_signals(object_class, search_signals, LAST_SIGNAL);

	klass->configure = NULL;
}

static void gernel_search_construct(	GernelSearch *search	)
/*	Construct the GernelSearch dialog	*/
{
	GnomeDialog	*dialog;
	GtkWidget	*hbox, *vbox;
	gchar		*tmp;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));

	tmp = g_strconcat(PROGRAM, ": ", _("Search Dialog"), NULL);
	gtk_window_set_title(GTK_WINDOW(search), tmp);
	g_free(tmp);

	dialog = GNOME_DIALOG(search);

	gnome_dialog_append_buttons(	dialog, 
					GNOME_STOCK_PIXMAP_SEARCH, 
					GNOME_STOCK_PIXMAP_PROPERTIES,
					NULL	);

	gnome_dialog_append_button_with_pixmap(	dialog,
						_("Hide"),
						GNOME_STOCK_BUTTON_CLOSE );

	gnome_dialog_append_button(dialog, GNOME_STOCK_BUTTON_HELP);
	gnome_dialog_set_default(dialog, 0);
	gnome_dialog_close_hides(dialog, TRUE);

	gnome_dialog_button_connect_object(	dialog,
						0,
						gernel_search_start,
						(gpointer)search	);

	gnome_dialog_button_connect_object(	dialog,
						1,
						gernel_search_configure,
						(gpointer)search	);

	gnome_dialog_button_connect_object(	dialog, 
						2, 
						gnome_dialog_close, 
						(gpointer)dialog	);

	gernel_search_construct_label(
			search,
			dialog->vbox,
			_("Search for variables with following specification:")
			);

	vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_add(GTK_CONTAINER(dialog->vbox), vbox);
	gtk_widget_show(vbox);

	hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_add(GTK_CONTAINER(vbox), hbox);
	gtk_widget_show(hbox);

	gernel_search_construct_menu(
			search,
			hbox,
			GERNEL_SEARCH_MENU_TYPE_ATTRIBUTES, 
			GERNEL_SEARCH_ATTRIBUTE_NAME, _("Name"),
			GERNEL_SEARCH_ATTRIBUTE_DESCR, _("Description"),
			GERNEL_SEARCH_ATTRIBUTE_TEXT, _("Help Text"),
			GERNEL_SEARCH_ATTRIBUTE_SEPARATOR, NULL,
			GERNEL_SEARCH_ATTRIBUTE_ALL, _("All Fields"),
			GERNEL_SEARCH_ATTRIBUTE_0
			);

	gernel_search_construct_menu(
			search,
			hbox,
			GERNEL_SEARCH_MENU_TYPE_MODES,
			GERNEL_SEARCH_MODE_CONTAINS, _("contains"),
			GERNEL_SEARCH_MODE_STARTS, _("starts with"),
			GERNEL_SEARCH_MODE_ENDS, _("ends in"),
			GERNEL_SEARCH_MODE_MATCHES, _("matches"),
			GERNEL_SEARCH_MODE_0
			);

	gernel_search_construct_entry(search, hbox);

	gernel_search_construct_check_button(
			search, 
			hbox, 
			GERNEL_SEARCH_CHECK_TYPE_CASE,
			_("Case Sensitive")
			);

	gernel_search_construct_check_button(	search,
						hbox,
						GERNEL_SEARCH_CHECK_TYPE_PHRASE,
						_("Interpret as Phrase") );

	gernel_search_construct_separator(search, dialog->vbox);

	hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_add(GTK_CONTAINER(dialog->vbox), hbox);
	gtk_widget_show(hbox);

	gernel_search_construct_label(search, hbox, _("Sort results in"));

	gernel_search_construct_menu(
			search,
			hbox,
			GERNEL_SEARCH_MENU_TYPE_SORT_ORDER,
			GERNEL_SEARCH_SORT_ORDER_ASCENDING, _("ascending"),
			GERNEL_SEARCH_SORT_ORDER_DESCENDING, _("descending"),
			GERNEL_SEARCH_SORT_ORDER_0
			);

	gernel_search_construct_label(search, hbox, _("order."));
	gernel_search_construct_list(search, dialog->vbox);
}

static void gernel_search_construct_separator(	GernelSearch *search,
						GtkWidget *container	)
/*	Add a separator to container	*/
{
	GtkWidget	*separator;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(container != NULL);
	g_return_if_fail(GTK_IS_CONTAINER(container));

	separator = gtk_hseparator_new();
	gtk_container_add(GTK_CONTAINER(container), separator);
	gtk_widget_show(separator);
}

static void gernel_search_construct_list(	GernelSearch *search,
						GtkWidget *container	)
/*	Construct a list widget for the search results	*/
{
	GtkWidget	*list;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(container != NULL);
	g_return_if_fail(GTK_IS_BOX(container));

	list = gernel_list_new();
	search->list = list;
	gtk_container_add(GTK_CONTAINER(container), list);
	gtk_widget_show(list);
}

static void gernel_search_construct_check_button(
		GernelSearch *search, 
		GtkWidget *container,
		enum GernelSearchCheckType check_type,
		gchar *label
		)
/*	Construct a check button	*/
{
	GtkWidget	*check_button;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(container != NULL);
	g_return_if_fail(GTK_IS_CONTAINER(container));

	check_button = gtk_check_button_new_with_label(label);

	switch (check_type) {
		case GERNEL_SEARCH_CHECK_TYPE_CASE:
			search->case_check = check_button;
			break;
		case GERNEL_SEARCH_CHECK_TYPE_PHRASE:
			search->phrase_check = check_button;
			break;
		default:
			g_assert_not_reached();
			break;
	}

	gtk_container_add(GTK_CONTAINER(container), check_button);
	gtk_widget_show(check_button);
}

static void gernel_search_construct_entry(	GernelSearch *search,
						GtkWidget *container	)
/*	Construct an entry for user input	*/
{
	GtkWidget	*entry, *editable;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(container != NULL);
	g_return_if_fail(GTK_IS_CONTAINER(container));

	entry = gnome_entry_new("FIXME");
	search->entry = entry;
	editable = gnome_entry_gtk_entry(GNOME_ENTRY(entry));

	gtk_signal_connect_object(	GTK_OBJECT(editable),
					"activate",
					gernel_search_start,
					GTK_OBJECT(search)	);

	gtk_container_add(GTK_CONTAINER(container), entry);
	gtk_widget_show(entry);
}

static void gernel_search_construct_label(	GernelSearch *search,
						GtkWidget *container,
						gchar *string	)
/*	Construct a label to be shown in the search dialog	*/
{
	GtkWidget	*label;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(container != NULL);
	g_return_if_fail(GTK_IS_CONTAINER(container));
	g_return_if_fail(string != NULL);

	label = gtk_label_new(string);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
/*	gtk_container_add(GTK_CONTAINER(container), label);	*/
	gtk_box_pack_start(GTK_BOX(container), label, FALSE, FALSE, 0);
	gtk_widget_show(label);
}

static void gernel_search_construct_menu(
		GernelSearch *search,
		GtkWidget *container,
		enum GernelSearchMenuType type,
		guint arg,
		gchar *label,
		...
		)
/*	Construct a menu showing labels	*/
{
	va_list			args;
	GtkWidget		*menu;
	GernelSearchMenu	*search_menu;
	gchar			*string;
	guint			flag;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(label != NULL);
	g_return_if_fail(container != NULL);
	g_return_if_fail(GTK_IS_CONTAINER(container));
	g_return_if_fail(type != GERNEL_SEARCH_MENU_TYPE_0);

	menu = gernel_search_menu_new();

	switch (type) {
		GtkSignalFunc	function;

		case GERNEL_SEARCH_MENU_TYPE_ATTRIBUTES:
			search->attribute_menu = menu;
			break;
		case GERNEL_SEARCH_MENU_TYPE_MODES:
			search->mode_menu = menu;
			break;
		case GERNEL_SEARCH_MENU_TYPE_SORT_ORDER:
			search->sort_order_menu = menu;
			function = gernel_search_set_sort_order;

			gtk_signal_connect_object(	GTK_OBJECT(menu),
							"selection_changed",
							function,
							GTK_OBJECT(search) );
			break;
		default:
			g_assert_not_reached();
			break;
	}

	search_menu = GERNEL_SEARCH_MENU(menu);

	va_start(args, label);
	flag = arg;
	string = label;

	while (flag != 0) {
		gernel_search_menu_add_from_data(search_menu, string, flag);
		flag = va_arg(args, guint);
		if (flag) string = va_arg(args, gchar*);
	}

	va_end(args);
	gtk_box_pack_start(GTK_BOX(container), menu, FALSE, FALSE, 0);
	gtk_widget_show(menu);
}

static void gernel_search_configure(	GernelSearch *search	)
/*	Emit the 'configure' signal; used to pop up the main property dialog
	and show the search page directly	*/
{
	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));

	gtk_signal_emit(GTK_OBJECT(search), search_signals[CONFIGURE]);
}

static void gernel_search_start(	GernelSearch *search	)
/*	Proceed with the actual search	*/
{
	enum GernelSearchMode		mode;
	enum GernelSearchCase		sensitivity;
	enum GernelSearchPhrase		phrase;
	gchar				*keyword;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(search->tree != NULL);

	gernel_list_clear(GERNEL_LIST(search->list));
	gernel_list_set_tree(GERNEL_LIST(search->list), search->tree);
	mode = gernel_search_get_mode(search);
	sensitivity = gernel_search_get_case(search);
	phrase = gernel_search_get_phrase(search);
	keyword = gernel_search_get_keyword(search);

	switch (phrase) {
		gchar	**words;
		gint	i;

		case GERNEL_SEARCH_PHRASE_SINGLE:

			gernel_search_for_word(	search, 
						keyword, 
						mode, 
						sensitivity	);

			break;
		case GERNEL_SEARCH_PHRASE_MULTIPLE:
			words = g_strsplit(keyword, " ", 0);

			for (i = 0; words[i] != NULL; i++)

				if (strlen(words[i]) > 0)

					gernel_search_for_word(	search,
								words[i],
								mode,
								sensitivity );

			break;
		default:
			g_assert_not_reached();
	}
}

static void gernel_search_for_word(	GernelSearch *search,
					gchar *word,
					enum GernelSearchMode mode,
					enum GernelSearchCase sensitive	)
/*	Search for a single word	*/
{
	enum GernelSearchAttribute	attribute;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(word != NULL);

	attribute = gernel_search_get_attribute(search);

	switch (attribute ) {
		case GERNEL_SEARCH_ATTRIBUTE_NAME:
			gernel_search_names(search, word, mode, sensitive);
			break;
		case GERNEL_SEARCH_ATTRIBUTE_DESCR:

			gernel_search_descriptions(	search, 
							word, 
							mode, 
							sensitive	);

			break;
		case GERNEL_SEARCH_ATTRIBUTE_TEXT:
			gernel_search_texts(search, word, mode, sensitive);
			break;
		case GERNEL_SEARCH_ATTRIBUTE_ALL:
			gernel_search_names(search, word, mode, sensitive);

			gernel_search_descriptions(	search, 
							word, 
							mode, 
							sensitive	);

			gernel_search_texts(search, word, mode, sensitive);
			gernel_search_remove_duplicates(search);
			break;
		default:
			g_assert_not_reached();
	}
}

static void gernel_search_names(	GernelSearch *search,
					gchar *keyword,
					enum GernelSearchMode search_mode,
					enum GernelSearchCase sensitivity )
/*	Search var_items' names	*/
{
	GernelVarItemSearchFunc	func;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(keyword != NULL);

	func = NULL;

	switch (search_mode) {
		case GERNEL_SEARCH_MODE_CONTAINS:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_contains_name_sensitive:
				gernel_var_item_compare_contains_name;

			break;
		case GERNEL_SEARCH_MODE_MATCHES:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_equals_name_sensitive:
				gernel_var_item_compare_equals_name;

			break;
		case GERNEL_SEARCH_MODE_STARTS:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_starts_name_sensitive:
				gernel_var_item_compare_starts_name;

			break;
		case GERNEL_SEARCH_MODE_ENDS:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_ends_name_sensitive:
				gernel_var_item_compare_ends_name;

			break;
		default:
			g_assert_not_reached();
			break;
	}

	gernel_search_real_search(search, func, keyword);
}

static void gernel_search_descriptions(	GernelSearch *search,
					gchar *keyword,
					enum GernelSearchMode search_mode,
					enum GernelSearchCase sensitivity )
/*	Search var_items' short descriptions	*/
{
	GernelTree		*tree;
	GernelVarItemSearchFunc	func;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(keyword != NULL);

	tree = GERNEL_TREE(search->tree);

	switch (search_mode) {
		case GERNEL_SEARCH_MODE_CONTAINS:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
			gernel_var_item_compare_contains_description_sensitive:
			gernel_var_item_compare_contains_description;

			break;
		case GERNEL_SEARCH_MODE_MATCHES:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
			gernel_var_item_compare_equals_description_sensitive:
			gernel_var_item_compare_equals_description;

			break;
		case GERNEL_SEARCH_MODE_STARTS:
			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
			gernel_var_item_compare_starts_description_sensitive:
			gernel_var_item_compare_starts_description;

			break;
		case GERNEL_SEARCH_MODE_ENDS:
			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
			gernel_var_item_compare_ends_description_sensitive:
			gernel_var_item_compare_ends_description;

			break;
		default:
			func = NULL;
			g_assert_not_reached();
			break;
	}

	gernel_search_real_search(search, func, keyword);
}

static void gernel_search_texts(	GernelSearch *search,
					gchar *keyword,
					enum GernelSearchMode search_mode,
					enum GernelSearchCase sensitivity )
/*	Search var_items' help texts	*/
{
	GernelTree		*tree;
	GernelVarItemSearchFunc	func;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(keyword != NULL);

	tree = GERNEL_TREE(search->tree);

	switch (search_mode) {
		case GERNEL_SEARCH_MODE_CONTAINS:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_contains_text_sensitive:
				gernel_var_item_compare_contains_text;

			break;
		case GERNEL_SEARCH_MODE_MATCHES:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_equals_text_sensitive:
				gernel_var_item_compare_equals_text;

			break;
		case GERNEL_SEARCH_MODE_STARTS:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_starts_text_sensitive:
				gernel_var_item_compare_starts_text;

			break;
		case GERNEL_SEARCH_MODE_ENDS:

			func = (sensitivity == GERNEL_SEARCH_CASE_SENSITIVE)?
				gernel_var_item_compare_ends_text_sensitive:
				gernel_var_item_compare_ends_text;

			break;
		default:
			func = NULL;
			g_assert_not_reached();
			break;
	}

	gernel_search_real_search(search, func, keyword);
}

static enum GernelSearchAttribute gernel_search_get_attribute(
		GernelSearch *search
		)
/*	Get the attribute currently selected by the user	*/
{
	GernelSearchMenu	*menu;

	g_return_val_if_fail(search != NULL, GERNEL_SEARCH_ATTRIBUTE_0);

	g_return_val_if_fail(	GERNEL_IS_SEARCH(search), 
				GERNEL_SEARCH_ATTRIBUTE_0	);

	menu = GERNEL_SEARCH_MENU(search->attribute_menu);

	return gernel_search_menu_get_identifier(menu);
}

static enum GernelSearchPhrase gernel_search_get_phrase(GernelSearch *search )
/*	Check if keyword is to be interpreted as a single phrase	*/
{
	GtkWidget	*check_button;

	g_return_val_if_fail(search != NULL, GERNEL_SEARCH_PHRASE_0);
	g_return_val_if_fail(GERNEL_IS_SEARCH(search), GERNEL_SEARCH_PHRASE_0);

	check_button = search->phrase_check;

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_button)))
		return GERNEL_SEARCH_PHRASE_SINGLE;
	else
		return GERNEL_SEARCH_PHRASE_MULTIPLE;
}

static enum GernelSearchCase gernel_search_get_case(	GernelSearch *search )
/*	Check case sensitivity chosen by user	*/
{
	GtkWidget	*check_button;

	g_return_val_if_fail(search != NULL, GERNEL_SEARCH_CASE_0);
	g_return_val_if_fail(GERNEL_IS_SEARCH(search), GERNEL_SEARCH_CASE_0);

	check_button = search->case_check;

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_button)))
		return GERNEL_SEARCH_CASE_SENSITIVE;
	else
		return GERNEL_SEARCH_CASE_INSENSITIVE;
}

static enum GernelSearchMode gernel_search_get_mode(	GernelSearch *search )
/*	Get the mode currently selected by the user	*/
{
	GernelSearchMenu	*menu;

	g_return_val_if_fail(search != NULL, GERNEL_SEARCH_MODE_0);
	g_return_val_if_fail(GERNEL_IS_SEARCH(search), GERNEL_SEARCH_MODE_0);

	menu = GERNEL_SEARCH_MENU(search->mode_menu);

	return gernel_search_menu_get_identifier(menu);
}

static gchar* gernel_search_get_keyword(	GernelSearch *search	)
/*	Get the keyword filled in by the user	*/
{
	GtkWidget	*entry;

	g_return_val_if_fail(search != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_SEARCH(search), NULL);

	entry = gnome_entry_gtk_entry(GNOME_ENTRY(search->entry));

	return gtk_entry_get_text(GTK_ENTRY(entry));
}

static void gernel_search_real_search(	GernelSearch *search,
					GernelVarItemSearchFunc function,
					gchar *pattern	)
/*	Search search's tree with function	*/
{	
	GernelTree		*tree;
	GernelList		*table;
	GSList			*slist, *list;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(function != NULL);
	g_return_if_fail(pattern != NULL);

	tree = GERNEL_TREE(search->tree);
	table = GERNEL_LIST(search->list);

	slist = gernel_search_foreach(search, tree, function, pattern);

	for (list = slist; list; list = list->next)
		gernel_list_append(table, list->data);

	if (slist != NULL) gernel_list_select(table, 0);
}

static void gernel_search_remove_duplicates(	GernelSearch *search	)
/*	Remove duplicate items from search; these items may occur in _ALL
	search mode	*/
{
	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));

	gernel_list_remove_duplicates(GERNEL_LIST(search->list));
}

static void gernel_search_set_sort_order(	GernelSearch *search,
						GtkWidget *menu	)
/*	Set sort mode of list according to the item selected in search_menu */
{
	GernelSearchMenu		*search_menu;
	GernelList			*list;
	enum GernelListSortOrder	sort_order;

	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(menu != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU(menu));

	search_menu = GERNEL_SEARCH_MENU(menu);
	list = GERNEL_LIST(search->list);

	switch (gernel_search_menu_get_identifier(search_menu)) {
		case GERNEL_SEARCH_SORT_ORDER_ASCENDING:
			sort_order = GERNEL_LIST_SORT_ORDER_ASCENDING;
			break;
		case GERNEL_SEARCH_SORT_ORDER_DESCENDING:
			sort_order = GERNEL_LIST_SORT_ORDER_DESCENDING;
			break;
		default:
			sort_order = GERNEL_LIST_SORT_ORDER_0;
			g_assert_not_reached();
			break;
	}

	gernel_list_set_sort_order(list, sort_order);
}

static void gernel_search_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelSearch	*search;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(object));

	search = GERNEL_SEARCH(object);

	switch (arg_id) {
		gboolean	value;
		gchar		*string;
	
		case ARG_LOCAL_NAME:
			string = GTK_VALUE_STRING(*arg);
			gernel_search_set_local_name(search, string);
			break;
		case ARG_SHOW_DESCRIPTIONS:
			value = GTK_VALUE_BOOL(*arg);
			gernel_search_show_descriptions(search, value);
			break;
		case ARG_SHOW_NAMES:
			value = GTK_VALUE_BOOL(*arg);
			gernel_search_show_names(search, value);
			break;
		case ARG_PREFERENCES:	/*	Fall Through	*/
		default:
			break;
	}
}

static void gernel_search_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelSearch	*search;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(object));

	search = GERNEL_SEARCH(object);

	switch (arg_id) {
		GtkWidget	*widget;
		gchar		*string;

		case ARG_SHOW_DESCRIPTIONS:
			GTK_VALUE_BOOL(*arg) = search->show_descriptions;
			break;
		case ARG_SHOW_NAMES:
			GTK_VALUE_BOOL(*arg) = search->show_names;
			break;
		case ARG_PREFERENCES:
			widget = gernel_search_preferences(search);
			GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(widget);
			break;
		case ARG_LOCAL_NAME:
			string = gernel_search_get_local_name(search);
			GTK_VALUE_STRING(*arg) = string;
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static GtkWidget* gernel_search_preferences(	GernelSearch *search	)
/*	Assemble a page to be used in a Gnome property box	*/
{
	GtkWidget		*page, *section;

	g_return_val_if_fail(search != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_SEARCH(search), NULL);

	page = erty_preference_page_new();
	section = erty_preference_section_new(_("Result List"));

	erty_preference_section_add_multiple(
			ERTY_PREFERENCE_SECTION(section),
			GTK_WIDGET(search),
			ERTY_PREFERENCE_SECTION_DEPENDENCY_0,
			NULL, ARG_0,
			NULL, ARG_0,
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Show Descriptions"), ARG_SHOW_DESCRIPTIONS,
			_("Show Internal Names"), ARG_SHOW_NAMES,
			NULL
			);

	gtk_container_add(GTK_CONTAINER(page), section);

	return page;
}

static void gernel_search_show_descriptions(	GernelSearch *search,
						gboolean show	)
/*	Show the descriptions column of results list	*/
{
	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));

	gernel_list_show_descriptions(GERNEL_LIST(search->list), show);
	search->show_descriptions = show;
	gernel_search_show_results(search);
}

static void gernel_search_show_results(	GernelSearch *search	)
/*	Hide results list altogether if neither column is visible	*/
{
	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));

	if (	(search->show_descriptions == FALSE) && 
		(search->show_names == FALSE)	)

		gtk_widget_hide(search->list);
	else
		gtk_widget_show(search->list);
}

static void gernel_search_show_names(	GernelSearch *search,
					gboolean show	)
/*	Show the names column of results list	*/
{
	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));

	gernel_list_show_names(GERNEL_LIST(search->list), show);
	search->show_names = show;
	gernel_search_show_results(search);
}

static GSList* gernel_search_foreach(	GernelSearch *search,
					GernelTree *tree,
					GernelVarItemSearchFunc function, 
					gpointer data	)
/*	Recursively traverses tree and adds output of function to a list */
{
	GList	*list;
	GSList	*result;

	for (	list = gernel_tree_children(tree), result = NULL;
		list; 
		list = list->next	) {

		GtkTreeItem	*item;
		GtkWidget	*widget;

		item = GTK_TREE_ITEM(list->data);

		if (item->subtree) {
			GernelTree	*tree;
			GSList		*slist;

			tree = GERNEL_TREE(item->subtree);
			slist = NULL;

			slist = gernel_search_foreach(	search, 
							tree, 
							function, 
							data	);

			if (slist != NULL)
				result = g_slist_concat(result, slist);
		}

		widget = (*function)(GERNEL_VAR_ITEM(item), data);

		if (widget != NULL)
			result = g_slist_append(result, widget);
	}

	return result;
}

static void gernel_search_set_local_name(	GernelSearch *search,
						gchar *name	)
/*	Set the local name of search to 'name'	*/
{
	g_return_if_fail(search != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH(search));
	g_return_if_fail(name != NULL);

	search->local_name = name;
}

static gchar* gernel_search_get_local_name(	GernelSearch *search	)
/*	Get the local name of search	*/
{
	g_return_val_if_fail(search != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_SEARCH(search), NULL);

	return search->local_name;
}
