#define GERNEL_TYPE_SEARCH	(gernel_search_get_type())

#define GERNEL_SEARCH(obj) \
	(GTK_CHECK_CAST(obj, GERNEL_TYPE_SEARCH, GernelSearch))

#define GERNEL_SEARCH_CLASS(klass) \
	GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_SEARCH, \
				GernelSearchClass	)

#define GERNEL_IS_SEARCH(obj) (GTK_CHECK_TYPE((obj), GERNEL_TYPE_SEARCH))

#define GERNEL_IS_SEARCH_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE((klass, GERNEL_TYPE_SEARCH))

typedef struct _GernelSearch		GernelSearch;
typedef struct _GernelSearchClass	GernelSearchClass;

struct _GernelSearch {
	GnomeDialog	dialog;

	GtkWidget	*tree, *mode_menu, *attribute_menu, *entry, *list,
			*case_check, *phrase_check, *sort_order_menu;

	gboolean	show_descriptions, show_names;
	gchar		*local_name;
};

struct _GernelSearchClass {
	GnomeDialogClass	parent_class;

	void (*configure)	(	GernelSearch *search	);
};

GtkType		gernel_search_get_type(	void	);
GtkWidget*	gernel_search_new(	gchar *name	);

GtkWidget*	gernel_search_new_with_tree(	gchar *name,
						GtkWidget *tree	);

void		gernel_search_set_tree(	GernelSearch *search,
					GtkWidget *tree	);
