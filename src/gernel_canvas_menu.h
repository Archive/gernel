#define GERNEL_TYPE_CANVAS_MENU	(gernel_canvas_menu_get_type())

#define GERNEL_CANVAS_MENU(obj) \
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_CANVAS_MENU, GernelCanvasMenu))

#define GERNEL_CANVAS_MENU_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_CANVAS_MENU, \
				GernelCanvasMenuClass	))

#define GERNEL_IS_CANVAS_MENU(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_CANVAS_MENU))

#define GERNEL_IS_CANVAS_MENU_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_CANVAS_MENU))

typedef struct _GernelCanvasMenu	GernelCanvasMenu;
typedef struct _GernelCanvasMenuClass	GernelCanvasMenuClass;

struct _GernelCanvasMenu {
	GtkMenu		menu;

	GtkWidget	*preferences;
	GnomeUIInfo	*uiinfo;
};

struct _GernelCanvasMenuClass {
	GtkMenuClass	parent_class;

	void (*preferences)	(	GernelCanvasMenu *canvas_menu	);
};

GtkType		gernel_canvas_menu_get_type(	void	);
GtkWidget*	gernel_canvas_menu_new(	GtkWidget *app	);

void		gernel_canvas_menu_popup(	GernelCanvasMenu *canvas_menu,
						guint32 time	);
