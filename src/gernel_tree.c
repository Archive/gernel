#include <gernel.h>
#include <gernel_tree.h>
#include <gernel_var_item.h>

static void gernel_tree_init(	GernelTree *tree	);
static void gernel_tree_class_init(	GernelTreeClass *klass	);
static void gernel_tree_construct(	GernelTree *tree	);
static GtkWidget* gernel_tree_last(	GernelTree *tree	);
static GtkWidget* gernel_tree_first(	GernelTree *tree	);
static void gernel_tree_peel(	GernelTree *tree	);

static void	gernel_tree_size_request(	GtkWidget *widget,
						GtkRequisition *requisition );

static void	gernel_tree_size_allocate(	GtkWidget *widget,
						GtkAllocation *allocation );

static void gernel_tree_scroll(	GernelTree *tree,
				GtkWidget *var_item	);

static GtkTree	*parent_class = NULL;

GtkType gernel_tree_get_type(	void	)
/*	Return type identifier for type GernelTree	*/
{
	static GtkType	tree_type = 0;

	if (!tree_type) {

		static const GtkTypeInfo	tree_info = {
				"GernelTree",
				sizeof(GernelTree),
				sizeof(GernelTreeClass),
				(GtkClassInitFunc)gernel_tree_class_init,
				(GtkObjectInitFunc)gernel_tree_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		tree_type = gtk_type_unique(gtk_tree_get_type(), &tree_info);
	}

	return tree_type;
}

GtkWidget* gernel_tree_new(	void	)
/*	Create an instance of GernelTree	*/
{
	GernelTree	*tree;

	tree = gtk_type_new(gernel_tree_get_type());
	gernel_tree_construct(tree);

	return GTK_WIDGET(tree);
}

static void gernel_tree_class_init(	GernelTreeClass *klass	)
/*	Initialize the GernelTree class	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_TREE);

	widget_class->size_request = gernel_tree_size_request;
	widget_class->size_allocate = gernel_tree_size_allocate;
}

static void gernel_tree_init(	GernelTree *tree	)
/*	Initialize the GernelTree widget	*/
{
}

GList* gernel_tree_get_selection(	GernelTree *tree	)
/*	Get selected item from tree	*/
{
	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);

	return GTK_TREE(tree)->selection;
}

static void gernel_tree_construct(	GernelTree *widget	)
/*	Construct a newly built tree	*/
{
	GtkTree	*tree;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_TREE(widget));

	tree = GTK_TREE(widget);

	gtk_tree_set_selection_mode(tree, GTK_SELECTION_SINGLE);
	gtk_tree_set_view_mode(tree, GTK_TREE_VIEW_ITEM);
}

static GtkWidget* gernel_tree_first(	GernelTree *tree	)
/*	Return the last item in the tree (first child)	*/
{
	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);
	g_return_val_if_fail(gernel_tree_children(tree) != NULL, NULL);

	return GTK_WIDGET(gernel_tree_children(tree)->data);
}

static GtkWidget* gernel_tree_last(	GernelTree *tree	)
/*	Return the last item in the tree	*/
{
	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);

	return GTK_WIDGET(g_list_last(gernel_tree_children(tree))->data);
}

void gernel_tree_branch(	GernelTree *tree,
				GtkWidget *branch	)
/*	Plug branch into last element of tree	*/
{
	GtkWidget	*tree_item;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));
	g_return_if_fail(branch != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	tree_item = gernel_tree_last(tree);
	gtk_tree_item_set_subtree(GTK_TREE_ITEM(tree_item), branch);
}

GList* gernel_tree_children(	GernelTree *tree	)
/*	Get list of tree's children	*/
{
	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);

	return GTK_TREE(tree)->children;
}

GtkWidget* gernel_tree_find(	GernelTree *tree,
				GernelTreeSearchFunc function,
				gpointer data	)
/*	recursively traverses tree and applies func to each of its elements
	until func returns TRUE; then stop traversal and return matched element
	*/
{
	GList	*list;

	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);

	for (list = gernel_tree_children(tree); list; list = list->next) {
		GtkWidget	*var_item;

		if ((var_item = (*function)(list->data, data)) != NULL)
			return var_item;
		else if (GTK_TREE_ITEM(list->data)->subtree) {
			GtkWidget	*subtree;

			subtree = GTK_TREE_ITEM(list->data)->subtree;

			var_item = gernel_tree_find(	GERNEL_TREE(subtree), 
							function, 
							data	);
		}

		if (var_item) return var_item;
	}

	return NULL;
}

void gernel_tree_select_item(	GernelTree *tree,
				GtkWidget *var_item	)
/*	Select var_item in tree	*/
{
	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gtk_tree_unselect_child(GTK_TREE(tree), var_item);
	gtk_tree_select_child(GTK_TREE(tree), var_item);
	gernel_tree_peel(GERNEL_TREE(var_item->parent));
	gernel_tree_scroll(tree, var_item);
}

void gernel_tree_select_next(	GernelTree *tree	)
/*	Select next item in tree	*/
{
	GtkWidget	*var_item;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	var_item = gernel_tree_get_sibling(tree, GERNEL_TREE_DIRECTION_NEXT);
	gernel_tree_select_item(tree, var_item);
}

void gernel_tree_select_previous(	GernelTree *tree	)
/*	Select previous item in tree	*/
{
	GtkWidget			*var_item;
	enum GernelTreeDirection	direction;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	direction = GERNEL_TREE_DIRECTION_PREVIOUS;
	var_item = gernel_tree_get_sibling(tree, direction);
	gernel_tree_select_item(tree, var_item);
}

void gernel_tree_select_up(	GernelTree *tree	)
/*	Select item above current selection of tree	*/
{
	GtkWidget	*var_item, *node;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	var_item = GTK_WIDGET(gernel_tree_get_selection(tree)->data);
	node = GTK_TREE(var_item->parent)->tree_owner;
	gtk_tree_item_collapse(GTK_TREE_ITEM(node));
	gernel_tree_select_item(tree, node);
}

void gernel_tree_select_down(	GernelTree *tree	)
/*	Selection item above current selection of tree	*/
{
	GtkWidget	*var_item, *first_item;
	GtkTreeItem	*tree_item;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	var_item = GTK_WIDGET(gernel_tree_get_selection(tree)->data);
	tree_item = GTK_TREE_ITEM(var_item);
	gtk_tree_item_expand(tree_item);
	first_item = gernel_tree_first(GERNEL_TREE(tree_item->subtree));
	gernel_tree_select_item(tree, first_item);
}

GtkWidget* gernel_tree_get_sibling(	GernelTree *tree,
					enum GernelTreeDirection direction )
/*	Get item next/previous/below/above current selection of tree	*/
{
	GtkWidget	*var_item, *new_item;
	GernelTree	*parent;
	GList		*children;

	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);

	var_item = GTK_WIDGET(gernel_tree_get_selection(tree)->data);
	parent = GERNEL_TREE(var_item->parent);
	children = gernel_tree_children(parent);
	while (children->data != var_item) children = children->next;
	new_item = NULL;

	switch (direction) {
		case GERNEL_TREE_DIRECTION_NEXT:
			if (children->next != NULL)
				new_item = GTK_WIDGET(children->next->data);
			break;
		case GERNEL_TREE_DIRECTION_PREVIOUS:
			if (children->prev != NULL)
				new_item = GTK_WIDGET(children->prev->data);
			break;
		case GERNEL_TREE_DIRECTION_UP:
		case GERNEL_TREE_DIRECTION_DOWN:
		default:
			break;
	}

	return new_item;
}

gboolean gernel_tree_is_interior_node(	GernelTree *tree,
					GtkWidget *var_item	)
/*	Test if var_item has a subtree	*/
{
	g_return_val_if_fail(tree != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), FALSE);
	g_return_val_if_fail(var_item != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), FALSE);

	return GTK_TREE_ITEM(var_item)->subtree != NULL;
}

static void gernel_tree_peel(	GernelTree *tree	)
/*	expand all ancestors of tree so tree is actually visible to the user */
{
	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	if (	(GTK_IS_TREE(GTK_TREE(tree)->tree_owner)) |
		(GTK_IS_TREE_ITEM(GTK_TREE(tree)->tree_owner))	) {

		GtkTree	*root;

		gtk_tree_item_expand(GTK_TREE_ITEM(GTK_TREE(tree)->tree_owner));
		root = GTK_TREE_ROOT_TREE(GTK_WIDGET(tree)->parent);
		gernel_tree_peel(GERNEL_TREE(root));
	}
}

static void gernel_tree_scroll(	GernelTree *tree,
				GtkWidget *var_item	)
/*	Scroll the tree's parent to the location of var_item	*/
{
	GtkWidget	*current, *widget;
	GtkAdjustment	*vadj;
	gint		position, step;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	while (gtk_events_pending()) gtk_main_iteration();
	position = 0;
	widget = GTK_WIDGET(tree);
	current = var_item;

	while (current != widget) {
		position += current->allocation.y;
		current = current->parent;
	}

	vadj = gtk_viewport_get_vadjustment(GTK_VIEWPORT(widget->parent));
	step = var_item->allocation.height;

	/*	var_item is to the bottom beyond visible page	*/
	if (position + step > vadj->value + vadj->page_size) position += step;

	gtk_adjustment_clamp_page(vadj, position, position);
}

void gernel_tree_append(	GernelTree *tree,
				GtkWidget *widget	)
/*	wrapper for GtkTree's append() function	*/
{
	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_TREE_ITEM(widget));
/*	g_return_if_fail(GERNEL_IS_VAR_ITEM(widget));	*/

	gtk_tree_append(GTK_TREE(tree), widget);
}

static void gernel_tree_size_request(	GtkWidget *widget,
					GtkRequisition *requisition	)
/*	Implements the default size requisition handler	*/
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_TREE(widget));

	(*GTK_WIDGET_CLASS(parent_class)->size_request)(widget, requisition);
/*g_print("gernel_tree_size_request:	width == %d\n", requisition->width);*/
}

static void gernel_tree_size_allocate(	GtkWidget *widget,
					GtkAllocation *allocation	)
/*	Implements the default size requisition handler	*/
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_TREE(widget));

/*g_print("gernel_tree_size_allocate:	width == %d\n", allocation->width);*/
	(*GTK_WIDGET_CLASS(parent_class)->size_allocate)(widget, allocation);
}

gboolean gernel_tree_is_branch(	GernelTree *tree,
				GtkWidget *var_item	)
/*	Test if var_item is a branch to a subtree	*/
{
	g_return_val_if_fail(tree != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), FALSE);
	g_return_val_if_fail(var_item != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), FALSE);

	return gernel_tree_get_branch(tree, var_item) != NULL;
}

GtkWidget* gernel_tree_get_branch(	GernelTree *tree,
					GtkWidget *var_item	)
/*	Get branch of var_item; returns NULL if var_item has no branch	*/
{
	g_return_val_if_fail(tree != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_TREE(tree), NULL);
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	return GTK_TREE_ITEM(var_item)->subtree;
}
