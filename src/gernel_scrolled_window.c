#include <gernel.h>
#include <gernel_scrolled_window.h>

static void gernel_scrolled_window_init(
		GernelScrolledWindow *scrolled_window
		);

static void gernel_scrolled_window_class_init(
		GernelScrolledWindowClass *klass
		);

static void gernel_scrolled_window_add(	GtkContainer *scrolled_window,
					GtkWidget *widget	);

static void	gernel_scrolled_window_size_allocate(
			GtkWidget *widget,
			GtkAllocation *allocation
			);

static GtkScrolledWindowClass	*parent_class = NULL;

GtkType gernel_scrolled_window_get_type(	void	)
/*	Return type identifier for type GernelScrolledWindow	*/
{
	static GtkType	scrolled_window_type = 0;

	if (!scrolled_window_type) {

		static const GtkTypeInfo	scrolled_window_info = {
			"GernelScrolledWindow",
			sizeof(GernelScrolledWindow),
			sizeof(GernelScrolledWindowClass),
			(GtkClassInitFunc)gernel_scrolled_window_class_init,
			(GtkObjectInitFunc)gernel_scrolled_window_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		scrolled_window_type = gtk_type_unique(
				gtk_scrolled_window_get_type(), 
				&scrolled_window_info
				);
	}

	return scrolled_window_type;
}

GtkWidget* gernel_scrolled_window_new(	void	)
/*	Create an instance of GernelScrolledWindow	*/
{
	GernelScrolledWindow	*scrolled_window;

	scrolled_window = gtk_type_new(gernel_scrolled_window_get_type());

	return GTK_WIDGET(scrolled_window);
}

static void gernel_scrolled_window_init(	GernelScrolledWindow *widget )
/*	Initialize the GernelScrolledWindow	*/
{
	GtkScrolledWindow	*scrolled_window;

	scrolled_window = GTK_SCROLLED_WINDOW(widget);

	gtk_scrolled_window_set_hadjustment(scrolled_window, NULL);
	gtk_scrolled_window_set_vadjustment(scrolled_window, NULL);

	gtk_scrolled_window_set_policy(	scrolled_window,
					GTK_POLICY_AUTOMATIC, 
					GTK_POLICY_AUTOMATIC	);
}

static void gernel_scrolled_window_class_init(
		GernelScrolledWindowClass *klass
		)
/*	Initialize the GernelScrolledWindowClass	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_SCROLLED_WINDOW);

	widget_class->size_allocate = gernel_scrolled_window_size_allocate;

	container_class->add = gernel_scrolled_window_add;
}

static void gernel_scrolled_window_add(	GtkContainer *container,
					GtkWidget *widget	)
/*	Generic add function for GernelScrolledWindow	*/
{
	GtkType			widget_type;

	g_return_if_fail(container != NULL);
	g_return_if_fail(GERNEL_IS_SCROLLED_WINDOW(container));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));

	widget_type = GTK_OBJECT_TYPE(widget);

	if (gtk_signal_lookup("set_scroll_adjustments", widget_type) != 0)
		(*GTK_CONTAINER_CLASS(parent_class)->add)(container, widget);
	else {
		GtkWidget		*viewport;
		GernelScrolledWindow	*scrolled_window;

		scrolled_window = GERNEL_SCROLLED_WINDOW(container);

		viewport = gtk_viewport_new(NULL, NULL);
		scrolled_window->viewport = viewport;
		(*GTK_CONTAINER_CLASS(parent_class)->add)(container, viewport);
		gtk_widget_show(viewport);

		gtk_container_add(GTK_CONTAINER(viewport), widget);
	}
}

void gernel_scrolled_window_set_hscrolling(
		GernelScrolledWindow *scrolled_window,
		GtkPolicyType type
		)
/*	Set the scrolling policy of the horizontal scrollbar	*/
{
	GtkScrolledWindow	*window;

	g_return_if_fail(scrolled_window != NULL);
	g_return_if_fail(GERNEL_IS_SCROLLED_WINDOW(scrolled_window));

	window = GTK_SCROLLED_WINDOW(scrolled_window);

	gtk_scrolled_window_set_policy(window, type, window->vscrollbar_policy);
}

void gernel_scrolled_window_set_vscrolling(
		GernelScrolledWindow *scrolled_window,
		GtkPolicyType type
		)
/*	Set the scrolling policy of the vertical scrollbar	*/
{
	GtkScrolledWindow	*window;

	g_return_if_fail(scrolled_window != NULL);
	g_return_if_fail(GERNEL_IS_SCROLLED_WINDOW(scrolled_window));

	window = GTK_SCROLLED_WINDOW(scrolled_window);

	gtk_scrolled_window_set_policy(window, window->hscrollbar_policy, type);
}

static GtkPolicyType gernel_scrolled_window_get_vscrolling(
		GernelScrolledWindow *scrolled_window
		)
/*	Get vertical scrolling policy of scrolled_window	*/
{
	g_return_val_if_fail(scrolled_window != NULL, -1);
	g_return_val_if_fail(GERNEL_IS_SCROLLED_WINDOW(scrolled_window), -1);

	return GTK_SCROLLED_WINDOW(scrolled_window)->vscrollbar_policy;
}

static GtkPolicyType gernel_scrolled_window_get_hscrolling(
		GernelScrolledWindow *scrolled_window
		)
/*	Get horizontal scrolling policy of scrolled_window	*/
{
	g_return_val_if_fail(scrolled_window != NULL, -1);
	g_return_val_if_fail(GERNEL_IS_SCROLLED_WINDOW(scrolled_window), -1);

	return GTK_SCROLLED_WINDOW(scrolled_window)->hscrollbar_policy;
}

static void gernel_scrolled_window_size_allocate(
		GtkWidget *widget,
		GtkAllocation *allocation
		)
/*	Implements the default size allocation handler	*/
{
	GtkPolicyType		vscrolling, hscrolling;
	GernelScrolledWindow	*scrolled_window;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_SCROLLED_WINDOW(widget));

	scrolled_window = GERNEL_SCROLLED_WINDOW(widget);
	vscrolling = gernel_scrolled_window_get_vscrolling(scrolled_window);
	hscrolling = gernel_scrolled_window_get_hscrolling(scrolled_window);

	if (	(vscrolling == GTK_POLICY_NEVER) &&
		(hscrolling == GTK_POLICY_NEVER)	) {

		GtkRequisition	requisition;

		gtk_widget_size_request(GTK_BIN(widget)->child, &requisition);

		if (allocation->height > requisition.height) {
			gtk_widget_queue_resize(widget);
			return;
		}
	}

	(*GTK_WIDGET_CLASS(parent_class)->size_allocate)(widget, allocation);
}
