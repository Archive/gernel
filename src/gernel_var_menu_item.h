#define	GERNEL_TYPE_VAR_MENU_ITEM	(gernel_var_menu_item_get_type())

#define	GERNEL_VAR_MENU_ITEM(obj) \
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_VAR_MENU_ITEM, GernelVarMenuItem))

#define	GERNEL_VAR_MENU_ITEM_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_VAR_MENU_ITEM, \
				GernelVarMenuItemClass	))

#define	GERNEL_IS_VAR_MENU_ITEM(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_VAR_MENU_ITEM))

#define GERNEL_IS_VAR_MENU_ITEM_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_VAR_MENU_ITEM))

typedef struct _GernelVarMenuItem	GernelVarMenuItem;
typedef struct _GernelVarMenuItemClass	GernelVarMenuItemClass;

struct _GernelVarMenuItem {
	GtkMenuItem	menu_item;

	GtkWidget	*var_item;
};

struct _GernelVarMenuItemClass {
	GtkMenuItemClass	parent_class;

	void (*current	)	(	GernelVarMenuItem *var_menu_item,
					GtkWidget *var_item	);
};

GtkType		gernel_var_menu_item_get_type(	void	);
GtkWidget*	gernel_var_menu_item_new(	GtkWidget *var_item	);

GtkWidget*	gernel_var_menu_item_new_with_label(	GtkWidget *var_item,
							gchar *label	);
