#define	GERNEL_TYPE_CANVAS_SECTION	(gernel_canvas_section_get_type())

#define GERNEL_CANVAS_SECTION(obj) \
	(GTK_CHECK_CAST(obj, GERNEL_TYPE_CANVAS_SECTION, GernelCanvasSection))

#define GERNEL_CANVAS_SECTION_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_CANVAS_SECTION, \
				GernelCanvasSectionClass)	)

#define GERNEL_IS_CANVAS_SECTION(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_CANVAS_SECTION))

#define GERNEL_IS_CANVAS_SECTION_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_CANVAS_SECTION))

enum GernelCanvasSectionType {	GERNEL_CANVAS_SECTION_TYPE_0,
				GERNEL_CANVAS_SECTION_TYPE_TITLE,
				GERNEL_CANVAS_SECTION_TYPE_PROMPT,
				GERNEL_CANVAS_SECTION_TYPE_HEADER,
				GERNEL_CANVAS_SECTION_TYPE_TEXT,
				GERNEL_CANVAS_SECTION_TYPE_BACKGROUND,
				GERNEL_CANVAS_SECTION_TYPE_TAIL	};

typedef struct _GernelCanvasSection	GernelCanvasSection;
typedef struct _GernelCanvasSectionClass	GernelCanvasSectionClass;

struct _GernelCanvasSection {
	GnomeCanvasGroup		section;

	enum GernelCanvasSectionType	type;
	GnomeCanvasItem			*foreground, *background;
	guint32				foreground_color, background_color;
	gchar				*text;
	GnomeCanvasGroup		*group;
	gint				width, height, x, y;
	gboolean			visible;
};

struct _GernelCanvasSectionClass {
	GnomeCanvasGroupClass	parent_class;
};

GtkType			gernel_canvas_section_get_type(	void	);

GernelCanvasSection*	gernel_canvas_section_new(
		enum GernelCanvasSectionType type,
		GtkWidget *canvas
		);

void			gernel_canvas_section_set_background_color(
		GernelCanvasSection *canvas_section,
		guint32 color
		);

void			gernel_canvas_section_set_foreground_color(
		GernelCanvasSection *canvas_section,
		guint32 color
		);

void			gernel_canvas_section_set_text(
		GernelCanvasSection *canvas_section,
		gchar *text
		);

void			gernel_canvas_section_set_width(
		GernelCanvasSection *canvas_section,
		gint width
		);

void			gernel_canvas_section_set_x(
		GernelCanvasSection *canvas_section,
		gint x
		);

void			gernel_canvas_section_set_y(
		GernelCanvasSection *canvas_section,
		gint y
		);

gint			gernel_canvas_section_get_height(
		GernelCanvasSection *canvas_section
		);

gint			gernel_canvas_section_get_width(
		GernelCanvasSection *canvas_section
		);

gint			gernel_canvas_section_get_y(
		GernelCanvasSection *canvas_section
		);

void			gernel_canvas_section_set_height(
		GernelCanvasSection *canvas_section
		);

void			gernel_canvas_section_set_height_custom(
		GernelCanvasSection *canvas_section,
		gint height
		);

void			gernel_canvas_section_set_font(
		GernelCanvasSection *canvas_section,
		GdkFont *font
		);

void			gernel_canvas_section_set_fontset(
		GernelCanvasSection *canvas_section,
		gchar *fontset
		);

void			gernel_canvas_section_hide(
		GernelCanvasSection *canvas_section
		);

void			gernel_canvas_section_show(
		GernelCanvasSection *canvas_section
		);

gboolean		gernel_canvas_section_is_visible(
		GernelCanvasSection *canvas_section
		);
