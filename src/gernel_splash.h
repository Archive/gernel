#define	GERNEL_TYPE_SPLASH	(gernel_splash_get_type())

#define	GERNEL_SPLASH(obj) \
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_SPLASH, GernelSplash))

#define GERNEL_SPLASH_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass) GERNEL_TYPE_SPLASH, GernelSplash))

#define GERNEL_IS_SPLASH(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_SPLASH))

#define GERNEL_IS_SPLASH_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass, GERNEL_TYPE_SPLASH))

typedef struct _GernelSplash		GernelSplash;
typedef struct _GernelSplashClass	GernelSplashClass;

struct _GernelSplash {
	GtkWindow	window;

	GtkWidget	*vbox, *major_status, *minor_status, *progress_bar;
	guint		timeout;
};

struct _GernelSplashClass {
	GtkWindowClass	parent_class;
};

GtkType 	gernel_splash_get_type(	void	);
GtkWidget*	gernel_splash_new(	void	);
GtkWidget*	gernel_splash_new_with_title(	gchar *title	);

void		gernel_splash_update(	GernelSplash *splash,
					gchar *major,
					gchar *minor	);

void		gernel_splash_start(	GernelSplash *splash	);
void		gernel_splash_stop(	GernelSplash *splash	);
