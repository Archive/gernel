#include <gernel.h>
#include <gernel_navigation.h>
#include <gernel_var_item.h>
#include <gernel_tree.h>
#include <gernel_dependency.h>

#define	MODULE_ITEM	"CONFIG_MODULES"

typedef GernelVarItem*	(*gernel_search_func)	(	gconstpointer a,
							gconstpointer b	);

enum GernelNavigationScrollType {	GERNEL_NAVIGATION_SCROLL_UP,
					GERNEL_NAVIGATION_SCROLL_DOWN,
					GERNEL_NAVIGATION_SCROLL_NEXT,
					GERNEL_NAVIGATION_SCROLL_PREVIOUS };

static GNode* gernel_navigation_negate_expression(
		GernelNavigation *navigation,
		GNode *condition
		);

static GSList* gernel_navigation_manage_stack(	GernelNavigation *navigation,
						GSList *stack,
						GCache *cache,
						gchar *input	);

static void gernel_navigation_set_hide_unmet(	GernelNavigation *navigation,
						gboolean flag	);

static void gernel_navigation_set_local_name(	GernelNavigation *navigation,
						gchar *name	);

static gchar* gernel_navigation_get_local_name(
		GernelNavigation *navigation
		);

static void gernel_navigation_init(	GernelNavigation *navigation	);
static void gernel_navigation_class_init(	GernelNavigationClass *klass );
static GtkWidget* gernel_navigation_preferences(GernelNavigation *navigation );

static void gernel_navigation_show_navigation(	GernelNavigation *navigation,
						gboolean flag	);

static void gernel_navigation_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_navigation_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static GtkWidget* gernel_navigation_construct_tree(
		GernelNavigation *navigation
		);

static void	gernel_navigation_size_request(	GtkWidget *widget,
						GtkRequisition *requisition );

static void	gernel_navigation_size_allocate(GtkWidget *widget,
						GtkAllocation *allocation );

static gchar* gernel_navigation_get_base_directory(
		GernelNavigation *navigation
		);

static void gernel_navigation_view_changed(	GernelNavigation *navigation,
						GtkWidget *var_item	);

static GtkWidget* gernel_navigation_fill_tree(	GernelNavigation *navigation,
						GSList *source_text,
						GSList *stack	);

static GSList* gernel_navigation_preprocess(	GernelNavigation *navigation,
						gchar *relative_path	);

static void gernel_navigation_allow_module(	GernelNavigation *navigation,
						GernelVarItem *var_item	);

static gchar* gernel_navigation_read_file(	GernelNavigation *navigation,
						gchar* path	);

static GSList* gernel_navigation_split(	gchar* text	);
static void gernel_navigation_module_settings(	GernelNavigation *navigation );
static void gernel_navigation_items_sensitive(	GernelNavigation *navigation );

static void gernel_navigation_read_contents(	GernelNavigation *navigation,
						GSList *lines	);

static void gernel_navigation_extract_title(	GernelNavigation *navigation,
						GSList *lines	);

static GSList* gernel_navigation_clear_garbage(	GSList *list	);

static void gernel_navigation_create_icons(	GernelNavigation *navigation,
						GdkWindow *window	);

static	void gernel_navigation_create_labels(	GernelNavigation *navigation );
static	void gernel_navigation_sync_icons(	GernelNavigation *navigation );

static	void gernel_navigation_foreach(	GernelNavigation *navigation,
					GtkWidget *tree,
					GernelVarItemFunc function,
					gpointer data	);

static void gernel_navigation_item_selected(	GernelNavigation *navigation,
						GernelVarItem *var_item,
						GtkWidget *tree	);

static void gernel_navigation_check_conditions(	GernelNavigation *navigation );

static void gernel_navigation_updated(	GernelNavigation *navigation,
					gchar *message1,
					gchar *message2	);

GSList* gernel_navigation_clear_garbage_defconfig(	GSList *lines	);
static void gernel_navigation_done_parsing(	GernelNavigation *navigation );
static void gernel_navigation_reset(	GernelNavigation *navigation	);

static gboolean gernel_navigation_legitimate_path(
		GernelNavigation *navigation, 
		gchar *path
		);

static void gernel_navigation_history(	GernelNavigation *navigation,
					GtkWidget *widget	);

void gernel_navigation_next(	GernelNavigation *navigation	);
void gernel_navigation_previous(	GernelNavigation *navigation	);
static void gernel_navigation_up_possible(	GernelNavigation *navigation );
static void gernel_navigation_down_possible(	GernelNavigation *navigation );
static void gernel_navigation_next_possible(	GernelNavigation *navigation );
static void gernel_navigation_previous_possible(GernelNavigation *navigation );

static void gernel_navigation_branch_tree(	GernelNavigation *navigation,
						GtkWidget *tree	);

static void gernel_navigation_scroll(
		GernelNavigation *navigation,
		enum GernelNavigationScrollType scroll_type
		);

static GernelScrolledWindowClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_LOCAL_NAME,
	ARG_SHOW_NAVIGATION,
	ARG_HIDE_UNMET,
	ARG_PREFERENCES	};

enum {	DEPENDANTS,
	PREREQUISITES,
	SELECTED,
	UNSELECTED,
	UPDATED,
	DONE_PARSING,
	HISTORY,
	UP_POSSIBLE,
	DOWN_POSSIBLE,
	NEXT_POSSIBLE,
	PREVIOUS_POSSIBLE,
	TITLE,
	VIEW_CHANGED,
	LAST_SIGNAL	};

static guint navigation_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_navigation_get_type(	void	)
/*	Return type identifier for type GernelNavigation	*/
{
	static GtkType	navigation_type = 0;

	if (!navigation_type) {

		static const GtkTypeInfo	navigation_info = {
			"GernelNavigation",
			sizeof(GernelNavigation),
			sizeof(GernelNavigationClass),
			(GtkClassInitFunc)gernel_navigation_class_init,
			(GtkObjectInitFunc)gernel_navigation_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		navigation_type = gtk_type_unique(
				GERNEL_TYPE_SCROLLED_WINDOW,
				&navigation_info
				);
	}

	return navigation_type;
}

GtkWidget* gernel_navigation_new(	gchar *name	)
/*	Create an instance of GernelNavigation	*/
{
	GtkWidget	*navigation;
	gchar		*path;

	g_return_val_if_fail(name != NULL, NULL);

g_print("gernel_navigation_new:	FIXME: move path concatenation to gernel_widget_new()\n");

	path = g_strconcat(name, "/", NULL);
	gnome_config_push_prefix(path);

	navigation = erty_widget_new(	gernel_navigation_get_type(),
					name,
					"show_navigation", TRUE,
					"hide_unmet", TRUE,
					NULL	);

	gnome_config_pop_prefix();
	g_free(path);
	gernel_navigation_set_local_name(GERNEL_NAVIGATION(navigation), name);

	return navigation;
}

static void gernel_navigation_init(	GernelNavigation *navigation	)
/*	Initialize the GernelNavigation	*/
{
	static GtkTargetEntry	target[] = {
			{ "application/octet-stream", 0, 0 },
			};

	gtk_drag_dest_set(	GTK_WIDGET(navigation), 
				GTK_DEST_DEFAULT_ALL, 
				target,
				1,
				GDK_ACTION_DEFAULT	);

	navigation->title = NULL;
	navigation->tree = NULL;
	navigation->base_directory = NULL;
	navigation->cache = NULL;
	navigation->template_path = NULL;
	navigation->contents_path = NULL;
	navigation->navigation_path = NULL;
	navigation->history = navigation->future = NULL;
}

static void gernel_navigation_class_init(	GernelNavigationClass *klass )
/*	Initialize the GernelNavigationClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkScrolledWindowClass	*scrolled_window_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	scrolled_window_class = (GtkScrolledWindowClass*)klass;

	parent_class = gtk_type_class(GERNEL_TYPE_SCROLLED_WINDOW);

	gtk_object_add_arg_type("GernelNavigation::preferences",
				GTK_TYPE_WIDGET,
				GTK_ARG_READABLE,
				ARG_PREFERENCES	);

	gtk_object_add_arg_type("GernelNavigation::local_name",
				GTK_TYPE_STRING,
				GTK_ARG_READWRITE,
				ARG_LOCAL_NAME	);

	erty_arg_add_multiple(
		"GernelNavigation",
		"show_navigation", GTK_TYPE_BOOL, ARG_SHOW_NAVIGATION,
		"hide_unmet", GTK_TYPE_BOOL, ARG_HIDE_UNMET,
		NULL
		);

	function_offset = GTK_SIGNAL_OFFSET(	GernelNavigationClass, 
						dependants	);

	navigation_signals[DEPENDANTS] = gtk_signal_new("dependants",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__BOOL,
							GTK_TYPE_NONE,
							1,
							GTK_TYPE_BOOL	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelNavigationClass, 
						prerequisites	);

	navigation_signals[PREREQUISITES] =

			gtk_signal_new(	"prerequisites",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__BOOL,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_BOOL	);


	function_offset = GTK_SIGNAL_OFFSET(GernelNavigationClass, selected);

	navigation_signals[SELECTED] =

			gtk_signal_new(	"selected",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__POINTER,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_POINTER	);

	function_offset = GTK_SIGNAL_OFFSET(GernelNavigationClass, unselected);

	navigation_signals[UNSELECTED] =

			gtk_signal_new(	"unselected",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelNavigationClass, updated);

	navigation_signals[UPDATED] =

			gtk_signal_new(	"updated",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__POINTER_POINTER,
					GTK_TYPE_NONE,
					2,
					GTK_TYPE_STRING,
					GTK_TYPE_STRING	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelNavigationClass, 
						done_parsing	);

	navigation_signals[DONE_PARSING] =

			gtk_signal_new(	"done_parsing",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelNavigationClass, history);

	navigation_signals[HISTORY] =

			gtk_signal_new(	"history",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__INT_INT,
					GTK_TYPE_NONE,
					2,
					GTK_TYPE_INT,
					GTK_TYPE_INT	);

	function_offset = GTK_SIGNAL_OFFSET(GernelNavigationClass, up_possible);

	navigation_signals[UP_POSSIBLE] =

			gtk_signal_new(	"up_possible",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__BOOL,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_BOOL	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelNavigationClass, 
						down_possible	);

	navigation_signals[DOWN_POSSIBLE] =

			gtk_signal_new(	"down_possible",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__BOOL,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_BOOL	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelNavigationClass,
						next_possible	);

	navigation_signals[NEXT_POSSIBLE] =

			gtk_signal_new(	"next_possible",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__BOOL,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_BOOL	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelNavigationClass,
						previous_possible	);

	navigation_signals[PREVIOUS_POSSIBLE] =

			gtk_signal_new(	"previous_possible",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__BOOL,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_BOOL	);

	function_offset = GTK_SIGNAL_OFFSET(GernelNavigationClass, title);

	navigation_signals[TITLE] =

			gtk_signal_new(	"title",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__STRING,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_STRING	);

	function_offset = GTK_SIGNAL_OFFSET(	GernelNavigationClass, 
						view_changed	);

	navigation_signals[VIEW_CHANGED] =

		gtk_signal_new(	"view_changed",
				GTK_RUN_FIRST,
				object_class->type,
				function_offset,
				gtk_marshal_NONE__POINTER_POINTER_POINTER,
				GTK_TYPE_NONE,
				3,
				GTK_TYPE_STRING,
				GTK_TYPE_STRING,
				GTK_TYPE_STRING	);

	gtk_object_class_add_signals(	object_class, 
					navigation_signals, 
					LAST_SIGNAL	);

	object_class->get_arg = gernel_navigation_get_arg;
	object_class->set_arg = gernel_navigation_set_arg;

	widget_class->size_request = gernel_navigation_size_request;
	widget_class->size_allocate = gernel_navigation_size_allocate;

	klass->dependants = NULL;
	klass->prerequisites = NULL;
	klass->selected = gernel_navigation_view_changed;
	klass->unselected = NULL;
	klass->updated = NULL;
	klass->done_parsing = NULL;
	klass->history = NULL;
	klass->up_possible = NULL;
	klass->down_possible = NULL;
	klass->next_possible = NULL;
	klass->previous_possible = NULL;
	klass->title = NULL;
	klass->view_changed = NULL;
}

static void gernel_navigation_history(	GernelNavigation *navigation,
					GtkWidget *widget	)
/*	Callback for the navigation's tree; manages the queue of items visited
	in past and future	*/
{
	GSList	*history, *future;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(widget));

	history = navigation->history;
	future = navigation->future;

	if ((history == NULL) || (history->data != widget)) {
		history = g_slist_prepend(history, widget);
		future = NULL;
	}

	navigation->history = history;
	navigation->future = future;

	gtk_signal_emit(	GTK_OBJECT(navigation),
				navigation_signals[HISTORY],
				g_slist_length(history),
				g_slist_length(future)	);
}

static GtkWidget* gernel_navigation_construct_tree(
		GernelNavigation *navigation
		)
/*	sets up a tree and its page (to be used by the notebook, e.g.) 	*/
{
	GtkWidget	*tree;

	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);

	tree = gernel_tree_new();

	gtk_signal_connect_object(	GTK_OBJECT(tree), 
					"select_child",
					gernel_navigation_item_selected,
					GTK_OBJECT(navigation)	);
					
	gtk_signal_connect_object(	GTK_OBJECT(tree),
					"select_child",
					gernel_navigation_history,
					GTK_OBJECT(navigation)	);

	gtk_signal_connect_object(	GTK_OBJECT(tree),
					"select_child",
					gernel_navigation_up_possible,
					GTK_OBJECT(navigation)	);

	gtk_signal_connect_object(	GTK_OBJECT(tree),
					"select_child",
					gernel_navigation_down_possible,
					GTK_OBJECT(navigation)	);
	
	gtk_signal_connect_object(	GTK_OBJECT(tree),
					"select_child",
					gernel_navigation_next_possible,
					GTK_OBJECT(navigation)	);

	gtk_signal_connect_object(	GTK_OBJECT(tree),
					"select_child",
					gernel_navigation_previous_possible,
					GTK_OBJECT(navigation)	);

	return tree;
}

static void gernel_navigation_reset(	GernelNavigation *navigation	)
/*	Reset the navigation in preparation for a new kernel source parse */
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	if (navigation->cache) {
		g_cache_destroy(navigation->cache);
		navigation->cache = NULL;
	}

	if (navigation->tree) {
		gtk_widget_destroy(navigation->tree);
		navigation->tree = NULL;
	}
}

void gernel_navigation_parse(	GernelNavigation *navigation,
				GdkWindow *window	)
/*	Parse paths and fill navigation with extracted information;
	navigation_path and contents_path are relative path names to 
	navigational files and help texts	*/
{
	GSList			*lines;
	GCache			*cache;
	gchar			*path;
	GtkWidget		*tree;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(window != NULL);
	g_return_if_fail(navigation->base_directory != NULL);

	gernel_navigation_reset(navigation);
	tree = gernel_navigation_construct_tree(navigation);
	gtk_container_add(GTK_CONTAINER(navigation), tree);
	navigation->tree = tree;
	gtk_widget_show(tree);

	/*	Set up navigational tree	*/
	gernel_navigation_updated(navigation, _("Generating tree..."), NULL);
	path = navigation->navigation_path;
	lines = gernel_navigation_preprocess(navigation, path);
	gernel_navigation_extract_title(navigation, lines);
	lines = gernel_navigation_clear_garbage(lines);

	cache = g_cache_new(	(GCacheNewFunc)gernel_var_item_new,
				(GCacheDestroyFunc)gtk_widget_destroy,
				(GCacheDupFunc)g_strdup,
				(GCacheDestroyFunc)g_free,
				(GHashFunc)g_str_hash,
				(GHashFunc)gernel_var_item_identity,
				(GCompareFunc)match	);

	navigation->cache = cache;
	gernel_navigation_fill_tree(navigation, lines, NULL);

	/*	FIXME: freeing of lines has bugs (and shouldn't be done by
		gernel_navigation_fill_tree() anyway); solve problem later when
		considering remodelling the parser as a GScanner or a Guile
		script	*/

g_print("gernel_navigation_parse:	mark\n");
	gernel_navigation_create_icons(navigation, window);
g_print("gernel_navigation_parse:	mark1\n");
	gernel_navigation_create_labels(navigation);
g_print("gernel_navigation_parse:	mark2\n");

	/*	Fill elements of navigation with contents	*/
	gernel_navigation_updated(navigation, _("Setting help texts..."), NULL);
g_print("gernel_navigation_parse:	mark3\n");
	path = navigation->contents_path;
	lines = gernel_navigation_preprocess(navigation, path);
g_print("gernel_navigation_parse:	mark4\n");
	lines = gernel_navigation_clear_garbage(lines);
g_print("gernel_navigation_parse:	mark5\n");
	gernel_navigation_read_contents(navigation, lines);
g_print("gernel_navigation_parse:	mark6\n");
	g_slist_foreach(lines, (GFunc)g_free, NULL);
g_print("gernel_navigation_parse:	mark7\n");
	g_slist_free(lines);
g_print("gernel_navigation_parse:	done\n");

	/*	Parse and apply default template	*/
	gernel_navigation_updated(navigation, _("Applying defaults..."), NULL);
	path = navigation->template_path;
	/*	FIXME: we currently parse the defconfig files from the kernel
		maintainers; this should only be done as a fallback in case
		<kernel source>/CONFIG_FILENAME does not exist, i.e. the user
		is configuring the kernel for the first time.	*/
	lines = gernel_navigation_preprocess(navigation, path);
	lines = gernel_navigation_clear_garbage_defconfig(lines);
	gernel_navigation_read_defaults(navigation, lines);
	g_slist_foreach(lines, (GFunc)g_free, NULL);
	g_slist_free(lines);

	/*	Check if items meet their conditions	*/
	gernel_navigation_updated(navigation, _("Check conditions..."), NULL);
	gernel_navigation_check_conditions(navigation);

	/*	Check if module settings are allowed	*/
	gernel_navigation_updated(navigation, _("Module dependency..."), NULL);
	gernel_navigation_module_settings(navigation);

	/*	Adjust sensitivity of all items	*/
	gernel_navigation_updated(navigation, _("Set sensitivites..."), NULL);
	gernel_navigation_items_sensitive(navigation);
	gernel_navigation_sync_icons(navigation);
	gernel_navigation_done_parsing(navigation);
}

void gernel_navigation_read_defaults(	GernelNavigation *navigation,
					GSList *lines	)
/*	Get default var_item values from lines and apply them
	FIXME: there may be (and often are) inconsistencies in the defconfig
	file; some variables are mentioned more than once, e.g.; might implement
	a check here or something the like	*/
{
	GSList	*list;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(lines != NULL);

	for (list = lines; list; list = list->next) {
		GernelVarItem	*var_item;
		gchar		*var_name, **linev;

		linev = g_strsplit(list->data, CONFIG_IN_BLANK, 1);
		var_name = linev[0];
		var_item = g_cache_insert(navigation->cache, var_name);
		gernel_var_item_set_value(var_item, linev[1]);
		g_strfreev(linev);
	}
}

static GSList* gernel_navigation_preprocess(	GernelNavigation *navigation,
						gchar *relative_path	)
/*	Preprocess file for parsing	*/
{
	gchar	*contents, *file, *base_directory;
	GSList	*lines;

	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);
	g_return_val_if_fail(relative_path != NULL, NULL);
	g_return_val_if_fail(navigation->base_directory != NULL, NULL);

	gernel_navigation_updated(navigation, NULL, relative_path);
	base_directory = navigation->base_directory;
	file = g_concat_dir_and_file(base_directory, relative_path);

	g_return_val_if_fail(g_file_exists(file), NULL);

	contents = gernel_navigation_read_file(navigation, file);
	g_free(file);
	lines = gernel_navigation_split(contents);
	g_free(contents);

	return lines;
}

static gchar* gernel_navigation_read_file(	GernelNavigation *navigation,
						gchar* path	)
/*	read contents of path	*/
{
	gint		file_descr;
	GIOChannel	*channel;
	guint		bytes;

	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);
	g_return_val_if_fail(path != NULL, NULL);
	g_return_val_if_fail(g_file_exists(path), NULL);

	if ((file_descr = open(path, O_RDONLY)) < 0) {
		gchar	*tmp;

		tmp = g_strconcat(_("Could not open file "), path, NULL);
		gnome_ok_dialog(tmp);
		g_free(tmp);

		return NULL;
	} else {
		guint	size = get_size(path);
		gchar	buffer[size];

		channel = g_io_channel_unix_new(file_descr);
		g_io_channel_read(channel, buffer, size, &bytes);
		g_io_channel_close(channel);
		close(file_descr);
		buffer[size - 1] = '\0';

		return g_strdup(buffer);
	}
}

static GSList* gernel_navigation_split(	gchar* text	)
/*	turns a text into a list of lines	*/
{
	GSList	*result;
	gchar	**lines;
	gint	i;

	g_return_val_if_fail(text != NULL, NULL);

	result = NULL;
	lines = g_strsplit(text, "\n", 0);

	for (i = 0; lines[i]; i++) {
		if (gtk_events_pending()) gtk_main_iteration();
		result = g_slist_append(result, lines[i]);
	}

	return result;
}


static void gernel_navigation_extract_title(	GernelNavigation *navigation,
						GSList *lines	)
/*	extract window title from lines and set window title	*/
{
	GSList	*element;
	gchar	*title, *directory;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(lines != NULL);

	element = g_slist_find_custom(	lines, 
					CONFIG_IN_TITLE, 
					(GCompareFunc)gernel_str_starts	);

	g_return_if_fail(element != NULL);

	title = get_enclosed(element->data, CONFIG_IN_TITLE_MARK);
	directory = gernel_navigation_get_base_directory(navigation);
	navigation->title = g_strconcat(title, " [", directory, "]", NULL);
	g_free(title);
	g_slist_remove_link(lines, element);

	gtk_signal_emit(	GTK_OBJECT(navigation), 
				navigation_signals[TITLE], 
				navigation->title	);
}

GtkWidget* gernel_navigation_get_selection(	GernelNavigation *navigation )
/*	Retrieve the selected item(s) in the navigation tree	*/
{
	GList	*selection;

	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);
	g_return_val_if_fail(navigation->tree != NULL, NULL);
	g_return_val_if_fail(GTK_IS_TREE(navigation->tree), NULL);

	selection = gernel_tree_get_selection(GERNEL_TREE(navigation->tree));

	return selection?GTK_WIDGET(selection->data):NULL;
}

static GtkWidget* gernel_navigation_fill_tree(	GernelNavigation *navigation,
						GSList *source_text,
						GSList *stack	)
/*	builds the tree from source_text; source_text should be squeezed through
	gernel_navigation_clear_garbage first	*/
{
	gchar	*str;
	GCache	*cache;

	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);
	g_return_val_if_fail(navigation->tree != NULL, NULL);

g_print("gernel_navigation_fill_tree:	invoked\n");
	if (source_text == NULL)
{g_print("gernel_navigation_fill_tree:	mark0\n");
 return GTK_WIDGET(navigation->tree);
}
	str = source_text->data;
	cache = navigation->cache;
g_print("gernel_navigation_fill_tree:	mark\n");

	if (match0(str, CONFIG_IN_BRANCH)) {
		GSList	*list;
		gchar	*tmp;

		tmp = g_strstrip(remove_strs(str, CONFIG_IN_BRANCH));
		list = gernel_navigation_preprocess(navigation, tmp);
		g_free(tmp);
		list = gernel_navigation_clear_garbage(list);
		source_text = g_slist_remove(source_text, source_text->data);
		source_text = g_slist_concat(list, source_text);

	} else if (match0(str, CONFIG_IN_SECTION_MARK)) {
		GtkWidget	*tree, *var_item;

		source_text = g_slist_remove(source_text, source_text->data);
		str = g_strstrip(source_text->data);
		tree = navigation->tree;
		var_item = gernel_var_item_new_from_raw(str, stack, cache);

		if (var_item->parent == NULL) {
			gernel_tree_append(GERNEL_TREE(tree), var_item);
			gtk_widget_show(var_item);
		}

		tree = gernel_navigation_construct_tree(navigation);
		gernel_navigation_branch_tree(navigation, tree);
		source_text = g_slist_remove(source_text, source_text->data);

	} else if (match0(str, CONFIG_IN_SECTION_END)) {
		navigation->tree = navigation->tree->parent;
		source_text = g_slist_remove(source_text, source_text->data);

	} else if (	(match0(str, CONFIG_IN_IF)) |
			(match0(str, CONFIG_IN_ELSE)) |
			(match0(str, CONFIG_IN_FI))	) {

		stack = gernel_navigation_manage_stack(	navigation, 
							stack, 
							cache, 
							str	);

		source_text = g_slist_remove(source_text, source_text->data);
	} else if (match0(str, CONFIG_IN_DEFINE)) {
		/*	FIXME: DEFINE variables not yet taken care of	*/
		source_text = g_slist_remove(source_text, source_text->data);
	} else {
		GtkWidget	*var_item, *tree;

		tree = navigation->tree;
		var_item = gernel_var_item_new_from_raw(str, stack, cache);

		if ((var_item != NULL) && (var_item->parent == NULL)) {
			gernel_tree_append(GERNEL_TREE(tree), var_item);
			gtk_widget_show(var_item);
		}

		source_text = g_slist_remove(source_text, source_text->data);
	}

	navigation->cache = cache;

	return gernel_navigation_fill_tree(navigation, source_text, stack);
}

static void gernel_navigation_branch_tree(	GernelNavigation *navigation,
						GtkWidget *branch	)
/*	Make tree a sub tree of (current) last item of navigation's tree */
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(branch != NULL);
	g_return_if_fail(GERNEL_IS_TREE(branch));

	gernel_tree_branch(GERNEL_TREE(navigation->tree), branch);
	navigation->tree = branch;
}

static GSList* gernel_navigation_clear_garbage(	GSList* list	)
/*	remove bash style comments, empty lines, tabs; join wrapped lines;
	join separated 'if [...] \n then' lines	*/
{
	gchar	*line;

	/*	skip lines that are NULL	*/
	while ((list != NULL) && (list->data == NULL)) 
		list = g_slist_remove(list, list->data);

	if (list == NULL) return NULL;

	line = g_strdup(g_strstrip(list->data));
	g_strdelimit(line, "	", ' ');

	while (is_wrapped(line)) {
		gchar	*first, *second;

		first = unwrap(line);
		list = g_slist_remove(list, list->data);

		second = list->data;
		g_strdelimit(second, "	", ' ');
		g_strstrip(second);

		line = g_strconcat(first, CONFIG_IN_BLANK, second, NULL);
		g_free(first);
		g_free(second);
	}

	if (	(list->next != NULL) &&
		match0(g_strstrip(G_CHAR(list->next->data)), CONFIG_IN_THEN) ) {
		gchar	*new_line;

		new_line = g_strconcat(	line, 
					CONFIG_IN_END_OF_COND, 
					CONFIG_IN_BLANK,
					CONFIG_IN_THEN, 
					NULL	);

		g_free(line);
		line = new_line;
		list = g_slist_remove(list, list->data);
	}

	if (	(strlen(line) == 0) || 
		(match0(line, CONFIG_IN_COMMENT)) ||
		(match0(line, CONFIG_IN_MAKE))	) {
		g_free(line);
		line = NULL;
	} else if (match0(line, CONFIG_IN_IF)) {
		gchar	*tmp;

		tmp = line;
		line = check_syntax(tmp);
		g_free(tmp);
	}

	list = g_slist_remove(list, list->data);

	if (line)
		return g_slist_prepend(	gernel_navigation_clear_garbage(list), 
					line	);
	else
		return gernel_navigation_clear_garbage(list);
}

static void gernel_navigation_read_contents(	GernelNavigation *navigation,
						GSList *lines	)
/*	reads the next variable help text from lines	*/
{
	GString			*help_text;
	GernelVarItem		*this_var = NULL;
	GCache			*cache;
	GernelTreeSearchFunc	function;
	GtkWidget		*tree, *item;
	GSList			*line;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	tree = navigation->tree;
	cache = navigation->cache;
	help_text = g_string_new(NULL);

	function = GERNEL_TREE_SEARCH_FUNC(
			gernel_var_item_compare_equals_description_sensitive
			);

	item = gernel_tree_find(GERNEL_TREE(tree), function, lines->data);
	this_var = GERNEL_VAR_ITEM(item);

	for (line = lines; line; line = line->next) {

		if (match0(line->data, CONFIG_IN_CONFIG)) {
			gchar	*str;

			gernel_var_item_set_long_description(	this_var, 
								help_text->str);

			g_string_free(help_text, TRUE);
			help_text = g_string_new(NULL);
			str = g_strstrip(line->data);
			this_var = GERNEL_VAR_ITEM(g_cache_insert(cache, str));
		}

		/*	EOF	*/
		if (line->next == NULL) break;

		if (	(!match0(line->data, CONFIG_IN_COMMENT)) &
			(!match0(line->data, CONFIG_IN_CONFIG)) &
			(!match0(line->next->data, CONFIG_IN_CONFIG))	) {

			help_text = g_string_append(	help_text, 
							g_strstrip(line->data));

			if (strlen(line->data) == 0)
				help_text = g_string_append_c(help_text, '\n');
			else
				help_text = g_string_append_c(help_text, ' ');
		}
	}	

	gernel_var_item_set_long_description(this_var, help_text->str);
}

static void gernel_navigation_view_changed(	GernelNavigation *navigation,
						GtkWidget *widget	)
/*	Emit the 'view_changed' signal	*/
{
	GernelVarItem	*var_item;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(widget));

	var_item = GERNEL_VAR_ITEM(widget);

	gtk_signal_emit(	GTK_OBJECT(navigation),
				navigation_signals[VIEW_CHANGED],
				gernel_var_item_get_description(var_item),
				gernel_var_item_get_name(var_item),
				gernel_var_item_get_text(var_item)	);
}

static void gernel_navigation_item_selected(	GernelNavigation *navigation,
						GernelVarItem *var_item,
						GtkWidget *tree	)
/*	Emit the "dependants" and " "prerequisites" signals	*/
{
	GtkObject	*object;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GTK_IS_TREE(tree));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	object = GTK_OBJECT(navigation);

	if (gernel_navigation_get_selection(navigation)) {
		gtk_signal_emit(object, navigation_signals[SELECTED], var_item);

		gtk_signal_emit(object, 
				navigation_signals[DEPENDANTS], 
				var_item->dependants?TRUE:FALSE	);

		gtk_signal_emit(object, 
				navigation_signals[PREREQUISITES], 
				var_item->condition?TRUE:FALSE	);
	} else {
		gtk_signal_emit(object, navigation_signals[UNSELECTED]);
		gtk_signal_emit(object, navigation_signals[DEPENDANTS], FALSE);

		gtk_signal_emit(	object, 
					navigation_signals[PREREQUISITES], 
					FALSE	);
	}
}

GSList* gernel_navigation_clear_garbage_defconfig(	GSList *lines	)
/*	Remove unneeded stuff from lines	*/
{
	GSList	*list;

	list = lines;

	if (list == NULL)
		return NULL;
	else if (strlen(list->data) == 0)
		return gernel_navigation_clear_garbage_defconfig(list->next);

	else if (	(match0(list->data, DEFCONFIG_COMMENT)) &
			(strstr(list->data, DEFCONFIG_IS_NOT_SET) == NULL) ) {
		/*	crap	*/
		gchar	*str;

		str = list->data;
		list = g_slist_remove(list, str);
		g_free(str);

		return gernel_navigation_clear_garbage_defconfig(list);

	} else if (match0(list->data, DEFCONFIG_COMMENT)) {
		/*	"is not set"	*/
		gchar	**tmpv, *tmp, *new;

		tmpv = g_strsplit(list->data, CONFIG_IN_BLANK, 0);
		tmp = tmpv[1];
		new = g_strconcat(tmp, CONFIG_IN_BLANK, CONFIG_IN_NO, NULL);
		g_strfreev(tmpv);
		tmp = list->data;
		list = g_slist_remove(list, tmp);
		g_free(tmp);

		return g_slist_prepend(
				gernel_navigation_clear_garbage_defconfig(list),
				new
				);

	} else {
		/*	standard definition	*/
		list->data = g_strdelimit(list->data, "=", ' ');

		return g_slist_prepend(
			gernel_navigation_clear_garbage_defconfig(list->next),
			list->data
			);
	}
}

static void gernel_navigation_updated(	GernelNavigation *navigation,
					gchar *message1,
					gchar *message2	)
/*	Emit the 'updated' signal	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(message1 || message2);

	gtk_signal_emit(	GTK_OBJECT(navigation), 
				navigation_signals[UPDATED], 
				message1,
				message2	);
}

void gernel_navigation_set_contents_path(	GernelNavigation *navigation,
						gchar *path	)
/*	Set the relative path to the file containing the information texts the
	navigation will be used to navigate through	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(path != NULL);
	g_return_if_fail(!g_path_is_absolute(path));

	if (navigation->contents_path) g_free(navigation->contents_path);
	navigation->contents_path = g_strdup(path);
}

void gernel_navigation_set_navigation_path(	GernelNavigation *navigation,
						gchar *path	)
/*	Set the relative path to the file containing the data specifying node
	information for the navigation's tree	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(path != NULL);
	g_return_if_fail(!g_path_is_absolute(path));

	if (navigation->navigation_path) g_free(navigation->navigation_path);
	navigation->navigation_path = g_strdup(path);
}

void gernel_navigation_set_template_path(	GernelNavigation *navigation,
						gchar *path	)
/*	Set the relative path to the file containing the default settings for
	the variables displayed in the navigation's tree	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(path != NULL);
	g_return_if_fail(!g_path_is_absolute(path));

	if (navigation->template_path) g_free(navigation->template_path);
	navigation->template_path = g_strdup(path);
}

void gernel_navigation_set_base_directory(	GernelNavigation *navigation,
						gchar *path	)
/*	Set the 'base_directory' field of navigation; files parsed in future
	should be relative to this path. directory will be duplicated and thus
	can be freed if appropriate	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(path != NULL);
	g_return_if_fail(g_path_is_absolute(path));
	g_return_if_fail(g_file_exists(path));

	if (!gernel_navigation_legitimate_path(navigation, path)) return;

	if (navigation->base_directory != NULL) 
		g_free(navigation->base_directory);

	navigation->base_directory = g_strdup(path);
}

static gchar* gernel_navigation_get_base_directory(
		GernelNavigation *navigation
		)
/*	Get the base directory	*/
{
	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);

	return navigation->base_directory;
}

static void gernel_navigation_done_parsing(	GernelNavigation *navigation )
/*	Emit the 'done_parsing' signal	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	gtk_signal_emit(	GTK_OBJECT(navigation), 
				navigation_signals[DONE_PARSING]);
}

static void gernel_navigation_check_conditions(	GernelNavigation *navigation )
/*	Check all items in the navigation's tree for whether they meet their
	prerequisities	*/
{
	GernelVarItemFunc	function;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_check_condition);
	gernel_navigation_foreach(navigation, navigation->tree, function, NULL);
}

static void gernel_navigation_create_icons(	GernelNavigation *navigation,
						GdkWindow *window	)
/*	Create the appropriate icons for the navigation tree's items	*/
{
	GernelVarItemFunc	function;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(window != NULL);

	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_icons);

	gernel_navigation_foreach(	navigation, 
					navigation->tree, 
					function,
					window	);
}

static void gernel_navigation_create_labels(	GernelNavigation *navigation )
/*	Create the description labels for navigation's items	*/
{
	GernelVarItemFunc	function;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_description);
	gernel_navigation_foreach(navigation, navigation->tree, function, NULL);
}

static void gernel_navigation_module_settings(	GernelNavigation *navigation )
/*	Check if module settings are allowed and synchronize items
	accordingly	*/
{
	GtkWidget		*tree;
	GernelVarItem		*var_item;
	GernelVarItemFunc	function;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	tree = navigation->tree;
	var_item = g_cache_insert(navigation->cache, MODULE_ITEM);
	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_allow_module);
	gernel_navigation_foreach(navigation, tree, function, var_item);
	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_mod_dep_sync);
	gernel_navigation_foreach(navigation, tree, function, navigation->tree);

	gtk_signal_connect_object(	GTK_OBJECT(var_item),
					"value_changed",
					gernel_navigation_allow_module,
					GTK_OBJECT(navigation)	);

	gtk_signal_connect_object(	GTK_OBJECT(var_item),
					"value_changed",
					gernel_navigation_sync_icons,
					GTK_OBJECT(navigation)	);
}

static void gernel_navigation_allow_module(	GernelNavigation *navigation,
						GernelVarItem *var_item	)
/*	[Dis]Allow all variables in navigation's tree to take on the 'module'
	setting; var_item is the unique 'MODULE_ITEM'	*/
{
	GernelVarItemFunc	function;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_allow_module);

	gernel_navigation_foreach(	navigation, 
					navigation->tree, 
					function,
					var_item	);
}

static void gernel_navigation_sync_icons(	GernelNavigation *navigation )
/*	Synchronize correct icons of navigation's tree	*/
{
	GernelVarItemFunc	function;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_icon_sync);

	gernel_navigation_foreach(	navigation,
					navigation->tree,
					function,
					NULL	);
}

static void gernel_navigation_items_sensitive(	GernelNavigation *navigation )
/*	Adjust sensitivity of navigation tree's items as appropriate	*/
{
	GernelVarItemFunc	function;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	function = GERNEL_VAR_ITEM_FUNC(gernel_var_item_sens_sync);
	gernel_navigation_foreach(navigation, navigation->tree, function, NULL);
}

static gboolean gernel_navigation_legitimate_path(
		GernelNavigation *navigation, 
		gchar *path
		)
/*	Test if path is a real linux kernel source path	*/
{
	GString	*message;
	gchar	*tmp;

	g_return_val_if_fail(navigation != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), FALSE);
	g_return_val_if_fail(path != NULL, FALSE);

	tmp = g_concat_dir_and_file(path, DOCUMENTATION_DIRNAME);

	if (g_file_exists(tmp)) {
		g_free(tmp);

		return TRUE;
	}

	g_free(tmp);
	message = g_string_new(NULL);

	g_string_sprintf(	message,
				_("'%s' does not seem to be a kernel source path"),
				path	);

	gnome_ok_dialog(message->str);
	g_string_free(message, TRUE);

	return FALSE;
}

void gernel_navigation_next(	GernelNavigation *navigation	)
/*	Scroll to the next item in the tree	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(gernel_navigation_get_selection(navigation) != NULL);

	gernel_navigation_scroll(navigation, GERNEL_NAVIGATION_SCROLL_NEXT);
}

void gernel_navigation_previous(	GernelNavigation *navigation	)
/*	Scroll to the previous item in the tree	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(gernel_navigation_get_selection(navigation) != NULL);

	gernel_navigation_scroll(navigation, GERNEL_NAVIGATION_SCROLL_PREVIOUS);
}

void gernel_navigation_up(	GernelNavigation *navigation	)
/*	Scroll to the higher level tree	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	gernel_navigation_scroll(navigation, GERNEL_NAVIGATION_SCROLL_UP);
}

void gernel_navigation_down(	GernelNavigation *navigation	)
/*	Scroll to the lower level tree	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	gernel_navigation_scroll(navigation, GERNEL_NAVIGATION_SCROLL_DOWN);
}

static void gernel_navigation_scroll(
		GernelNavigation *navigation,
		enum GernelNavigationScrollType scroll_type
		)
/*	Scroll navigation's tree as specified by scroll_type	*/
{

	GtkWidget	*current;
	GernelTree	*tree;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	tree = GERNEL_TREE(navigation->tree);

	switch (scroll_type) {
		case GERNEL_NAVIGATION_SCROLL_NEXT:
			gernel_tree_select_next(tree);
			break;
		case GERNEL_NAVIGATION_SCROLL_PREVIOUS:
			gernel_tree_select_previous(tree);
			break;
		case GERNEL_NAVIGATION_SCROLL_UP:	
			gernel_tree_select_up(tree);
			break;
		case GERNEL_NAVIGATION_SCROLL_DOWN:
			gernel_tree_select_down(tree);
			break;
		default:
			break;
	}

	current = gernel_navigation_get_selection(navigation);
}

static void gernel_navigation_up_possible(	GernelNavigation *navigation )
/*	Emit the 'up_possible' signal	*/
{
	GtkWidget	*selection;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	selection = gernel_navigation_get_selection(navigation);

	if ((selection != NULL) && (navigation->tree != selection->parent))

		gtk_signal_emit(	GTK_OBJECT(navigation),
					navigation_signals[UP_POSSIBLE],
					TRUE	);
	else

		gtk_signal_emit(	GTK_OBJECT(navigation), 
					navigation_signals[UP_POSSIBLE], 
					FALSE	);
}

static void gernel_navigation_down_possible(	GernelNavigation *navigation )
/*	Emit the 'down_possible' signal	*/
{
	GtkWidget	*selection;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	selection = gernel_navigation_get_selection(navigation);

	if (selection != NULL) {
		gboolean	flag;
		GernelTree	*tree;

		tree = GERNEL_TREE(navigation->tree);
		flag = gernel_tree_is_interior_node(tree, selection);

		gtk_signal_emit(	GTK_OBJECT(navigation),
					navigation_signals[DOWN_POSSIBLE],
					flag	);

	} else

		gtk_signal_emit(	GTK_OBJECT(navigation),
					navigation_signals[DOWN_POSSIBLE],
					FALSE	);
}

static void gernel_navigation_next_possible(	GernelNavigation *navigation )
/*	Emit the 'next_possible' signal	*/
{
	GtkWidget	*selection;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	selection = gernel_navigation_get_selection(navigation);

	if (selection) {
		GtkWidget	*next;

		next = gernel_tree_next(GERNEL_TREE(navigation->tree));

		gtk_signal_emit(	GTK_OBJECT(navigation),
					navigation_signals[NEXT_POSSIBLE],
					next != NULL	);
	} else
		gtk_signal_emit(	GTK_OBJECT(navigation), 
					navigation_signals[NEXT_POSSIBLE], 
					FALSE	);
}

static void gernel_navigation_previous_possible(
		GernelNavigation *navigation
		)
/*	Emit the 'previous_possible' signal	*/
{
	GtkWidget	*selection;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	selection = gernel_navigation_get_selection(navigation);

	if (selection != NULL) {
		GtkWidget	*previous;

		previous = gernel_tree_previous(GERNEL_TREE(navigation->tree));

		gtk_signal_emit(	GTK_OBJECT(navigation),
					navigation_signals[PREVIOUS_POSSIBLE],
					previous != NULL	);
	} else

		gtk_signal_emit(	GTK_OBJECT(navigation),
					navigation_signals[PREVIOUS_POSSIBLE],
					FALSE	);
}

void gernel_navigation_back(	GernelNavigation *navigation	)
/*	Move back in history	*/
{
	GtkWidget	*var_item;
	GSList		*history, *future;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	history = navigation->history;
	future = navigation->future;

	future = push(future, history->data);
	history = history->next;
	var_item = GTK_WIDGET(history->data);
	navigation->history = history;
	navigation->future = future;
	gernel_tree_select_item(GERNEL_TREE(navigation->tree), var_item);
}

void gernel_navigation_forward(	GernelNavigation *navigation	)
/*	Move forward in history	*/
{
	GSList		*history, *future;
	GtkWidget	*var_item;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	history = navigation->history;
	future = navigation->future;
	history = push(history, future->data);
	future = future->next;
	var_item = GTK_WIDGET(history->data);
	navigation->history = history;
	navigation->future = future;
	gernel_tree_select_item(GERNEL_TREE(navigation->tree), var_item);
}

static void gernel_navigation_size_request(	GtkWidget *widget,
						GtkRequisition *requisition )
/*	Implements the default size requisition handler	*/
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(widget));

	(*GTK_WIDGET_CLASS(parent_class)->size_request)(widget, requisition);
/*g_print("gernel_navigation_size_request:	width == %d\n", requisition->width);*/
}

static void gernel_navigation_size_allocate(	GtkWidget *widget,
						GtkAllocation *allocation )
/*	Implements the default size allocation handler	*/
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(widget));

/*g_print("gernel_navigation_size_allocate:	width == %d\n", allocation->width);*/
	(*GTK_WIDGET_CLASS(parent_class)->size_allocate)(widget, allocation);
}

void gernel_navigation_prerequisites(	GernelNavigation *navigation	)
/*	create a condition tree for currently selected item and show it inside a
	dialog	*/
{
	GtkWidget	*dependency, *var_item;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	var_item = gernel_navigation_get_selection(navigation);

	dependency = gernel_dependency_new(
			GERNEL_DEPENDENCY_MODE_PREREQUISITES,
			var_item
			);

	gernel_dependency_set_title(
			GERNEL_DEPENDENCY(dependency),
			gernel_var_item_get_name(GERNEL_VAR_ITEM(var_item))
			);

	gtk_widget_show_all(dependency);
}

void gernel_navigation_dependants(	GernelNavigation *navigation	)
/*	Create a visible list of dependants inside a dialog	*/
{
	GtkWidget	*dependency, *var_item;
	GSList		*list;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	var_item = gernel_navigation_get_selection(
			GERNEL_NAVIGATION(navigation)
			);

	dependency = gernel_dependency_new(
			GERNEL_DEPENDENCY_MODE_DEPENDANTS,
			var_item
			);

	gernel_dependency_set_title(
			GERNEL_DEPENDENCY(dependency), 
			gernel_var_item_get_name(GERNEL_VAR_ITEM(var_item))
			);

	for (	list = GERNEL_VAR_ITEM(var_item)->dependants; 
		list; 
		list = list->next	) {

		GernelVarItem	*item;

		item = GERNEL_VAR_ITEM(list->data);

		gernel_dependency_add_info(	GERNEL_DEPENDENCY(dependency), 
						gernel_var_item_get_name(item));
	}

	gtk_widget_show_all(dependency);
}

static void gernel_navigation_foreach(	GernelNavigation *navigation,
					GtkWidget *tree,
					GernelVarItemFunc function,
					gpointer data	)
/*	recursively traverses tree and applies func to each of its elements */
{
	GList		*list;
	GernelTree	*branch;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));
	g_return_if_fail(function != NULL);

	branch = GERNEL_TREE(tree);

	for (list = gernel_tree_children(branch); list; list = list->next) {
		GtkWidget	*var_item;

		var_item = GTK_WIDGET(list->data);

		if (gernel_tree_is_branch(branch, var_item)) {
			GtkWidget	*widget;

			widget = gernel_tree_get_branch(branch, var_item);

			gernel_navigation_foreach(	navigation, 
							widget, 
							function, 
							data	);
		}

		(*function)(GERNEL_VAR_ITEM(var_item), data);
	}
}

static void gernel_navigation_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelNavigation	*navigation;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(object));

	navigation = GERNEL_NAVIGATION(object);

	switch (arg_id) {
		GtkWidget	*widget;
		gchar		*str;

		case ARG_LOCAL_NAME:
			str = gernel_navigation_get_local_name(navigation);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_PREFERENCES:
			widget = gernel_navigation_preferences(navigation);
			GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(widget);
			break;
		case ARG_SHOW_NAVIGATION:
			widget = GTK_WIDGET(navigation);
			GTK_VALUE_BOOL(*arg) = GTK_WIDGET_VISIBLE(widget);
			break;
		case ARG_HIDE_UNMET:
			GTK_VALUE_BOOL(*arg) = navigation->hide_unmet;
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void gernel_navigation_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelNavigation	*navigation;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(object));

	navigation = GERNEL_NAVIGATION(object);

	switch (arg_id) {
		gboolean	flag;
		gchar		*string;

		case ARG_LOCAL_NAME:
			string = GTK_VALUE_STRING(*arg);
			gernel_navigation_set_local_name(navigation, string);
			break;
		case ARG_SHOW_NAVIGATION:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_navigation_show_navigation(navigation, flag);
			break;
		case ARG_HIDE_UNMET:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_navigation_set_hide_unmet(navigation, flag);
			break;
		case ARG_PREFERENCES:	/*	Fall Through	*/
		default:
			break;
	}
}

static void gernel_navigation_set_hide_unmet(	GernelNavigation *navigation,
						gboolean flag	)
/*	Define whether items (leaves) whose preconditions are not met should
	be visible	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

g_print("gernel_navigation_set_hide_unmet:	FIXME: implement\n");
	navigation->hide_unmet = flag;
}

static GtkWidget* gernel_navigation_preferences(
		GernelNavigation *navigation
		)
/*	Assemble a page to be used in a GnomeApp's property box	*/
{
	GtkWidget		*page, *general, *prerequisite, *widget,
				*leaves;

	GtkContainer		*container;

	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);

	widget = GTK_WIDGET(navigation);

	page = erty_preference_page_new();
	container = GTK_CONTAINER(page);
	general = erty_preference_section_new(_("General"));

	prerequisite = erty_preference_section_add(
			ERTY_PREFERENCE_SECTION(general),
			widget,
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Show Navigation"),
			ARG_SHOW_NAVIGATION
			);

	gtk_container_add(container, general);
	leaves = erty_preference_section_new(_("Display Attributes"));

	erty_preference_section_add(
			ERTY_PREFERENCE_SECTION(leaves),
			widget,
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Show items only if their preconditions are met"),
			ARG_HIDE_UNMET
			);

	gtk_container_add(container, leaves);

	erty_preference_section_add_dependencies(
			ERTY_PREFERENCE_SECTION(general),
			prerequisite,
			leaves,
			NULL
			);

	return page;
}

static void gernel_navigation_show_navigation(	GernelNavigation *navigation,
						gboolean flag	)
/*	Show/hide navigation according to flag	*/
{
	GtkWidget	*widget;

	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	widget = GTK_WIDGET(navigation);

	if (flag)
		gtk_widget_show(widget);
	else
		gtk_widget_hide(widget);
}

static void gernel_navigation_set_local_name(	GernelNavigation *navigation,
						gchar *name	)
/*	Set the local name of navigation	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));

	navigation->local_name = name;
}

static gchar* gernel_navigation_get_local_name(
	GernelNavigation *navigation
	)
/*	Get the local name of navigation	*/
{
	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);

	return navigation->local_name;
}

static GSList* gernel_navigation_manage_stack(	GernelNavigation *navigation,
						GSList *stack,
						GCache *cache,
						gchar *input	)
/*	stack is used for tracing variable dependancies: vars mentioned in if-
	conditions will be pushed to the stack; all vars following then will be
	mentioned in all of the stack's vars.
	input decides whether variables will be pushed or pop to/fro the stack;
 */
{
	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);

g_print("gernel_navigation_manage_stack:	invoked\n");
	if (match0(input, CONFIG_IN_IF)) {
		gchar	**expr, *tmp;

g_print("gernel_navigation_manage_stack:	mark1\n");
		/*	extract /expr/ from if condition	*/
		tmp = g_strdelimit(input, "[", ']');
		expr = g_strsplit(input, "]", 0);
		tmp = g_strstrip(expr[1]);
		stack = push(stack, create_stack_item(NULL, tmp, cache));
		g_strfreev(expr);
	} else if (match0(input, CONFIG_IN_FI)) {
g_print("gernel_navigation_manage_stack:	mark2\n");

		g_return_val_if_fail(stack != NULL, NULL);

		stack = stack->next;
g_print("gernel_navigation_manage_stack:	mark2.5\n");
	} else if (match0(input, CONFIG_IN_ELSE)) {
		GNode	*condition, *negation;
g_print("gernel_navigation_manage_stack:	mark3\n");

		g_return_val_if_fail(stack != NULL, NULL);
		g_return_val_if_fail(stack->data != NULL, NULL);
		g_return_val_if_fail(stack->next != NULL, NULL);

		condition = stack->data;
		stack = stack->next;

		negation = gernel_navigation_negate_expression(	navigation, 
								condition );
g_print("gernel_navigation_manage_stack:	mark3.2\n");

		stack = push(stack, negation);
g_print("gernel_navigation_manage_stack:	mark3.4\n");
	} else
g_print("gernel_navigation_manage_stack:	ignoring '%s'\n", input);
g_print("gernel_navigation_manage_stack:	done\n");

	return stack;
}

static GNode* gernel_navigation_negate_expression(
		GernelNavigation *navigation,
		GNode *condition
		)
/*	negate the boolean expression tree condition	*/
{
	GNode	*invert;

	g_return_val_if_fail(navigation != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_NAVIGATION(navigation), NULL);
	g_return_val_if_fail(condition != NULL, NULL);

	invert = g_node_new(GINT_TO_POINTER(OPERATOR_NOT));
g_print("gernel_navigation_negate_expression:	FIXME: uncomment line\n");
/*	g_node_append(invert, gernel_node_copy(condition));	*/

	return invert;
}

void gernel_navigation_save_as(	GernelNavigation *navigation,
				gchar *path	)
/*	Save current configuration under 'path'	*/
{
	g_return_if_fail(navigation != NULL);
	g_return_if_fail(GERNEL_IS_NAVIGATION(navigation));
	g_return_if_fail(path != NULL);
	g_return_if_fail(g_file_exists(path));

g_print("gernel_navigation_save_as:	path == %s\n", path);
}
