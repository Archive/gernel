#define GERNEL_TYPE_MENU_BAR	(gernel_menu_bar_get_type())

#define	GERNEL_MENU_BAR(obj) \
	(GTK_CHECK_CAST(	(obj), \
				GERNEL_TYPE_MENU_BAR, \
				GernelMenuBar	))

#define	GERNEL_MENU_BAR_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_MENU_BAR, \
				GernelMenuBarClass	))

#define GERNEL_IS_MENU_BAR(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_MENU_BAR))

#define GERNEL_IS_MENU_BAR_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_MENU_BAR))

typedef struct _GernelMenuBar		GernelMenuBar;
typedef struct _GernelMenuBarClass	GernelMenuBarClass;

struct _GernelMenuBar {
	GtkMenuBar	menu_bar;

	GtkWidget	*dependants, *prerequisites, *template, *back, 
			*forward, *save, *save_as,
			*next, *up, *down, *previous, *reload, *find, *about,
			*load_directory, *new_window, *preferences, *compile;

	GnomeUIInfo	*uiinfo;
};

struct _GernelMenuBarClass {
	GtkMenuBarClass		parent_class;

	void (*directory)	(	GernelMenuBar *menu_bar,
					gchar *directory	);

	void (*next)		(	GernelMenuBar *menu_bar	);
	void (*previous)	(	GernelMenuBar *menu_bar	);
	void (*up)		(	GernelMenuBar *menu_bar	);
	void (*down)		(	GernelMenuBar *menu_bar	);
	void (*back)		(	GernelMenuBar *menu_bar	);
	void (*forward)		(	GernelMenuBar *menu_bar	);
	void (*find)		(	GernelMenuBar *menu_bar	);
	void (*prerequisites)	(	GernelMenuBar *menu_bar	);
	void (*dependants)	(	GernelMenuBar *menu_bar	);
	void (*preferences)	(	GernelMenuBar *menu_bar	);
	void (*compile)		(	GernelMenuBar *menu_bar	);
	void (*new_window)	(	GernelMenuBar *menu_bar	);
	void (*save)		(	GernelMenuBar *menu_bar	);
	void (*save_as)		(	GernelMenuBar *menu_bar	);
};

GtkType		gernel_menu_bar_get_type(	void	);
GtkWidget*	gernel_menu_bar_new(	GtkWidget *window	);

void		gernel_menu_bar_set_dependants(	GernelMenuBar *menu_bar,
						gboolean flag	);

void		gernel_menu_bar_set_prerequisites(	GernelMenuBar *menu_bar,
							gboolean flag	);

void		gernel_menu_bar_set_save_as(	GernelMenuBar *menu_bar,
						gboolean flag	);

void		gernel_menu_bar_set_history(	GernelMenuBar *menu_bar,
						guint history,
						guint future	);

void 		gernel_menu_bar_set_up(	GernelMenuBar *menu_bar,
					gboolean flag	);

void		gernel_menu_bar_set_down(	GernelMenuBar *menu_bar,
						gboolean flag	);

void		gernel_menu_bar_set_next(	GernelMenuBar *menu_bar,
						gboolean flag	);

void		gernel_menu_bar_set_previous(	GernelMenuBar *menu_bar,
						gboolean flag	);
