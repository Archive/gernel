#include <gnome.h>
#include <gernel_canvas_text.h>

/*	Object argument IDs	*/
enum {	ARG_0,
	ARG_Y1,
	ARG_Y2,
	ARG_X1,
	ARG_X2,
	ARG_FONTSET,
	ARG_TEXT	};

static void 	gernel_canvas_text_class_init(	GernelCanvasTextClass *klass );
static void	gernel_canvas_text_init(	GernelCanvasText *canvas_text );
static void	gernel_canvas_text_update(
		GernelCanvasText *canvas_text
		);

static void gernel_canvas_text_set_gc(	GernelCanvasText *canvas_text	);

static gint	gernel_canvas_text_get_font_height(
		GernelCanvasText *canvas_text
		);

static void	gernel_canvas_text_set_real_width(
		GernelCanvasText *canvas_text
		);

static gint gernel_canvas_text_get_bottom_margin(
		GernelCanvasText *canvas_text
		);

static void	gernel_canvas_text_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void	gernel_canvas_text_set_height(	GernelCanvasText *canvas_text,
						gint height	);

static void gernel_canvas_text_draw(	GnomeCanvasItem *item, 
					GdkDrawable *drawable, 
					int x, 
					int y, 
					int width, 
					int height	);

static GnomeIconTextItemClass	*parent_class = NULL;

GtkType gernel_canvas_text_get_type(	void	)
/*	Type retriever for GernelCanvasText	*/
{
        static GtkType text_type = 0;

        if (!text_type) {
                GtkTypeInfo text_info = {
                        "GernelCanvasText",
                        sizeof (GernelCanvasText),
                        sizeof (GernelCanvasTextClass),
                        (GtkClassInitFunc)gernel_canvas_text_class_init,
                        (GtkObjectInitFunc)gernel_canvas_text_init,
                        NULL, /* reserved_1 */
                        NULL, /* reserved_2 */
                        (GtkClassInitFunc) NULL
                };

                text_type = gtk_type_unique(	gnome_icon_text_item_get_type(),
						&text_info	);
        }

        return text_type;
}

static void gernel_canvas_text_init(	GernelCanvasText *canvas_text	)
/*	Initialize canvas_text	*/
{
	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));

	canvas_text->width = 0.0;
	canvas_text->text = NULL;
	canvas_text->x = (double)GNOME_PAD_SMALL;
	canvas_text->y = 0.0;
	canvas_text->right_margin = (double)GNOME_PAD_SMALL;
	canvas_text->bottom_margin = 0.0;
	canvas_text->gc = NULL;
	canvas_text->color = 0;
	canvas_text->color_change_pending = FALSE;
}

GernelCanvasText* gernel_canvas_text_new(	GnomeCanvasGroup *group	)
/*	Create an instance of GernelCanvasText	*/
{
	GnomeCanvasItem		*canvas_item;
	GernelCanvasText	*canvas_text;

	g_return_val_if_fail(group != NULL, NULL);
	g_return_val_if_fail(GNOME_IS_CANVAS_GROUP(group), NULL);

	canvas_item = gnome_canvas_item_new(
			group, 
			gernel_canvas_text_get_type(),
			NULL
			);

	canvas_text = GERNEL_CANVAS_TEXT(canvas_item);

	return canvas_text;
}

static void gernel_canvas_text_class_init(	GernelCanvasTextClass *class )
/*	Class initializer for GernelCanvasText	*/
{
	GtkObjectClass		*object_class;
	GnomeCanvasItemClass	*canvas_item_class;

	object_class = (GtkObjectClass *)class;
	canvas_item_class = (GnomeCanvasItemClass*)class;

	parent_class = gtk_type_class(GNOME_TYPE_ICON_TEXT_ITEM);

	gtk_object_add_arg_type(	"GernelCanvasText::text",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_TEXT	);

	gtk_object_add_arg_type(	"GernelCanvasText::fontset",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_FONTSET	);

	gtk_object_add_arg_type(	"GernelCanvasText::y2",
					GTK_TYPE_DOUBLE,
					GTK_ARG_READABLE,
					ARG_Y2	);

	gtk_object_add_arg_type(	"GernelCanvasText::x1",
					GTK_TYPE_DOUBLE,
					GTK_ARG_READWRITE,
					ARG_X1	);

	gtk_object_add_arg_type(	"GernelCanvasText::x2",
					GTK_TYPE_DOUBLE,
					GTK_ARG_READWRITE,
					ARG_X2	);

	object_class->set_arg = gernel_canvas_text_set_arg;

	canvas_item_class->draw = gernel_canvas_text_draw;
}

void gernel_canvas_text_set_width(	GernelCanvasText *canvas_text,
					gint width	)
/*	Set the maximal width of canvas_text; if the width of canvas_text is 
	more than it needs to display the text, GnomeIconTextItem centers it;
	we avoid this by calling gernel_canvas_text_set_width() again at the
	end in case the actual width is less than the maximum width
	FIXME: could be done more efficiently	*/
/*	NOTE: 'width' should contain 'x' offset already. It's a bit 
	cumbersome...	*/
{
	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));
	g_return_if_fail(width >= 0);

	if (canvas_text->text != NULL) {
		g_assert(width >= canvas_text->x);
		g_assert(width >= canvas_text->x +canvas_text->right_margin);

		canvas_text->width = width - canvas_text->x;
		canvas_text->width -= canvas_text->right_margin;
	} else
		canvas_text->width = 0;

	gernel_canvas_text_set_real_width(canvas_text);
}

static void gernel_canvas_text_set_real_width(	GernelCanvasText *canvas_text )
/*	Set the actual width of canvas_text	*/
{
	GnomeIconTextItem	*icon_text_item;

	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));
	
	icon_text_item = GNOME_ICON_TEXT_ITEM(canvas_text);

	if (canvas_text->text != NULL) {
		GnomeCanvasItem	*canvas_item;
		double		x1, y1, y2, x2;
		gint		actual_width;

		canvas_item = GNOME_CANVAS_ITEM(icon_text_item);
		gnome_canvas_item_show(canvas_item);
		gernel_canvas_text_update(canvas_text);
		gnome_canvas_item_get_bounds(canvas_item, &x1, &y1, &x2, &y2);
		gernel_canvas_text_set_height(canvas_text, y2 - y1);
		actual_width = (gint)(x2 - x1);

		if (actual_width < canvas_text->width) {
			canvas_text->width = (double)actual_width;
			gernel_canvas_text_set_real_width(canvas_text);
		}
	} else {
		gnome_canvas_item_hide(GNOME_CANVAS_ITEM(icon_text_item));
		gernel_canvas_text_set_height(canvas_text, 0);
	}
}

static void gernel_canvas_text_update(	GernelCanvasText *canvas_text	)
/*	Rerender the canvas_text	*/
{
	GnomeIconTextItem	*icon_text_item;

	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));
	g_return_if_fail(canvas_text->width >= 0);

	if ((canvas_text->text == NULL) || (canvas_text->width == 0.0)) 
		return;

	icon_text_item = GNOME_ICON_TEXT_ITEM(canvas_text);

	gnome_icon_text_item_configure(	icon_text_item, 
					(gint)canvas_text->x, 
					(gint)canvas_text->y,
					canvas_text->width,
					canvas_text->fontset, 
					canvas_text->text, 
					FALSE, 
					TRUE	);
}

static void gernel_canvas_text_set_height(	GernelCanvasText *canvas_text,
						gint height	)
/*	Set height of canvas_text	*/
{
	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));

	canvas_text->height =	height + 
				canvas_text->y + 
				canvas_text->bottom_margin;
}

static void gernel_canvas_text_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Set_arg handler for the GernelCanvasText item (designed to replace
	gnome_icon_text_item_configure() with an alternative more in line 
	with the standard GnomeCanvasItem set arg handlers	*/
{
	GernelCanvasText	*canvas_text;

	canvas_text = GERNEL_CANVAS_TEXT(object);

	switch (arg_id) {
		case ARG_FONTSET:
			gernel_canvas_text_set_fontset(	canvas_text, 
							GTK_VALUE_STRING(*arg));
			break;
		case ARG_TEXT:
			canvas_text->text = GTK_VALUE_STRING(*arg);
			gernel_canvas_text_update(canvas_text);

			break;
		case ARG_X1:
			canvas_text->x = GTK_VALUE_DOUBLE(*arg);
			break;
		default:
			g_assert_not_reached();
			break;
	}
}

static void gernel_canvas_text_draw(	GnomeCanvasItem *item, 
					GdkDrawable *drawable, 
					int x, 
					int y, 
					int width, 
					int height	)
/*	Draw method handler for the icon text item	*/
{
	gint			xoffset, yoffset;
	GernelCanvasText	*canvas_text;

	canvas_text = GERNEL_CANVAS_TEXT(item);

	gernel_canvas_text_set_gc(canvas_text);

	if (canvas_text->color_change_pending)

		gernel_canvas_text_set_color(	canvas_text, 
						canvas_text->color	);


	xoffset = (gint)(item->x1 - x);
	yoffset = (gint)(item->y1 - y);

	gnome_icon_paint_text(	GNOME_ICON_TEXT_ITEM(item)->ti,
				drawable,
				canvas_text->gc,
				xoffset,
				yoffset,
				GTK_JUSTIFY_LEFT	);
}

static void gernel_canvas_text_set_gc(	GernelCanvasText *canvas_text	)
/*	Check if canvas_text has a gc yet; if not, create one	*/
{
	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));

	if (canvas_text->gc == NULL) {
		GdkWindow	*window;
		GnomeCanvasItem	*canvas_item;

		canvas_item = GNOME_CANVAS_ITEM(canvas_text);

		window = GTK_WIDGET(canvas_item->canvas)->window;
		canvas_text->gc = gdk_gc_new(window);
	}
}

gint gernel_canvas_text_get_height(	GernelCanvasText *canvas_text	)
/*	Retrieve the height rendered for the item	*/
{
	g_return_val_if_fail(canvas_text != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text), 0);

	return (gint)canvas_text->height;
}

void gernel_canvas_text_set_y(	GernelCanvasText *canvas_text,
				gint y	)
/*	Set y offset of canvas_text	*/
{
	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));

	canvas_text->y = y;
}

void gernel_canvas_text_set_fontset(	GernelCanvasText *canvas_text, 
					gchar *fontset	)
/*	Set fontset to be used by canvas_text	*/
{
	gint	margin;

	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));

	canvas_text->fontset = fontset;
	margin = gernel_canvas_text_get_font_height(canvas_text) / 3;
	gernel_canvas_text_set_y(canvas_text, margin);
	margin = gernel_canvas_text_get_bottom_margin(canvas_text);
	canvas_text->bottom_margin = margin;
}

gchar* gernel_canvas_text_get_fontset(	GernelCanvasText *canvas_text	)
/*	Get the font currently displayed by canvas_text	*/
{
	g_return_val_if_fail(canvas_text != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text), NULL);

	return canvas_text->fontset;
}

static gint gernel_canvas_text_get_bottom_margin(
		GernelCanvasText *canvas_text
		)
/*	Compute the bottom margin from canvas_text's fontset	*/
{
	g_return_val_if_fail(canvas_text != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text), 0);

	return 0;
}

static gint gernel_canvas_text_get_font_height(	GernelCanvasText *canvas_text )
/*	Compute height of the font; used e.g. for the y coordinate of
	canvas_text	*/
{
	gint	height;
	gchar	*text = "Mg";

	g_return_val_if_fail(canvas_text != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text), 0);
	g_return_val_if_fail(canvas_text->fontset != NULL, 0);

	height = gdk_text_height(
			gdk_fontset_load(canvas_text->fontset), 
			text, 
			strlen(text)
			);

	return height;
}

void gernel_canvas_text_set_color(	GernelCanvasText *canvas_text,
					guint32 color	)
/*	Set text color	*/
{
	GdkColor	*foreground;
	GdkColormap	*colormap;

	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));

	canvas_text->color = color;

	if (canvas_text->gc == NULL) {
		canvas_text->color_change_pending = TRUE;

		return;
	} else
		canvas_text->color_change_pending = FALSE;

	foreground = g_new(GdkColor, 1);

	foreground->red = (color & 0xff000000) >> 16;
	foreground->green = (color & 0x00ff0000) >> 8;
	foreground->blue = color & 0x0000ff00;

	colormap = gdk_colormap_get_system();
	gdk_colormap_alloc_color(colormap, foreground, FALSE, TRUE);
	gdk_gc_set_foreground(canvas_text->gc, foreground);
}

void gernel_canvas_text_set_x(	GernelCanvasText *canvas_text,
				gint x	)
/*	Set the offset value of canvas_text within its canvas	*/
{
	g_return_if_fail(canvas_text != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_TEXT(canvas_text));

	canvas_text->x = (gdouble)x;
}
