#define	GERNEL_TYPE_SETTER	(gernel_setter_get_type())

#define GERNEL_SETTER(obj) \
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_SETTER, GernelSetter))

#define GERNEL_SETTER_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_SETTER, \
				GernelSetterClass	))

#define GERNEL_IS_SETTER(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_SETTER))

#define	GERNEL_IS_SETTER_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_SETTER))

typedef struct _GernelSetter		GernelSetter;
typedef struct _GernelSetterClass	GernelSetterClass;

struct _GernelSetter {
	GnomeDockItem	dock_item;

	GtkWidget	*menu, *yes, *module, *no, *value, *label;
	gchar		*local_name;
};

struct _GernelSetterClass {
	GnomeDockItemClass	parent_class;

	void (*view_changed)	(	GernelSetter *setter,
					gchar *description,
					gchar *name,
					gchar *text	);
};

GtkType		gernel_setter_get_type(	void	);
GtkWidget*	gernel_setter_new(	gchar *name	);

void		gernel_setter_update(	GernelSetter *setter,
					GtkWidget *var_item	);

void		gernel_setter_reset(	GernelSetter *setter	);
