#define GERNEL_TYPE_DOCK (gernel_dock_get_type())
#define GERNEL_DOCK(obj) (GTK_CHECK_CAST(obj, GERNEL_TYPE_DOCK, GernelDock))

#define GERNEL_DOCK_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass), GERNEL_TYPE_DOCK, GernelDockclass))

#define GERNEL_IS_DOCK(obj) (GTK_CHECK_TYPE((obj), GERNEL_TYPE_DOCK))

#define GERNEL_IS_DOCK_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_DOCK))

typedef struct _GernelDock	GernelDock;
typedef struct _GernelDockClass	GernelDockClass;

struct _GernelDock {
	GtkHBox		hbox;

	GtkWidget	*dock, *dock_item, *widget;
	gboolean	detachable;
};

struct _GernelDockClass {
	GtkHBoxClass			parent_class;

	void (*width_customizable)	(	GernelDock *dock,
						gboolean flag	);

	void (*child_attached)		(	GernelDock *dock	);
	void (*child_detached)		(	GernelDock *dock	);
};

GtkType		gernel_dock_get_type(	void	);
GernelDock*	gernel_dock_new(	void	);

void		gernel_dock_set_hscrolling(	GernelDock *dock,
						GtkPolicyType policy_type );

void		gernel_dock_set_detachable(	GernelDock *dock,
						gboolean flag	);

void		gernel_dock_no_vscrolling(	GernelDock *dock	);
void		gernel_dock_no_hscrolling(	GernelDock *dock	);
