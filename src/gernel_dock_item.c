#include <gernel.h>
#include <gernel_dock_item.h>
#include <gernel_scrolled_window.h>

/*	FIXME: dependency should be removed	*/
#include <gernel_canvas.h>

#define	DRAG_HANDLE_SIZE	10

static void	gernel_dock_item_class_init(	GernelDockItemClass *klass );
static void	gernel_dock_item_init(	GernelDockItem *dock_item	);
static void	gernel_dock_item_attached(	GernelDockItem *dock_item );
static void	gernel_dock_item_detached(	GernelDockItem *dock_item );

static void	gernel_dock_item_size_request(	GtkWidget *dock_item,
						GtkRequisition *requisition );

static void	gernel_dock_item_size_allocate(	GtkWidget *widget,
						GtkAllocation *allocation );

static void	gernel_dock_item_construct(	GernelDockItem *dock_item );

static void	gernel_dock_item_child_adjust(	GernelDockItem *item	);

static void	gernel_dock_item_add(	GtkContainer *container,
					GtkWidget *widget	);

static void	gernel_dock_item_remove(GtkContainer *container,
					GtkWidget *widget	);

static void	gernel_dock_item_double_click(	GernelDockItem *dock_item,
						GdkEventButton *event	);

static gboolean gernel_dock_item_has_scrolled_window(
		GernelDockItem *dock_item
		);

static GtkWidget* gernel_dock_item_get_scrolled_window(
		GernelDockItem *dock_item
		);

enum {	ATTACHED,
	DETACHED,
	DOUBLE_CLICK,
	LAST_SIGNAL	};

static guint dock_item_signals[LAST_SIGNAL] = { 0 };
static GernelDockItemClass *parent_class = NULL;

static void gernel_dock_item_class_init(	GernelDockItemClass *klass )
/*	Initializes the GernelDockItem class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkLayoutClass		*layout_class;
	GnomeDockItemClass	*dock_item_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	layout_class = (GtkLayoutClass*)klass;
	dock_item_class = (GnomeDockItemClass*)klass;

	parent_class = gtk_type_class(gnome_dock_item_get_type());

	function_offset = GTK_SIGNAL_OFFSET(GernelDockItemClass, attached);

	dock_item_signals[ATTACHED] = gtk_signal_new(	"attached",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelDockItemClass, detached);

	dock_item_signals[DETACHED] = gtk_signal_new(	"detached",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelDockItemClass, double_click);

	dock_item_signals[DOUBLE_CLICK] =

			gtk_signal_new(	"double_click",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class, 
					dock_item_signals, 
					LAST_SIGNAL	);

	widget_class->size_request = gernel_dock_item_size_request;
	widget_class->size_allocate = gernel_dock_item_size_allocate;

	container_class->add = gernel_dock_item_add;
	container_class->remove = gernel_dock_item_remove;

	klass->attached = NULL;
	klass->detached = NULL;
	klass->double_click = NULL;
}

GtkType gernel_dock_item_get_type(	void	)
/*	Return type identifier for type GernelDockItem	*/
{
	static GtkType	dock_item_type = 0;

	if (!dock_item_type) {

		static const	GtkTypeInfo dock_item_info = {
			"GernelDockItem",
			sizeof(GernelDockItem),
			sizeof(GernelDockItemClass),
			(GtkClassInitFunc)gernel_dock_item_class_init,
			(GtkObjectInitFunc)gernel_dock_item_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
			};

		dock_item_type = gtk_type_unique(
				gnome_dock_item_get_type(), 
				&dock_item_info
				);
	}

	return dock_item_type;
}

static void gernel_dock_item_init(	GernelDockItem *dock_item	)
{
	dock_item->scrolled_window = NULL;
	dock_item->floating = FALSE;
}

GtkWidget* gernel_dock_item_new(	void	)
/*	Create the dock item	*/
{
	GernelDockItem	*dock_item;

	dock_item = gtk_type_new(gernel_dock_item_get_type());
	gernel_dock_item_construct(dock_item);

	return GTK_WIDGET(dock_item);
}

static void gernel_dock_item_construct(	GernelDockItem *dock_item	)
/*	Construct the dock item..	*/
{
	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	gnome_dock_item_construct(	GNOME_DOCK_ITEM(dock_item), 
					"Canvas", 
					GNOME_DOCK_ITEM_BEH_EXCLUSIVE |
					GNOME_DOCK_ITEM_BEH_NEVER_HORIZONTAL );

	gtk_signal_connect(	GTK_OBJECT(dock_item),
				"button_release_event",
				gernel_dock_item_double_click,
				NULL	);
}

static void gernel_dock_item_child_adjust(	GernelDockItem *dock_item )
/*	Decide how to set sizes depending on the dock item being attached */
{
	GnomeDockItem	*item;

	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	item = GNOME_DOCK_ITEM(dock_item);

	if ((!item->is_floating) && (dock_item->floating)) {
		dock_item->floating = FALSE;
		gernel_dock_item_attached(dock_item);
	} else if ((item->is_floating) && !(dock_item->floating)) {
		dock_item->floating = TRUE;
		gernel_dock_item_detached(dock_item);
	}
}

static void gernel_dock_item_attached(	GernelDockItem *dock_item	)
/*	Emit the 'attached' signal	*/
{
	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	gtk_signal_emit(GTK_OBJECT(dock_item), dock_item_signals[ATTACHED]);
}

static void gernel_dock_item_detached(	GernelDockItem *dock_item	)
/*	Emit the 'detached' signal	*/
{
	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	gtk_signal_emit(GTK_OBJECT(dock_item), dock_item_signals[DETACHED]);
}

static void gernel_dock_item_size_request(	GtkWidget      *widget,
						GtkRequisition *requisition )
{
	GtkBin		*bin;
	GnomeDockItem	*dock_item;
	GernelDockItem	*item;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GNOME_IS_DOCK_ITEM (widget));
	g_return_if_fail (requisition != NULL);

	bin = GTK_BIN (widget);
	dock_item = GNOME_DOCK_ITEM (widget);
	item = GERNEL_DOCK_ITEM(widget);

	if (item->floating)
		gtk_widget_size_request(item->scrolled_window, requisition);
	else {
		GtkRequisition	child_requisition;

		gtk_widget_size_request(	item->scrolled_window, 
						&child_requisition	);
	}

	gernel_dock_item_child_adjust(item);
}

static void gernel_dock_item_double_click(	GernelDockItem *dock_item,
						GdkEventButton *event	)
/*	Emit the 'double_click' signal	*/
{
	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	if (event->type == GDK_2BUTTON_PRESS)

		gtk_signal_emit(	GTK_OBJECT(dock_item), 
					dock_item_signals[DOUBLE_CLICK]	);
}

static void dock_band_double_click(	GnomeDockItem *float_item,
					GtkWidget *dock	)
/*	Signal handler to attach dock_item on double mouse clicks	*/
/*	FIXME:	remove function.	*/
{
		GtkWidget	*child, *canvas, *scrolled, *new_item;

		/*	child is the viewport below the dock item	*/
{
		child = gnome_dock_item_get_child(float_item);
		new_item = GTK_WIDGET(gernel_dock_item_new());

		gnome_dock_add_item(	GNOME_DOCK(dock), 
					GNOME_DOCK_ITEM(new_item), 
					GNOME_DOCK_LEFT, 
					0, 
					0, 
					0, 
					TRUE	);

		gtk_widget_show(new_item);
		canvas = GTK_BIN(GTK_BIN(child)->child)->child;

		g_return_if_fail(GERNEL_IS_CANVAS(canvas));

		scrolled = gtk_widget_get_ancestor(
				canvas, 
				GTK_TYPE_SCROLLED_WINDOW	);

		gtk_scrolled_window_set_policy(	GTK_SCROLLED_WINDOW(scrolled),
						GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC	);

		gtk_widget_reparent(child, new_item);
		gtk_widget_destroy(GTK_WIDGET(float_item));
	}
}

void gernel_dock_item_set_detachable(	GernelDockItem *dock_item,
					gboolean flag	)
/*	Decide whether to allow the user dragging the dock_item around	*/
{
	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	if (flag)
		GNOME_DOCK_ITEM(dock_item)->behavior =
			GNOME_DOCK_ITEM_BEH_EXCLUSIVE | 
			GNOME_DOCK_ITEM_BEH_NEVER_HORIZONTAL;
	else
		GNOME_DOCK_ITEM(dock_item)->behavior =
			GNOME_DOCK_ITEM_BEH_EXCLUSIVE | 
			GNOME_DOCK_ITEM_BEH_NEVER_HORIZONTAL |
			GNOME_DOCK_ITEM_BEH_NEVER_FLOATING;
}

static void	gernel_dock_item_add(	GtkContainer *container,
					GtkWidget *widget	)
/*	Implements the 'add' handler of GtkContainer	*/
{
	GernelDockItem	*dock_item;

	g_return_if_fail(container != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(container));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));

	dock_item = GERNEL_DOCK_ITEM(container);

	/*	FIXME: A mess, but works	*/
	if (dock_item->scrolled_window != NULL) {
		GtkContainer		*window;

		window = GTK_CONTAINER(dock_item->scrolled_window);
		gtk_container_add(window, widget);
	} else if (GERNEL_IS_SCROLLED_WINDOW(widget)) {
		dock_item->scrolled_window = widget;
		(*GTK_CONTAINER_CLASS(parent_class)->add)(container, widget);
	} else {
		GtkWidget	*window;

		window = gernel_scrolled_window_new();
		(*GTK_CONTAINER_CLASS(parent_class)->add)(container, window);
		dock_item->scrolled_window = window;
		gernel_dock_item_set_hscrolling(dock_item, GTK_POLICY_NEVER);
		gernel_dock_item_add(container, widget);
		gtk_widget_show(window);
		return;
	}

	gtk_signal_connect_object(	GTK_OBJECT(widget),
					"show",
					gtk_widget_show,
					GTK_OBJECT(container)	);

	gtk_signal_connect_object(	GTK_OBJECT(widget),
					"hide",
					gtk_widget_hide,
					GTK_OBJECT(container)	);
}

void	gernel_dock_item_set_hscrolling(GernelDockItem *dock_item,
					GtkPolicyType type	)
/*	Set the scrolling policy of child in case it was packed with a 
	GernelScrolledWindow	*/
{
	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	if (dock_item->scrolled_window != NULL) {
		GtkWidget		*window;
		GernelScrolledWindow	*scrolled_window;

		window = dock_item->scrolled_window;
		scrolled_window = GERNEL_SCROLLED_WINDOW(window);
		gernel_scrolled_window_set_hscrolling(scrolled_window, type);
	}
}

void gernel_dock_item_set_vscrolling(	GernelDockItem *dock_item,
					GtkPolicyType type	)
/*	Set the scrolling policy of child in case it was packed with a 
	scrolled window	*/
{
	g_return_if_fail(dock_item != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(dock_item));

	if (gernel_dock_item_has_scrolled_window(dock_item)) {
		GtkWidget		*scrolled;
		GernelScrolledWindow	*scrolled_window;

		scrolled = gernel_dock_item_get_scrolled_window(dock_item);
		scrolled_window = GERNEL_SCROLLED_WINDOW(scrolled);
		gernel_scrolled_window_set_vscrolling(scrolled_window, type);
	}
}

static gboolean gernel_dock_item_has_scrolled_window(
		GernelDockItem *dock_item
		)
/*	Check if dock_item's contents is packed in a scrolled window	*/
{
	g_return_val_if_fail(dock_item != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_DOCK_ITEM(dock_item), FALSE);

	return gernel_dock_item_get_scrolled_window(dock_item) != NULL;
}

static GtkWidget* gernel_dock_item_get_scrolled_window(
		GernelDockItem *dock_item
		)
/*	Get dock_item's scrolled window	*/
{
	g_return_val_if_fail(dock_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_DOCK_ITEM(dock_item), NULL);

	return dock_item->scrolled_window;
}

static void gernel_dock_item_size_allocate(	GtkWidget *widget,
						GtkAllocation *allocation )
/*	Implements default size allocation handler	*/
{
	GernelDockItem	*dock_item;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(widget));

	dock_item = GERNEL_DOCK_ITEM(widget);

	/*	FIXME: When user detaches dock item, gnome allocates us
		width 1. This is before GernelDockItem (or GnomeDockItem)
		knows about itself being detached, so the floating flag is
		set incorrectly in this particular situation; the following
		hack workds around this but is ugly. We need to find out why
		and where Gnome assigns 1	*/
	/*	FIXME2: the hack also has the disadvantage that it marks the
		dock item as floating if the user actually minimizes it to
		1x1 manually	*/
	if ((allocation->width == 1) && (!dock_item->floating)) {
		gernel_dock_item_detached(dock_item);
		dock_item->floating = TRUE;
	}

	if (dock_item->floating) {
		/*	workaround to prevent GnomeDockItem's size allocation
			handler from getting requisition of my scrolled 
			window/canvas	*/
		GnomeDockItem	*di;
		guint		float_width, float_height;
		GtkAllocation	child_allocation;
		GtkRequisition	requisition;

		di = GNOME_DOCK_ITEM(widget);

		gtk_widget_size_request(	dock_item->scrolled_window,
						&requisition	);

		float_width = requisition.width;
		float_height = requisition.height + DRAG_HANDLE_SIZE;

		child_allocation.x = 0;
		child_allocation.y = DRAG_HANDLE_SIZE;
		child_allocation.width = float_width;
		child_allocation.height = float_height - DRAG_HANDLE_SIZE;

		gdk_window_resize(	di->float_window, 
					float_width, 
					float_height	);

		gdk_window_move_resize(	di->bin_window, 
					0, 
					0, 
					float_width, 
					float_height	);

		gtk_widget_size_allocate(	dock_item->scrolled_window, 
						&child_allocation	);
	} else
		(*GTK_WIDGET_CLASS(parent_class)->size_allocate)
				(widget, allocation);
}

GtkWidget* gernel_dock_item_get_child(	GernelDockItem *dock_item	)
/*	Get the child packed into dock_item	*/
{
	GnomeDockItem	*item;

	g_return_val_if_fail(dock_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_DOCK_ITEM(dock_item), NULL);

	item = GNOME_DOCK_ITEM(dock_item);

	return gnome_dock_item_get_child(item);
}

static void gernel_dock_item_remove(	GtkContainer *container,
					GtkWidget *widget	)
/*	Implements 'remove' handler for this class	*/
{
	GernelDockItem	*dock_item;

	g_return_if_fail(container != NULL);
	g_return_if_fail(GERNEL_IS_DOCK_ITEM(container));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(widget->parent == GTK_WIDGET(container));

	dock_item = GERNEL_DOCK_ITEM(container);

	dock_item->scrolled_window = NULL;
	(*GTK_CONTAINER_CLASS(parent_class)->remove)(container, widget);
}
