#define GERNEL_TYPE_PASSWORD	(gernel_password_get_type())

#define GERNEL_PASSWORD(obj) \
	(GTK_CHECK_CAST(obj, GERNEL_TYPE_PASSWORD, GernelPassword))

#define GERNEL_PASSWORD_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(klass, GERNEL_TYPE_PASSWORD, GernelPassword))

#define GERNEL_IS_PASSWORD(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_PASSWORD))

#define GERNEL_IS_PASSWORD_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE(klass, GERNEL_TYPE_PASSWORD))

typedef struct _GernelPassword		GernelPassword;
typedef struct _GernelPasswordClass	GernelPasswordClass;

struct _GernelPassword {
	GnomeDialog	dialog;
};

struct _GernelPasswordClass {
	GnomeDialogClass	parent_class;

	void (*configure)	(	GernelPassword *password	);
};

GtkType		gernel_password_get_type(	void	);
GtkWidget*	gernel_password_new(	void	);
