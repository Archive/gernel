#include <gernel.h>
#include <gernel_search_menu.h>
#include <gernel_search_menu_item.h>

static void gernel_search_menu_add(	GernelSearchMenu *search_menu,
					GtkWidget *widget	);

static void 	gernel_search_menu_init(	GernelSearchMenu *search_menu );
static void	gernel_search_menu_class_init(	GernelSearchMenuClass *klass );
static void gernel_search_menu_construct(	GernelSearchMenu *search_menu );
static void gernel_search_menu_show(	GtkWidget *widget	);

static void gernel_search_menu_selection_changed(
		GernelSearchMenu *search_menu
		);

static GtkOptionMenuClass	*parent_class = NULL;

enum {	SELECTION_CHANGED,
	LAST_SIGNAL	};

static guint search_menu_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_search_menu_get_type(	void	)
/*	Return type identifier for type GernelSearchMenu	*/
{
	static GtkType	search_menu_type = 0;

	if (!search_menu_type) {

		static const GtkTypeInfo	search_menu_info = {
				"GernelSearchMenu",
				sizeof(GernelSearchMenu),
				sizeof(GernelSearchMenuClass),
				(GtkClassInitFunc)gernel_search_menu_class_init,
				(GtkObjectInitFunc)gernel_search_menu_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		search_menu_type = gtk_type_unique(	GTK_TYPE_OPTION_MENU, 
							&search_menu_info );
	}

	return search_menu_type;
}

GtkWidget* gernel_search_menu_new(	void	)
/*	Create an instance of GernelSearchMenu	*/
{
	GernelSearchMenu	*search_menu;

	search_menu = gtk_type_new(gernel_search_menu_get_type());
	gernel_search_menu_construct(search_menu);

	return GTK_WIDGET(search_menu);
}

static void gernel_search_menu_init(	GernelSearchMenu *search_menu	)
/*	Initialize a GernelSearchMenu	*/
{
	search_menu->menu = NULL;
}

static void gernel_search_menu_class_init(	GernelSearchMenuClass *klass )
/*	Initialize the GernelSearchMenu class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkButtonClass		*button_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	button_class = (GtkButtonClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_OPTION_MENU);

	widget_class->show = gernel_search_menu_show;

	function_offset = GTK_SIGNAL_OFFSET(	GernelSearchMenuClass, 
						selection_changed	);

	search_menu_signals[SELECTION_CHANGED] =

			gtk_signal_new(	"selection_changed",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class, 
					search_menu_signals, 
					LAST_SIGNAL	);

	klass->selection_changed = NULL;
}

static void gernel_search_menu_construct(	GernelSearchMenu *search_menu )
/*	Construct the internal widgets	*/
{
	GtkWidget	*menu;

	g_return_if_fail(search_menu != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU(search_menu));

	menu  = gtk_menu_new();
	search_menu->menu = menu;

	gtk_signal_connect_object(	GTK_OBJECT(menu),
					"selection_done",
					gernel_search_menu_selection_changed,
					GTK_OBJECT(search_menu)	);

	gtk_widget_show(menu);
}

static void gernel_search_menu_selection_changed(
		GernelSearchMenu *search_menu
		)
/*	Emit the 'selection_changed' signal	*/
{
	g_return_if_fail(search_menu != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU(search_menu));

	gtk_signal_emit(	GTK_OBJECT(search_menu), 
				search_menu_signals[SELECTION_CHANGED]	);
}

static void gernel_search_menu_add(	GernelSearchMenu *search_menu,
					GtkWidget *widget	)
/*	Reimplementation of 'add' function	*/
{
	g_return_if_fail(search_menu != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU(search_menu));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU_ITEM(widget));

	gtk_menu_append(GTK_MENU(search_menu->menu), widget);
}

GtkWidget* gernel_search_menu_get_current_item(	GernelSearchMenu *search_menu )
/*	Get currently selected item	*/
{
	g_return_val_if_fail(search_menu != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_SEARCH_MENU(search_menu), NULL);

	return gtk_menu_get_active(GTK_MENU(search_menu->menu));
}

static void gernel_search_menu_show(	GtkWidget *widget	)
/*	Reimplementation of 'show' function; works around a bug in 
	GtkOptionMenu that prevents showing first item of menu when set_menu()
	is called too early (i.e., before menu is complete)	*/
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU(widget));

	gtk_option_menu_set_menu(	GTK_OPTION_MENU(widget), 
					GERNEL_SEARCH_MENU(widget)->menu );

	(*GTK_WIDGET_CLASS(parent_class)->show)(widget);
}

void gernel_search_menu_add_from_data(	GernelSearchMenu *search_menu, 
					gchar *str,
					gboolean flag	)
/*	Shortcut for adding new menu item to search_menu	*/
{
	GtkWidget	*menu_item;

	g_return_if_fail(search_menu != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU(search_menu));

	if (str != NULL)
		menu_item = gernel_search_menu_item_new_from_data(str, flag);
	else {
		GtkWidget	*separator;

		separator = gtk_hseparator_new();
		gtk_widget_show(separator);

		menu_item = gernel_search_menu_item_new();
		gtk_container_add(GTK_CONTAINER(menu_item), separator);
		gtk_widget_set_sensitive(menu_item, FALSE);
	}

	gernel_search_menu_add(search_menu, menu_item);
	gtk_widget_show(menu_item);
}

gint gernel_search_menu_get_identifier(	GernelSearchMenu *search_menu	)
/*	Get attribute of the currently activated item of search_menu	*/
{
	GtkWidget		*menu_item;
	GernelSearchMenuItem	*search_menu_item;

	g_return_val_if_fail(search_menu != NULL, -1);
	g_return_val_if_fail(GERNEL_IS_SEARCH_MENU(search_menu), -1);

	menu_item = gernel_search_menu_get_current_item(search_menu);
	search_menu_item = GERNEL_SEARCH_MENU_ITEM(menu_item);

	return gernel_search_menu_item_get_identifier(search_menu_item);
}
