#include <gernel.h>
#include <gernel_dependency.h>
#include <gernel_app.h>
#include <gernel_navigation.h>
#include <gernel_var_item.h>
#include <gernel_splash.h>
#include <math.h>

gchar* gernel_color_32_to_string(	guint32 color	)
/*	Convert color to string	*/
{
	GString	*string;
	gchar	*str;

	string = g_string_new(NULL);
	g_string_sprintf(string, "%08x", color);
	str = string->str;
	g_string_free(string, FALSE);

	return str;
}

guint32 gernel_color_string_to_32(	gchar *color	)
/*	Convert a string defining a color (e.g. "ffff00aa") to the 
	corresponding guint32 word	*/
{
	gint	value;
	gint	i, digits = 8;

	g_return_val_if_fail(color != NULL, 0);

	value = 0;

	for (i = 0; i < digits; i++) {
		gint	tmp;

		tmp = 0;

		if ((color[i] >= '0') & (color[i] <= '9'))
			tmp = color[i] - 48;
		else if ((color[i] >= 'a') & (color[i] <= 'f'))
			tmp = color[i] - 87;
		else
			g_assert_not_reached();

		value += tmp * (gint)pow(16, (digits - i - 1));
	}

	return value;
}

gboolean gernel_str_ends_insensitive(	gchar *string,
					gchar *pattern	)
/*	Do a case insensitive match on string, pattern	*/
{
	gchar		*tmp1, *tmp2;
	gboolean	result;

	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	tmp1 = g_strdup(string);
	tmp2 = g_strdup(pattern);
	g_strdown(tmp1);
	g_strdown(tmp2);

	result = gernel_str_ends(tmp1, tmp2);

	g_free(tmp1);
	g_free(tmp2);

	return result;
}

gboolean gernel_str_ends(	gchar *string,
				gchar *pattern	)
/*	Checks if string ends in pattern	*/
{
	gchar		*tmp1, *tmp2;
	gboolean	result;

	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	tmp1 = g_strdup(string);
	tmp2 = g_strdup(pattern);
	g_strreverse(tmp1);
	g_strreverse(tmp2);

	result = gernel_str_starts(tmp1, tmp2);

	g_free(tmp1);
	g_free(tmp2);

	return result;
}

gboolean gernel_str_equals_insensitive(	gchar *string,
					gchar *pattern	)
/*	Check if string matches pattern, case insensitive	*/
{
	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	return g_strcasecmp(string, pattern) != 0;
}

gboolean gernel_str_equals(	gchar *string,
				gchar *pattern	)
/*	To be used as a GCompareFunc in g_slist_find_custom; returns 0 if 
	strings match	*/
{
	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	return strcmp(string, pattern) != 0;
}

gboolean gernel_int_equals(	gint first,
				gint second	)
/*	Return TRUE if first is bigger than second	*/
{
	return first > second;
}

gboolean gernel_str_contains_insensitive(	gchar *string,
						gchar *pattern	)
/*	Checks if pattern is contained in string, case insensitive	*/
{
	gchar		*tmp1, *tmp2;
	gboolean	result;

	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	tmp1 = g_strdup(string);
	tmp2 = g_strdup(pattern);
	g_strdown(tmp1);
	g_strdown(tmp2);

	result = gernel_str_contains(tmp1, tmp2);

	g_free(tmp1);
	g_free(tmp2);

	return result;
}

gboolean gernel_str_contains(	gchar *string,
				gchar *pattern	)
/*	Checks if pattern is contained in string; returns 0 if pattern is part
	of string	*/
{
	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	return strstr(string, pattern) == NULL;
}

gboolean gernel_str_starts_insensitive(	gchar *string,
					gchar *pattern	)
/*	Checks if string starts with pattern, case insensitive	*/
{
	gchar		*tmp1, *tmp2;
	gboolean	result;

	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	tmp1 = g_strdup(string);
	tmp2 = g_strdup(pattern);
	g_strdown(tmp1);
	g_strdown(tmp2);

	result = gernel_str_starts(tmp1, tmp2);

	g_free(tmp1);
	g_free(tmp2);

	return result;
}

gboolean gernel_str_starts(	gchar *string,
				gchar *pattern	)
/*	Checks if string starts with pattern; returns 0 if this is the case */
{
	g_return_val_if_fail(string != NULL, TRUE);
	g_return_val_if_fail(pattern != NULL, TRUE);

	return strncmp(string, pattern, strlen(pattern)) != 0;
}

gint map_type(	gchar *type	)
/*	maps type to the respective value of config_in_type	*/
{
	if (strstr(type, CONFIG_IN_O) != NULL) return OPERATOR_OR;
	if (strstr(type, CONFIG_IN_A) != NULL) return OPERATOR_AND;
	if (	(strstr(type, CONFIG_IN_NOT) != NULL) &
		(strstr(type, CONFIG_IN_NEQUAL)	== NULL)	)
		return OPERATOR_NOT;

	return OPERATOR_NONE;
}

GNode* gernel_node_copy(	GNode *original	)
/*	Generates a newly allocated copy of original	*/
{
	if (G_NODE_IS_LEAF(original))
		/*	BASIS.	*/
		return g_node_new(original->data);
	else {
		/*	INDUCTION.	*/
		GNode	*node, *child;

		node = g_node_new(original->data);

		for (child = original->children; child; child = child->next)
			g_node_append(node, gernel_node_copy(child));

		return node;
	}
}

guint get_size(	gchar*	path	)
/*	get size of file	*/
{
	struct stat	buf;

	if (lstat(path, &buf)) return 0;

	return (guint)buf.st_size;
}

int main (int argc, char *argv[])
{
	gchar			**args;
	struct poptOption	*options;
	poptContext		ctx;
	gint			i;
	GtkWidget		*app;

	options = NULL;
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);
	windows = 0;
	erty_init(PROGRAM);

	gnome_init_with_popt_table(	PACKAGE, 
					VERSION, 
					argc, 
					argv,
					options,
					0,
					&ctx	);

	args = (gchar**)poptGetArgs(ctx);

	if (args) {
g_print("main:	FIXME: maintain a count of apps; every GernelApp should receive its index and decide for itself whether to exit or close the app\n");
		for (i = 0; args[i]; i++) {
			GtkWidget		*splash;
			GernelNavigation	*navigation;
			GernelApp		*application;

			app = gernel_app_new();
			application = GERNEL_APP(app);
			gernel_app_set_path(application, args[i]);

			splash = gernel_splash_new_with_title(PROGRAM);
			gernel_app_set_splash(application, splash);
			gernel_splash_start(GERNEL_SPLASH(splash));
			gtk_widget_show(splash);

			navigation = GERNEL_NAVIGATION(application->navigation);
			gernel_navigation_parse(navigation, splash->window);
g_print("main:	mark\n");
			gernel_app_construct_search(application);
			gernel_app_remove_splash(application);
			gtk_widget_destroy(splash);
			gtk_widget_show(app);
		}
	} else {
		app = gernel_app_new();
		gtk_widget_show(app);
	}

	poptFreeContext(ctx);
	gtk_main ();

	return 0;
}

void close_window(	GtkWidget *button,
			GtkWidget *window	)
/*	callback to close window	*/
{
	g_return_if_fail(window != NULL);

	if (windows > 1) {
		gtk_widget_destroy(window);
		windows--;
	} else
		gtk_main_quit();
}

GNode* create_stack_item(	GNode *operator,
				gchar *expr,
				GCache *cache	)
/*	Generate an item to be pushed to the stack. expr is expected to hold
	a Config Language like /expr/	*/
{
	gchar			**_expr_, **tmpv;
	GNode			*new;
	gint			i;
	enum gernel_boolean_op	op;

	g_strstrip(expr);

	op = -1;
	op = map_type(expr);

	if ((op == OPERATOR_NOT) | (op == OPERATOR_AND) | (op == OPERATOR_OR)) {
		/*	INDUCTION.	*/
		gchar	*tmp;

		switch (op) {
			case OPERATOR_OR:
				tmp = CONFIG_IN_O;
				break;
			case OPERATOR_AND:
				tmp = CONFIG_IN_A;
				break;
			case OPERATOR_NOT:
				tmp = CONFIG_IN_NOT;
				break;
			default:
				tmp = NULL;
		}

		tmpv = g_strsplit(expr, tmp, 0);
		new = g_node_new(GINT_TO_POINTER(op));

		if (operator) {
			g_node_append(operator, new); 
			new = operator;
		}

		for (i = 0; tmpv[i]; i++) 
			if (strlen(tmpv[i])) {
				GNode	*node;
				gchar	*tmp;

				tmp = tmpv[i];
				node = create_stack_item(new, tmp, cache);
				g_node_append(new, node);
			}

		g_strfreev(tmpv);

		return new;
	} else {
		/*	BASIS.	*/
		GernelVarItem		*item;
		gernel_single_expr	*leaf_expr;
		gchar			*value, *var_name, *operator;

		g_strdelimit(expr, "\"$", ' ');
		_expr_ = g_strsplit(expr, CONFIG_IN_BLANK, 0);
		var_name = NULL;
		operator = NULL;
		value = NULL;

		for (i = 0; _expr_[i]; i++) {

			if (strlen(_expr_[i]) > 0) {
				if (var_name == NULL) 
					var_name = g_strdup(_expr_[i]);
				else if (operator == NULL) 
					operator = g_strdup(_expr_[i]);
				else {
					value = g_strdup(_expr_[i]);
					break;
				}
			}
		}

		g_strfreev(_expr_);
		leaf_expr = g_new(gernel_single_expr, 1);
		item = GERNEL_VAR_ITEM(g_cache_insert(cache, var_name));
		leaf_expr->var_item = item;

		if (match(operator, CONFIG_IN_EQUAL))
			leaf_expr->operator = COMPARE_EQUAL;
		else
			leaf_expr->operator = COMPARE_NOT_EQUAL;

		leaf_expr->value = value;

		return g_node_new(leaf_expr);
	}
}

GSList* push(	GSList* stack,
		gpointer element	)
/*	pushes element to stack	*/
{
	return g_slist_prepend(stack, element);
}
	
gint array_length(	gchar* array[]	)
/*	gets number of fields of array	*/
{
	gint	number = 0;

	while (array[number]) number++;

	return number;
}

gchar* get_last(	gchar** strs	)
/*	get last field of strs	*/
{
	gint	n = 0;

	while (strs[n]) n++;

	return strs[n-1];
}

gchar* unwrap(	gchar* s	)
/*	removes trailing backslashes from s	*/
{
	g_strstrip(s);
	g_strdelimit(s, "\\", ' ');
	g_strstrip(s);

	return s;
}

gboolean is_wrapped(	gchar* s	)
/*	decides whether str is a wrapped line	*/
{
	return ((s == NULL) || (s[strlen(s) - 1] != '\\'))?FALSE:TRUE;
}

gchar* check_syntax(	gchar *if_expr	)
/*	check if ifcond follows Config Language format and fix errors
	e.g. turn 
		if [ "$CONFIG_BLA" = "y" -o CONFIG_MOO ]; then
	into
		if [ "$CONFIG_BLA" = "y" -o "$CONFIG_MOO" = "y" ]; then

	returns the (corrected) if expression	*/
{
	gchar	*str, **conditions;

	/*	BASIS.	*/
	if (if_expr == NULL) return NULL;

	/*	INDUCTION.	*/
	conditions = g_strsplit(if_expr, CONFIG_IN_BLANK, 1);

	if (match0(conditions[0], CONFIG_IN_CONFIG)) {

		g_print(	_("bad if-condition: ...%s %s\n"), 
				conditions[0], 
				conditions[1]	);

		str = g_strconcat(	CONFIG_IN_IMPL_IF_QUOTE,
					CONFIG_IN_VAR_DENOTE,
					conditions[0],
					CONFIG_IN_IMPL_IF_QUOTE,
					CONFIG_IN_IMPL_IF_ADD,
					check_syntax(conditions[1]),
					NULL	);
	} else 
		str = g_strconcat(	conditions[0], 
					CONFIG_IN_BLANK,
					check_syntax(conditions[1]), 
					NULL	);

	g_strfreev(conditions);

	return str;
}

GSList* rm_defconfig_specials(	GSList* defconfig	)
/*	remove (and free()!) comments, empty lines from defconfig list	*/
{
	GSList	*list;

	list = defconfig;

	if (defconfig == NULL)
		return NULL;
	else if (strlen(list->data) == 0)
		return rm_defconfig_specials(list->next);

	else if (	(match0(list->data, DEFCONFIG_COMMENT)) &
			(strstr(list->data, DEFCONFIG_IS_NOT_SET) == NULL) ) {
		/*	crap	*/
		gchar	*str;

		str = list->data;
		list = g_slist_remove(list, str);
		g_free(str);

		return rm_defconfig_specials(list);

	} else if (match0(list->data, DEFCONFIG_COMMENT)) {
		/*	"is not set"	*/
		gchar	**tmpv, *tmp, *new;

		tmpv = g_strsplit(list->data, CONFIG_IN_BLANK, 0);
		tmp = tmpv[1];
		new = g_strconcat(tmp, CONFIG_IN_BLANK, CONFIG_IN_NO, NULL);
		g_strfreev(tmpv);
		tmp = list->data;
		list = g_slist_remove(list, tmp);
		g_free(tmp);

		return g_slist_prepend(rm_defconfig_specials(list), new);

	} else {
		/*	standard definition	*/
		list->data = g_strdelimit(list->data, "=", ' ');

		return g_slist_prepend(	rm_defconfig_specials(list->next),
					list->data	);
	}
}

gchar* get_enclosed(	gchar* str,
			gchar* delim	)
/*	retrieves first item enclosed in delims; return_str should be freed when
	no longer needed	*/
{
	gchar	**sep_str, *return_str;

	g_return_val_if_fail(str != NULL, NULL);

	sep_str = g_strsplit(str, delim, 0);
	return_str = sep_str[1]?g_strdup(g_strstrip(sep_str[1])):g_strdup(str);
	g_strfreev(sep_str);

	return return_str;
}

gchar* get_var_name(	gchar* str, 
			gchar* var_start	)
/*	extracts variable, the start of which is given by var_start, from str.
	str is a Config Language type variable definition.
	returned var_name should be freed when no longer needed.	*/
{
	gchar	**sep_str, *result;

	result = NULL;
	sep_str = g_strsplit(str, CONFIG_IN_SHORT_DESCR_MARK, 2);

	switch (array_length(sep_str)) {
		gchar	**tmpv;

		case 1:
			tmpv = g_strsplit(str, " ", 2);

			if (match0(g_strstrip(tmpv[1]), var_start))
				result = g_strstrip(g_strdup(tmpv[1]));
			else
				g_print("get_var_name:	Error: %s\n", str);

			g_strfreev(tmpv);
			break;
		case 3:
			if (match0(g_strstrip(sep_str[2]), var_start)) {
				gchar	**tmpv;

				tmpv = g_strsplit(sep_str[2], " ", 1);
				result = g_strstrip(g_strdup(tmpv[0]));
				g_strfreev(tmpv);
			}

			break;
		default:
			break;
	}

	g_strfreev(sep_str);

	return result;
}

gchar* remove_strs(	gchar* str,
			gchar* obsolete	)
/*	removes all occurences of obsolete from str	*/
{
	gchar	**sep_str;
	gchar	*tmp;

	sep_str = g_strsplit(str, obsolete, 0);
	tmp = g_strjoinv(NULL, sep_str);
	g_strfreev(sep_str);

	return tmp;
}

gboolean match(	gchar *str1,
		gchar *str2	)
/*	reliable frontend to strcmp	*/
{
	g_return_val_if_fail(str1 != NULL, FALSE);
	g_return_val_if_fail(str2 != NULL, FALSE);

	if (strcmp(str1, str2) == 0)
		return TRUE;
	else
		return FALSE;
}

gboolean match0(	gchar *str1,
			gchar *str2	)
/*	check if str2 is the beginning of str1	*/
{
	g_return_val_if_fail(str1 != NULL, FALSE);
	g_return_val_if_fail(str2 != NULL, FALSE);

	if (strncmp(str1, str2, strlen(str2)) == 0)
		return TRUE;
	else
		return FALSE;
}
