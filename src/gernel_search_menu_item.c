#include <gernel.h>
#include <gernel_search_menu_item.h>

static void 	gernel_search_menu_item_init(
		GernelSearchMenuItem *search_menu_item
		);

static void	gernel_search_menu_item_class_init(
		GernelSearchMenuItemClass *klass
		);

static GtkMenuItemClass	*parent_class = NULL;

GtkType gernel_search_menu_item_get_type(	void	)
/*	Return type identifier for type GernelSearchMenuItem	*/
{
	static GtkType	search_menu_item_type = 0;

	if (!search_menu_item_type) {

		static const GtkTypeInfo	search_menu_item_info = {
			"GernelSearchMenuItem",
			sizeof(GernelSearchMenuItem),
			sizeof(GernelSearchMenuItemClass),
			(GtkClassInitFunc)gernel_search_menu_item_class_init,
			(GtkObjectInitFunc)gernel_search_menu_item_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		search_menu_item_type = gtk_type_unique(
				GTK_TYPE_MENU_ITEM, 
				&search_menu_item_info
				);
	}

	return search_menu_item_type;
}

static void gernel_search_menu_item_init(
		GernelSearchMenuItem *search_menu_item
		)
/*	Initialize the GernelSearchMenuItem	*/
{
}

static void gernel_search_menu_item_class_init(
		GernelSearchMenuItemClass *klass
		)
/*	Initialize the GernelSearchMenuItem class	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkItemClass		*item_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	item_class = (GtkItemClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_MENU_ITEM);
}

GtkWidget* gernel_search_menu_item_new(	void	)
/*	Create an instance of GernelSearchMenuItem	*/
{
	GernelSearchMenuItem	*search_menu_item;

	search_menu_item = gtk_type_new(gernel_search_menu_item_get_type());

	return GTK_WIDGET(search_menu_item);
}

GtkWidget* gernel_search_menu_item_new_from_data(	gchar *label,
							guint identifier )
/*	Create an instance of GernelSearchMenuItem, complete with label and
	identifier	*/
{
	GernelSearchMenuItem	*menu_item;

	menu_item = GERNEL_SEARCH_MENU_ITEM(gernel_search_menu_item_new());
	gernel_search_menu_item_set_label(menu_item, label);
	gernel_search_menu_item_set_identifier(menu_item, identifier);

	return GTK_WIDGET(menu_item);
}

void gernel_search_menu_item_set_label(	GernelSearchMenuItem *search_menu_item,
					gchar *label	)
/*	Set label of menu item	*/
{
	GtkWidget	*accel_label;

	g_return_if_fail(search_menu_item != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU_ITEM(search_menu_item));
	g_return_if_fail(label != NULL);

	accel_label = gtk_accel_label_new(label);
	gtk_misc_set_alignment(GTK_MISC(accel_label), 0.0, 0.5);
	gtk_container_add(GTK_CONTAINER(search_menu_item), accel_label);

	gtk_accel_label_set_accel_widget(	GTK_ACCEL_LABEL(accel_label),
						GTK_WIDGET(search_menu_item) );

	gtk_widget_show(accel_label);
}

void gernel_search_menu_item_set_identifier(
		GernelSearchMenuItem *search_menu_item,
		guint identifier
		)
/*	Set identifier field of search_menu_item	*/
{
	g_return_if_fail(search_menu_item != NULL);
	g_return_if_fail(GERNEL_IS_SEARCH_MENU_ITEM(search_menu_item));

	search_menu_item->identifier = identifier;
}

gint gernel_search_menu_item_get_identifier(
		GernelSearchMenuItem *search_menu_item
		)
/*	Get identifier field of search_menu_item	*/
{
	g_return_val_if_fail(search_menu_item != NULL, -1);
	g_return_val_if_fail(GERNEL_IS_SEARCH_MENU_ITEM(search_menu_item), -1);

	return search_menu_item->identifier;
}
