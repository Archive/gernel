#include <gernel.h>
#include <gernel_toolbar.h>

static void gernel_toolbar_init(	GernelToolbar *toolbar	);
static void gernel_toolbar_class_init(	GernelToolbarClass *klass	);
static void gernel_toolbar_construct(	GernelToolbar *toolbar	);
static void gernel_toolbar_reset(	GernelToolbar *toolbar	);
static void gernel_toolbar_up(	GernelToolbar *toolbar	);
static void gernel_toolbar_down(	GernelToolbar *toolbar	);
static void gernel_toolbar_next(	GernelToolbar *toolbar	);
static void gernel_toolbar_previous(	GernelToolbar *toolbar	);
static void gernel_toolbar_back(	GernelToolbar *toolbar	);
static void gernel_toolbar_forward(	GernelToolbar *toolbar	);
static void gernel_toolbar_find(	GernelToolbar *toolbar	);
static void gernel_toolbar_close(	GernelToolbar *toolbar	);

static GtkToolbarClass	*parent_class = NULL;

enum {	NEXT,
	PREVIOUS,
	UP,
	DOWN,
	BACK,
	FORWARD,
	FIND,
	CLOSE,
	LAST_SIGNAL	};

static guint toolbar_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_toolbar_get_type(	void	)
/*	Return type identifier for type GernelToolbar	*/
{
	static GtkType	toolbar_type = 0;

	if (!toolbar_type) {


		static const GtkTypeInfo	toolbar_info = {
			"GernelToolbar",
			sizeof(GernelToolbar),
			sizeof(GernelToolbarClass),
			(GtkClassInitFunc)gernel_toolbar_class_init,
			(GtkObjectInitFunc)gernel_toolbar_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		toolbar_type = gtk_type_unique(	gtk_toolbar_get_type(),
						&toolbar_info	);
	}

	return toolbar_type;
}

GtkWidget* gernel_toolbar_new(	void	)
/*	Create an instance of GernelToolbar	*/
{
	GernelToolbar	*toolbar;

	toolbar = gtk_type_new(gernel_toolbar_get_type());
	gernel_toolbar_construct(toolbar);

	return GTK_WIDGET(toolbar);
}

static void gernel_toolbar_init(	GernelToolbar *toolbar	)
/*	Initialize the GernelToolbar	*/
{
	toolbar->next = NULL;
	toolbar->previous = NULL;
	toolbar->up = NULL;
	toolbar->down = NULL;
	toolbar->back = NULL;
	toolbar->forward = NULL;
	toolbar->find = NULL;
	toolbar->close = NULL;
}

static void gernel_toolbar_class_init(	GernelToolbarClass *klass	)
/*	Initialize the GernelToolbarClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_TOOLBAR);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, up);

	toolbar_signals[UP] = gtk_signal_new(	"up",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__NONE,
						GTK_TYPE_NONE,
						0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, down);

	toolbar_signals[DOWN] = gtk_signal_new(	"down",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__NONE,
						GTK_TYPE_NONE,
						0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, next);

	toolbar_signals[NEXT] = gtk_signal_new(	"next",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__NONE,
						GTK_TYPE_NONE,
						0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, previous);

	toolbar_signals[PREVIOUS] = gtk_signal_new(	"previous",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, back);

	toolbar_signals[BACK] = gtk_signal_new(	"back",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__NONE,
						GTK_TYPE_NONE,
						0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, forward);

	toolbar_signals[FORWARD] = gtk_signal_new(	"forward",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, find);

	toolbar_signals[FIND] = gtk_signal_new(	"find",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__NONE,
						GTK_TYPE_NONE,
						0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelToolbarClass, close);

	toolbar_signals[CLOSE] = gtk_signal_new("close",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__NONE,
						GTK_TYPE_NONE,
						0	);

	gtk_object_class_add_signals(	object_class, 
					toolbar_signals, 
					LAST_SIGNAL	);

	klass->up = NULL;
	klass->down = NULL;
	klass->previous = NULL;
	klass->next = NULL;
	klass->back = NULL;
	klass->forward = NULL;
	klass->find = NULL;
}

static void gernel_toolbar_construct(	GernelToolbar *toolbar	)
/*	Construct the toolbar	*/
{
	GnomeUIInfo toolbar_menu[] = {

		/*	0	*/
		GNOMEUIINFO_ITEM_STOCK(	_("Back"),
					_("Move back in history"),
					NULL,
					GNOME_STOCK_PIXMAP_BACK	),

		/*	1	*/
		GNOMEUIINFO_ITEM_STOCK(	_("Forward"),
					_("Move forward in history"),
					NULL,
					GNOME_STOCK_PIXMAP_FORWARD	),

		/*	2	*/
		GNOMEUIINFO_SEPARATOR,

		/*	3	*/
		GNOMEUIINFO_ITEM_STOCK(	_("Find..."),
					_("Find a variable"),
					NULL,
					GNOME_STOCK_PIXMAP_SEARCH	),

		/*	4	*/
		GNOMEUIINFO_SEPARATOR,

		/*	5	*/
		GNOMEUIINFO_ITEM_STOCK(	_("Previous"),
					_("Go to previous item"),
					NULL,
					GNOME_STOCK_PIXMAP_BACK	),

		/*	6	*/
		GNOMEUIINFO_ITEM_STOCK(	_("Up"),
					_("Go to next higher level item"),
					NULL,
					GNOME_STOCK_PIXMAP_UP	),

		/*	7	*/
		GNOMEUIINFO_ITEM_STOCK(	_("Down"),
					_("Go to next lower level item"),
					NULL,
					GNOME_STOCK_PIXMAP_DOWN	),

		/*	8	*/
		GNOMEUIINFO_ITEM_STOCK(	_("Next"),
					_("Go to next item"),
					NULL,
					GNOME_STOCK_PIXMAP_FORWARD	),

		GNOMEUIINFO_END
	};

	GtkAccelGroup	*accel_group;
	GtkToolbar	*bar;
	GtkObject	*object;

	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	bar = GTK_TOOLBAR(toolbar);
	object = GTK_OBJECT(toolbar);

	accel_group = gtk_accel_group_new();
	gtk_toolbar_set_style(bar, GTK_TOOLBAR_BOTH);
	gnome_app_fill_toolbar(bar, toolbar_menu, accel_group);

	toolbar->next = toolbar_menu[8].widget;
	toolbar->find = toolbar_menu[3].widget;
	toolbar->up = toolbar_menu[6].widget;
	toolbar->down = toolbar_menu[7].widget;
	toolbar->previous = toolbar_menu[5].widget;
	toolbar->back = toolbar_menu[0].widget;
	toolbar->forward = toolbar_menu[1].widget;
	gernel_toolbar_reset(toolbar);

/*FIXME: implement the multiple connection func	*/
	gtk_signal_connect_object(	GTK_OBJECT(toolbar->next),
					"clicked",
					gernel_toolbar_next,
					object	);

	gtk_signal_connect_object(	GTK_OBJECT(toolbar->previous),
					"clicked",
					gernel_toolbar_previous,
					object	);

	gtk_signal_connect_object(	GTK_OBJECT(toolbar->up),
					"clicked",
					gernel_toolbar_up,
					object	);

	gtk_signal_connect_object(	GTK_OBJECT(toolbar->down),
					"clicked",
					gernel_toolbar_down,
					object	);

	gtk_signal_connect_object(	GTK_OBJECT(toolbar->back),
					"clicked",
					gernel_toolbar_back,
					object	);

	gtk_signal_connect_object(	GTK_OBJECT(toolbar->forward),
					"clicked",
					gernel_toolbar_forward,
					object	);

	gtk_signal_connect_object(	GTK_OBJECT(toolbar->find),
					"clicked",
					gernel_toolbar_find,
					object	);

	if (toolbar->close != NULL)

		gtk_signal_connect_object(	GTK_OBJECT(toolbar->close),
						"clicked",
						gernel_toolbar_close,
						object	);
}

static void gernel_toolbar_find(	GernelToolbar *toolbar	)
/*	Emit the 'find' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[FIND]);
}

static void gernel_toolbar_close(	GernelToolbar *toolbar	)
/*	Emit the 'close' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[CLOSE]);
}

static void gernel_toolbar_reset(	GernelToolbar *toolbar	)
/*	Reset toolbar's components' sensitivity	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_widget_set_sensitive(toolbar->next, FALSE);
	gtk_widget_set_sensitive(toolbar->find, FALSE);
	gtk_widget_set_sensitive(toolbar->up, FALSE);
	gtk_widget_set_sensitive(toolbar->down, FALSE);
	gtk_widget_set_sensitive(toolbar->previous, FALSE);
	gtk_widget_set_sensitive(toolbar->back, FALSE);
	gtk_widget_set_sensitive(toolbar->forward, FALSE);
}

void gernel_toolbar_set_history(	GernelToolbar *toolbar,
					guint history,
					guint future	)
/*	Set sensitivity of toolbar's history- and future widgets	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_widget_set_sensitive(toolbar->back, history > 1);
	gtk_widget_set_sensitive(toolbar->forward, future > 0);
}

void gernel_toolbar_set_up(	GernelToolbar *toolbar,
				gboolean flag	)
/*	Set sensitivity of toolbar's 'up' button	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_widget_set_sensitive(toolbar->up, flag);
}

void gernel_toolbar_set_down(	GernelToolbar *toolbar,
				gboolean flag	)
/*	Set sensitivity of toolbar's 'down' button	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_widget_set_sensitive(toolbar->down, flag);
}

void gernel_toolbar_set_next(	GernelToolbar *toolbar,
				gboolean flag	)
/*	Set sensitivity of toolbar's 'next' button	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_widget_set_sensitive(toolbar->next, flag);
}

void gernel_toolbar_set_previous(	GernelToolbar *toolbar	,
					gboolean flag	)
/*	Set sensitivity of toolbar's 'previous' button	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_widget_set_sensitive(toolbar->previous, flag);
}

void gernel_toolbar_set_find(	GernelToolbar *toolbar	)
/*	Set sensitivity of toolbar's 'find' button	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_widget_set_sensitive(toolbar->find, TRUE);
}

static void gernel_toolbar_up(	GernelToolbar *toolbar	)
/*	Emit the 'up' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[UP]);
}

static void gernel_toolbar_down(	GernelToolbar *toolbar	)
/*	Emit the 'down' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[DOWN]);
}

static void gernel_toolbar_next(	GernelToolbar *toolbar	)
/*	Emit the 'next' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[NEXT]);
}

static void gernel_toolbar_previous(	GernelToolbar *toolbar	)
/*	Emit the 'previous' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[PREVIOUS]);
}

static void gernel_toolbar_back(	GernelToolbar *toolbar	)
/*	Emit the 'back' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[BACK]);
}

static void gernel_toolbar_forward(	GernelToolbar *toolbar	)
/*	Emit the 'forward' signal	*/
{
	g_return_if_fail(toolbar != NULL);
	g_return_if_fail(GERNEL_IS_TOOLBAR(toolbar));

	gtk_signal_emit(GTK_OBJECT(toolbar), toolbar_signals[FORWARD]);
}
