#include <gernel.h>
#include <gernel_var_item.h>
#include <gernel_var_item_icon_set.h>

static gboolean gernel_var_item_is_dependant(	GernelVarItem *var_item,
						GernelVarItem *dependant );

static void	gernel_var_item_init(	GernelVarItem *var_item	);
static void	gernel_var_item_class_init(	GernelVarItemClass *klass );
static void	gernel_var_item_value_changed(	GernelVarItem *var_item	);
static void	gernel_var_item_destroy(	GtkObject *var_item	);
static gboolean eval(	gernel_single_expr *expr	);

static gchar* gernel_var_item_get_field(	GernelVarItem *var_item,
						enum GernelVarItemField field );

static GernelVarItem* item_list_match(		GSList *list,
						gchar *var,
						enum GernelVarItemField field );

static GCompareFunc	gernel_var_item_get_compare_function(
		enum GernelVarItemMatchMode mode,
		enum GernelVarItemCase sensitivity
		);

static void	gernel_var_item_inline_dep(	GernelVarItem *var_item,
						gchar *dependency,
						GCache *cache	);

static gboolean gernel_var_item_dependant(	GNode *single_expr,
						GernelVarItem *dep	);

static void	gernel_var_item_set_font(	GernelVarItem *var_item,
						gchar *font	);

static void	gernel_var_item_set_type(	GernelVarItem *var_item,
						enum GernelVarItemType type);

static void	gernel_var_item_set_option_entries(
		GernelVarItem *var_item,
		gchar *str,
		GSList *stack,
		GCache *cache
		);

static void	gernel_var_item_set_condition(	GernelVarItem *var_item,
						GSList *stack	);

static gint	condition_count(	GNode *condition	);

static void gernel_var_item_pack_icons(	GernelVarItem *var_item,
					GtkWidget *var_item_icon_set	);

static void gernel_var_item_set_description(	GernelVarItem *var_item,
						gchar *description	);

static void gernel_var_item_construct_set(
		GernelVarItem *var_item, 
		enum GernelVarItemIconSetType type,
		GdkWindow *window,
		GSList *strings
		);

void gernel_var_item_choice_labels(	GtkWidget *hbox,
					GSList *entries	);

static GtkTreeItemClass	*parent_class = NULL;

enum {	VALUE_CHANGED,
	LAST_SIGNAL	};

static guint var_item_signals[LAST_SIGNAL] = { 0 };

GtkType gernel_var_item_get_type(	void	)
/*	return type identifier for type GernelVarItem	*/
{
	static GtkType	var_item_type = 0;

	if (!var_item_type) {

		static const GtkTypeInfo var_item_info = {
				"GernelVarItem",
				sizeof(GernelVarItem),
				sizeof(GernelVarItemClass),
				(GtkClassInitFunc)gernel_var_item_class_init,
				(GtkObjectInitFunc)gernel_var_item_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
				};

		var_item_type = gtk_type_unique(	GTK_TYPE_TREE_ITEM, 
							&var_item_info	);

		gtk_type_set_chunk_alloc(var_item_type, 16);
	}

	return var_item_type;
}

void gernel_var_item_update_by_entry(	GtkEntry *entry,
					GernelVarItem *var_item	)
/*	update var_item by the value entry	*/
{
	gernel_var_item_set_value(var_item, gtk_entry_get_text(entry));
	gernel_var_item_icon_sync(var_item);
}

GernelVarItem* gernel_var_item_identity(	GernelVarItem *var_item	)
/*	return the var_item; needed for cache management	*/
{
	return var_item;
}

GtkWidget* gernel_var_item_new(	gchar *var_name	)
/*	create an instance of GernelVarItem	*/
{
	GernelVarItem	*var_item;

	var_item = gtk_type_new(gernel_var_item_get_type());
	var_item->name = var_name;

	return GTK_WIDGET(var_item);
}

GtkWidget* gernel_var_item_new_from_raw(	gchar *str,
						GSList *stack,
						GCache *cache	)
/*	creates a new item from str which holds information about the respective
	kernel variable or kernel section	*/
{
	GernelVarItem	*var_item;
	GtkWidget	*widget_item;
	gchar		**strv, *var_name, *description;

	g_return_val_if_fail(str != NULL, NULL);

	str = g_strstrip(str);
	var_name = get_var_name(str, CONFIG_IN_CONFIG);

	if (var_name != NULL) {
		var_item = GERNEL_VAR_ITEM(g_cache_insert(cache, var_name));
	} else {
		GtkWidget	*item;
		item = gernel_var_item_new(NULL);
		var_item = GERNEL_VAR_ITEM(item);
	}

	widget_item = GTK_WIDGET(var_item);

	strv = g_strsplit(str, CONFIG_IN_BLANK, 0);

	if (var_item->type == GERNEL_VAR_ITEM_TYPE_0)
		gernel_var_item_set_type_from_str(var_item, strv[0]);
	/*else
		we are dealing with a variable that has previously been inserted
		into the tree; probably, the type we now encounter is define_*;
		since we cannot just overwrite previously set types in all 
		cases, we should install a mechanism that sets the value
		of this variable according to its condition *once*.	*/

	g_strfreev(strv);
	strv = NULL;

/*g_print("gernel_var_item_new_from_raw:	FIXME: make font customizable\n");*/
/*	if (var_item->type != GERNEL_VAR_ITEM_TYPE_HEADLINE)
		gernel_var_item_set_font(var_item, SMALL_FONT_SET);	*/

	if (var_item->description == NULL) {
		description = get_enclosed(str, CONFIG_IN_SHORT_DESCR_MARK);

		if (strlen(description) == 0) {
			g_free(description);
			return NULL;
		} else
			gernel_var_item_set_description(var_item, description);
	}

	switch (var_item->type) {
		gchar		*tmp;

		case GERNEL_VAR_ITEM_TYPE_HEADLINE:
			break;
		case GERNEL_VAR_ITEM_TYPE_CHOICE:

			gernel_var_item_set_option_entries(	var_item, 
								str, 
								stack,
								cache	);

			strv = g_strsplit(str, CONFIG_IN_BLANK, 0);
			gernel_var_item_set_value(var_item, get_last(strv));

			g_strfreev(strv);
			break;
		case GERNEL_VAR_ITEM_TYPE_DEP_BOOL:	
		case GERNEL_VAR_ITEM_TYPE_DEP_MBOOL:	
		case GERNEL_VAR_ITEM_TYPE_DEP_HEX:
		case GERNEL_VAR_ITEM_TYPE_DEP_STRING:
		case GERNEL_VAR_ITEM_TYPE_DEP_INT:
		case GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE:

			strv = g_strsplit(	str, 
						CONFIG_IN_SHORT_DESCR_MARK, 
						0	);

			tmp = g_strdup(get_last(strv));
			g_strfreev(strv);
			g_strstrip(tmp);
			strv = g_strsplit(tmp, CONFIG_IN_BLANK, 1);
			g_free(tmp);
			tmp = strv[1];

			if (match0(tmp, CONFIG_IN_VAR_DENOTE)) {

				gernel_var_item_inline_dep(	var_item, 
								tmp,
								cache	);

				gernel_var_item_set_false(var_item);
			} else
				gernel_var_item_set_value(var_item, tmp);

			g_strfreev(strv);

			break;
		case GERNEL_VAR_ITEM_TYPE_HEX:
		case GERNEL_VAR_ITEM_TYPE_STRING:
		case GERNEL_VAR_ITEM_TYPE_INT:
			strv = g_strsplit(str, CONFIG_IN_SHORT_DESCR_MARK, 0);
			tmp = g_strdup(get_last(strv));
			g_strfreev(strv);
			g_strstrip(tmp);
			strv = g_strsplit(tmp, CONFIG_IN_BLANK, 0);
			g_free(tmp);
			tmp = g_strdup(get_last(strv));
			g_strfreev(strv);

			if (match0(tmp, CONFIG_IN_VAR_DENOTE)) {
				GernelVarItem	*item;
				gchar		*var_name;

				var_name = remove_strs(
						tmp, 
						CONFIG_IN_VAR_DENOTE
						);

				g_free(tmp);

				item = GERNEL_VAR_ITEM(
						g_cache_insert(cache, var_name)
						);

				g_free(var_name);
				tmp = g_strdup(gernel_var_item_get_value(item));
			}

			gernel_var_item_set_value(var_item, tmp);
			g_free(tmp);

			break;
		case GERNEL_VAR_ITEM_TYPE_DEFINE_BOOL:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE:
			/*	FIXME	*/
			gernel_var_item_set_false(var_item);

			break;
		default:
			strv = g_strsplit(str, CONFIG_IN_BLANK, 0);
			gernel_var_item_set_value(var_item, get_last(strv));
			g_strfreev(strv);

			break;
	}

	gernel_var_item_set_condition(var_item, stack);

	return widget_item;
}

static void gernel_var_item_init(	GernelVarItem *var_item	)
/*	initialize a GernelVarItem	*/
{
	GtkWidget	*hbox;

	GTK_WIDGET_SET_FLAGS(var_item, GTK_CAN_FOCUS);
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(hbox);
	gtk_container_add(GTK_CONTAINER(var_item), hbox);
	var_item->type = GERNEL_VAR_ITEM_TYPE_0;
	var_item->description = NULL;
	var_item->long_description = NULL;
	var_item->name = NULL;
	var_item->option_entries = NULL;
	var_item->dependants = NULL;
	var_item->condition = NULL;
	var_item->value = NULL;
	gernel_var_item_set_false(var_item);
	var_item->allow_mod = TRUE;
	var_item->allow_yes = TRUE;
	var_item->var_item_icon_set = NULL;
}

static void gernel_var_item_class_init(	GernelVarItemClass *klass	)
/*	initializes the GernelVarItem class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkItemClass		*item_class;
	GtkTreeItemClass	*tree_item_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	item_class = (GtkItemClass*)klass;
	tree_item_class = (GtkTreeItemClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_TREE_ITEM);

	function_offset = GTK_SIGNAL_OFFSET(GernelVarItemClass, value_changed);

	var_item_signals[VALUE_CHANGED] =

			gtk_signal_new(	"value_changed",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class, 
					var_item_signals, 
					LAST_SIGNAL	);

	object_class->destroy = gernel_var_item_destroy;

	klass->value_changed = NULL;
}

static gint condition_count(	GNode *condition	)
/*	returns number of preconditional expressions of var_item	*/
{
	/*	BASIS.	*/
	if (G_NODE_IS_LEAF(condition))
		return 1;
	else {
	/*	INDUCTION.	*/
		gint	count;
		GNode	*child;

		count = 0;

		for (child = condition->children; child; child = child->next)
			count = count + condition_count(child);

		return count;
	}
}

void gernel_var_item_set_long_description(	GernelVarItem *var_item,
						gchar *long_description	)
/*	Set value of long_description of var_item	*/
{
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(long_description != NULL);

	var_item->long_description = g_strdup(long_description);
}

static void gernel_var_item_set_description(	GernelVarItem *var_item,
						gchar *description	)
/*	set value of var_item->description to description	*/
{
/*	GtkWidget	*label;
	GtkBox		*box;	*/

	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(GTK_IS_BOX(GTK_BIN(var_item)->child));

	var_item->description = description;
}

static void gernel_var_item_set_font(	GernelVarItem *var_item,
					gchar *font	)
/*	set font of var_item	*/
{
	var_item->fontset = font;
}

void gernel_var_item_set_type(	GernelVarItem *var_item,
				enum GernelVarItemType type	)
/*	set type of var_item	*/
{
	var_item->type = type;
}

void gernel_var_item_set_type_from_str(	GernelVarItem *var_item,
					gchar *string	)
/*	Map string to a GernelVarItemType	*/
{
	enum GernelVarItemType	type;

	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(string != NULL);

	if (match(string, CONFIG_IN_BOOL)) 
		type = GERNEL_VAR_ITEM_TYPE_BOOL;
	else if (match(string, CONFIG_IN_TRISTATE)) 
		type = GERNEL_VAR_ITEM_TYPE_TRISTATE;
	else if (match(string, CONFIG_IN_HEX)) 
		type = GERNEL_VAR_ITEM_TYPE_HEX;
	else if (match(string, CONFIG_IN_CHOICE)) 
		type = GERNEL_VAR_ITEM_TYPE_CHOICE;
	else if (match(string, CONFIG_IN_DEFINE_BOOL)) 
		type = GERNEL_VAR_ITEM_TYPE_DEFINE_BOOL;
	else if (match(string, CONFIG_IN_DEP_BOOL)) 
		type = GERNEL_VAR_ITEM_TYPE_DEP_BOOL;
	else if (match(string, CONFIG_IN_DEP_MBOOL)) 
		type = GERNEL_VAR_ITEM_TYPE_DEP_MBOOL;
	else if (match(string, CONFIG_IN_DEP_TRISTATE)) 
		type = GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE;
	else if (match(string, CONFIG_IN_HEADLINE)) 
		type = GERNEL_VAR_ITEM_TYPE_HEADLINE;
	else if (match(string, CONFIG_IN_STRING)) 
		type = GERNEL_VAR_ITEM_TYPE_STRING;
	else if (match(string, CONFIG_IN_INT)) 
		type = GERNEL_VAR_ITEM_TYPE_INT;
	else if (match(string, CONFIG_IN_ARCH)) 
		type = GERNEL_VAR_ITEM_TYPE_ARCH;
	else
		type = GERNEL_VAR_ITEM_TYPE_0;

	gernel_var_item_set_type(var_item, type);
}

void gernel_var_item_set_value(	GernelVarItem *var_item,
				gchar *str	)
/*	set current value of var_item; for items of type CHOICE, we expect
	the appropriate option entry of var_item passed as value.	*/
{
	gchar	*value;

	g_return_if_fail(str != NULL);

	value = g_strdup(str);
	g_strstrip(value);

	switch (var_item->type) {
		GSList	*list;
		gchar	*val;

		case GERNEL_VAR_ITEM_TYPE_CHOICE:
			list = var_item->option_entries;
			gernel_var_item_set_false_all(var_item);

			while (list) {
				GernelVarItem	*item;

				item = GERNEL_VAR_ITEM(list->data);

				if (match(item->description, value)) {
					gernel_var_item_set_true(item);
					g_free(value);

					return;
				}

				list = list->next;
			} 

			g_print(	"%s has no corresponding key in '%s',",
					value,
					var_item->description	);

			list = var_item->option_entries;

			while (list) {
				GernelVarItem	*item;

				item = GERNEL_VAR_ITEM(list->data);

				if (match0(item->description, value)) {

					g_print(_(" using best match '%s'.\n"),
						item->description	);

					gernel_var_item_set_true(item);
					g_free(value);

					return;
				}

				list = list->next;
			}

			g_print(_(" cannot correct error.\n"));
			g_error(_("Syntax Error in Configuration"));

			break;
		case GERNEL_VAR_ITEM_TYPE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_BOOL:
			if (	(!match(value, CONFIG_IN_YES)) &
				(!match(value, CONFIG_IN_MOD)) &
				(!match(value, CONFIG_IN_NO)) &
				(!match0(value, CONFIG_IN_VAR_DENOTE))	)

				gernel_var_item_set_false(var_item);
			else {
				val = gernel_var_item_get_value(var_item);
				if (val != NULL) g_free(val);
				var_item->value = value;
			}

			break;
		default:
			val = gernel_var_item_get_value(var_item);
			if (val != NULL) g_free(val);
			var_item->value = value;
			break;
	}
}

static void gernel_var_item_destroy(	GtkObject *object	)
/*	Destructor for GernelVarItem	*/
{
	GernelVarItem	*var_item;
	gchar		*name, *value;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(object));

	var_item = GERNEL_VAR_ITEM(object);
	if (var_item->description) g_free(var_item->description);
	if (var_item->long_description) g_free(var_item->long_description);

	value = gernel_var_item_get_value(var_item);
	if (value != NULL) g_free(value);

	name = gernel_var_item_get_name(var_item);
	if (name) g_free(name);

	if (var_item->option_entries) {
		GSList		*item;

		for (item = var_item->option_entries; item; item = item->next)
			gernel_var_item_destroy(GTK_OBJECT(item->data));

		g_slist_free(var_item->option_entries);
	}

	if (var_item->dependants) g_slist_free(var_item->dependants);
	if (var_item->condition) g_node_destroy(var_item->condition);

/*FIXME	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(*GTK_OBJECT_CLASS(parent_class)->destroy)(object);	*/
}

gboolean gernel_var_item_meets_condition(	GNode *term	)
/*	evaluates a GernelVarItem->precondition like expression tree	*/
{
	GNode		*expr;
	gboolean	b;

	b = TRUE;

	/*	BASIS.	*/
	if (term == NULL) return b;
	if (G_NODE_IS_LEAF(term)) return eval(GERNEL_SINGLE_EXPR(term->data));

	/*	INDUCTION.	*/
	switch (GPOINTER_TO_INT(term->data)) {
		case OPERATOR_AND:
			b = TRUE;

			for (expr = term->children; expr; expr = expr->next)
				b = b & gernel_var_item_meets_condition(expr);

			break;
		case OPERATOR_OR:
			b = FALSE;

			for (expr = term->children; expr; expr = expr->next)
				b = b | gernel_var_item_meets_condition(expr);

			break;
		case OPERATOR_NOT:
			b = ~gernel_var_item_meets_condition(term->children);
			break;
		default:

			g_print(	"Operator %s not recognized\n", 
					G_CHAR(term->data)	);

			break;
	}

	return b;
}

void gernel_var_item_mod_dep_sync(	GernelVarItem *var_item	)
/*	In case this var_item is set to 'm', inform all of its dependants */
{
	gchar	*value;

	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	value = gernel_var_item_get_value(var_item);

	if (match(value, CONFIG_IN_MOD))

		g_slist_foreach(	var_item->dependants, 
					(GFunc)gernel_var_item_allow_yes,
					var_item	);
}

void gernel_var_item_sens_sync(	GernelVarItem *var_item	)
/*	sets item's sensitivity according to the variable's dependency
	requirements	*/
{
	gboolean	sensitivity;

	g_return_if_fail(var_item != NULL);

	if (var_item == NULL) return;
	sensitivity = gernel_var_item_meets_condition(var_item->condition);

	switch (var_item->type) {
		GtkWidget	*widget;

		case GERNEL_VAR_ITEM_TYPE_DEFINE_BOOL:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_HEX:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_INT:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_STRING:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE:
			break;
		default:
			widget = GTK_BIN(var_item)->child;
			gtk_widget_set_sensitive(widget, sensitivity);
	}
}

void gernel_var_item_set_mod(	GernelVarItem *var_item)
/*	sets current value of this_var to 'm'; value should be free'd when
	no longer needed	*/
{
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gernel_var_item_set_value(var_item, CONFIG_IN_MOD);
	gernel_var_item_value_changed(var_item);
}

void gernel_var_item_no_previous(	GernelVarItem *var_item	)
/*	sets the prev_mod flag to FALSE to prevent a change of CONFIG_MODULES
	to set this var to 'module'	*/
{
	switch (var_item->type) {
		case GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE: 
		case GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_TRISTATE:
			var_item->prev_mod = FALSE;
			var_item->prev_yes = FALSE;
			break;
		default:
			break;
	}
}

void gernel_var_item_allow_yes_deps(	GernelVarItem *var_item	)
/*	inform all dependants of var_item (which is required to be of type
	*_TRISTATE) about their prerequisite not allowing for 'yes' setting
 */
{
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	g_slist_foreach(	var_item->dependants, 
				(GFunc)gernel_var_item_allow_yes,
				var_item	);

	g_slist_foreach(	var_item->dependants,
				(GFunc)gernel_var_item_icon_sync,
				NULL	);
}

void gernel_var_item_allow_yes(	GernelVarItem *dependant,
				GernelVarItem *var_item	)
/*	inform dependant of its prerequisite (var_item) setting and set its
	flags accordingly	*/
{
	switch (dependant->type) {
		gchar	*value;

		case GERNEL_VAR_ITEM_TYPE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE:
			value = gernel_var_item_get_value(var_item);

			if (match(value, CONFIG_IN_YES)) {
				dependant->allow_yes = TRUE;

				if (dependant->prev_yes) {
					gernel_var_item_set_true(dependant);
					dependant->prev_yes = FALSE;
				}

			} else if (match(value, CONFIG_IN_MOD)){
				gchar	*tmp;

				dependant->allow_yes = FALSE;
				tmp = gernel_var_item_get_value(dependant);

				if (match(tmp, CONFIG_IN_YES)) {
					gernel_var_item_set_mod(dependant);
					dependant->prev_yes = TRUE;
				}
			}

			break;
		default:
			break;
	}
}

void gernel_var_item_allow_module(	GernelVarItem *var_item,
					GernelVarItem *mod_item	)
/*	inform a *_TRISTATE variable of its prerequisite (mod_item) setting and
	set its flags accordingly;	*/
{
	switch (var_item->type) {
		gchar	*value;

		case GERNEL_VAR_ITEM_TYPE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE:
			value = gernel_var_item_get_value(mod_item);

			if (match(value, CONFIG_IN_YES)) {
				var_item->allow_mod = TRUE;

				if (var_item->prev_mod)
					gernel_var_item_set_mod(var_item);

				var_item->prev_mod = FALSE;
			} else {
				gchar	*val;

				val = gernel_var_item_get_value(var_item);
				var_item->allow_mod = FALSE;

				if (match(val, CONFIG_IN_MOD)) {
					gernel_var_item_set_true(var_item);
					var_item->prev_mod = TRUE;
				}
			}

			break;
		default:
			break;
	}
}

void gernel_var_item_icon_sync(	GernelVarItem *var_item	)
/*	show the appropriate icon connected to var (and hide the others)
 */
{
	GSList				*list;
	enum GernelVarItemIconSetValue	type;
	gint				index;
	gchar				*value;

	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(gernel_var_item_get_value(var_item) != NULL);

	index = -1;
	value = gernel_var_item_get_value(var_item);

	switch (var_item->type) {
		GernelVarItem	*item;

		case GERNEL_VAR_ITEM_TYPE_HEX:
		case GERNEL_VAR_ITEM_TYPE_INT:
		case GERNEL_VAR_ITEM_TYPE_STRING:
			type = GERNEL_VAR_ITEM_ICON_SET_VALUE_VALUE;
			break;
		case GERNEL_VAR_ITEM_TYPE_CHOICE:
			item = NULL;
			type = GERNEL_VAR_ITEM_ICON_SET_VALUE_STRING;

			for (	list = var_item->option_entries;
				list;
				list = list->next	) {

				item = GERNEL_VAR_ITEM(list->data);
				value = gernel_var_item_get_value(item);
				if (match(value, CONFIG_IN_YES)) break;
			}

			index = g_slist_index(var_item->option_entries, item);
			break;
		case GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_TRISTATE:

			if (match(value, CONFIG_IN_YES))
				type = GERNEL_VAR_ITEM_ICON_SET_VALUE_YES;
			else if (match(value, CONFIG_IN_MOD))
				type = GERNEL_VAR_ITEM_ICON_SET_VALUE_MODULE;
			else 
				type = GERNEL_VAR_ITEM_ICON_SET_VALUE_NO;

			break;
		case GERNEL_VAR_ITEM_TYPE_DEP_BOOL:
		case GERNEL_VAR_ITEM_TYPE_DEP_MBOOL:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_BOOL:
		case GERNEL_VAR_ITEM_TYPE_BOOL:

			if (match(value, CONFIG_IN_YES))
				type = GERNEL_VAR_ITEM_ICON_SET_VALUE_YES;
			else
				type = GERNEL_VAR_ITEM_ICON_SET_VALUE_NO;

			break;
		default:
			type = GERNEL_VAR_ITEM_ICON_SET_VALUE_0;
			break;
	}

	if (type != GERNEL_VAR_ITEM_ICON_SET_VALUE_0) {
		GernelVarItemIconSet		*icon_set;

		icon_set= GERNEL_VAR_ITEM_ICON_SET(var_item->var_item_icon_set);

		gernel_var_item_icon_set_set_value(	icon_set, 
							type, 
							index, 
							value	);
	}
}

void gernel_var_item_set_false(	GernelVarItem *var_item	)
/*	sets current value of this_var to 'n'; value should be free'd when
	no longer needed	*/
{
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gernel_var_item_set_value(var_item, CONFIG_IN_NO);
	gernel_var_item_value_changed(var_item);
}

void gernel_var_item_set_false_all(	GernelVarItem *var_item	)
/*	sets all option entries of var_item (which is of type CHOICE) to false
 */
{
	GSList	*list;

	for (list = var_item->option_entries; list; list = list->next)
		gernel_var_item_set_false(GERNEL_VAR_ITEM(list->data));
}

void gernel_var_item_set_true(	GernelVarItem *var_item	)
/*	sets current value of this_var to 'y'; value should be free'd when
	no longer needed	*/
{
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gernel_var_item_set_value(var_item, CONFIG_IN_YES);
	gernel_var_item_value_changed(var_item);
}

void gernel_var_item_set_condition(	GernelVarItem *var_item,
					GSList *prereq_stack	)
/*	prereq_stack is assumed to contain boolean expression trees (see 
	gernel_var_item.h for explanation). The trees in the stack will be
	glued together by this function and the resulting expression tree
	taken as var_item's condition	*/
{
	GSList	*stack;
	GNode	*new_condition;

	if (var_item->type == GERNEL_VAR_ITEM_TYPE_HEADLINE) return;
	if (prereq_stack == NULL) return;

	if (var_item->condition) {
		new_condition = g_node_new(GINT_TO_POINTER(OPERATOR_AND));
		g_node_append(new_condition, var_item->condition);
		var_item->condition = new_condition;
	} else if (g_slist_length(prereq_stack) > 1) {
		new_condition = g_node_new(GINT_TO_POINTER(OPERATOR_AND));
		var_item->condition = new_condition;
	} else {
		var_item->condition = gernel_node_copy(prereq_stack->data);
		return;
	}

	for (stack = prereq_stack; stack; stack = stack->next) {
		g_return_if_fail(G_NODE_IS_ROOT(stack->data));

		g_node_append(	var_item->condition, 
				gernel_node_copy(stack->data)	);

	}
}

void gernel_var_item_inline_dep(	GernelVarItem *var_item,
					gchar *dependency,
					GCache *cache	)
/*	apply inline dependencies to the condition tree of var_item	*/
{
	GNode		*condition;
	GString		*expr;
	gchar		**exprv;
	gint		i;

/*	FIXME:	inline dependencies make up for an overhead in variables 
	searches. consider maintaining a list (?) of already found variables
	to avoid searching for the same variable multiple times	*/

	exprv = g_strsplit(dependency, CONFIG_IN_BLANK, 0);
	expr = g_string_new(NULL);

	for (i = 0; exprv[i]; i++) {
		if (strlen(exprv[i])) {
			expr = g_string_append(expr, CONFIG_IN_VAR_QUOTE);
			expr = g_string_append(expr, exprv[i]);
			expr = g_string_append(expr, CONFIG_IN_VAR_QUOTE);
			expr = g_string_append(expr, CONFIG_IN_BLANK);
			expr = g_string_append(expr, CONFIG_IN_NEQUAL);
			expr = g_string_append(expr, CONFIG_IN_BLANK);
			expr = g_string_append(expr, CONFIG_IN_NO);

			if (exprv[i+1]) {
				expr = g_string_append(expr, CONFIG_IN_BLANK);
				expr = g_string_append(expr, CONFIG_IN_A);
				expr = g_string_append(expr, CONFIG_IN_BLANK);
			}
		}
	}

	condition = create_stack_item(NULL, expr->str, cache);
	g_string_free(expr, TRUE);
	g_strfreev(exprv);

	gernel_var_item_set_condition(	var_item, 
					g_slist_append(NULL, condition) );
}

static gboolean gernel_var_item_dependant(	GNode *single_expr,
						GernelVarItem *dep	)
/*	callback function to inform the GernelVarItem in single_expr of its
	dependant dep	*/
{
	GernelVarItem	*var_item;

	g_return_val_if_fail(dep != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(dep), FALSE);
	g_return_val_if_fail(single_expr != NULL, FALSE);
	g_return_val_if_fail(G_NODE_IS_LEAF(single_expr), FALSE);
	g_return_val_if_fail(single_expr->data != NULL, FALSE);

g_print("gernel_var_item_dependant:	FIXME: ignoring dependancy checking\n");
return FALSE;
	g_return_val_if_fail(
		GERNEL_SINGLE_EXPR(single_expr->data)->var_item != NULL, FALSE);

g_print("gernel_var_item_item_dependant:	invoked\n");
	var_item = GERNEL_SINGLE_EXPR(single_expr->data)->var_item;
g_print("gernel_var_item_item_dependant:	mark0\n");

	if (!gernel_var_item_is_dependant(var_item, dep))
{
g_print("gernel_var_item_dependant:	mark\n");

		var_item->dependants = g_slist_append(	var_item->dependants, 
							dep	);
g_print("gernel_var_item_dependant:	mark2\n");
}

g_print("gernel_var_item_item_dependant:	done\n");
	return FALSE;
}

void gernel_var_item_check_condition(	GernelVarItem *var_item	)
/*	inform each prerequisite of var_item about var_item	*/
{
	g_return_if_fail(var_item != NULL);

	if (var_item->condition == NULL) return;

	g_node_traverse(	var_item->condition, 
				G_IN_ORDER, 
				G_TRAVERSE_LEAFS, 
				-1, 
				(GNodeTraverseFunc)gernel_var_item_dependant,
				var_item	);
}

static GernelVarItem* item_list_match(
		GSList *list,
		gchar *var,
		enum GernelVarItemField field
		)
/*	checks if var (description/var_name, depends on search_mode) matches any
	of the GernelVarItems in list; returns matched GernelVarItem if
	appropriate	*/
{
	g_return_val_if_fail(var != NULL, NULL);

	/*	INDUCTION.	*/
	if (list) {
		gchar		*str;
		GernelVarItem	*item;

		item = GERNEL_VAR_ITEM(list->data);
		str = gernel_var_item_get_field(item, field);

		g_return_val_if_fail(str != NULL, NULL);

		if (match(str, var)) return item;
		return item_list_match(list->next, var, field);
	} else
		/*	BASIS.	*/
		return NULL;
}

GtkWidget* gernel_var_item_compare_equals_name_sensitive(
		GernelVarItem *var_item,
		gchar *name
		)
/*	Return var_item if name == var_item's name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item, 
					name,
					GERNEL_VAR_ITEM_FIELD_NAME,
					GERNEL_VAR_ITEM_MATCH_MODE_EQUALS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_equals_name(	GernelVarItem *var_item,
						gchar *name	)
/*	Return var_item if name == var_item's name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item, 
					name,
					GERNEL_VAR_ITEM_FIELD_NAME,
					GERNEL_VAR_ITEM_MATCH_MODE_EQUALS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_ends_name_sensitive(	GernelVarItem *var_item,
							gchar *name	)
/*	Return var_item if its name field ends in name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					name,
					GERNEL_VAR_ITEM_FIELD_NAME,
					GERNEL_VAR_ITEM_MATCH_MODE_ENDS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE	);
}

GtkWidget* gernel_var_item_compare_ends_name(	GernelVarItem *var_item,
						gchar *name	)
/*	Return var_item if its name field ends in name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					name,
					GERNEL_VAR_ITEM_FIELD_NAME,
					GERNEL_VAR_ITEM_MATCH_MODE_ENDS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_starts_name_sensitive(
		GernelVarItem *var_item,
		gchar *name
		)
/*	Return var_item if its name field starts with name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					name,
					GERNEL_VAR_ITEM_FIELD_NAME,
					GERNEL_VAR_ITEM_MATCH_MODE_STARTS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_starts_name(	GernelVarItem *var_item,
						gchar *name	)
/*	Return var_item if its name field starts with name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					name,
					GERNEL_VAR_ITEM_FIELD_NAME,
					GERNEL_VAR_ITEM_MATCH_MODE_STARTS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_contains_name_sensitive(
		GernelVarItem *var_item,
		gchar *name
		)
/*	Return var_item if name is contained in var_item's name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item, 
					name, 
					GERNEL_VAR_ITEM_FIELD_NAME, 
					GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE	);
}

GtkWidget* gernel_var_item_compare_contains_name(	GernelVarItem *var_item,
							gchar *name	)
/*	Return var_item if name is contained in var_item's name	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	return gernel_var_item_compare(	var_item, 
					name, 
					GERNEL_VAR_ITEM_FIELD_NAME, 
					GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_equals_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if description equals var_item's description	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_EQUALS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE	);
}

GtkWidget* gernel_var_item_compare_equals_description(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if description equals var_item's description	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_EQUALS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_starts_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if var_item's description starts with description */
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_STARTS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_starts_description(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if var_item's description starts with description */
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_STARTS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_ends_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if var_item's description ends in description	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_ENDS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE	);
}

GtkWidget* gernel_var_item_compare_ends_description(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if var_item's description ends in description	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_ENDS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_contains_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if description is contained in var_item's description */
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_contains_description(
		GernelVarItem *var_item,
		gchar *description
		)
/*	Return var_item if description is contained in var_item's description */
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(description != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					description,
					GERNEL_VAR_ITEM_FIELD_DESCRIPTION,
					GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_contains_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		)
/*	Return var_item if text is contained in var_item's help text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_contains_text(	GernelVarItem *var_item,
							gchar *text	)
/*	Return var_item if text is contained in var_item's help text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_starts_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		)
/*	Return var_item if var_item's text field starts with text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_STARTS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_starts_text(	GernelVarItem *var_item,
						gchar *text	)
/*	Return var_item if var_item's text field starts with text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_STARTS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_ends_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		)
/*	Return var_item if var_item's text field ends in text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_ENDS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_ends_text(	GernelVarItem *var_item,
						gchar *text	)
/*	Return var_item if var_item's text field ends in text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_ENDS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare_equals_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		)
/*	Return var_item if var_item's text field equals text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_EQUALS,
					GERNEL_VAR_ITEM_CASE_SENSITIVE );
}

GtkWidget* gernel_var_item_compare_equals_text(	GernelVarItem *var_item,
						gchar *text	)
/*	Return var_item if var_item's text field equals text	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(text != NULL, NULL);

	return gernel_var_item_compare(	var_item,
					text,
					GERNEL_VAR_ITEM_FIELD_TEXT,
					GERNEL_VAR_ITEM_MATCH_MODE_EQUALS,
					GERNEL_VAR_ITEM_CASE_INSENSITIVE );
}

GtkWidget* gernel_var_item_compare(	GernelVarItem *var_item,
					gchar* pattern,
					enum GernelVarItemField field,
					enum GernelVarItemMatchMode mode,
					enum GernelVarItemCase sensitivity )
/*	compares selected variable in tree to that specified by pattern
	if they match, return the gernel_var_item	*/
{
	GCompareFunc	function;
	gchar		*string;

	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);
	g_return_val_if_fail(pattern != NULL, NULL);

	function = NULL;

	switch (var_item->type) {
		case GERNEL_VAR_ITEM_TYPE_HEADLINE:
			string = NULL;
			break;
		case GERNEL_VAR_ITEM_TYPE_CHOICE:
			string = NULL;

/*			if (	((search_mode == BY_DESCRIPTION) && 
				(match(var_item->description, pattern))) ||

				(item_list_match(var_item->option_entries,
						pattern,
						search_mode
						) != NULL)	)

				result = GTK_WIDGET(var_item);	*/

			break;
		default:
			string = gernel_var_item_get_field(var_item, field);

			function = gernel_var_item_get_compare_function(
					mode,
					sensitivity
					);
	}

	if (	(string != NULL) && 
		(pattern != NULL) &&
		((*function)(string, pattern) == FALSE)	)

		return GTK_WIDGET(var_item);
	else
		return NULL;
}

static GCompareFunc gernel_var_item_get_compare_function(
		enum GernelVarItemMatchMode mode,
		enum GernelVarItemCase sensitivity
		)
/*	Return the compare function corresponding to mode	*/
{
	GCompareFunc	func;

	func = NULL;

	switch (mode) {
		case GERNEL_VAR_ITEM_MATCH_MODE_ENDS:

			func = (sensitivity == GERNEL_VAR_ITEM_CASE_SENSITIVE)?
				(GCompareFunc)gernel_str_ends:
				(GCompareFunc)gernel_str_ends_insensitive;

			break;
		case GERNEL_VAR_ITEM_MATCH_MODE_STARTS:

			func = (sensitivity == GERNEL_VAR_ITEM_CASE_SENSITIVE)?
				(GCompareFunc)gernel_str_starts:
				(GCompareFunc)gernel_str_starts_insensitive;

			break;
		case GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS:

			func = (sensitivity == GERNEL_VAR_ITEM_CASE_SENSITIVE)?
				(GCompareFunc)gernel_str_contains:
				(GCompareFunc)gernel_str_contains_insensitive;

			break;
		case GERNEL_VAR_ITEM_MATCH_MODE_EQUALS:

			func = (sensitivity == GERNEL_VAR_ITEM_CASE_SENSITIVE)?
				(GCompareFunc)gernel_str_equals:
				(GCompareFunc)gernel_str_equals_insensitive;

			break;
		default:
			g_assert_not_reached();
			break;
	}

	return func;
}

static gchar* gernel_var_item_get_field(	GernelVarItem *var_item,
						enum GernelVarItemField field )
/*	Gets field of var_item corresponding to field	*/
{
	gchar	*string;

	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	string = NULL;

	switch (field) {
		case GERNEL_VAR_ITEM_FIELD_NAME:
			string = gernel_var_item_get_name(var_item);
			break;
		case GERNEL_VAR_ITEM_FIELD_TEXT:
			string = gernel_var_item_get_text(var_item);
			break;
		case GERNEL_VAR_ITEM_FIELD_DESCRIPTION:
			string = gernel_var_item_get_description(var_item);
			break;
		default:
			g_assert_not_reached();
			break;
	}

	return string;
}

void gernel_var_item_deps_sync(	GernelVarItem *var_item)
/*	for each in this variable's dependants: check if their preconditions are
	met and set their sensitivity accordingly	*/
{
	GSList		*list;

	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(GTK_IS_TREE(GTK_WIDGET(var_item)->parent));

	for (list = var_item->dependants; list; list = list->next)
		gernel_var_item_sens_sync(GERNEL_VAR_ITEM(list->data));
}

void gernel_var_item_deps_sync_all(	GernelVarItem *choice_item	)
/*	dependency syncs a choice_item	*/
{
	GSList	*list;

	for (list = choice_item->option_entries; list; list = list->next)
		gernel_var_item_deps_sync(GERNEL_VAR_ITEM(list->data));
}

void gernel_var_item_description(	GernelVarItem *var_item	)
/*	Create descriptive label for var_item	*/
{
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	if (var_item->description) {
		GtkWidget	*label;
		GtkBox		*box;

		label = gtk_label_new(var_item->description);
		box = GTK_BOX(GTK_BIN(var_item)->child);
		gtk_box_pack_start(box, label, FALSE, FALSE, 0);

		if (var_item->fontset) {
			GdkFont		*font;
			GtkStyle	*style;

			font = gdk_fontset_load(var_item->fontset);
			style = gtk_style_copy(gtk_widget_get_style(label));
			style->font = font;
			gtk_widget_set_style(label, style);
		}

		gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
		gtk_widget_show(label);
	}
}

void gernel_var_item_icons(	GernelVarItem *var_item,
				GdkWindow *window	)
/*	create icons (or other explanatory widgets) for var_item	*/
{
	GSList				*var_items;
	enum GernelVarItemIconSetType	flag;

	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(window != NULL);

	var_items = NULL;
	flag = GERNEL_VAR_ITEM_ICON_SET_TYPE_0;

	switch (var_item->type) {
		case GERNEL_VAR_ITEM_TYPE_HEX:
		case GERNEL_VAR_ITEM_TYPE_INT:
		case GERNEL_VAR_ITEM_TYPE_STRING:
			flag = GERNEL_VAR_ITEM_ICON_SET_TYPE_VALUE;
			var_items = g_slist_append(var_items, var_item);
			break;
		case GERNEL_VAR_ITEM_TYPE_CHOICE:
			flag = GERNEL_VAR_ITEM_ICON_SET_TYPE_CHOICE;
			var_items = var_item->option_entries;
			break;
		case GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE:
		case GERNEL_VAR_ITEM_TYPE_TRISTATE:
			flag = GERNEL_VAR_ITEM_ICON_SET_TYPE_TRISTATE;
			break;
		case GERNEL_VAR_ITEM_TYPE_DEP_BOOL:
		case GERNEL_VAR_ITEM_TYPE_DEP_MBOOL:
		case GERNEL_VAR_ITEM_TYPE_DEFINE_BOOL:
		case GERNEL_VAR_ITEM_TYPE_BOOL:
			flag = GERNEL_VAR_ITEM_ICON_SET_TYPE_BOOL;
			break;
		default:
			return;
			break;
	}

	gernel_var_item_construct_set(var_item, flag, window, var_items);
}

void gernel_var_item_set_option_entries(	GernelVarItem *var_item,
						gchar *str,
						GSList *stack,
						GCache *cache	)
/*	sets up a list of GernelVarItems to be used as the option_entries list
	of var_item.  str is in config.in style CHOICE format	*/
{
	GSList		*entries = NULL;
	gint		n;
	gchar		*tmp, **s;

	g_return_if_fail(str != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	tmp = get_enclosed(str, CONFIG_IN_VAR_QUOTE);
	s = g_strsplit(tmp, CONFIG_IN_BLANK, 0);
	g_free(tmp);
	n = 0;

	while (s[n]) {
		gchar		*description, *var_name, *line;
		GtkWidget	*entry;

		/*	get rid of empty fields	*/
		while (strlen(s[n]) == 0) if (s[n] != NULL) n++; else break;

		description = s[n];
		n++;
		while (strlen(s[n]) == 0) n++;
		var_name = s[n];
		line = g_strconcat("bool '", description, "' ", var_name, NULL);
		entry = gernel_var_item_new_from_raw(line, stack, cache);
		gtk_widget_set_parent(GTK_WIDGET(entry), GTK_WIDGET(var_item));
		g_free(line);
		entries = g_slist_append(entries, entry);
		n++;
	}

	g_strfreev(s);
	var_item->option_entries = entries;
}

static void	gernel_var_item_value_changed(	GernelVarItem *var_item	)
/*	Emit the 'value_changed' signal	*/
{
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	gtk_signal_emit(GTK_OBJECT(var_item), var_item_signals[VALUE_CHANGED]);
}

static void gernel_var_item_pack_icons(	GernelVarItem *var_item, 
					GtkWidget *var_item_icon_set	)
/*	Pack icon_set into var_item	*/
{
	GtkBox	*box;

	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(var_item_icon_set != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM_ICON_SET(var_item_icon_set));

	box = GTK_BOX(GTK_BIN(var_item)->child);
	gtk_box_pack_start(box, var_item_icon_set, FALSE, FALSE, 0);
}

static void gernel_var_item_construct_set(
		GernelVarItem *var_item, 
		enum GernelVarItemIconSetType type,
		GdkWindow *window,
		GSList *var_items
		)
/*	Construct the GernelVarItemIconSet for var_item	*/
{
	GtkWidget	*var_item_icon_set;

	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));
	g_return_if_fail(type != GERNEL_VAR_ITEM_ICON_SET_TYPE_0);
	g_return_if_fail(window != NULL);

	var_item_icon_set = gernel_var_item_icon_set_new(	window, 
								type, 
								var_items );

	gernel_var_item_pack_icons(var_item, var_item_icon_set);
	var_item->var_item_icon_set = var_item_icon_set;
	gtk_widget_show(var_item_icon_set);
}

gchar* gernel_var_item_get_description(	GernelVarItem *var_item	)
/*	Retrieve var_item's short description	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	return var_item->description;
}

gchar* gernel_var_item_get_name(	GernelVarItem *var_item	)
/*	Return field name of var_item	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	return var_item->name;
}

gchar* gernel_var_item_get_text(	GernelVarItem *var_item	)
/*	Return field long_description of var_item 	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	return var_item->long_description;
}

gchar* gernel_var_item_get_value(	GernelVarItem *var_item	)
/*	Return field value of var_item	*/
{
	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	return var_item->value;
}

static gboolean eval(	gernel_single_expr *expr	)
/*	evaluates a single expression (e.g. y != n)	*/
{
	gboolean	b;
	gchar		*current, *value;

	b = FALSE;

g_print("eval:	FIXME: not evaluating\n");
g_print("eval:	FIXME: (not here) GernelSearch dialog should use the Liberty entry\n");
g_print("eval:	FIXME: (not here) variables selected in GernelSearch dialog don't show up in navigation/viewer\n");
return b;
	g_return_val_if_fail(expr->var_item != NULL, FALSE);

	current = gernel_var_item_get_value(expr->var_item);
	value = expr->value;

	switch (expr->operator) {
		case COMPARE_EQUAL:
			b = match(current, value);
			break;
		case COMPARE_NOT_EQUAL:
			b = !match(current, value);
			break;
		default:

			g_print(	"eval: Operator %d not recognized\n", 
					expr->operator	);

			break;
	}

	return b;
}

static gboolean gernel_var_item_is_dependant(	GernelVarItem *var_item,
						GernelVarItem *dependant )
/*	Check if dependant is already mentioned in var_item's list of 
	dependants	*/
{
	g_return_val_if_fail(var_item != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), FALSE);

	return g_slist_index(var_item->dependants, dependant) >= 0;
}

