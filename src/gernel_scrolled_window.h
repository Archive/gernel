#define	GERNEL_TYPE_SCROLLED_WINDOW	(gernel_scrolled_window_get_type())

#define GERNEL_SCROLLED_WINDOW(obj) \
	(GTK_CHECK_CAST(	(obj), \
				GERNEL_TYPE_SCROLLED_WINDOW, \
				GernelScrolledWindow	))

#define GERNEL_SCROLLED_WINDOW_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				GERNEL_TYPE_SCROLLED_WINDOW, \
				GernelScrolledWindowClass	))

#define GERNEL_IS_SCROLLED_WINDOW(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_SCROLLED_WINDOW))

#define GERNEL_IS_SCROLLED_WINDOW_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_SCROLLED_WINDOW))

typedef struct _GernelScrolledWindow		GernelScrolledWindow;
typedef struct _GernelScrolledWindowClass	GernelScrolledWindowClass;

struct _GernelScrolledWindow {
	GtkScrolledWindow	scrolled_window;

	GtkWidget		*viewport;
};

struct _GernelScrolledWindowClass {
	GtkScrolledWindowClass	parent_class;
};

GtkType		gernel_scrolled_window_get_type(	void	);
GtkWidget* 	gernel_scrolled_window_new(	void	);

void		gernel_scrolled_window_set_hscrolling(
		GernelScrolledWindow  *scrolled_window,
		GtkPolicyType type
		);

void		gernel_scrolled_window_set_vscrolling(
		GernelScrolledWindow *scrolled_window,
		GtkPolicyType type
		);
