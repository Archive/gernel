#define GERNEL_TYPE_APP	(gernel_app_get_type())
#define GERNEL_APP(obj)	(GTK_CHECK_CAST((obj), GERNEL_TYPE_APP, GernelApp))

#define GERNEL_APP_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass), GERNEL_TYPE_APP, GernelAppClass))

#define GERNEL_IS_APP(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_APP))

#define GERNEL_IS_APP_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_APP))

typedef struct _GernelApp	GernelApp;
typedef struct _GernelAppClass	GernelAppClass;

struct _GernelApp {
	GnomeApp	app;

	GtkWidget	*navigation, *setter, *menu_bar, *toolbar, *canvas,
			*search, *compiler;

	guint		splash_handler;
};

struct _GernelAppClass {
	GnomeAppClass	parent_class;

	void (*close)	(	GernelApp *app	);
};

GtkType		gernel_app_get_type(	void	);
GtkWidget*	gernel_app_new(	void	);

void		gernel_app_set_path(	GernelApp *app,
					gchar *path	);

void		gernel_app_set_splash(	GernelApp *app,
					GtkWidget *splash	);

void		gernel_app_generate_images(	GernelApp *app,
						GtkWidget *window	);

void		gernel_app_remove_splash(	GernelApp *app	);
void		gernel_app_construct_search(	GernelApp *app	);
