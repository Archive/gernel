#include <gernel_scrolled_window.h>

#define GERNEL_TYPE_LIST	(gernel_list_get_type())

#define GERNEL_LIST(obj) \
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_LIST, GernelList))

#define GERNEL_LIST_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_LIST, \
				GernelListClass	))

#define GERNEL_IS_LIST(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_LIST))

#define	GERNEL_IS_LIST_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_LIST))

enum GernelListSortOrder {	GERNEL_LIST_SORT_ORDER_0,
				GERNEL_LIST_SORT_ORDER_ASCENDING,
				GERNEL_LIST_SORT_ORDER_DESCENDING	};

typedef	struct	_GernelList		GernelList;
typedef struct	_GernelListClass	GernelListClass;

struct _GernelList {
	GernelScrolledWindow	window;

	GtkWidget		*list, *tree;
	guint			titles;
};

struct _GernelListClass {
	GernelScrolledWindowClass	parent_class;
};

GtkType 	gernel_list_get_type(	void	);
GtkWidget*	gernel_list_new(	void	);

void		gernel_list_set_tree(	GernelList *list,
					GtkWidget *tree	);

void		gernel_list_append(	GernelList *list,
					GtkWidget *var_item	);

void		gernel_list_select(	GernelList *list,
					guint row	);

void		gernel_list_clear(	GernelList *list	);

void		gernel_list_set_sort_order(	GernelList *list,
						enum GernelListSortOrder order);

void		gernel_list_show_descriptions(	GernelList *list,
						gboolean flag	);

void		gernel_list_show_names(	GernelList *list,
					gboolean flag	);

void		gernel_list_remove_duplicates(	GernelList *list	);
