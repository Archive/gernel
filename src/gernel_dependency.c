#include <gernel.h>
#include <gernel_dependency.h>
#include <gernel_var_item.h>
#include <gernel_scrolled_window.h>
#include <gernel_tree.h>

static void gernel_dependency_init(	GernelDependency *dependency	);
static void gernel_dependency_class_init(	GernelDependencyClass *klass );

static void gernel_dependency_construct(	GernelDependency *dependency,
						GernelDependencyMode mode );

static void gernel_dependency_construct_tree(	GNode *condition, 
						GtkWidget *tree	);

static GnomeDialogClass	*parent_class = NULL;

static void gernel_dependency_add(	GtkContainer *container,
					GtkWidget *widget	);

GtkType gernel_dependency_get_type(	void	)
/*	Return type identifier for type GernelDependency	*/
{
	static GtkType	dependency_type = 0;

	if (!dependency_type) {

		static const GtkTypeInfo	dependency_info = {
			"GernelDependency",
			sizeof(GernelDependency),
			sizeof(GernelDependencyClass),
			(GtkClassInitFunc)gernel_dependency_class_init,
			(GtkObjectInitFunc)gernel_dependency_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		dependency_type = gtk_type_unique(	gnome_dialog_get_type(),
							&dependency_info );
	}

	return dependency_type;
}

GtkWidget* gernel_dependency_new(	GernelDependencyMode mode,
					GtkWidget *var_item	)
/*	Create an instance of GernelDependency	*/
{
	GernelDependency	*dependency;
	GtkWidget		*tree;

	g_return_val_if_fail(var_item != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_VAR_ITEM(var_item), NULL);

	dependency = gtk_type_new(gernel_dependency_get_type());
	gernel_dependency_construct(dependency, mode);
	tree = GERNEL_DEPENDENCY(dependency)->viewer;

	if (mode == GERNEL_DEPENDENCY_MODE_PREREQUISITES)

		gernel_dependency_construct_tree(
				GERNEL_VAR_ITEM(var_item)->condition, 
				dependency->viewer
				);

	return GTK_WIDGET(dependency);
}

static void gernel_dependency_class_init(	GernelDependencyClass *klass )
/*	Initialize the GernelDependencyClass	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkWindowClass		*window_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	window_class = (GtkWindowClass*)klass;

	parent_class = gtk_type_class(GNOME_TYPE_DIALOG);

	container_class->add = gernel_dependency_add;
}

static void gernel_dependency_init(	GernelDependency *dependency	)
/*	Initialize a GernelDependency	*/
{
	dependency->mode = GERNEL_DEPENDENCY_MODE_DEPENDANTS;
	dependency->viewer = NULL;
}

static void gernel_dependency_construct(	GernelDependency *dependency,
						GernelDependencyMode mode )
/*	Assemble the newly created GernelDependency	*/
{
	GtkWidget	*viewer, *scrolled_window;

	g_return_if_fail(dependency != NULL);
	g_return_if_fail(GERNEL_IS_DEPENDENCY(dependency));

	dependency->mode = mode;
	viewer = NULL;

	gnome_dialog_append_button(	GNOME_DIALOG(dependency), 
					GNOME_STOCK_BUTTON_CLOSE	);

	gnome_dialog_set_close(GNOME_DIALOG(dependency), TRUE);

	scrolled_window = gernel_scrolled_window_new();
	gtk_container_add(GTK_CONTAINER(dependency), scrolled_window);
	gtk_widget_show(scrolled_window);

	switch (mode) {
		GtkWidget	*view;

		case GERNEL_DEPENDENCY_MODE_DEPENDANTS:
			viewer = gtk_clist_new(1);

			gtk_container_add(	GTK_CONTAINER(scrolled_window), 
						viewer	);

			break;
		case GERNEL_DEPENDENCY_MODE_PREREQUISITES:
			viewer = gernel_tree_new();
			view = gtk_viewport_new(NULL, NULL);
			gtk_container_add(GTK_CONTAINER(scrolled_window), view);
			gtk_container_add(GTK_CONTAINER(view), viewer);
			gtk_widget_show(view);
			break;
		default:
			g_assert_not_reached();
			break;
	}

	dependency->viewer = viewer;
	gtk_widget_show(viewer);
}

void gernel_dependency_set_title(	GernelDependency *dependency,
					gchar *title	)
/*	Set the title of the dialog	*/
{
	GtkWidget	*label;
	GString		*string;

	g_return_if_fail(dependency != NULL);
	g_return_if_fail(GERNEL_IS_DEPENDENCY(dependency));
	g_return_if_fail(title != NULL);

	string = g_string_new(NULL);

	switch (dependency->mode) {
		case GERNEL_DEPENDENCY_MODE_DEPENDANTS:
			g_string_sprintf(string, _("Dependants of %s:"), title);
			break;
		case GERNEL_DEPENDENCY_MODE_PREREQUISITES:

			g_string_sprintf(	string,
						_("Prerequisites of %s:"),
						title	);

			break;
		default:
			g_assert_not_reached();
	}

	gtk_window_set_title(GTK_WINDOW(dependency), string->str);
	label = gtk_label_new(string->str);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_container_add(GTK_CONTAINER(dependency), label);
	gtk_widget_show(label);
	g_string_free(string, TRUE);
}

static void gernel_dependency_add(	GtkContainer *container,
					GtkWidget *widget	)
/*	Generic add-function for GernelDependency	*/
{
	g_return_if_fail(container != NULL);
	g_return_if_fail(GERNEL_IS_DEPENDENCY(container));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));

	gtk_box_pack_start(	GTK_BOX(GNOME_DIALOG(container)->vbox),
				widget,
				FALSE,
				FALSE,
				0	);
}

void gernel_dependency_add_info(	GernelDependency *dependency,
					gchar *info	)
/*	Add a line to dependency's list	*/
{
	g_return_if_fail(dependency != NULL);
	g_return_if_fail(GERNEL_IS_DEPENDENCY(dependency));
	g_return_if_fail(info != NULL);

	gtk_clist_append(GTK_CLIST(dependency->viewer), &info);
}

static void gernel_dependency_construct_tree(	GNode *condition, 
						GtkWidget *tree	)
/*	map condition onto a newly created GtkTree	*/
{
	GtkWidget	*item;

	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	/*	BASIS.	*/
	if (condition == NULL) return;

	if (G_NODE_IS_LEAF(condition)) {
		gchar			*label;
		gernel_single_expr	*single;
	
		single = GERNEL_SINGLE_EXPR(condition->data);

		label = g_strconcat(
				gernel_var_item_get_name(single->var_item),
				single->operator == COMPARE_EQUAL?
					"==":
					"!=",
				single->value,
				NULL	);

/*		item = gernel_var_item_new(label);	*/
		item = gtk_tree_item_new_with_label(label);
		gernel_tree_append(GERNEL_TREE(tree), item);
		gtk_widget_show(item);
	} else {
		/*	INDUCTION.	*/
		GtkWidget	*new_tree, *pixmap, *hbox;
		GNode		*cond;

		switch (GPOINTER_TO_INT(condition->data)) {
			case OPERATOR_AND:
				pixmap = gtk_pixmap_new(and_data, and_mask);
				break;
			case OPERATOR_OR:
				pixmap = gtk_pixmap_new(or_data, or_mask);
				break;
			case OPERATOR_NOT:
				pixmap = gtk_pixmap_new(not_data, not_mask);
				break;
			default:
				pixmap = NULL;
				break;
		}

		gtk_widget_show(pixmap);

		item = gtk_tree_item_new();
		gernel_tree_append(GERNEL_TREE(tree), item);
		hbox = gtk_hbox_new(FALSE, 0);
		gtk_widget_show(hbox);
		gtk_container_add(GTK_CONTAINER(item), hbox);
		gtk_box_pack_start(GTK_BOX(hbox), pixmap, FALSE, FALSE, 0);

		new_tree = gernel_tree_new();
		gtk_widget_show(new_tree);
		gernel_tree_branch(GERNEL_TREE(tree), new_tree);
		gtk_tree_item_expand(GTK_TREE_ITEM(item));
		gtk_widget_show(item);

		for (cond = condition->children; cond; cond = cond->next)
			gernel_dependency_construct_tree(cond, new_tree);
	}
}
