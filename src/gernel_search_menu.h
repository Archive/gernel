#define	GERNEL_TYPE_SEARCH_MENU	(gernel_search_menu_get_type())

#define GERNEL_SEARCH_MENU(obj) \
	(GTK_CHECK_CAST(obj, GERNEL_TYPE_SEARCH_MENU, GernelSearchMenu))

#define	GERNEL_SEARCH_MENU_CLASS(klass) \
	GTK_CHECK_CLASS_CAST(	(klass), \
				GERNEL_TYPE_SEARCH_MENU, \
				GernelSearchMenuClass	)

#define	GERNEL_IS_SEARCH_MENU(obj) \
	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_SEARCH_MENU))

#define GERNEL_IS_SEARCH_MENU_CLASS(klass) \
	GTK_CHECK_CLASS_TYPE((klass, GERNEL_TYPE_SEARCH_MENU))

typedef struct _GernelSearchMenu	GernelSearchMenu;
typedef struct _GernelSearchMenuClass	GernelSearchMenuClass;

struct _GernelSearchMenu {
	GtkOptionMenu	option_menu;

	GtkWidget	*menu;
};

struct _GernelSearchMenuClass {
	GtkOptionMenuClass		parent_class;

	void (*selection_changed)	(	GernelSearchMenu *search_menu );
};

GtkType		gernel_search_menu_get_type(	void	);
GtkWidget*	gernel_search_menu_new(	void	);

GtkWidget*	gernel_search_menu_get_current_item(
		GernelSearchMenu *search_menu
		);

/*void		gernel_search_menu_add(	GernelSearchMenu *search_menu,
					GtkWidget *item	);*/

void		gernel_search_menu_add_from_data(
		GernelSearchMenu *search_menu,
		gchar *string,
		gboolean flag
		);

gint		gernel_search_menu_get_identifier(
		GernelSearchMenu *search_menu
		);
