#include <config.h>
#include <gnome.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <fcntl.h>

#include <erty/erty.h>

#define PROGRAM		"The Gernel"
#define DESCRIPTION	"A configuration tool for the Linux kernel."
#define	AUTHOR		"Fritz Jetzek (fritz.jetzek@buggerz.com)"
#define COPYRIGHT	"(c) 1999-2001 buggerz.com"
#define LOGO		"gernel.logo.png"

#define DEFCONFIG_IS_NOT_SET	"is not set"
#define DEFCONFIG_COMMENT	"#"

#define CONFIG_IN_TITLE_MARK	"\""

#define DOCUMENTATION_DIRNAME	"Documentation"

/*	types used in config.in; will be mapped to enum 'type' (see below)
	*/
#define CONFIG_IN_FILENAME	"config.in"

#define CONFIG_IN_TITLE		"mainmenu_name"
#define CONFIG_IN_SECTION_MARK	"mainmenu_option"

#define CONFIG_IN_HEADLINE	"comment"
#define CONFIG_IN_TEXT		"text"
#define CONFIG_IN_BOOL		"bool"
#define CONFIG_IN_HEX		"hex"
#define CONFIG_IN_INT		"int"
#define CONFIG_IN_STRING	"string"
#define CONFIG_IN_TRISTATE	"tristate"
#define CONFIG_IN_DEFINE	"define_"
#define CONFIG_IN_DEFINE_BOOL	"define_bool"
#define CONFIG_IN_DEFINE_HEX	"define_hex"
#define CONFIG_IN_DEFINE_INT	"define_int"
#define CONFIG_IN_DEFINE_STRING	"define_string"
#define CONFIG_IN_DEFINE_TRISTATE	"define_tristate"
#define CONFIG_IN_DEP_BOOL	"dep_bool"
#define CONFIG_IN_DEP_MBOOL	"dep_mbool"
#define CONFIG_IN_DEP_HEX	"dep_hex"
#define CONFIG_IN_DEP_INT	"dep_int"
#define CONFIG_IN_DEP_STRING	"dep_string"
#define CONFIG_IN_DEP_TRISTATE	"dep_tristate"
#define CONFIG_IN_UNSET		"unset"
#define CONFIG_IN_CHOICE	"choice"
#define CONFIG_IN_NCHOICE	"nchoice"
#define CONFIG_IN_ARCH		"ARCH"

#define CONFIG_IN_BLANK		" "
/*	addendums for illegal implicit variable checks:	*/
#define CONFIG_IN_IMPL_IF_QUOTE	"\""
#define CONFIG_IN_IMPL_IF_ADD	" = \"y\" "

#define CONFIG_IN_COMMENT	"#"
#define CONFIG_IN_SECTION_END	"endmenu"
#define CONFIG_IN_CONFIG	"CONFIG_"
#define CONFIG_IN_SHORT_DESCR_MARK	"'"
#define CONFIG_IN_BRANCH	"source"
#define CONFIG_IN_IF		"if"
#define CONFIG_IN_ELSE		"else"
#define CONFIG_IN_FI		"fi"
#define CONFIG_IN_VAR_DENOTE	"$"
#define CONFIG_IN_NOT		"!"
#define CONFIG_IN_VAR_QUOTE	"\""
#define CONFIG_IN_THEN		"then"
#define CONFIG_IN_END_OF_COND	";"
#define CONFIG_IN_AND		"AND"
#define CONFIG_IN_A		"-a"
#define CONFIG_IN_OR		"OR"
#define CONFIG_IN_O		"-o"
#define CONFIG_IN_EQUAL		"="
#define CONFIG_IN_NEQUAL	"!="
#define CONFIG_IN_YES		"y"
#define CONFIG_IN_NO		"n"
#define CONFIG_IN_MOD		"m"
#define CONFIG_IN_MAKE		"$MAKE"

#define CONFIGURE_HELP_FILENAME	"Configure.help"
#define	CONFIGURE_HELP_COMMENT	"# "
#define	CONFIGURE_HELP_INDENT	"  "

#define DEFCONFIG_FILENAME	"defconfig"
#define CONFIG_FILENAME		".config.save"

#define ARCH_DIRNAME		"arch"

#define G_LIST(list)	((GList*) (list))
#define G_SLIST(list)	((GSList*) (list))
#define G_CHAR(str)	((gchar*) (str))
#define G_HASH_TABLE(item)	((GHashTable*) (item))
#define GTK_BOX_CHILD(item)	((GtkBoxChild*) (item))

/*	variables	*/
GdkPixmap	*and_data, *or_data, *not_data;
GdkBitmap	*and_mask, *or_mask, *not_mask;

/*	keep track of how many main windows exist	*/
guint		windows;
GdkPixmap	*splash_pixmap;

/*	functions	*/
gboolean	gernel_str_ends_insensitive(	gchar *string,
						gchar *pattern	);

gboolean	gernel_str_ends(	gchar *string,
					gchar *pattern	);

gboolean	gernel_str_equals_insensitive(	gchar *string,
						gchar *pattern	);

gboolean	gernel_str_equals(	gchar *string,
					gchar *pattern	);

gboolean	gernel_int_equals(	gint first,
					gint second	);

gboolean	gernel_str_contains_insensitive(	gchar *string,
							gchar *pattern	);

gboolean	gernel_str_contains(	gchar *string,
					gchar *pattern	);

gboolean	gernel_str_starts_insensitive(	gchar *string,
						gchar *pattern	);

gboolean	gernel_str_starts(	gchar *string,
					gchar *pattern	);

GSList* rm_defconfig_specials(	GSList *defconfig	);

gboolean match0(	gchar *str1,
			gchar *str2	);

gboolean match(	gchar *str1,
		gchar *str2	);

void close_window(	GtkWidget *button,
			GtkWidget *window	);

GNode* gernel_node_copy(	GNode *original	);
void create_window(	gchar *path	);
gchar* check_syntax(	gchar *ifcond	);

GNode* create_stack_item(	GNode *operator,
				gchar *expr,
				GCache *cache	);

gboolean is_wrapped(	gchar *str	);

gchar* unwrap(	gchar* str	);
guint get_size(	gchar* path	);
gint map_type(	gchar *type	);

GSList* push(	GSList* stack,
		gpointer element	);

gint array_length(	gchar* array[]	);

void read_help(	GSList* raw_text,
		GtkWidget *tree,
		GCache *cache	);

gchar* get_last(	gchar** strs	);

gchar* get_var_name(	gchar* str,
			gchar* var_start	);

gchar* get_enclosed(	gchar* str,
			gchar* delim	);

gchar* remove_strs(	gchar* str,
			gchar* obsolete	);

guint32 gernel_color_string_to_32(	gchar *color	);
gchar*	gernel_color_32_to_string(	guint32 color	);
