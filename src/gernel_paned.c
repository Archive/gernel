#include <gernel.h>
#include <gernel_paned.h>

static void gernel_paned_init(	GernelPaned *paned	);
static void gernel_paned_class_init(	GernelPanedClass *klass	);
static void gernel_paned_set_position_right_no_remember(GernelPaned *paned );
static void gernel_paned_set_position_left_no_remember(	GernelPaned *paned );

void gernel_paned_set_position_no_remember(	GernelPaned *paned,
						gint handle_position	);

static void gernel_paned_set_handle(	GernelPaned *paned,
					gboolean flag	);

static void gernel_paned_size_request(	GtkWidget      *widget,
					GtkRequisition *requisition	);

static void gernel_paned_set_last_handle_position(	GernelPaned *paned );

static void gernel_paned_set_handle_position(	GernelPaned *paned,
						gint handle_position	);

static void gernel_paned_add(	GtkContainer *container,
				GtkWidget *widget	);

static void gernel_paned_handle(	GernelPaned *paned	);

static gboolean gernel_paned_child_changes_width_customizability(
		GernelPaned *paned,
		GtkWidget *widget
		);

static GernelPanedClass	*parent_class = NULL;

GtkType gernel_paned_get_type(	void	)
/*	Return type identifier for type GernelPaned	*/
{
	static GtkType	paned_type = 0;

	if (!paned_type){

		static const GtkTypeInfo	paned_info = {
				"GernelPaned",
				sizeof(GernelPaned),
				sizeof(GernelPanedClass),
				(GtkClassInitFunc)gernel_paned_class_init,
				(GtkObjectInitFunc)gernel_paned_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		paned_type = gtk_type_unique(	gtk_hpaned_get_type(), 
						&paned_info	);
	}

	return paned_type;
}

GtkWidget* gernel_paned_new(	void	)
/*	Create and instance of GernelPaned	*/
{
	GernelPaned	*paned;

	paned = gtk_type_new(gernel_paned_get_type());

	return GTK_WIDGET(paned);
}

static void gernel_paned_init(	GernelPaned	*paned	)
/*	Initialize the GernelPaned	*/
{
	paned->handle_position = 0;
}

static void gernel_paned_class_init(	GernelPanedClass *klass	)
/*	Initialize the GernelPanedClass	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkPanedClass		*paned_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	paned_class = (GtkPanedClass*)klass;

	parent_class = gtk_type_class(gtk_hpaned_get_type());

	container_class->add =  gernel_paned_add;

	widget_class->size_request = gernel_paned_size_request;
}

static void gernel_paned_add(	GtkContainer *container,
				GtkWidget *widget	)
/*	Generic add function for GernelPaned	*/
{
	GernelPaned	*paned;
	GtkSignalFunc	function;

	g_return_if_fail(container != NULL);
	g_return_if_fail(GERNEL_IS_PANED(container));

	g_return_if_fail(	GTK_PANED(container)->child1 == NULL ||
				GTK_PANED(container)->child2 == NULL	);

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));

	paned = GERNEL_PANED(container);

	if (GTK_PANED(paned)->child1) {
		gtk_paned_pack2(GTK_PANED(paned), widget, TRUE, TRUE);
		function = gernel_paned_set_position_right_no_remember;

		gtk_signal_connect_object(	GTK_OBJECT(widget),
						"hide",
						function,
						GTK_OBJECT(paned)	);

	} else {
		gtk_paned_pack1(GTK_PANED(paned), widget, TRUE, TRUE);
		function = gernel_paned_set_position_left_no_remember;

		gtk_signal_connect_object(	GTK_OBJECT(widget),
						"hide",
						function,
						GTK_OBJECT(paned)	);
	}

	gtk_signal_connect_object(	GTK_OBJECT(widget),
					"show",
					GTK_SIGNAL_FUNC(gernel_paned_handle),
					GTK_OBJECT(paned)	);

	gtk_signal_connect_object(	GTK_OBJECT(widget),
					"show",
					gernel_paned_set_last_handle_position,
					GTK_OBJECT(paned)	);

	gtk_signal_connect_object(	GTK_OBJECT(widget),
					"hide",
					GTK_SIGNAL_FUNC(gernel_paned_handle),
					GTK_OBJECT(paned)	);

	if (gernel_paned_child_changes_width_customizability(paned, widget))

		gtk_signal_connect_object(	GTK_OBJECT(widget),
						"width_customizable",
						gernel_paned_set_handle,
						GTK_OBJECT(paned)	);
}

static void gernel_paned_set_handle(	GernelPaned *paned,
					gboolean flag	)
/*	Set visibility of paned's handle	*/
{
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	if (GTK_PANED(paned)->handle == NULL) return;

	if (flag)
		gdk_window_show(GTK_PANED(paned)->handle);
	else
		gdk_window_hide(GTK_PANED(paned)->handle);
}

static gboolean gernel_paned_child_changes_width_customizability(
		GernelPaned *paned,
		GtkWidget *widget
		)
/*	Checks if widget might prevent user from changing its size; needed e.g.
	when a GernelDock is packed into paned and user detaches the dock item;
	paned should hide its handle then	*/
{
	GtkArg	*args;
	guint32	*flags;
	guint	n_args;
	gint	i;
	GtkType	type;

	g_return_val_if_fail(paned != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_PANED(paned), FALSE);
	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), FALSE);

	type = GTK_OBJECT_TYPE(widget);
	args = gtk_object_query_args(type, &flags, &n_args);

	for (i = 0; i < n_args; i++) {
		gchar	**strv;

		strv = g_strsplit(args[i].name, "::", 1);

		if (!gernel_str_equals(strv[1], "width_customizable")) {
			g_strfreev(strv);

			return TRUE;
		}

		g_strfreev(strv);
	}

	return FALSE;
}

static gboolean gernel_paned_get_width_customizable(	GernelPaned *paned,
							GtkWidget *widget )
/*	Checks if widget is currently customizable in width	*/
{
	g_return_val_if_fail(paned != NULL, FALSE);
	g_return_val_if_fail(GERNEL_IS_PANED(paned), FALSE);
	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), FALSE);

	g_return_val_if_fail(	(widget != GTK_PANED(paned)->child1) ||
				(widget != GTK_PANED(paned)->child2), FALSE);

	if (!gernel_paned_child_changes_width_customizability(paned, widget))
		return TRUE;
	else {
		GtkArg	args[1];

		args[0].name = g_strconcat(	gtk_widget_get_name(widget), 
						"::width_customizable", 
						NULL	);

		gtk_object_getv(GTK_OBJECT(widget), 1, args);

		return GTK_VALUE_BOOL(args[0]);
	}
}

static void gernel_paned_handle(	GernelPaned *paned	)
/*	Show/hide the handle	*/
{
	GtkPaned	*handle_paned;

	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	handle_paned = GTK_PANED(paned);

	if (	(handle_paned->child1) && 
		(handle_paned->child2) &&
		(GTK_WIDGET_VISIBLE(handle_paned->child1)) &&
		(GTK_WIDGET_VISIBLE(handle_paned->child2)) &&

		(gernel_paned_get_width_customizable(	paned, 
							handle_paned->child1))&&

		(gernel_paned_get_width_customizable(	paned, 
							handle_paned->child2 )))

		gernel_paned_set_handle(paned, TRUE);
	else
		gernel_paned_set_handle(paned, FALSE);
}

static void gernel_paned_size_request(	GtkWidget      *widget,
					GtkRequisition *requisition	)
/*	The size_request re-implementation; difference to GtkHPaned: adjusts
	overall-size if any of the children is invisible	*/
{
	GtkPaned 	*paned;
	GtkRequisition	child_requisition;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_PANED(widget));
	g_return_if_fail(requisition != NULL);

	paned = GTK_PANED(widget);
	requisition->width = 0;
	requisition->height = 0;

	if (paned->child1 && GTK_WIDGET_VISIBLE (paned->child1)) {
		gtk_widget_size_request (paned->child1, &child_requisition);

		requisition->height = child_requisition.height;
		requisition->width = child_requisition.width;
	}

	if (paned->child2 && GTK_WIDGET_VISIBLE (paned->child2)) {
		gtk_widget_size_request (paned->child2, &child_requisition);

		requisition->height = MAX(	requisition->height, 
						child_requisition.height );

		requisition->width += child_requisition.width;
	} else if (paned->child2 && !GTK_WIDGET_VISIBLE (paned->child2))
		/*	Canvas detached	*/
		gtk_widget_size_request (paned->child2, &child_requisition);

	requisition->width += 	GTK_CONTAINER(paned)->border_width * 2 + 
				paned->gutter_size;

	requisition->height += GTK_CONTAINER(paned)->border_width * 2;

}

void gernel_paned_set_position(	GernelPaned *paned,
				gint handle_position	)
/*	Adjust the position of the handle to handle_position	*/
{
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	gtk_paned_set_position(GTK_PANED(paned), handle_position);
	gernel_paned_set_handle_position(paned, handle_position);
}

void gernel_paned_set_position_no_remember(	GernelPaned *paned,
						gint handle_position	)
/*	Adjust the position of the handle to handle_position	*/
{
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	gtk_paned_set_position(GTK_PANED(paned), handle_position);
}

void gernel_paned_set_position_right(	GernelPaned *paned	)
/*	Set the handle position to the utmost right	*/
{
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	gernel_paned_set_position(paned, GTK_WIDGET(paned)->allocation.width);
}

static void gernel_paned_set_position_right_no_remember(GernelPaned *paned )
/*	Set the handle position to the utmost right; don't remember position */
{
	GtkWidget	*widget;

	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	widget = GTK_WIDGET(paned);

	gernel_paned_set_position_no_remember(paned, widget->allocation.width);
}

static void gernel_paned_set_position_left_no_remember(	GernelPaned *paned )
/*	Set the handle position to the utmost left; don't remember position */
{
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	gernel_paned_set_position_no_remember(paned, 0);
}

static void gernel_paned_set_handle_position(	GernelPaned *paned,
						gint handle_position	)
/*	Set field 'handle_position'	*/
{
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	paned->handle_position = handle_position;
}

static void gernel_paned_set_last_handle_position(	GernelPaned *paned )
/*	Adjust position of handle to last setting stored	*/
{
	g_return_if_fail(paned != NULL);
	g_return_if_fail(GERNEL_IS_PANED(paned));

	gernel_paned_set_position(paned, paned->handle_position);
}
