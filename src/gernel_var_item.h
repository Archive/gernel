typedef struct _GernelVarItem		GernelVarItem;
typedef struct _GernelVarItemClass	GernelVarItemClass;

enum GernelVarItemField {	GERNEL_VAR_ITEM_FIELD_NAME,
				GERNEL_VAR_ITEM_FIELD_TEXT,
				GERNEL_VAR_ITEM_FIELD_DESCRIPTION	};

enum GernelVarItemMatchMode {	GERNEL_VAR_ITEM_MATCH_MODE_CONTAINS,
				GERNEL_VAR_ITEM_MATCH_MODE_ENDS,
				GERNEL_VAR_ITEM_MATCH_MODE_STARTS,
				GERNEL_VAR_ITEM_MATCH_MODE_EQUALS	};

enum GernelVarItemCase {	GERNEL_VAR_ITEM_CASE_0,
				GERNEL_VAR_ITEM_CASE_SENSITIVE,
				GERNEL_VAR_ITEM_CASE_INSENSITIVE	};


/*	GernelVarItems will constantly be checked for fulfillment of their
	conditions. They will only be sensitive if the condition is met.
	A condition is a boolean expression tree with gernel_boolean_op's as
	interior nodes and gernel_single_expr's as leaves	*/

/*	Data type for operators of atomic expressions (see below)	*/

enum gernel_compare_op {	COMPARE_EQUAL,
				COMPARE_NOT_EQUAL	};

enum gernel_boolean_op {	OPERATOR_NONE,
				OPERATOR_AND,
				OPERATOR_OR,
				OPERATOR_NOT	};

typedef GtkWidget* (*GernelVarItemSearchFunc)	(	GernelVarItem *var_item,
							gchar *pattern	);

#define GERNEL_VAR_ITEM_SEARCH_FUNC(function) \
	((GernelVarItemSearchFunc)function)

typedef GtkWidget* (*GernelVarItemFunc)	(	GernelVarItem *var_item,
						gpointer data	);

#define GERNEL_VAR_ITEM_FUNC(function)	(	(GernelVarItemFunc)function )

typedef struct {
	GernelVarItem		*var_item;
	enum gernel_compare_op	operator;
	gchar			*value;
} gernel_single_expr;

#define GERNEL_SINGLE_EXPR(expr)	((gernel_single_expr*)(expr))
/*	Macro defs for condition tree operators - the interior nodes of the
	trees	*/

enum GernelVarItemType {	GERNEL_VAR_ITEM_TYPE_0,
				GERNEL_VAR_ITEM_TYPE_SECTION_MARK,
				GERNEL_VAR_ITEM_TYPE_HEADLINE,
				GERNEL_VAR_ITEM_TYPE_TEXT,
				GERNEL_VAR_ITEM_TYPE_BOOL,
				GERNEL_VAR_ITEM_TYPE_HEX,
				GERNEL_VAR_ITEM_TYPE_ARCH,
				GERNEL_VAR_ITEM_TYPE_INT,
				GERNEL_VAR_ITEM_TYPE_STRING,
				GERNEL_VAR_ITEM_TYPE_TRISTATE,
				GERNEL_VAR_ITEM_TYPE_DEFINE_BOOL,
				GERNEL_VAR_ITEM_TYPE_DEFINE_HEX,
				GERNEL_VAR_ITEM_TYPE_DEFINE_INT,
				GERNEL_VAR_ITEM_TYPE_DEFINE_STRING,
				GERNEL_VAR_ITEM_TYPE_DEFINE_TRISTATE,
				GERNEL_VAR_ITEM_TYPE_DEP_BOOL,
				GERNEL_VAR_ITEM_TYPE_DEP_MBOOL,
				GERNEL_VAR_ITEM_TYPE_DEP_HEX,
				GERNEL_VAR_ITEM_TYPE_DEP_INT,
				GERNEL_VAR_ITEM_TYPE_DEP_STRING,
				GERNEL_VAR_ITEM_TYPE_DEP_TRISTATE,
				GERNEL_VAR_ITEM_TYPE_UNSET,
				GERNEL_VAR_ITEM_TYPE_CHOICE,
				GERNEL_VAR_ITEM_TYPE_NCHOICE	};

/*	notebook_page	:	the help page
 *
 *	config_in_type	:	type of the kernel variable (e.g. 'choice')
 *
 *	description	:	short description of the kernel variable 
 *				(e.g. 'Platform support')
 *
 *	long_description:	the help text
 *
 *	var_name	:	explicit name of the kernel variable 
 *				(e.g. 'CONFIG_PPC')
 *
 *	option_entries	:	if kernel_var is of type 'choice', this list 
 *				contains the possible entries
 *
 *	value		:	current value of the variable (e.g. 'y')
 *
 *	allow_mod	:	for *TRISTATE variables, this specifies whether
 *				the variable may be set to 'module'
 *
 *	allow_yes	:	for *_TRISTATE variables, this specifies whether
 *				the variable may be set to 'yes'
 *
 *	prev_mod	:	for *TRISTATE variables, save if the variable
 *				has been set to 'module' before automatically
 *				being changed by a change to CONFIG_MODULES.
 *				If CONFIG_MODULES is switched back and forth,
 *				this allows to automatically toggle the variable
 *				back to 'module'
 *
 *	prev_yes	:	for dependants of *_TRISTATE variables, this
 *				flag keeps track of whether the dependant has
 *				been set to 'yes' before it was forced to 'mod'
 *				by a change of its prerequisite
 *
 *	dependants	:	a list of variables which depend on this 
 *				variable (e.g. most variables in section 
 *				'SCSI support' depend on variable 'CONFIG_SCSI')
 *
 *	condition	:	an expression tree that needs
 *				to be true before this item will even be 
 *				sensitive (e.g. "CONFIG_SCSI == 'y' is a 
 *				preconditional expression for most of the 
 *				items in the SCSI section)
 *
 *	var_item_icon_set:	The GernelVarItemIconSet representing the 
 *				current value of the variable
 */

struct _GernelVarItem
{
	GtkTreeItem		tree_item;

	enum GernelVarItemType	type;
	gchar*			description;
	gchar*			long_description;
	gchar*			name;
	GSList*			option_entries;
	gchar*			value;
	gchar			*fontset;
	gboolean		allow_mod, prev_mod, allow_yes, prev_yes;
	GSList*			dependants;
	GNode*			condition;
	GtkWidget		*var_item_icon_set;
};

struct _GernelVarItemClass
{
	GtkTreeItemClass	parent_class;

	void (*value_changed)	(GernelVarItem *var_item);
};

#define GERNEL_TYPE_VAR_ITEM	(gernel_var_item_get_type())

#define GERNEL_VAR_ITEM(obj)	(					\
		GTK_CHECK_CAST(obj, GERNEL_TYPE_VAR_ITEM, GernelVarItem)\
		)

#define GERNEL_VAR_ITEM_CLASS(klass)	(				\
		GTK_CHECK_CLASS_CAST(	(klass), 			\
					GERNEL_TYPE_VAR_ITEM,		\
					GernelVarItemClass		\
					)				\
		)

#define GERNEL_IS_VAR_ITEM(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_VAR_ITEM))

#define GERNEL_IS_VAR_ITEM_CLASS(klass)	(				\
		GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_VAR_ITEM)	\
		)


GtkType		gernel_var_item_get_type(	void	);
GtkWidget*	gernel_var_item_new(		gchar *var_name	);

GtkWidget*	gernel_var_item_new_from_raw(	gchar *str,
						GSList *stack,
						GCache *cache	);

void		gernel_var_item_set_value(	GernelVarItem *var_item,
						gchar *value	);

void		gernel_var_item_icons(		GernelVarItem *var_item,
						GdkWindow *window	);

void 		gernel_var_item_icon_sync(	GernelVarItem *var_item	);
void		gernel_var_item_sens_sync(	GernelVarItem *var_item	);
void 		gernel_var_item_deps_sync(	GernelVarItem *var_item	);
void 		gernel_var_item_deps_sync_all(	GernelVarItem *var_item	);
gboolean	gernel_var_item_meets_condition(	GNode *expression );
void 		gernel_var_item_set_mod(	GernelVarItem *var_item	);
void 		gernel_var_item_set_false(	GernelVarItem *var_item	);
void 		gernel_var_item_set_false_all(	GernelVarItem *var_item	);
void 		gernel_var_item_set_true(	GernelVarItem *var_item	);
void 		gernel_var_item_check_condition(GernelVarItem *var_item	);

GtkWidget* gernel_var_item_compare(	GernelVarItem *var_item,
					gchar* var,
					enum GernelVarItemField field,
					enum GernelVarItemMatchMode match_mode,
					enum GernelVarItemCase sensitivity );

GtkWidget*	gernel_var_item_compare_equals_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		);

GtkWidget*	gernel_var_item_compare_equals_text(	GernelVarItem *var_item,
							gchar *text	);

GtkWidget*	gernel_var_item_compare_contains_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		);

GtkWidget*	gernel_var_item_compare_contains_text(	GernelVarItem *var_item,
							gchar *text	);

GtkWidget*	gernel_var_item_compare_ends_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		);

GtkWidget*	gernel_var_item_compare_ends_text(	GernelVarItem *var_item,
							gchar *text	);

GtkWidget*	gernel_var_item_compare_starts_text_sensitive(
		GernelVarItem *var_item,
		gchar *text
		);

GtkWidget*	gernel_var_item_compare_starts_text(	GernelVarItem *var_item,
							gchar *text	);

GtkWidget*	gernel_var_item_compare_equals_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_equals_description(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_starts_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_starts_description(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_ends_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_ends_description(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_contains_description_sensitive(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_contains_description(
		GernelVarItem *var_item,
		gchar *description
		);

GtkWidget*	gernel_var_item_compare_ends_name_sensitive(
		GernelVarItem *var_item,
		gchar *name
		);

GtkWidget*	gernel_var_item_compare_ends_name(	GernelVarItem *var_item,
							gchar *name	);

GtkWidget*	gernel_var_item_compare_equals_name_sensitive(
		GernelVarItem *var_item,
		gchar *name
		);

GtkWidget*	gernel_var_item_compare_equals_name(	GernelVarItem *var_item,
							gchar *name	);

GtkWidget*	gernel_var_item_compare_starts_name_sensitive(
		GernelVarItem *var_item,
		gchar *name
		);

GtkWidget*	gernel_var_item_compare_starts_name(	GernelVarItem *var_item,
							gchar *name	);

GtkWidget*	gernel_var_item_compare_contains_name_sensitive(
		GernelVarItem *var_item,
		gchar *name
		);

GtkWidget*	gernel_var_item_compare_contains_name(	GernelVarItem *var_item,
							gchar *name	);

void 		gernel_var_item_update_by_entry(GtkEntry *entry,
						GernelVarItem *var_item	);

GernelVarItem*	gernel_var_item_identity(	GernelVarItem *var_item	);

void		gernel_var_item_allow_module(	GernelVarItem *var_item,
						GernelVarItem *mod_item	);

void		gernel_var_item_no_previous(	GernelVarItem *var_item	);
void		gernel_var_item_allow_yes_deps(	GernelVarItem *var_item	);

void		gernel_var_item_allow_yes(	GernelVarItem *dependant,
						GernelVarItem *var_item	);

void		gernel_var_item_mod_dep_sync(	GernelVarItem *var_item	);
void		gernel_var_item_description(	GernelVarItem *var_item	);

void		gernel_var_item_set_long_description(
		GernelVarItem *var_item,
		gchar *long_description
		);

gchar*		gernel_var_item_get_description(GernelVarItem *var_item	);

void		gernel_var_item_allow_mod_all(	GernelVarItem *var_item,
						GtkWidget *tree	);

void		gernel_var_item_allow_module(	GernelVarItem *var_item,
						GernelVarItem *mod_item	);

gchar*		gernel_var_item_get_name(	GernelVarItem *var_item	);
gchar*		gernel_var_item_get_text(	GernelVarItem *var_item	);
gchar*		gernel_var_item_get_value(	GernelVarItem *var_item	);
gchar*		gernel_var_item_get_description(GernelVarItem *var_item	);

void		gernel_var_item_set_type_from_str(	GernelVarItem *var_item,
							gchar *string	);
