#include	<gernel_canvas_section.h>

typedef struct _GernelCanvas		GernelCanvas;
typedef struct _GernelCanvasClass	GernelCanvasClass;

enum GernelCanvasElement {	GERNEL_CANVAS_ELEMENT_0,
				GERNEL_CANVAS_ELEMENT_TITLE,
				GERNEL_CANVAS_ELEMENT_PROMPT,
				GERNEL_CANVAS_ELEMENT_HEADER,
				GERNEL_CANVAS_ELEMENT_TEXT,
				GERNEL_CANVAS_ELEMENT_TAIL	};

struct _GernelCanvas {
	GnomeCanvas		canvas;

	gchar			*local_name;
	GdkFont			*fonts[GERNEL_CANVAS_SECTION_TYPE_TAIL +1];
	gchar			*fontsets[GERNEL_CANVAS_SECTION_TYPE_TAIL +1];

/*	These store color settings that don't necessarily have to match
	the actual display; the user might prefere the colors-from-theme
	scheme. To recall custom color values, these variables are
	provided	*/
	guint32			layout_element_color, background_color,
				fonts_color;

	/*	A list of GernelCanvasItems	*/
	GSList			*mask;

	gboolean		shown, detachable, colors_from_theme,
				fonts_from_theme, show_title, show_headers,
				show_prompts, show_text;

	GernelCanvasSection	*background;
};

struct _GernelCanvasClass {
	GnomeCanvasClass	parent_class;

	void (*detachable)	(	GernelCanvas *canvas,
					gboolean flag	);

	void (*update)		(	GernelCanvas *canvas	);

	void (*menu)		(	GernelCanvas *canvas,
					guint32 time	);

	void (*configure)	(	GernelCanvas *canvas	);
};

#define GERNEL_TYPE_CANVAS	(gernel_canvas_get_type())

#define GERNEL_CANVAS(obj) \
	(GTK_CHECK_CAST(obj, GERNEL_TYPE_CANVAS, GernelCanvas))

#define GERNEL_CANVAS_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass), GERNEL_TYPE_CANVAS, GernelCanvasclass))

#define GERNEL_IS_CANVAS(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_CANVAS))

#define GERNEL_IS_CANVAS_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_CANVAS))

GtkType		gernel_canvas_get_type(	void	);

GtkWidget*	gernel_canvas_new(	gchar *name,
					...	); 

void gernel_canvas_set_contents(	GernelCanvas *canvas,
					gchar *str,
					...	);

void gernel_canvas_update_contents(	GernelCanvas *canvas,
					gchar *title,
					gchar *prompt,
					gchar *text	);

void gernel_canvas_construct_menu(	GernelCanvas *canvas,
					guint32 time,
					GtkWidget *app	);
