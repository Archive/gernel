#define GERNEL_TYPE_TREE	(gernel_tree_get_type())

#define GERNEL_TREE(obj) \
	(GTK_CHECK_CAST((obj), GERNEL_TYPE_TREE, GernelTree))

#define GERNEL_TREE_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass), GERNEL_TREE, GernelTreeClass))

#define GERNEL_IS_TREE(obj)	(GTK_CHECK_TYPE((obj), GERNEL_TYPE_TREE))

#define GERNEL_IS_TREE_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), GERNEL_TYPE_TREE))

#define	gernel_tree_next(obj) \
	(gernel_tree_get_sibling(obj, GERNEL_TREE_DIRECTION_NEXT))

#define gernel_tree_previous(obj) \
	(gernel_tree_get_sibling(obj, GERNEL_TREE_DIRECTION_PREVIOUS))

typedef GtkWidget*	(*GernelTreeSearchFunc)	(	GtkWidget *var_item,
							gchar *pattern	);

#define GERNEL_TREE_SEARCH_FUNC(function)	((GernelTreeSearchFunc)function)

typedef struct _GernelTree	GernelTree;
typedef struct _GernelTreeClass	GernelTreeClass;

enum GernelTreeDirection {	GERNEL_TREE_DIRECTION_NEXT,
				GERNEL_TREE_DIRECTION_PREVIOUS,
				GERNEL_TREE_DIRECTION_UP,
				GERNEL_TREE_DIRECTION_DOWN	};

struct _GernelTree {
	GtkTree	tree;
};

struct _GernelTreeClass {
	GtkTreeClass	parent_class;
};

GtkType		gernel_tree_get_type(	void	);
GtkWidget*	gernel_tree_new(	void	);
GList*		gernel_tree_get_selection(	GernelTree *tree	);

void		gernel_tree_branch(	GernelTree *tree,
					GtkWidget *branch	);

void		gernel_tree_select_item(	GernelTree *tree,
						GtkWidget *var_item	);

GList*		gernel_tree_children(	GernelTree *tree	);
void		gernel_tree_select_next(	GernelTree *tree	);
void		gernel_tree_select_previous(	GernelTree *tree	);
void		gernel_tree_select_up(	GernelTree *tree	);
void 		gernel_tree_select_down(	GernelTree *tree	);

gboolean	gernel_tree_is_interior_node(	GernelTree *tree,
						GtkWidget *var_item	);

GtkWidget*	gernel_tree_get_sibling(
		GernelTree *tree,
		enum GernelTreeDirection direction
		);

GtkWidget*	gernel_tree_find(	GernelTree *tree,
					GernelTreeSearchFunc function,
					gpointer data	);

void		gernel_tree_append(	GernelTree *tree,
					GtkWidget *widget	);

gboolean	gernel_tree_is_branch(	GernelTree *tree,
					GtkWidget *var_item	);

GtkWidget*	gernel_tree_get_branch(	GernelTree *tree,
					GtkWidget* var_item	);
