#include <gernel.h>
#include <gernel_password.h>

static void 	gernel_password_init(	GernelPassword *password	);
static void	gernel_password_class_init(	GernelPasswordClass *klass );
static void	gernel_password_construct(	GernelPassword *password );
static void	gernel_password_start(	GernelPassword *password	);

static void	gernel_password_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void	gernel_password_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static GtkWidget*	gernel_password_preferences(
		GernelPassword *password
		);

enum {	ARG_0,
	ARG_PREFERENCES	};

enum {	CONFIGURE,
	LAST_SIGNAL	};

static guint password_signals[LAST_SIGNAL] = { 0 };
static GnomeDialogClass *parent_class = NULL;

GtkType gernel_password_get_type(	void	)
/*	Return type identifier for type GernelSearch	*/
{
	static GtkType	password_type = 0;

	if (!password_type) {

		static const GtkTypeInfo	password_info = {
				"GernelPassword",
				sizeof(GernelPassword),
				sizeof(GernelPasswordClass),
				(GtkClassInitFunc)gernel_password_class_init,
				(GtkObjectInitFunc)gernel_password_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		password_type = gtk_type_unique(	GNOME_TYPE_DIALOG, 
							&password_info	);
	}

	return password_type;
}

GtkWidget* gernel_password_new(	void	)
/*	Create an instance of GernelPassword	*/
{
	GernelPassword	*password;

	password = gtk_type_new(gernel_password_get_type());
	gernel_password_construct(password);

	return GTK_WIDGET(password);
}

static void gernel_password_init(	GernelPassword *password	)
/*	Initialize a GernelPassword dialog	*/
{
}

static void gernel_password_class_init(	GernelPasswordClass *klass	)
/*	Initialize a GernelPassword class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkWindowClass		*window_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	window_class = (GtkWindowClass*)klass;

	parent_class = gtk_type_class(GNOME_TYPE_DIALOG);

	gtk_object_add_arg_type(	"GernelPassword::preferences",
					GTK_TYPE_WIDGET,
					GTK_ARG_READABLE,
					ARG_PREFERENCES	);

	object_class->get_arg = gernel_password_get_arg;
	object_class->set_arg = gernel_password_set_arg;

	function_offset = GTK_SIGNAL_OFFSET(GernelPasswordClass, configure);

	password_signals[CONFIGURE] = gtk_signal_new(	"configure",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	gtk_object_class_add_signals(	object_class, 
					password_signals, 
					LAST_SIGNAL	);

	klass->configure = NULL;
}

static void gernel_password_construct(	GernelPassword *password	)
/*	Construct the GernelPassword dialog	*/
{
	GnomeDialog	*dialog;

	g_return_if_fail(password != NULL);
	g_return_if_fail(GERNEL_IS_PASSWORD(password));

	dialog = GNOME_DIALOG(password);

	gnome_dialog_append_buttons(	dialog,
					GNOME_STOCK_BUTTON_OK,
					GNOME_STOCK_BUTTON_CANCEL,
					NULL	);

	gnome_dialog_set_default(dialog, 0);

	gnome_dialog_button_connect_object(	dialog,
						0,
						gernel_password_start,
						(gpointer)password	);

	gnome_dialog_button_connect_object(	dialog,
						1,
						gnome_dialog_close,
						(gpointer)dialog	);
}

static void gernel_password_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelPassword	*password;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_PASSWORD(object));

	password = GERNEL_PASSWORD(object);

	switch (arg_id) {
		case ARG_PREFERENCES:	/*	Fall Through	*/
		default:
			break;
	}
}

static void gernel_password_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelPassword	*password;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_PASSWORD(object));

	password = GERNEL_PASSWORD(object);

	switch (arg_id) {
		GtkWidget	*widget;

		case ARG_PREFERENCES:
			widget = gernel_password_preferences(password);
			GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(widget);
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static GtkWidget* gernel_password_preferences(	GernelPassword *password )
/*	Assemble a page to be used as a section in a GnomeProperty box page */
/*	FIXME: all other *_preferences() functions return a 
	GernelPreferencePage; this one does not because its outcome is not
	intended to make up a single page in the preferences dialog (it is used
	together with the GernelCompiler preference page). This is probably bad
	style...	*/
{
	GtkWidget		*section;

	g_return_val_if_fail(password != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_PASSWORD(password), NULL);

	section = erty_preference_section_new(_("Password Dialog"));

	return section;
}

static void gernel_password_start(	GernelPassword *password	)
/*	Emit the 'password' signal	*/
{
}
