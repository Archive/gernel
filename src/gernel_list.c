#include <gernel.h>
#include <gernel_list.h>
#include <gernel_tree.h>
#include <gernel_var_item.h>

#define GERNEL_LIST_DEFAULT_HEIGHT	200
#define GERNEL_LIST_DEFAULT_WIDTH	200

enum GernelListColumn {	GERNEL_LIST_COLUMN_0,
			GERNEL_LIST_COLUMN_DESCRIPTIONS,
			GERNEL_LIST_COLUMN_NAMES	};


static void	gernel_list_init(	GernelList *list	);
static void	gernel_list_class_init(	GernelListClass *klass	);
static guint	gernel_list_get_titles(	GernelList *list	);
static gint gernel_list_get_rows(	GernelList *list	);

static void gernel_list_sort(	GernelList *list,
				gint column	);

static void gernel_list_remove(	GernelList *list,
				gint row	);

/*	Sort list by column	*/
static void	gernel_list_set_titles(	GernelList *list,
					guint titles	);

static void gernel_list_select_in_tree(	GernelList *list,
					gint row,
					gint column	);

static gint gernel_list_get_column(	GernelList *list,
					enum GernelListColumn column	);

static void	gernel_list_construct(	GernelList *list,
					gchar **titles	);

static void gernel_list_size_request(	GtkWidget *widget,
					GtkRequisition *requisition	);

static void gernel_list_show_column(	GernelList *list,
					enum GernelListColumn column,
					gboolean flag	);

static gchar* gernel_list_get_string(	GernelList *list,
					gint row,
					gint column	);

static GernelScrolledWindowClass	*parent_class = NULL;

GtkType gernel_list_get_type(	void	)
/*	Return type identifier for type GernelList	*/
{
	static GtkType	list_type = 0;

	if (!list_type) {

		static const GtkTypeInfo	list_info = {
				"GernelList",
				sizeof(GernelList),
				sizeof(GernelListClass),
				(GtkClassInitFunc)gernel_list_class_init,
				(GtkObjectInitFunc)gernel_list_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		list_type = gtk_type_unique(	GERNEL_TYPE_SCROLLED_WINDOW, 
						&list_info	);
	}

	return list_type;
}

GtkWidget* gernel_list_new(	void	)
/*	Create an instance of GernelList	*/
{
	GernelList	*list;

	gchar		*titles[] = {	_("Internal Name"),
					_("Short Description"),
					NULL	};

	list = gtk_type_new(gernel_list_get_type());
	gernel_list_construct(list, titles);

	return GTK_WIDGET(list);
}

static void gernel_list_init(	GernelList *list	)
/*	Initialize a GernelList	*/
{
	list->list = NULL;
	list->tree = NULL;
	list->titles = 0;
}

static void gernel_list_class_init(	GernelListClass *klass	)
/*	Initialize a GernelList class	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkScrolledWindowClass	*scrolled_window_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	scrolled_window_class = (GtkScrolledWindowClass*)klass;

	parent_class = gtk_type_class(GERNEL_TYPE_SCROLLED_WINDOW);

	widget_class->size_request = gernel_list_size_request;
}

static void gernel_list_construct(	GernelList *list,
					gchar **titles	)
/*	Initialize the internal list of GernelList	*/
{
	GtkWidget	*clist;
	gint		i;

	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	clist = gtk_clist_new_with_titles(array_length(titles), titles);
	gernel_list_set_titles(list, array_length(titles));

	for (i = 0; i < gernel_list_get_titles(list); i++)
		gtk_clist_set_column_auto_resize(GTK_CLIST(clist), i, TRUE);

	gtk_signal_connect_object(	GTK_OBJECT(clist),
					"click_column",
					gernel_list_sort,
					GTK_OBJECT(list)	);

	list->list = clist;
	gtk_container_add(GTK_CONTAINER(list), clist);
	gtk_widget_show(clist);
}

static void gernel_list_sort(	GernelList *list,
				gint column	)
/*	Sort list by column	*/
{
	GtkCList	*clist;

	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	clist = GTK_CLIST(list->list);

	gtk_clist_set_sort_column(clist, column);
	gtk_clist_sort(clist);
}

static guint gernel_list_get_titles(	GernelList *list	)
/*	Get number of titles	*/
{
	g_return_val_if_fail(list != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_LIST(list), 0);

	return list->titles;
}

static void gernel_list_set_titles(	GernelList *list,
					guint titles	)
/*	Set number of titles	*/
{
	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	list->titles = titles;
}

void gernel_list_set_tree(	GernelList *list,
				GtkWidget *tree	)
/*	Make list show its selection in tree whenever user selects a row */
{
	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));
	g_return_if_fail(tree != NULL);
	g_return_if_fail(GERNEL_IS_TREE(tree));

	list->tree = tree;

	gtk_signal_connect_object(	GTK_OBJECT(list->list),
					"select_row",
					gernel_list_select_in_tree,
					GTK_OBJECT(list)	);
}

static void gernel_list_select_in_tree(	GernelList *list,
					gint row,
					gint column	)
/*	Select current list selection in list's tree	*/
{
	gchar			*name;
	GtkWidget		*var_item;
	GernelTreeSearchFunc	function;
	GernelTree		*tree;

	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	name = gernel_list_get_string(list, row, column);
	tree = GERNEL_TREE(list->tree);
	function = GERNEL_TREE_SEARCH_FUNC(gernel_var_item_compare_equals_name);
	var_item = gernel_tree_find(tree, function, name);
	gernel_tree_select_item(tree, var_item);
}

void gernel_list_append(	GernelList *list, 
				GtkWidget *var_item	)
/*	Append new line to list	*/
{
	gchar		*row[2];
	GernelVarItem	*item;

	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));
	g_return_if_fail(var_item != NULL);
	g_return_if_fail(GERNEL_IS_VAR_ITEM(var_item));

	item = GERNEL_VAR_ITEM(var_item);

	row[0] = gernel_var_item_get_name(item);
	row[1] = gernel_var_item_get_description(item);

	gtk_clist_append(GTK_CLIST(list->list), row);
}

void gernel_list_select(	GernelList *list,
				guint row	)
/*	Select row of list	*/
{
	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	gtk_clist_select_row(GTK_CLIST(list->list), row, 0);
}

void gernel_list_clear(	GernelList *list	)
/*	Clear list	*/
{
	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	gtk_clist_clear(GTK_CLIST(list->list));
}

static void gernel_list_size_request(	GtkWidget *widget,
					GtkRequisition *requisition	)
/*	Reimplementation for GtkWidget's size requisition function; uses a
	bigger default size than GtkScrolledWindow and distributes space for
	clist's columns evenly	*/
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_LIST(widget));
	g_return_if_fail(requisition != NULL);

	requisition->width = GERNEL_LIST_DEFAULT_WIDTH;
	requisition->height = GERNEL_LIST_DEFAULT_HEIGHT;
}

void gernel_list_set_sort_order(	GernelList *list, 
					enum GernelListSortOrder sort_order )
/*	Set sort order of list	*/
{
	GtkCList	*clist;

	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	clist = GTK_CLIST(list->list);

	switch (sort_order) {
		case GERNEL_LIST_SORT_ORDER_ASCENDING:
			gtk_clist_set_sort_type(clist, GTK_SORT_ASCENDING);
			break;
		case GERNEL_LIST_SORT_ORDER_DESCENDING:
			gtk_clist_set_sort_type(clist, GTK_SORT_DESCENDING);
			break;
		default:
			g_assert_not_reached();
			break;
	}
}

void gernel_list_show_names(	GernelList *list,
				gboolean flag	)
/*	Show/hide the column containing the variable names	*/
{
	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	gernel_list_show_column(list, GERNEL_LIST_COLUMN_NAMES, flag);
}

void gernel_list_show_descriptions(	GernelList *list,
					gboolean flag	)
/*	Show/hide the column containing the variable descriptions	*/
{
	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	gernel_list_show_column(list, GERNEL_LIST_COLUMN_DESCRIPTIONS, flag);
}

static void gernel_list_show_column(	GernelList *list,
					enum GernelListColumn column,
					gboolean flag	)
/*	Show/hide column as specified by flag	*/
{
	GtkCList	*clist;

	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));
	g_return_if_fail(column != GERNEL_LIST_COLUMN_0);

	clist = GTK_CLIST(list->list);

	switch (column) {
		case GERNEL_LIST_COLUMN_DESCRIPTIONS:
			gtk_clist_set_column_visibility(clist, 1, flag);
			break;
		case GERNEL_LIST_COLUMN_NAMES:
			gtk_clist_set_column_visibility(clist, 0, flag);
			break;
		default:
			g_assert_not_reached();
			break;
	}
}

void gernel_list_remove_duplicates(	GernelList *list	)
/*	Remove duplicate lines from list	*/
{
	gint	column, row, rows;
	GSList	*indices, *slist;

	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	column = gernel_list_get_column(list, GERNEL_LIST_COLUMN_NAMES);
	rows = gernel_list_get_rows(list);
	indices = NULL;

	for (row = 0; row < rows; row++) {
		gchar	*name;
		gint	i;

		name = gernel_list_get_string(list, row, column);

		for (i = row + 1; i < rows; i++) {
			gchar	*string;

			string = gernel_list_get_string(list, i, column);

			if (gernel_str_equals(name, string) == 0) {
				gpointer	data;
				GCompareFunc	function;

				data = GINT_TO_POINTER(i);
				function = (GCompareFunc)gernel_int_equals;

				indices = g_slist_insert_sorted(indices, 
								data,
								function );
			}
		}
	}

	for (	slist = g_slist_reverse(indices); 
		slist != NULL; 
		slist = slist->next	)

		gernel_list_remove(list, GPOINTER_TO_INT(slist->data));
}

static void gernel_list_remove(	GernelList *list,
				gint row	)
/*	Remove row specified by 'row' from list	*/
{
	g_return_if_fail(list != NULL);
	g_return_if_fail(GERNEL_IS_LIST(list));

	gtk_clist_remove(GTK_CLIST(list->list), row);
}

static gint gernel_list_get_column(	GernelList *list,
					enum GernelListColumn column	)
/*	Get the index of the column specified by 'column'	*/
{
	gint	value;

	g_return_val_if_fail(list != NULL, -1);
	g_return_val_if_fail(GERNEL_IS_LIST(list), -1);
	g_return_val_if_fail(column != GERNEL_LIST_COLUMN_0, -1);

	value = 0;

	switch (column) {
		case GERNEL_LIST_COLUMN_NAMES:
			value = 0;
			break;
		case GERNEL_LIST_COLUMN_DESCRIPTIONS:
			value = 1;
			break;
		default:
			g_assert_not_reached();
	}

	return value;
}

static gint gernel_list_get_rows(	GernelList *list	)
/*	Get the number of rows in list	*/
{
	g_return_val_if_fail(list != NULL, -1);
	g_return_val_if_fail(GERNEL_IS_LIST(list), -1);

	return GTK_CLIST(list->list)->rows;
}

static gchar* gernel_list_get_string(	GernelList *list,
					gint row,
					gint column	)
/*	Get the string located at row, column in list	*/
{
	gchar	*string;

	g_return_val_if_fail(list != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_LIST(list), NULL);

	gtk_clist_get_text(GTK_CLIST(list->list), row, column, &string);

	return string;
}
