#include <gernel.h>
#include <gernel_canvas.h>
#include <gernel_canvas_menu.h>
#include <gdk/gdkprivate.h>

#define	GERNEL_CANVAS_SPACING	GNOME_PAD_SMALL

#define	GERNEL_CANVAS_FONTSET_TITLE \
"-adobe-helvetica-medium-r-normal-*-18-*-*-*-p-*-*-*,\
-cronyx-helvetica-medium-r-normal-*-20-*-*-*-p-*-koi8-r,\
-*-*-medium-r-normal-*-18-*-*-*-*-*-ksc5601.1987-0"

#define	GERNEL_CANVAS_FONTSET_HEADER \
"-adobe-helvetica-medium-r-*-*-14-*-*-*-p-*-*-*,\
-cronyx-helvetica-medium-r-normal-*-17-*-*-*-p-*-koi8-r,\
-*-*-medium-r-no-*-*-14-*-*-*-*-*-ksc5601.1987-0,*"

#define	GERNEL_CANVAS_FONTSET_TEXT \
"-adobe-helvetica-medium-r-*-*-10-*-*-*-p-*-*-*,\
-cronyx-helvetica-medium-r-normal-*-11-*-*-*-p-*-koi8-r,\
-*-*-medium-r-*-*-10-*-*-*-*-*-ksc5601.1987-0,*"

static gchar* gernel_canvas_get_fontset_header(	GernelCanvas *canvas	);
static gchar* gernel_canvas_get_fontset_text(	GernelCanvas *canvas	);

static void gernel_canvas_set_fontset_header(	GernelCanvas *canvas,
						gchar *fontset	);

static void gernel_canvas_set_fontset_text(	GernelCanvas *canvas,
						gchar *fontset	);

static void gernel_canvas_set_fontset_real(
		GernelCanvas *canvas,
		enum GernelCanvasSectionType type
		);

static void gernel_canvas_set_fontset(	GernelCanvas *canvas,
					gchar *fontset,
					enum GernelCanvasSectionType type );

static void gernel_canvas_set_fontset_title(	GernelCanvas *canvas,
						gchar *fontset	);

static gchar* gernel_canvas_get_fontset(GernelCanvas *canvas,
					enum GernelCanvasSectionType type );

static gchar* gernel_canvas_get_fontset_title(	GernelCanvas *canvas	);

static void gernel_canvas_set_font_real(GernelCanvas *canvas,
					enum GernelCanvasSectionType type );

extern GtkWidget* gernel_font_picker_new_with_label(	gchar *label	);

static void gernel_canvas_set_local_name(	GernelCanvas *canvas,
						gchar *name	);

static void	gernel_canvas_init(	GernelCanvas *canvas	);
static void	gernel_canvas_class_init(	GernelCanvasClass *klass );
static GtkWidget* gernel_canvas_preferences(	GernelCanvas *canvas	);
static void	gernel_canvas_configure(	GernelCanvas *canvas	);

static void	gernel_canvas_construct_section(
		GernelCanvas *canvas, 
		enum GernelCanvasSectionType type
		);

enum GernelCanvasSectionType gernel_canvas_map_element(
		GernelCanvas *canvas,
		enum GernelCanvasElement element
		);

static gchar* bigger_font(	gchar *font	);

static void gernel_canvas_set_font_header(	GernelCanvas *canvas,
						GdkFont *font	);

static void gernel_canvas_set_font_text(	GernelCanvas *canvas,
						GdkFont *font	);

static void gernel_canvas_set_color_fonts(	GernelCanvas *canvas,
						guint32 color	);

static void gernel_canvas_set_color_theme(	GernelCanvas *canvas,
						gboolean flag	);

static void gernel_canvas_set_color_layout_elements(	GernelCanvas *canvas,
							guint32 color	);

static void gernel_canvas_set_color_background(	GernelCanvas *canvas,
						guint32 color	);

static guint32 gernel_canvas_get_color_layout_elements(GernelCanvas *canvas);
static guint32 gernel_canvas_get_color_fonts(	GernelCanvas *canvas	);

static void gernel_canvas_set_show_title(	GernelCanvas *canvas,
						gboolean flag	);

static void gernel_canvas_set_show_headers(	GernelCanvas *canvas,
						gboolean flag	);

static void gernel_canvas_set_show_text(	GernelCanvas *canvas,
						gboolean flag	);

static void gernel_canvas_set_show_prompts(	GernelCanvas *canvas,
						gboolean flag	);

static void gernel_canvas_set_show(	GernelCanvas *canvas,
					enum GernelCanvasSectionType type,
					gboolean flag	);

static void gernel_canvas_set_font(	GernelCanvas *canvas,
					GdkFont *font,
					enum GernelCanvasSectionType type );

static void gernel_canvas_set_font_title(	GernelCanvas *canvas,
						GdkFont *font	);

static void gernel_canvas_set_font_theme(	GernelCanvas *canvas,
						gboolean flag	);

static guint32 gernel_canvas_get_color_background(
		GernelCanvas *canvas
		);

static void gernel_canvas_request_update(	GernelCanvas *canvas	);

static void	gernel_canvas_size_allocate(	GtkWidget *widget,
						GtkAllocation *allocation );

static void gernel_canvas_set_style(	GernelCanvas *canvas	);

static void	gernel_canvas_size_request(	GtkWidget *widget,
						GtkRequisition *requisition );

static void gernel_canvas_update(	GernelCanvas *canvas	);

static void gernel_canvas_detachable(	GernelCanvas *canvas,
					gboolean detachable	);

static void	gernel_canvas_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void	gernel_canvas_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void gernel_canvas_menu(	GernelCanvas *canvas,
				GdkEventButton *event	);

static void gernel_canvas_add(	GernelCanvas *canvas,
				GernelCanvasSection *canvas_section	);

static void gernel_canvas_construct_background(	GernelCanvas *canvas	);

static GnomeCanvasClass *parent_class = NULL;

enum {	ARG_0,
	ARG_LOCAL_NAME,	/*	A name unique to particular instance of
				aspects of preference setting	*/
	ARG_SHOW_TITLE,
	ARG_SHOW_PROMPTS,
	ARG_SHOW_HEADERS,
	ARG_SHOW_TEXT,
	ARG_SECTIONS,
	ARG_SHOW_CANVAS,
	ARG_DETACHABLE,
	ARG_COLOR_THEME,
	ARG_COLOR_CUSTOM,
	ARG_COLOR_BACKGROUND,
	ARG_COLOR_LAYOUT,
	ARG_COLOR_FONTS,
	ARG_FONT_THEME,
	ARG_FONT_CUSTOM,
	ARG_FONT_TITLE,
	ARG_FONTSET_TITLE,
	ARG_FONTSET_HEADER,
	ARG_FONTSET_TEXT,
	ARG_FONT_HEADER,
	ARG_FONT_TEXT,
	ARG_PREFERENCES	};

enum {	DETACHABLE,
	UPDATE,
	MENU,
	CONFIGURE,
	LAST_SIGNAL	};

static guint canvas_signals[LAST_SIGNAL] = { 0 };

static void gernel_canvas_construct_sections(	GernelCanvas *canvas,
						va_list	args	);

static void gernel_canvas_set_show_canvas(	GernelCanvas *canvas,
						gboolean show	);

static void gernel_canvas_size_allocate(	GtkWidget *widget,
						GtkAllocation *allocation )
/*	Implements the standard allocate function	*/
{
	GernelCanvas		*canvas;
	GernelCanvasSection	*tail;
	gint			height, width, tail_height, tail_y;
	GSList			*list;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(widget));

	canvas = GERNEL_CANVAS(widget);

	GTK_WIDGET_CLASS(parent_class)->size_allocate(widget, allocation);

	height = 0;

	/*	Subtract right margin from allocated width	*/
	width = allocation->width;

	for (list = canvas->mask; list; list = list->next) {
		GernelCanvasSection	*canvas_section;

		canvas_section = GERNEL_CANVAS_SECTION(list->data);
		gernel_canvas_section_set_width(canvas_section, width);
		gernel_canvas_section_set_height(canvas_section);
		gernel_canvas_section_set_x(canvas_section, 0);
		gernel_canvas_section_set_y(canvas_section, height);
		height += gernel_canvas_section_get_height(canvas_section);
		if (list->next != NULL) height += GERNEL_CANVAS_SPACING;
	}

	width = MAX(width, allocation->width) - 1;
	height = MAX(height, allocation->height) - 1;

	gnome_canvas_set_scroll_region(	GNOME_CANVAS(canvas), 
					0, 
					0, 
					width,
					height	);

	/*	The background of the canvas	*/
	gernel_canvas_section_set_width(canvas->background, width);
	gernel_canvas_section_set_height_custom(canvas->background, height);

	/*	The last item is a special case	*/
	tail = GERNEL_CANVAS_SECTION(g_slist_last(canvas->mask)->data);

	g_return_if_fail(tail->type == GERNEL_CANVAS_SECTION_TYPE_TAIL);

	tail_y = MAX(allocation->height, height);
	tail_height = gernel_canvas_section_get_height(tail);
	gernel_canvas_section_set_y(tail, tail_y - tail_height);
}

static void gernel_canvas_size_request(	GtkWidget *widget,
					GtkRequisition *requisition	)
/*	Implementation of the default size request handler	*/
{
	GernelCanvas		*canvas;
	GernelCanvasSection	*canvas_section;
	gint			height;
	GSList			*list;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(widget));

	canvas = GERNEL_CANVAS(widget);
	height = 0;
	canvas_section = GERNEL_CANVAS_SECTION(canvas->mask->data);
	requisition->width = gernel_canvas_section_get_width(canvas_section);

	for (list = canvas->mask; list; list = list->next) {
		canvas_section = GERNEL_CANVAS_SECTION(list->data);
		height += gernel_canvas_section_get_height(canvas_section);
		if (list->next != NULL) height += GNOME_PAD_SMALL;
	}

	requisition->height = height;
}

static void gernel_canvas_class_init(	GernelCanvasClass *klass	)
/*	Initializes the GernelCanvas class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkLayoutClass		*layout_class;
	GnomeCanvasClass	*canvas_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	layout_class = (GtkLayoutClass*)klass;
	canvas_class = (GnomeCanvasClass*)klass;

	parent_class = gtk_type_class(gnome_canvas_get_type());

	gtk_object_add_arg_type("GernelCanvas::preferences",
				GTK_TYPE_WIDGET,
				GTK_ARG_READABLE,
				ARG_PREFERENCES	);

	gtk_object_add_arg_type("GernelCanvas::sections",
				GTK_TYPE_POINTER,
				GTK_ARG_WRITABLE,
				ARG_SECTIONS	);

	gtk_object_add_arg_type("GernelCanvas::local_name",
				GTK_TYPE_STRING,
				GTK_ARG_READWRITE,
				ARG_LOCAL_NAME	);

	erty_arg_add_multiple(
		"GernelCanvas",
		"show_title", GTK_TYPE_BOOL, ARG_SHOW_TITLE,
		"show_headers", GTK_TYPE_BOOL, ARG_SHOW_HEADERS,
		"show_text", GTK_TYPE_BOOL, ARG_SHOW_TEXT,
		"show_prompts", GTK_TYPE_BOOL, ARG_SHOW_PROMPTS,
		"detachable", GTK_TYPE_BOOL, ARG_DETACHABLE,
		"show_canvas", GTK_TYPE_BOOL, ARG_SHOW_CANVAS,
		"color_theme", GTK_TYPE_BOOL, ARG_COLOR_THEME,
		"color_custom", GTK_TYPE_BOOL, ARG_COLOR_CUSTOM,
		"color_background", GTK_TYPE_STRING, ARG_COLOR_BACKGROUND,
		"color_layout_elements", GTK_TYPE_STRING, ARG_COLOR_LAYOUT,
		"color_fonts", GTK_TYPE_STRING, ARG_COLOR_FONTS,
		"font_theme", GTK_TYPE_BOOL, ARG_FONT_THEME,
		"font_custom", GTK_TYPE_BOOL, ARG_FONT_CUSTOM,
		"fontset_title", GTK_TYPE_STRING, ARG_FONTSET_TITLE,
		"fontset_text", GTK_TYPE_STRING, ARG_FONTSET_TEXT,
		"fontset_header", GTK_TYPE_STRING, ARG_FONTSET_HEADER,
		NULL
		);

	function_offset = GTK_SIGNAL_OFFSET(GernelCanvasClass, detachable);

	canvas_signals[DETACHABLE] =

			gtk_signal_new(	"detachable",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__BOOL,
					GTK_TYPE_NONE,
					1,
					GTK_TYPE_BOOL	);

	function_offset = GTK_SIGNAL_OFFSET(GernelCanvasClass, update);

	canvas_signals[UPDATE] =

			gtk_signal_new(	"update",
					GTK_RUN_LAST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	function_offset = GTK_SIGNAL_OFFSET(GernelCanvasClass, menu);

	canvas_signals[MENU] = gtk_signal_new(	"menu",
						GTK_RUN_FIRST,
						object_class->type,
						function_offset,
						gtk_marshal_NONE__UINT,
						GTK_TYPE_NONE,
						1,
						GTK_TYPE_ULONG	);

	function_offset = GTK_SIGNAL_OFFSET(GernelCanvasClass, configure);

	canvas_signals[CONFIGURE] = gtk_signal_new(	"configure",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	gtk_object_class_add_signals(object_class, canvas_signals, LAST_SIGNAL);

	object_class->get_arg = gernel_canvas_get_arg;
	object_class->set_arg = gernel_canvas_set_arg;

	widget_class->size_allocate = gernel_canvas_size_allocate;
	widget_class->size_request = gernel_canvas_size_request;

	klass->detachable = NULL;
	klass->update = gernel_canvas_update;
	klass->configure = NULL;
}

static void gernel_canvas_update(	GernelCanvas *canvas	)
/*	Update canvas	*/
{
	GnomeCanvasItem	*root;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	root = GNOME_CANVAS_ITEM(gnome_canvas_root(GNOME_CANVAS(canvas)));
	gnome_canvas_item_request_update(root);
	gtk_widget_queue_resize(GTK_WIDGET(canvas));
}

GtkType gernel_canvas_get_type(	void	)
/*	Return type identifier for type GernelCanvas	*/
{
	static GtkType	canvas_type = 0;

	if (!canvas_type) {

		static const	GtkTypeInfo canvas_info = {
				"GernelCanvas",
				sizeof(GernelCanvas),
				sizeof(GernelCanvasClass),
				(GtkClassInitFunc)gernel_canvas_class_init,
				(GtkObjectInitFunc)gernel_canvas_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
				};

		canvas_type = gtk_type_unique(	gnome_canvas_get_type(), 
						&canvas_info	);

	}

	return canvas_type;
}

GtkWidget* gernel_canvas_new(	gchar *name,
				...	)
/*	Create an instance of GernelCanvas, the Gernel's text viewer;
	width: initial width of canvas
	height: 
	type, ...: list of arguments to build a mask from, with types	*/
{
	GtkWidget	*widget;
	va_list		args;
	GernelCanvas	*canvas;
	gchar		*path;

	g_return_val_if_fail(name != NULL, NULL);

	va_start(args, name);
	path = g_strconcat(name, "/", NULL);
g_print("gernel_canvas_new:	FIXME: I thing push/pop_prefix() no longer needed (Gerk doesn't use it)\n");
	gnome_config_push_prefix(path);

	/*	IMPORTANT: sections are constructed _after_ setting 
		preferences; those settings that apply to the sections have
		therefore to be set_real() in construct_sections() again */

	widget = erty_widget_new(
			gernel_canvas_get_type(),
			name,
			"color_background", "cccccccc",
			"color_layout_elements", "bbbbbbbb",
			"color_fonts", "aaaaaaaa",
			"show_canvas", TRUE,
			"detachable", TRUE,
			"show_title", TRUE,
			"show_headers", TRUE,
			"show_prompts", FALSE,
			"show_text", TRUE,
			"font_theme", TRUE,
			"font_custom", FALSE,
			"fontset_title", GERNEL_CANVAS_FONTSET_TITLE,
			"fontset_header", GERNEL_CANVAS_FONTSET_HEADER,
			"fontset_text", GERNEL_CANVAS_FONTSET_TEXT,
			"color_theme", TRUE,
			"color_custom", FALSE,
			NULL
			);

	gnome_config_pop_prefix();
	g_free(path);
	canvas = GERNEL_CANVAS(widget);

	/*	Local name and section masks should not be stored as
		preferences	*/
	gernel_canvas_set_local_name(canvas, name);
	gernel_canvas_construct_sections(canvas, args);
	va_end(args);

	return widget;
}

static void gernel_canvas_set_local_name(	GernelCanvas *canvas,
						gchar *name	)
/*	Set the name of canvas to 'name'	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	canvas->local_name = name;
}

static void gernel_canvas_init(	GernelCanvas *canvas	)
/*	Initialize a GernelCanvas	*/
{
	GtkObject	*object;
	gint		i;

	object = GTK_OBJECT(canvas);

	canvas->mask = NULL;
	canvas->background = NULL;

	for (i = 0; i <= GERNEL_CANVAS_SECTION_TYPE_TAIL; i++) {
		canvas->fonts[i] = NULL;
		canvas->fontsets[i] = NULL;
	}

	canvas->layout_element_color = 0x00000000;
	canvas->background_color = 0x00000000;
	canvas->fonts_color = 0x00000000;

	gtk_signal_connect(	object,
				"button_press_event",
				gernel_canvas_menu,
				NULL	);

	gtk_signal_connect(object, "style_set", gernel_canvas_set_style, NULL);
}

static void gernel_canvas_construct_sections(	GernelCanvas *canvas,
						va_list	args	)
/*	Construct a GernelCanvas	*/
{
	enum GernelCanvasElement	element;
	enum GernelCanvasSectionType	type;

	element = va_arg(args, enum GernelCanvasElement);

	while (element != GERNEL_CANVAS_ELEMENT_0) {
		enum GernelCanvasSectionType	section_type;

		section_type = gernel_canvas_map_element(canvas, element);
		gernel_canvas_construct_section(canvas, section_type);
		element = va_arg(args, enum GernelCanvasElement);
	}

	/*	It is possible that fonts have been set before sections were
		constructed; checking for fonts already set:	*/

	for (	type = GERNEL_CANVAS_SECTION_TYPE_0; 
		type <= GERNEL_CANVAS_SECTION_TYPE_TAIL;
		type++	)

		if (canvas->fontsets[type] != NULL)
			gernel_canvas_set_fontset_real(canvas, type);

	/*	Same applies for other preference flags:	*/

	gernel_canvas_set_show(	canvas, 
				GERNEL_CANVAS_SECTION_TYPE_PROMPT,
				canvas->show_prompts	);

	gernel_canvas_set_show(	canvas,
				GERNEL_CANVAS_SECTION_TYPE_TITLE,
				canvas->show_title	);

	gernel_canvas_set_show(	canvas,
				GERNEL_CANVAS_SECTION_TYPE_HEADER,
				canvas->show_headers	);

	gernel_canvas_set_color_theme(canvas, canvas->colors_from_theme);
}

static void gernel_canvas_set_style(	GernelCanvas *canvas	)
/*	Handle the 'style_set' signal	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gernel_canvas_set_color_theme(canvas, canvas->colors_from_theme);
	gernel_canvas_set_font_theme(canvas, canvas->fonts_from_theme);
}

enum GernelCanvasSectionType gernel_canvas_map_element(
		GernelCanvas *canvas,
		enum GernelCanvasElement element
		)
/*	Map element to corresponding GernelCanvasSectionType	*/
{
	enum GernelCanvasSectionType	type;

	g_return_val_if_fail(canvas != NULL, GERNEL_CANVAS_SECTION_TYPE_0);

	g_return_val_if_fail(	GERNEL_IS_CANVAS(canvas), 
				GERNEL_CANVAS_SECTION_TYPE_0	);

	g_return_val_if_fail(	element != GERNEL_CANVAS_ELEMENT_0, 
				GERNEL_CANVAS_SECTION_TYPE_0	);

	switch (element) {
		case GERNEL_CANVAS_ELEMENT_TITLE:
			type = GERNEL_CANVAS_SECTION_TYPE_TITLE;
			break;
		case GERNEL_CANVAS_ELEMENT_PROMPT:
			type = GERNEL_CANVAS_SECTION_TYPE_PROMPT;
			break;
		case GERNEL_CANVAS_ELEMENT_HEADER:
			type = GERNEL_CANVAS_SECTION_TYPE_HEADER;
			break;
		case GERNEL_CANVAS_ELEMENT_TEXT:
			type = GERNEL_CANVAS_SECTION_TYPE_TEXT;
			break;
		case GERNEL_CANVAS_ELEMENT_TAIL:
			type = GERNEL_CANVAS_SECTION_TYPE_TAIL;
			break;
		case GERNEL_CANVAS_ELEMENT_0:
		default:
			type = GERNEL_CANVAS_SECTION_TYPE_0;
			g_assert_not_reached();
			break;
	}

	return type;
}

static void gernel_canvas_construct_background(	GernelCanvas *canvas	)
/*	Construct a background for the canvas	*/
{
	enum GernelCanvasSectionType	section_type;
	GtkWidget			*widget;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(canvas->background == NULL);

	widget = GTK_WIDGET(canvas);
	section_type = GERNEL_CANVAS_SECTION_TYPE_BACKGROUND;

	canvas->background = gernel_canvas_section_new(section_type, widget);
}

static void gernel_canvas_construct_section(
		GernelCanvas *canvas,
		enum GernelCanvasSectionType type
		)
/*	Construct a new GernelCanvasItem	*/
{
	GernelCanvasSection	*canvas_section;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0);

	canvas_section = gernel_canvas_section_new(type, GTK_WIDGET(canvas));

	switch (type) {
		case GERNEL_CANVAS_SECTION_TYPE_TITLE:
			g_return_if_fail(canvas->mask == NULL);
			break;
		case GERNEL_CANVAS_SECTION_TYPE_TAIL:
			g_return_if_fail(canvas->mask != NULL);
			break;
		case GERNEL_CANVAS_SECTION_TYPE_PROMPT:
		case GERNEL_CANVAS_SECTION_TYPE_HEADER:
		case GERNEL_CANVAS_SECTION_TYPE_TEXT:
			break;
		default:
			g_assert_not_reached();
			break;
	}

	gernel_canvas_add(canvas, canvas_section);
}

static void gernel_canvas_add(	GernelCanvas *canvas,
				GernelCanvasSection *canvas_section	)
/*	Add canvas_section to canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(canvas_section != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS_SECTION(canvas_section));

	canvas->mask = g_slist_append(canvas->mask, canvas_section);
}

void gernel_canvas_update_contents(	GernelCanvas *canvas,
					gchar *title,
					gchar *prompt,
					gchar *text	)
/*	Callback function for gernel_canvas_set_contents(); currently used as a 
	callback for the menus of CHOICE variables	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	g_return_if_fail(	(title != NULL) || 
				(prompt != NULL) || 
				(text != NULL)	);

	gernel_canvas_set_contents(	canvas,
					title,
					prompt,
					NULL,
					text,
					NULL,
					NULL	);
}

void gernel_canvas_set_contents(	GernelCanvas *canvas,
					gchar *str,
					...	)
/*	Fill the canvas with the strings given; the strings have to be passed
	in the same order as the GernelCanvas has filled with its graphical
	counterparts.	*/
{
	va_list		args;
	GSList		*list;
	gchar		*string;
	GtkAllocation	allocation;
	gint		height, width;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	allocation = GTK_WIDGET(canvas)->allocation;
	width = MAX(allocation.width, GNOME_PAD);
	height = 0;
	va_start(args, str);
	string = str;

	for (list = canvas->mask; list; list = list->next) {
		GernelCanvasSection	*section;

		section = GERNEL_CANVAS_SECTION(list->data);

		switch (section->type) {
			gchar	*str;

			case GERNEL_CANVAS_SECTION_TYPE_TITLE:
			case GERNEL_CANVAS_SECTION_TYPE_TEXT:
				if (!canvas->show_text) break;

				gernel_canvas_section_set_text(	section,
								string	);

				break;
			case GERNEL_CANVAS_SECTION_TYPE_PROMPT:

				str = g_strconcat(	_("Internal Name: "),
							string, 
							NULL	);

				gernel_canvas_section_set_text(section, str);
				break;
			case GERNEL_CANVAS_SECTION_TYPE_HEADER:
				g_return_if_fail(string == NULL);

				gernel_canvas_section_set_text(
						section, 
						_("Description")
						);

				break;
			case GERNEL_CANVAS_SECTION_TYPE_TAIL:
				g_return_if_fail(string == NULL);
			default:
				break;
		}

		gernel_canvas_section_set_width(section, width);
		gernel_canvas_section_set_height(section);
		gernel_canvas_section_set_x(section, 0);
		gernel_canvas_section_set_y(section, height);
		height += gernel_canvas_section_get_height(section);
		if (list->next != NULL) height += GERNEL_CANVAS_SPACING;

		string = va_arg(args, gchar*);
	}

	va_end(args);

	gernel_canvas_request_update(canvas);
}

static void gernel_canvas_request_update(	GernelCanvas *canvas	)
/*	Emit the 'update' signal	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gtk_signal_emit(GTK_OBJECT(canvas), canvas_signals[UPDATE]);
}

static void gernel_canvas_set_show_canvas(	GernelCanvas *canvas,
						gboolean show	)
/*	Preference function to show/hide the canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	if (show)
		gtk_widget_show(GTK_WIDGET(canvas));
	else
		gtk_widget_hide(GTK_WIDGET(canvas));

	canvas->shown = show;
}

static void gernel_canvas_detachable(	GernelCanvas *canvas,
					gboolean flag	)
/*	Preference function to lock/unlock the canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gtk_signal_emit(GTK_OBJECT(canvas), canvas_signals[DETACHABLE], flag);
	canvas->detachable = flag;
}

static GtkWidget* gernel_canvas_preferences(	GernelCanvas *canvas	)
/*	Assemble a page to be used in a GnomeApp's property box	*/
{
	GtkWidget		*page, *general, *colors, *fonts, *elements, 
				*widget, *prerequisite, *detachable;

	GtkContainer		*container;

	g_return_val_if_fail(canvas != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), NULL);

	widget = GTK_WIDGET(canvas);

	page = erty_preference_page_new();
	container = GTK_CONTAINER(page);
	general = erty_preference_section_new(_("General"));

/*	prerequisite = erty_preference_section_add_multiple(
			ERTY_PREFERENCE_SECTION(general),
			widget,
			ERTY_PREFERENCE_SECTION_DEPENDENCY_A, 
			_("Show Viewer"), ARG_SHOW_CANVAS, 
			NULL, ARG_0,
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Viewer is Detachable"), ARG_DETACHABLE, 
			NULL
			);	*/

	prerequisite = erty_preference_section_add(
			ERTY_PREFERENCE_SECTION(general),
			widget,
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Show Viewer"), 
			ARG_SHOW_CANVAS
			);

	detachable = erty_preference_section_add(
			ERTY_PREFERENCE_SECTION(general),
			widget,
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Viewer is Detachable"),
			ARG_DETACHABLE
			);

	gtk_container_add(container, general);
	fonts = erty_preference_section_new(_("Fonts"));

	erty_preference_section_add_multiple(
			ERTY_PREFERENCE_SECTION(fonts),
			widget,
			ERTY_PREFERENCE_SECTION_DEPENDENCY_XOR,
			_("Use Fonts from Current Theme"), ARG_FONT_THEME,
			_("Use Custom Fonts:"), ARG_FONT_CUSTOM,
			ERTY_PREFERENCE_SECTION_WIDGET_FONT_PICKER,
			_("Title:"), ARG_FONTSET_TITLE,
			_("Header:"), ARG_FONTSET_HEADER,
			_("Text:"), ARG_FONTSET_TEXT,
			NULL
			);

	gtk_container_add(container, fonts);
	colors = erty_preference_section_new(_("Colors"));

	erty_preference_section_add_multiple(
			ERTY_PREFERENCE_SECTION(colors),
			widget,
			ERTY_PREFERENCE_SECTION_DEPENDENCY_XOR,
			_("Use Colors from Current Theme"), ARG_COLOR_THEME,
			_("Use Custom Colors:"), ARG_COLOR_CUSTOM,
			ERTY_PREFERENCE_SECTION_WIDGET_COLOR_PICKER,
			_("Background:"), ARG_COLOR_BACKGROUND,
			_("Layout Elements:"), ARG_COLOR_LAYOUT,
			_("Fonts:"), ARG_COLOR_FONTS,
			NULL
			);

	gtk_container_add(container, colors);
	elements = erty_preference_section_new(_("Elements"));

	erty_preference_section_add_multiple(
			ERTY_PREFERENCE_SECTION(elements),
			widget,
			ERTY_PREFERENCE_SECTION_DEPENDENCY_0,
			NULL, ARG_0,
			NULL, ARG_0,
			ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
			_("Show Title"), ARG_SHOW_TITLE,
			_("Show Variable Name"), ARG_SHOW_PROMPTS,
			_("Show Headers"), ARG_SHOW_HEADERS,
			_("Show Text"), ARG_SHOW_TEXT,
			NULL
			);

	gtk_container_add(container, elements);

	erty_preference_section_add_dependencies(
			ERTY_PREFERENCE_SECTION(general), 
			prerequisite,
			detachable,
			colors,
			elements,
			fonts,
			NULL
			);

	return page;
}

static void gernel_canvas_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting for this object	*/
{
	GernelCanvas	*canvas;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(object));

	canvas = GERNEL_CANVAS(object);


	switch (arg_id) {
		GtkWidget	*widget;
		guint32		value;
		gchar		*str;

		case ARG_PREFERENCES:
			widget = gernel_canvas_preferences(canvas);
			GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(widget);
			break;
		case ARG_LOCAL_NAME:
			GTK_VALUE_STRING(*arg) = canvas->local_name;
			break;
		case ARG_SHOW_CANVAS:
			GTK_VALUE_BOOL(*arg) = canvas->shown;
			break;
		case ARG_SHOW_TITLE:
			GTK_VALUE_BOOL(*arg) = canvas->show_title;
			break;
		case ARG_SHOW_HEADERS:
			GTK_VALUE_BOOL(*arg) = canvas->show_headers;
			break;
		case ARG_SHOW_TEXT:
			GTK_VALUE_BOOL(*arg) = canvas->show_text;
			break;
		case ARG_SHOW_PROMPTS:
			GTK_VALUE_BOOL(*arg) = canvas->show_prompts;
			break;
		case ARG_DETACHABLE:
			GTK_VALUE_BOOL(*arg) = canvas->detachable;
			break;
		case ARG_COLOR_BACKGROUND:
			value = gernel_canvas_get_color_background(canvas);
			str = gernel_color_32_to_string(value);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_COLOR_LAYOUT:

			value = gernel_canvas_get_color_layout_elements(
				canvas
				);

			str = gernel_color_32_to_string(value);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_COLOR_FONTS:
			value = gernel_canvas_get_color_fonts(canvas);
			str = gernel_color_32_to_string(value);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_COLOR_THEME:
			GTK_VALUE_BOOL(*arg) = canvas->colors_from_theme;
			break;
		case ARG_COLOR_CUSTOM:
			GTK_VALUE_BOOL(*arg) = !canvas->colors_from_theme;
			break;
		case ARG_FONT_THEME:
			GTK_VALUE_BOOL(*arg) = canvas->fonts_from_theme;
			break;
		case ARG_FONT_CUSTOM:
			GTK_VALUE_BOOL(*arg) = !canvas->fonts_from_theme;
			break;
		case ARG_FONTSET_TITLE:
			str = gernel_canvas_get_fontset_title(canvas);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_FONTSET_TEXT:
			str = gernel_canvas_get_fontset_text(canvas);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_FONTSET_HEADER:
			str = gernel_canvas_get_fontset_header(canvas);
			GTK_VALUE_STRING(*arg) = str;
			break;
		case ARG_SECTIONS:
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static gchar* gernel_canvas_get_fontset_title(	GernelCanvas *canvas	)
/*	Get the custom fontset used for the title element	*/
{
	enum GernelCanvasSectionType	type;

	g_return_val_if_fail(canvas != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), NULL);

	type = GERNEL_CANVAS_SECTION_TYPE_TITLE;

	return gernel_canvas_get_fontset(canvas, type);
}

static gchar* gernel_canvas_get_fontset_text(	GernelCanvas *canvas	)
/*	Get the custom fontset used for the title element	*/
{
	enum GernelCanvasSectionType	type;

	g_return_val_if_fail(canvas != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), NULL);

	type = GERNEL_CANVAS_SECTION_TYPE_TEXT;

	return gernel_canvas_get_fontset(canvas, type);
}

static gchar* gernel_canvas_get_fontset_header(	GernelCanvas *canvas	)
/*	Get the custom fontset used for the header elements	*/
{
	enum GernelCanvasSectionType	type;

	g_return_val_if_fail(canvas != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), NULL);

	type = GERNEL_CANVAS_SECTION_TYPE_HEADER;

	return gernel_canvas_get_fontset(canvas, type);
}

static gchar* gernel_canvas_get_fontset(GernelCanvas *canvas,
					enum GernelCanvasSectionType type )
/*	Get the custom fontset used for 'type'	*/
{
	g_return_val_if_fail(canvas != NULL, NULL);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), NULL);
	g_return_val_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0, NULL);

	return canvas->fontsets[type];
}

static void gernel_canvas_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	GernelCanvas	*canvas;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(object));

	canvas = GERNEL_CANVAS(object);

	switch (arg_id) {
		guint32		color;
		gboolean	flag;
		va_list		*list;
		gchar		*string;

		case ARG_LOCAL_NAME:
			string = GTK_VALUE_STRING(*arg);
			gernel_canvas_set_local_name(canvas, string);
			break;
		case ARG_SHOW_CANVAS:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_show_canvas(canvas, flag);
			break;
		case ARG_SHOW_TITLE:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_show_title(canvas, flag);
			break;
		case ARG_SHOW_HEADERS:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_show_headers(canvas, flag);
			break;
		case ARG_SHOW_TEXT:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_show_text(canvas, flag);
			break;
		case ARG_SHOW_PROMPTS:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_show_prompts(canvas, flag);
			break;
		case ARG_DETACHABLE:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_detachable(canvas, flag);
			break;
		case ARG_COLOR_BACKGROUND:
			string = GTK_VALUE_STRING(*arg);
			color = gernel_color_string_to_32(string);
			gernel_canvas_set_color_background(canvas, color);
			break;
		case ARG_COLOR_LAYOUT:
			string = GTK_VALUE_STRING(*arg);
			color = gernel_color_string_to_32(string);
			gernel_canvas_set_color_layout_elements(canvas, color);
			break;
		case ARG_COLOR_FONTS:
			string = GTK_VALUE_STRING(*arg);
			color = gernel_color_string_to_32(string);
			gernel_canvas_set_color_fonts(canvas, color);
			break;
		case ARG_COLOR_THEME:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_color_theme(canvas, flag);
			break;
		case ARG_COLOR_CUSTOM:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_color_theme(canvas, !flag);
			break;
		case ARG_FONT_THEME:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_font_theme(canvas, flag);
			break;
		case ARG_FONT_CUSTOM:
			flag = GTK_VALUE_BOOL(*arg);
			gernel_canvas_set_font_theme(canvas, !flag);
			break;
		case ARG_FONTSET_TITLE:
			string = GTK_VALUE_STRING(*arg);
			gernel_canvas_set_fontset_title(canvas, string);
			break;
		case ARG_FONTSET_TEXT:
			string = GTK_VALUE_STRING(*arg);
			gernel_canvas_set_fontset_text(canvas, string);
			break;
		case ARG_FONTSET_HEADER:
			string = GTK_VALUE_STRING(*arg);
			gernel_canvas_set_fontset_header(canvas, string);
			break;
		case ARG_SECTIONS:
			list = GTK_VALUE_POINTER(*arg);
			gernel_canvas_construct_sections(canvas, *list);
			break;
		case ARG_PREFERENCES:
		default:
			break;
	}
}

static void gernel_canvas_set_color_background(	GernelCanvas *canvas,
						guint32 color	)
/*	Set background color of canvas to 'color'	*/
{
	GernelCanvasSection	*canvas_section;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	if (canvas->background == NULL)
		gernel_canvas_construct_background(canvas);

	canvas_section = canvas->background;
	gernel_canvas_section_set_background_color(canvas_section, color);
	if (!canvas->colors_from_theme) canvas->background_color = color;	
}

static guint32 gernel_canvas_get_color_background(
		GernelCanvas *canvas
		)
/*	Get custom background color of canvas	*/
{
	g_return_val_if_fail(canvas != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), 0);

	return canvas->background_color;
}

static void gernel_canvas_set_color_layout_elements(	GernelCanvas *canvas,
							guint32 color	)
/*	Set color of the canvas elements	*/
{
	GSList	*list;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	for (list = canvas->mask; list != NULL; list = list->next) {
		GernelCanvasSection	*section;

		section = GERNEL_CANVAS_SECTION(list->data);
		gernel_canvas_section_set_background_color(section, color);
	}

	if (!canvas->colors_from_theme) canvas->layout_element_color = color;
}

static void gernel_canvas_set_color_fonts(	GernelCanvas *canvas,
						guint32 color	)
/*	Set font color	*/
{
	GSList	*list;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	for (list = canvas->mask; list != NULL; list = list->next) {
		GernelCanvasSection	*section;

		section = GERNEL_CANVAS_SECTION(list->data);

		gernel_canvas_section_set_foreground_color(section, color);
	}

	if (!canvas->colors_from_theme) canvas->fonts_color = color;
}

static void gernel_canvas_set_font_title(	GernelCanvas *canvas,
						GdkFont *font	)
/*	Set the font of all 'title' elements in canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(font != NULL);

	gernel_canvas_set_font(canvas, font, GERNEL_CANVAS_SECTION_TYPE_TITLE);
}

static void gernel_canvas_set_fontset_title(	GernelCanvas *canvas,
						gchar *fontset	)
/*	Set the fontset of the title element	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(fontset != NULL);

	gernel_canvas_set_fontset(	canvas,
					fontset,
					GERNEL_CANVAS_SECTION_TYPE_TITLE );
}

static void gernel_canvas_set_fontset_header(	GernelCanvas *canvas,
						gchar *fontset	)
/*	Set the fontset of the header elements	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(fontset != NULL);

	gernel_canvas_set_fontset(	canvas,
					fontset,
					GERNEL_CANVAS_SECTION_TYPE_HEADER );
}

static void gernel_canvas_set_fontset_text(	GernelCanvas *canvas,
						gchar *fontset	)
/*	Set the fontset of the text element	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(fontset != NULL);

	gernel_canvas_set_fontset(	canvas,
					fontset,
					GERNEL_CANVAS_SECTION_TYPE_TEXT	);
}

static void gernel_canvas_set_font_header(	GernelCanvas *canvas,
						GdkFont *font	)
/*	Set the font of all 'header' elements in canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(font != NULL);

	gernel_canvas_set_font(canvas, font, GERNEL_CANVAS_SECTION_TYPE_HEADER);
}

static void gernel_canvas_set_font_text(	GernelCanvas *canvas,
						GdkFont *font	)
/*	Set the font of all 'text' elements in canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(font != NULL);

	gernel_canvas_set_font(canvas, font, GERNEL_CANVAS_SECTION_TYPE_TEXT);
	gernel_canvas_set_font(canvas, font, GERNEL_CANVAS_SECTION_TYPE_PROMPT);
}

static void gernel_canvas_set_font(	GernelCanvas *canvas,
					GdkFont *font,
					enum GernelCanvasSectionType type )
/*	Set font of all elements in canvas of type 'type'; fonts are stored
	in GernelCanvas to allow setting fonts before constructing sections*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(font != NULL);
	g_return_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0);

	canvas->fonts[type] = font;
	gernel_canvas_set_font_real(canvas, type);
}

static void gernel_canvas_set_fontset(	GernelCanvas *canvas,
					gchar *fontset,
					enum GernelCanvasSectionType type )
/*	Set fontset for elements specified by 'type'	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(fontset != NULL);
	g_return_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0);

	canvas->fontsets[type] = fontset;
	gernel_canvas_set_fontset_real(canvas, type);
	if (!canvas->fonts_from_theme) canvas->fontsets[type] = fontset;	
}

static void gernel_canvas_set_font_real(GernelCanvas *canvas,
					enum GernelCanvasSectionType type )
/*	Apply the stored fonts to all sections of type 'type'	*/
{
	GSList	*list;
	GdkFont	*font;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0);
	g_return_if_fail(canvas->fonts[type] != NULL);

	font = canvas->fonts[type];

	for (list = canvas->mask; list != NULL; list = list->next) {
		GernelCanvasSection	*canvas_section;

		canvas_section = GERNEL_CANVAS_SECTION(list->data);

		if (canvas_section->type == type)
			gernel_canvas_section_set_font(canvas_section, font);
	}
}

static void gernel_canvas_set_fontset_real(
		GernelCanvas *canvas,
		enum GernelCanvasSectionType type
		)
/*	Apply the fontset stored with key 'type' to elements of type 'type' */
{
	GSList	*list;
	gchar	*str;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0);

	str = canvas->fontsets[type];

	for (list = canvas->mask; list != NULL; list = list->next) {
		GernelCanvasSection	*canvas_section;

		canvas_section = GERNEL_CANVAS_SECTION(list->data);

		if (canvas_section->type == type)
			gernel_canvas_section_set_fontset(canvas_section, str);
	}
}

static void gernel_canvas_menu(	GernelCanvas *canvas,
				GdkEventButton *event	)
/*	Emit the 'menu' signal	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(event != NULL);

	if ((event->type == GDK_BUTTON_PRESS) && (event->button == 3))

		gtk_signal_emit(	GTK_OBJECT(canvas), 
					canvas_signals[MENU], 
					event->time	);
}

void gernel_canvas_construct_menu(	GernelCanvas *canvas,
					guint32 time,
					GtkWidget *app	)
/*	Construct the canvas menu for canvas, using app for hints	*/
{
	GtkWidget	*canvas_menu;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(app != NULL);
	g_return_if_fail(GNOME_IS_APP(app));

	canvas_menu = gernel_canvas_menu_new(app);

	gtk_signal_connect_object(	GTK_OBJECT(canvas_menu),
					"preferences",
					gernel_canvas_configure,
					GTK_OBJECT(canvas)	);

	gernel_canvas_menu_popup(GERNEL_CANVAS_MENU(canvas_menu), time);
}

static void gernel_canvas_configure(	GernelCanvas *canvas	)
/*	Emit the 'configure' signal in order to popup the main preferences 
	dialog with canvas page on top	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gtk_signal_emit(GTK_OBJECT(canvas), canvas_signals[CONFIGURE]);
}

static guint32 gernel_canvas_get_color_layout_elements(
		GernelCanvas *canvas
		)
/*	Get custom color of the layout elements	*/
{
	g_return_val_if_fail(canvas != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), 0);

	return canvas->layout_element_color;
}

static guint32 gernel_canvas_get_color_fonts(	GernelCanvas *canvas	)
/*	Get custom font color	*/
{
	g_return_val_if_fail(canvas != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), 0);

	return canvas->fonts_color;
}

static guint32 gernel_canvas_convert_color(	GernelCanvas *canvas,
						GdkColor *color	)
/*	Convert a GdkColor to the corresponding guint32 word	*/
{
	guint8	r, g, b;
	guint32	tmp, result;

	g_return_val_if_fail(canvas != NULL, 0);
	g_return_val_if_fail(GERNEL_IS_CANVAS(canvas), 0);

	r = color->red;
	g = color->green;
	b = color->blue;

	tmp = 0x00000000;
	result = (tmp | r) << 24;
	result = result | ((tmp | g) << 16);
	result = result | ((tmp | b) << 8);

	return result;
}

static void gernel_canvas_set_color_theme(	GernelCanvas *canvas,
						gboolean flag	)
/*	Take color scheme from currently loaded GTK+ theme, or from custom
	colors defined by user	*/
{
	guint32		color;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	canvas->colors_from_theme = flag;

	if (flag == TRUE) {
		GtkStyle	*style;
		GdkColor	theme;

		style = gtk_widget_get_style(GTK_WIDGET(canvas));
		theme = style->light[GTK_STATE_NORMAL];
		color = gernel_canvas_convert_color(canvas, &theme);
		gernel_canvas_set_color_background(canvas, color);
		theme = style->bg[GTK_STATE_NORMAL];
		color = gernel_canvas_convert_color(canvas, &theme);
		gernel_canvas_set_color_layout_elements(canvas, color);
		theme = style->text[GTK_STATE_NORMAL];
		color = gernel_canvas_convert_color(canvas, &theme);
		gernel_canvas_set_color_fonts(canvas, color);
	} else {
		color = canvas->layout_element_color;
		gernel_canvas_set_color_layout_elements(canvas, color);
		gernel_canvas_set_color_fonts(canvas, canvas->fonts_color);
	}
}

static void gernel_canvas_set_font_theme(	GernelCanvas *canvas,
						gboolean flag	)
/*	Take font scheme from currently loaded GTK+ theme, or from custom
	fonts defined by user	*/ 
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	canvas->fonts_from_theme = flag;

	if (flag == TRUE) {
		GdkFontPrivate	*private;
		GtkStyle	*style;
		gchar		*font;

		style = gtk_widget_get_style(GTK_WIDGET(canvas));
		private = (GdkFontPrivate*)style->font;
		gernel_canvas_set_font_text(canvas, style->font);
		font = bigger_font(G_CHAR(private->names->data));
		gernel_canvas_set_font_header(canvas, gdk_font_load(font));
		font = bigger_font(bigger_font(font));
		gernel_canvas_set_font_title(canvas, gdk_font_load(font));
	}
}

static gchar* bigger_font(	gchar *font	)
/*	Enlarge 'font'	*/
{
	gint	size, tmp;
	gchar	**tmpv, str[4], *value;

	g_return_val_if_fail(font != NULL, NULL);

	tmpv = g_strsplit(font, "-", 0);
	size = atoi(tmpv[8]);
	size += 20;
	tmp = size / 100;
	str[0] = tmp + 48;
	tmp = size - (tmp * 100);
	str[1] = tmp / 10 + 48;
	tmp = (tmp / 10) * 10;
	str[2] = size - tmp - ((size / 100) * 100) + 48;
	str[3] = '\0';
	g_free(tmpv[8]);
	tmpv[8] = str;
	value = g_strjoinv("-", tmpv);

	return value;
}

static void gernel_canvas_set_show_title(	GernelCanvas *canvas,
						gboolean flag	)
/*	Show/hide the title of the canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gernel_canvas_set_show(	canvas, 
				GERNEL_CANVAS_SECTION_TYPE_TITLE, 
				flag	);

	canvas->show_title = flag;
}

static void gernel_canvas_set_show_headers(	GernelCanvas *canvas,
						gboolean flag	)
/*	Show/hide all headers in the canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gernel_canvas_set_show(	canvas,
				GERNEL_CANVAS_SECTION_TYPE_HEADER,
				flag	);

	canvas->show_headers = flag;
}

static void gernel_canvas_set_show_text(	GernelCanvas *canvas,
						gboolean flag	)
/*	Show/hide the actual help text in the canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gernel_canvas_set_show(	canvas,
				GERNEL_CANVAS_SECTION_TYPE_TEXT,
				flag	);

	canvas->show_text = flag;
}

static void gernel_canvas_set_show_prompts(	GernelCanvas *canvas,
						gboolean flag	)
/*	Show/hide all prompts in the canvas	*/
{
	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));

	gernel_canvas_set_show(	canvas,
				GERNEL_CANVAS_SECTION_TYPE_PROMPT,
				flag	);

	canvas->show_prompts = flag;
}

static void gernel_canvas_set_show(	GernelCanvas *canvas,
					enum GernelCanvasSectionType type,
					gboolean flag	)
/*	Show/hide the section type(s) specified by type	*/
{
	GSList	*list;

	g_return_if_fail(canvas != NULL);
	g_return_if_fail(GERNEL_IS_CANVAS(canvas));
	g_return_if_fail(type != GERNEL_CANVAS_SECTION_TYPE_0);

	for (list = canvas->mask; list != NULL; list = list->next) {
		GernelCanvasSection	*canvas_section;

		canvas_section = GERNEL_CANVAS_SECTION(list->data);

		if (canvas_section->type == type) {

			if (flag)
				gernel_canvas_section_show(canvas_section);
			else
				gernel_canvas_section_hide(canvas_section);
		}
	}
}
