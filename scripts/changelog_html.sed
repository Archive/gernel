#create <ul> tags
s/^$/<ul>/

#create </ul> tags
/<ul>/{
N
s/<ul>\n2000/<\/ul>\
2000/
}

#replace mail addresses
s/ \([^ ][^ ]* [^ ][^ ]*\)[	 ][	 ]*<\(.*\)>/ <a href="mailto:\2">\1<\/a>/

#indent asterisks
s/\*/<li>/

#emphasize dates
s/[0-9][0-9]*/<b>&<\/b>/g

#emphasize function names
#s/\([a-z_0-9]*()\)/<i>\1<\/i>/g

#link function names, e.g. foo() becomes http://cvs.gnome.org/lxr/ident?i=foo
s/[ 	>]\([^ ][^ ]*\)()/ <a href="http:\/\/cvs\.gnome\.org\/lxr\/ident?i=\1">\1<\/a>()/
