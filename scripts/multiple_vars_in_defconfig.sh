#!/bin/sh
#
# Checks for multiple definitions of variables in a defconfig file.
# Requires path to that file as argument.
# e.g. ./multiple_vars_in_defconfig.sh /usr/src/linux/arch/i386/defconfig
#

sed -e 's/# \(CONFIG[^ ]*\).*/\1=/;/#/d;/^$/d' $1 | awk -F= '{ print $1 }' | sort > defconfig.mnemonic
sed -e 's/# \(CONFIG[^ ]*\).*/\1=/;/#/d;/^$/d' $1 | awk -F= '{ print $1 }' | sort -u > defconfig.raw
echo "These variables occur more than once:"
diff defconfig.mnemonic defconfig.raw | sed -ne 's/^[><]//p'
rm defconfig.mnemonic defconfig.raw
