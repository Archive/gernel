#define ERTY_TYPE_PREFERENCE_PAGE	(erty_preference_page_get_type())

#define ERTY_PREFERENCE_PAGE(obj) \
	(GTK_CHECK_CAST(	(obj), \
				ERTY_TYPE_PREFERENCE_PAGE, \
				ErtyPreferencePage	))

#define ERTY_PREFERENCE_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				ERTY_TYPE_PREFERENCE_PAGE, \
				ErtyPreferencePageClass	))

#define ERTY_IS_PREFERENCE_PAGE(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_PREFERENCE_PAGE))

#define ERTY_IS_PREFERENCE_PAGE_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_PREFERENCE_PAGE))

typedef struct _ErtyPreferencePage		ErtyPreferencePage;
typedef struct _ErtyPreferencePageClass	ErtyPreferencePageClass;

struct _ErtyPreferencePage {
	GtkVBox		vbox;

	gboolean	complete;
	gpointer	data;
};

struct _ErtyPreferencePageClass {
	GtkVBoxClass 	parent_class;

	void (*changed)	(ErtyPreferencePage *preference_page);
	void (*apply)	(ErtyPreferencePage *preference_page);
	void (*save)	(ErtyPreferencePage *preference_page);
};

GtkType		erty_preference_page_get_type(	void	);

GtkWidget* 	erty_preference_page_new(	void	);

void		erty_preference_page_changed(
		ErtyPreferencePage *preference_page
		);

void		erty_preference_page_save(
		ErtyPreferencePage *preference_page
		);

void		erty_preference_page_apply(
		ErtyPreferencePage *preference_page
		);

void		erty_preference_page_set_data(
		ErtyPreferencePage *preference_page,
		gpointer data
		);

gpointer	erty_preference_page_get_data(
		ErtyPreferencePage *preference_page
		);

gboolean	erty_preference_page_get_complete(
		ErtyPreferencePage *preference_page
		);
