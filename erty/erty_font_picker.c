#include <erty.h>
#include <X11/Xlib.h>
#include <gdk/gdkprivate.h>
#include <erty_entry.h>

static void erty_font_picker_set_fontset(	ErtyFontPicker *font_picker,
						gchar *fontset	);

static gchar* erty_font_picker_get_fontset(	ErtyFontPicker *font_picker );
static void erty_font_picker_init(	ErtyFontPicker *font_picker	);
static void erty_font_picker_class_init(	ErtyFontPicker *font_picker );
static void erty_font_picker_changed(	ErtyFontPicker *font_picker	);

static void erty_font_picker_set_font(ErtyFontPicker *font_picker,
					GdkFont *font	);

static GdkFont* erty_font_picker_get_font(
		ErtyFontPicker *font_picker
		);

static void erty_font_picker_set_font_dialog_to_entry(
		ErtyFontPicker *font_picker
		);

static void erty_font_picker_set_font_entry_to_dialog(
		ErtyFontPicker *font_picker
		);

static void erty_font_picker_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void erty_font_picker_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static ErtyPickerClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_DISPLAY_VALUE,
	ARG_FONTSET,
	ARG_FONT	};

GtkType erty_font_picker_get_type(	void	)
/*	Return type identifier for type ErtyFontPicker	*/
{
	static GtkType	font_picker_type = 0;

	if (!font_picker_type) {

		static const GtkTypeInfo	font_picker_info = {
			"ErtyFontPicker",
			sizeof(ErtyFontPicker),
			sizeof(ErtyFontPickerClass),
			(GtkClassInitFunc)erty_font_picker_class_init,
			(GtkObjectInitFunc)erty_font_picker_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		font_picker_type = gtk_type_unique(
				erty_picker_get_type(),
				&font_picker_info
				);
	}

	return font_picker_type;
}

GtkWidget* erty_font_picker_new(	void	)
/*	Create an instance of ErtyFontPicker	*/
{
	ErtyFontPicker	*font_picker;

	font_picker = gtk_type_new(erty_font_picker_get_type());

	return GTK_WIDGET(font_picker);
}

GtkWidget* erty_font_picker_new_with_label(	gchar *label	)
/*	Create an instance of ErtyFontPicker with label	*/
{
	GtkWidget	*font_picker;

	g_return_val_if_fail(label != NULL, NULL);

	font_picker = erty_font_picker_new();
	erty_picker_set_label(ERTY_PICKER(font_picker), label);

	erty_picker_set_mode(	ERTY_PICKER(font_picker),
				ERTY_PICKER_LABEL_FIRST	);

	return font_picker;
}

static void erty_font_picker_init(	ErtyFontPicker *font_picker	)
/*	Initialize a ErtyFontPicker	*/
{
	GtkWidget	*entry, *box, *label, *fp_box;
	GtkObject	*object;
	GtkBox		*hbox;
	GnomeFontPicker	*picker;

	g_return_if_fail(font_picker != NULL);
	g_return_if_fail(ERTY_IS_FONT_PICKER(font_picker));

	hbox = GTK_BOX(font_picker);

	fp_box = gtk_hbox_new(FALSE, 0);
	erty_picker_set_selector(ERTY_PICKER(font_picker), fp_box);
	gtk_widget_show(fp_box);

	entry = erty_entry_new("ErtyFontPicker");
	font_picker->entry = entry;
	gtk_box_pack_start(GTK_BOX(fp_box), entry, FALSE, FALSE, 0);
	gtk_widget_show(entry);

	box = gtk_hbox_new(FALSE, 0);

	label = gtk_label_new(_("Browse..."));
	gtk_container_add(GTK_CONTAINER(box), label);
	gtk_widget_show(label);

	picker = GNOME_FONT_PICKER(gnome_font_picker_new());
	font_picker->selector = picker;

	gnome_font_picker_set_mode(	picker, 
					GNOME_FONT_PICKER_MODE_USER_WIDGET );

	gnome_font_picker_uw_set_widget(picker, box);

	gtk_signal_connect_object_after(
			GTK_OBJECT(picker),
			"clicked",
			erty_font_picker_set_font_dialog_to_entry,
			GTK_OBJECT(font_picker)
			);

	object = GTK_OBJECT(font_picker->entry);

	gtk_signal_connect_object(
			object,
			"changed",
			erty_font_picker_set_font_entry_to_dialog,
			GTK_OBJECT(font_picker)
			);

	gtk_signal_connect_object(	object,
					"changed",
					erty_font_picker_changed,
					GTK_OBJECT(font_picker)	);

	gtk_signal_connect_object(
			GTK_OBJECT(picker),
			"font_set",
			erty_font_picker_set_font_dialog_to_entry,
			GTK_OBJECT(font_picker)
			);

	gtk_signal_connect_object(	GTK_OBJECT(picker),
					"font_set",
					erty_font_picker_changed,
					GTK_OBJECT(font_picker)	);

	gtk_widget_show(box);
	gtk_container_add(GTK_CONTAINER(fp_box), GTK_WIDGET(picker));
	gtk_widget_show(GTK_WIDGET(picker));
}

static void erty_font_picker_class_init(	ErtyFontPicker *klass	)
/*	Initialize the ErtyFontPicker class	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBoxClass		*box_class;
	GtkHBoxClass		*hbox_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	box_class = (GtkBoxClass*)klass;
	hbox_class = (GtkHBoxClass*)klass;

	parent_class = gtk_type_class(ERTY_TYPE_PICKER);

	gtk_object_add_arg_type(	"ErtyFontPicker::font",
					GTK_TYPE_POINTER,
					GTK_ARG_READWRITE,
					ARG_FONT	);

	gtk_object_add_arg_type(	"ErtyFontPicker::fontset",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_FONTSET	);

	gtk_object_add_arg_type(	"ErtyFontPicker::display_value",
					GTK_TYPE_POINTER,
					GTK_ARG_READWRITE,
					ARG_DISPLAY_VALUE	);

	object_class->get_arg = erty_font_picker_get_arg;
	object_class->set_arg = erty_font_picker_set_arg;
}

static void erty_font_picker_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implementes argument setting for this object	*/
{
	ErtyFontPicker	*font_picker;

	font_picker = ERTY_FONT_PICKER(object);

	switch (arg_id) {
		GdkFont	*font;
		gchar	*str;

		case ARG_FONT:
			font = GTK_VALUE_POINTER(*arg);
			erty_font_picker_set_font(font_picker, font);
			break;
		case ARG_FONTSET:
		case ARG_DISPLAY_VALUE:
			str = GTK_VALUE_STRING(*arg);
			erty_font_picker_set_fontset(font_picker, str);
			break;
		default:
			break;
	}
}

static void erty_font_picker_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument getting	*/
{
	ErtyFontPicker	*font_picker;

	g_return_if_fail(object != NULL);
	g_return_if_fail(ERTY_IS_FONT_PICKER(object));

	font_picker = ERTY_FONT_PICKER(object);

	switch (arg_id) {
		GdkFont	*font;
		gchar	*str;

		case ARG_FONT:
			font = erty_font_picker_get_font(font_picker);
			GTK_VALUE_POINTER(*arg) = font;
			break;
		case ARG_DISPLAY_VALUE:
		case ARG_FONTSET:
			str = erty_font_picker_get_fontset(font_picker);

			if (str != NULL)
				GTK_VALUE_STRING(*arg)  = str;
			else
				arg->type = GTK_TYPE_INVALID;

			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void erty_font_picker_set_font(ErtyFontPicker *font_picker,
					GdkFont *font	)
/*	Show the font specified by 'font' in the entry	*/
/*	FIXME:	once GDK provides a way to retrieve font names from GdkFont
	structs, go over this function again	*/
{
	gint		i, number;
	XFontStruct	**font_structs;
	gchar		**font_names, *font_name;
	GdkFontPrivate	*private;

	g_return_if_fail(font_picker != NULL);
	g_return_if_fail(ERTY_IS_FONT_PICKER(font_picker));
	g_return_if_fail(font != NULL);

	private = (GdkFontPrivate*)font;
	number = XFontsOfFontSet(private->xfont, &font_structs, &font_names);

	for (i = 0; i < number; i++)

		erty_entry_set_current(	ERTY_ENTRY(font_picker->entry), 
					font_names[i]	);

	font_name = number?font_names[0]:(gchar*)private->names->data;
	erty_entry_set_current(ERTY_ENTRY(font_picker->entry), font_name);
}

static void erty_font_picker_set_fontset(	ErtyFontPicker *font_picker,
						gchar *fontset	)
/*	Set the font to be displayed in entry and picker widgets	*/
{
	gchar		*str;

	g_return_if_fail(font_picker != NULL);
	g_return_if_fail(ERTY_IS_FONT_PICKER(font_picker));
	g_return_if_fail(fontset != NULL);

	if (strstr(fontset, ",") != NULL) {
/*	if (erty_str_contains(fontset, ",") == FALSE) {	*/
		/*	Would it make more sense to set entry text to full
			fontset name?	*/
		gchar	**tmpv;

		tmpv = g_strsplit(fontset, ",", 1);
		str = g_strdup(tmpv[0]);
		g_strfreev(tmpv);
	} else
		str = fontset;

	erty_entry_set_current(ERTY_ENTRY(font_picker->entry), str);
	g_free(str);
}

static GdkFont* erty_font_picker_get_font(	ErtyFontPicker *font_picker )
/*	Get font currently displayed by font_picker	*/
{
	gchar	*font;

	g_return_val_if_fail(font_picker != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_FONT_PICKER(font_picker), NULL);

	font = gnome_font_picker_get_font_name(font_picker->selector);

	return gdk_font_load(font);
}

static gchar* erty_font_picker_get_fontset(	ErtyFontPicker *font_picker )
/*	Get fontset currently displayed by font_picker	*/
{
	gchar	*font;

	g_return_val_if_fail(font_picker != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_FONT_PICKER(font_picker), NULL);

	font = erty_entry_get_current(ERTY_ENTRY(font_picker->entry));

	return font != NULL?g_strjoin(",", font, font, font, NULL):NULL;
}

static void erty_font_picker_set_font_entry_to_dialog(
		ErtyFontPicker *font_picker
		)
/*	Callback to set the font specified in the entry in the dialog	*/
{
	gchar		*font;

	g_return_if_fail(font_picker != NULL);
	g_return_if_fail(ERTY_IS_FONT_PICKER(font_picker));

	font = erty_entry_get_current(ERTY_ENTRY(font_picker->entry));

	g_return_if_fail(font != NULL);

	gnome_font_picker_set_font_name(font_picker->selector, font);
}

static void erty_font_picker_set_font_dialog_to_entry(
		ErtyFontPicker *font_picker
		)
/*	Callback to set the font specified in the dialog in the entry	*/
{
	GdkFont	*font;

	g_return_if_fail(font_picker != NULL);
	g_return_if_fail(ERTY_IS_FONT_PICKER(font_picker));

	font = gnome_font_picker_get_font(font_picker->selector);
	erty_font_picker_set_font(font_picker, font);
}

static void erty_font_picker_changed(	ErtyFontPicker *font_picker	)
/*	Emit the "changed" signal	*/
{
	g_return_if_fail(font_picker != NULL);
	g_return_if_fail(ERTY_IS_FONT_PICKER(font_picker));

	erty_picker_changed(ERTY_PICKER(font_picker));
}
