#include <erty.h>

static void erty_preferences_init(	ErtyPreferences *preferences	);
static void erty_preferences_class_init(	ErtyPreferencesClass *klass );
static void erty_preferences_changed(	ErtyPreferences *preferences	);
static void erty_preferences_close(	ErtyPreferences *preferences	);

static gchar* erty_preferences_query_local_name(
		ErtyPreferences *preferences,
		GtkWidget *widget
		);

static GtkWidget* erty_preferences_add_from_widget(
		ErtyPreferences *preferences,
		GtkWidget *widget,
		gchar *label
		);

static void erty_preferences_add_page(
		ErtyPreferences *preferences,
		GtkWidget *widget,
		gchar *label
		);

static GnomePropertyBox	*parent_class = NULL;

GtkType erty_preferences_get_type(	void	)
/*	Return type identifier for type ErtyPreferences	*/
{
	static GtkType	preferences_type = 0;

	if (!preferences_type) {

		static const GtkTypeInfo	preferences_info = {
			"ErtyPreferences",
			sizeof(ErtyPreferences),
			sizeof(ErtyPreferencesClass),
			(GtkClassInitFunc)erty_preferences_class_init,
			(GtkObjectInitFunc)erty_preferences_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		preferences_type = gtk_type_unique(
				gnome_property_box_get_type(), 
				&preferences_info
				);
	}

	return preferences_type;
}

GtkWidget* erty_preferences_new(	void	)
/*	Create an instance of ErtyPreferences	*/
{
	ErtyPreferences	*preferences;

	preferences = gtk_type_new(erty_preferences_get_type());

	return GTK_WIDGET(preferences);
}

static void erty_preferences_class_init(	ErtyPreferencesClass *klass )
/*	Initialize the ErtyPreferences class	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkWindowClass		*window_class;
	GnomeDialogClass	*dialog_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	window_class = (GtkWindowClass*)klass;
	dialog_class = (GnomeDialogClass*)klass;

	parent_class = gtk_type_class(GNOME_TYPE_PROPERTY_BOX);
}

static void erty_preferences_init(	ErtyPreferences *preferences	)
/*	Initialize a ErtyPreferences widget	*/
{
	gchar	*tmp;

	g_return_if_fail(preferences != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCES(preferences));
	g_return_if_fail(base_name != NULL);

	preferences->pages = NULL;
	tmp = g_strconcat(base_name, ": ", _("Preferences"), NULL);
	gtk_window_set_title(GTK_WINDOW(preferences), tmp);
	g_free(tmp);
}

static void erty_preferences_add_page(
		ErtyPreferences *preferences,
		GtkWidget *widget,
		gchar *string
		)
/*	Add a new page to preferences	*/
{
	GnomePropertyBox	*property_box;
	ErtyPreferencePage	*preference_page;
	GtkWidget		*button, *label;

	g_return_if_fail(preferences != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCES(preferences));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(widget));
	g_return_if_fail(string != NULL);
	
	property_box = GNOME_PROPERTY_BOX(preferences);
	preference_page = ERTY_PREFERENCE_PAGE(widget);

	label = gtk_label_new(string);

	gtk_signal_connect_object(	GTK_OBJECT(preference_page),
					"changed",
					erty_preferences_changed,
					GTK_OBJECT(preferences)	);

	button = GNOME_PROPERTY_BOX(preferences)->apply_button;

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					erty_preference_page_apply,
					GTK_OBJECT(preference_page)	);

	gnome_property_box_append_page(	property_box, 
					GTK_WIDGET(preference_page), 
					label	);

	button = GNOME_PROPERTY_BOX(preferences)->ok_button;

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					erty_preference_page_save,
					GTK_OBJECT(preference_page)	);

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					erty_preference_page_apply,
					GTK_OBJECT(preference_page)	);

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					erty_preferences_close,
					GTK_OBJECT(preferences)	);

	preferences->pages = g_slist_append(	preferences->pages, 
						preference_page	);
}

static void erty_preferences_close(	ErtyPreferences *preferences	)
/*	Close the dialog if appropriate	*/
{
	GSList		*list;
	gboolean	complete;

	g_return_if_fail(preferences != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCES(preferences));

	complete = TRUE;

	for (list = preferences->pages; list != NULL; list = list->next) {
		ErtyPreferencePage	*preference_page;

		preference_page = ERTY_PREFERENCE_PAGE(list->data);

		if (!erty_preference_page_get_complete(preference_page)) {
			complete = FALSE;

			break;
		}
	}

	/*	Hack: GnomePropertyBox calls gnome_dialog_close() which we want 
		to prevent	*/

	if (!complete) gtk_main();
}

static GtkWidget* erty_preferences_add_from_widget(
		ErtyPreferences *preferences,
		GtkWidget *widget,
		gchar *label
		)
/*	Add a single preference page by querying widget	*/
{
	GtkWidget	*page;
	GtkArg		arg;

	g_return_val_if_fail(preferences != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_PREFERENCES(preferences), NULL);
	g_return_val_if_fail(widget != NULL, NULL);
	g_return_val_if_fail(GTK_WIDGET(widget), NULL);
	g_return_val_if_fail(label != NULL, NULL);

	arg = erty_arg_query(widget, "preferences");

	g_assert(arg.type == GTK_TYPE_WIDGET);

	page = GTK_WIDGET(GTK_VALUE_OBJECT(arg));
	erty_preferences_add_page(preferences, page, label);

	return page;
}

static gchar* erty_preferences_query_local_name(
		ErtyPreferences *preferences,
		GtkWidget *widget
		)
/*	Query the argument "local_name" from 'widget'	*/
{
	GtkArg	arg;

	g_return_val_if_fail(preferences != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_PREFERENCES(preferences), NULL);
	g_return_val_if_fail(widget != NULL, NULL);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), NULL);

	arg = erty_arg_query(widget, "local_name");

	g_assert(arg.type == GTK_TYPE_STRING);

	return GTK_VALUE_STRING(arg);
}

static void erty_preferences_add_from_widget_with_data(
		ErtyPreferences *preferences,
		GtkWidget *widget,
		gchar *label,
		gpointer data
		)
/*	Add a new preference page and set its data field	*/
{
	GtkWidget		*page;
	ErtyPreferencePage	*preference_page;

	g_return_if_fail(preferences != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCES(preferences));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_WIDGET(widget));
	g_return_if_fail(label != NULL);

	page = erty_preferences_add_from_widget(preferences, widget, label);
	preference_page = ERTY_PREFERENCE_PAGE(page);
	erty_preference_page_set_data(preference_page, data);
}

void erty_preferences_add_from_widgets(
		ErtyPreferences *preferences,
		GtkWidget *widget,
		gpointer data,
		...
		)
/*	Add preferences pages from widget(s)	*/
{
	va_list		args;
	GtkWidget	*current_widget;
	gpointer	current_data;

	g_return_if_fail(preferences != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCES(preferences));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));

	current_widget = widget;
	current_data = data;
	va_start(args, data);

	while (current_widget != NULL) {
		gchar 	*label;

		label = erty_preferences_query_local_name(
				preferences,
				current_widget
				);

		g_assert(label != NULL);

		erty_preferences_add_from_widget_with_data(
				preferences, 
				current_widget,
				label,
				current_data
				);

		current_widget = va_arg(args, GtkWidget*);

		if (current_widget != NULL)
			current_data = va_arg(args, gpointer);
		else
			current_data = NULL;
	}

	va_end(args);
}

static void erty_preferences_changed(	ErtyPreferences *preferences	)
/*	Callback to set the sensitivity of the 'apply' button	*/
{
	GtkWidget	*button;

	g_return_if_fail(preferences != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCES(preferences));

	button = GNOME_PROPERTY_BOX(preferences)->apply_button;
	gtk_widget_set_sensitive(button, TRUE);
}

void erty_preferences_show_page(	ErtyPreferences *preferences,
					gpointer data	)
/*	Look up the page containing data and show that one on top of the
	notebook	*/
{
	gint	index;
	GSList	*slist;

	g_return_if_fail(preferences != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCES(preferences));
	g_return_if_fail(data != NULL);

	for (	slist = preferences->pages, index = 0; 
		slist != NULL; 
		slist = slist->next, index++	) {

		ErtyPreferencePage	*preference_page;
		gpointer		page_data;

		preference_page = ERTY_PREFERENCE_PAGE(slist->data);
		page_data = erty_preference_page_get_data(preference_page);

		if (page_data == data) {
			GnomePropertyBox	*property_box;
			GtkNotebook		*notebook;

			property_box = GNOME_PROPERTY_BOX(preferences);
			notebook = GTK_NOTEBOOK(property_box->notebook);

			gtk_notebook_set_page(notebook, index);
			break;
		}
	}
}
