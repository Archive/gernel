#include <erty.h>
#include <erty_check_button.h>

static void erty_check_button_init(	ErtyCheckButton *check_button	);
static void erty_check_button_class_init(	ErtyCheckButtonClass *klass );

static void erty_check_button_get_arg(GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void erty_check_button_set_arg(GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void erty_check_button_construct(
		ErtyCheckButton *check_button
		);

static void erty_check_button_changed(
		ErtyCheckButton *check_button
		);

static GtkCheckButtonClass	*parent_class = NULL;

enum {	CHANGED,
	ACTIVE,
	INACTIVE,
	LAST_SIGNAL	};

enum {	ARG_0,
	ARG_DISPLAY_VALUE	};

static guint check_button_signals[LAST_SIGNAL] = { 0 };

GtkType erty_check_button_get_type(	void	)
/*	Return type identifier for type ErtyCheckButton	*/
{
	static GtkType	check_button_type = 0;

	if (!check_button_type) {

		static const GtkTypeInfo	check_button_info = {
			"ErtyCheckButton",
			sizeof(ErtyCheckButton),
			sizeof(ErtyCheckButtonClass),
			(GtkClassInitFunc)erty_check_button_class_init,
			(GtkObjectInitFunc)erty_check_button_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		check_button_type = gtk_type_unique(
				gtk_check_button_get_type(),
				&check_button_info
				);
	}

	return check_button_type;
}

GtkWidget* erty_check_button_new(	void	)
/*	Create an instance of ErtyCheckButton	*/
{
	ErtyCheckButton	*check_button;

	check_button = gtk_type_new(erty_check_button_get_type());
	erty_check_button_construct(check_button);

	return GTK_WIDGET(check_button);
}

GtkWidget* erty_check_button_new_with_label(	gchar *label	)
{
	GtkWidget	*check_button, *label_widget;

	g_return_val_if_fail(label != NULL, NULL);

	check_button = erty_check_button_new();
	label_widget = gtk_label_new(label);
	gtk_misc_set_alignment(GTK_MISC(label_widget), 0, 0.5);
	gtk_container_add(GTK_CONTAINER(check_button), label_widget);
	gtk_widget_show(label_widget);

	return check_button;
}

static void erty_check_button_init(	ErtyCheckButton *check_button	)
/*	Initialize the ErtyCheckButton	*/
{
}

static void erty_check_button_class_init(	ErtyCheckButtonClass *klass )
/*	Initialize the ErtyCheckButtonClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkButtonClass		*button_class;
	GtkToggleButtonClass	*toggle_button_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	button_class = (GtkButtonClass*)klass;
	toggle_button_class = (GtkToggleButtonClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_CHECK_BUTTON);

	gtk_object_add_arg_type(	"ErtyCheckButton::display_value",
					GTK_TYPE_BOOL,
					GTK_ARG_READWRITE,
					ARG_DISPLAY_VALUE	);

	object_class->get_arg = erty_check_button_get_arg;
	object_class->set_arg = erty_check_button_set_arg;

	function_offset = GTK_SIGNAL_OFFSET(ErtyCheckButtonClass, changed);

	check_button_signals[CHANGED] = 

		gtk_signal_new(	"changed",
				GTK_RUN_FIRST,
				object_class->type,
				function_offset,
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE,
				0	);

	function_offset = GTK_SIGNAL_OFFSET(ErtyCheckButtonClass, active);

	check_button_signals[ACTIVE] =

		gtk_signal_new(	"active",
				GTK_RUN_FIRST,
				object_class->type,
				function_offset,
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE,
				0	);

	function_offset = GTK_SIGNAL_OFFSET(ErtyCheckButtonClass, inactive);

	check_button_signals[INACTIVE] =

		gtk_signal_new(	"inactive",
				GTK_RUN_FIRST,
				object_class->type,
				function_offset,
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE,
				0	);

	gtk_object_class_add_signals(	object_class,
					check_button_signals,
					LAST_SIGNAL	);

	klass->changed = NULL;
	klass->active = NULL;
	klass->inactive = NULL;
}

static void erty_check_button_construct(	ErtyCheckButton *check_button)
/*	Setup the ErtyCheckButton	*/
{
	g_return_if_fail(check_button != NULL);
	g_return_if_fail(ERTY_IS_CHECK_BUTTON(check_button));

	gtk_signal_connect_object(	GTK_OBJECT(check_button),
					"toggled",
					erty_check_button_changed,
					GTK_OBJECT(check_button)	);
}

static void erty_check_button_changed(	ErtyCheckButton *check_button)
/*	Emit the "changed" signal; check for other signals to be emitted */
{
	g_return_if_fail(check_button != NULL);
	g_return_if_fail(ERTY_IS_CHECK_BUTTON(check_button));

	gtk_signal_emit(	GTK_OBJECT(check_button),
				check_button_signals[CHANGED]	);

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_button)))

		gtk_signal_emit(	GTK_OBJECT(check_button),
					check_button_signals[ACTIVE]	);

	else

		gtk_signal_emit(	GTK_OBJECT(check_button),
					check_button_signals[INACTIVE]	);
}

gboolean erty_check_button_get_active(	ErtyCheckButton *check_button)
/*	Check if button is active	*/
{
	g_return_val_if_fail(check_button != NULL, FALSE);
	g_return_val_if_fail(ERTY_IS_CHECK_BUTTON(check_button), FALSE);

	return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_button));
}

void erty_check_button_set_active(	ErtyCheckButton *check_button,
					gboolean flag	)
/*	Set activity of check_button to flag	*/
{
	g_return_if_fail(check_button != NULL);
	g_return_if_fail(ERTY_IS_CHECK_BUTTON(check_button));

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_button), flag);
}

static void erty_check_button_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument setting for this object	*/
{
	ErtyCheckButton	*check_button;

	g_return_if_fail(object != NULL);
	g_return_if_fail(ERTY_IS_CHECK_BUTTON(object));

	check_button = ERTY_CHECK_BUTTON(object);

	switch (arg_id) {
		case ARG_DISPLAY_VALUE:

			erty_check_button_set_active(	check_button, 
							GTK_VALUE_BOOL(*arg) );

			break;
		default:
			break;
	}
}

static void erty_check_button_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument getting for this object	*/
{
	ErtyCheckButton	*check_button;

	g_return_if_fail(object != NULL);
	g_return_if_fail(ERTY_IS_CHECK_BUTTON(object));

	check_button = ERTY_CHECK_BUTTON(object);

	switch (arg_id) {
		gboolean	value;

		case ARG_DISPLAY_VALUE:
			value = erty_check_button_get_active(check_button);
			GTK_VALUE_BOOL(*arg) = value;
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}
