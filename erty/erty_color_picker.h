#include "erty_picker.h"

#define ERTY_TYPE_COLOR_PICKER	(erty_color_picker_get_type())

#define ERTY_COLOR_PICKER(obj) \
	(GTK_CHECK_CAST((obj), ERTY_TYPE_COLOR_PICKER, ErtyColorPicker))

#define ERTY_COLOR_PICKER_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				ERTY_TYPE_COLOR_PICKER, \
				ErtyColorPickerClass	))

#define ERTY_IS_COLOR_PICKER(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_COLOR_PICKER))

#define ERTY_IS_COLOR_PICKER_CLASS(klass), \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_COLOR_PICKER))

typedef struct _ErtyColorPicker	ErtyColorPicker;
typedef struct _ErtyColorPickerClass	ErtyColorPickerClass;

struct _ErtyColorPicker {
	ErtyPicker		picker;

	GnomeColorPicker	*selector;
};

struct _ErtyColorPickerClass {
	ErtyPickerClass	parent_class;
};

GtkType		erty_color_picker_get_type(	void	);

GtkWidget*	erty_color_picker_new(	void	);
GtkWidget*	erty_color_picker_new_with_label(	gchar *label	);
