#include <erty.h>
#include <math.h>

static void erty_config_get_default(	GtkArg *arg	);

void erty_init(	gchar *name	)
/*	Initialize Liberty with the base name for config paths	*/
{
	g_return_if_fail(name != NULL);

	base_name = name;
}

GtkArg erty_arg_query(	GtkWidget *widget,
			gchar *argument	)
/*	Query 'widget' for 'argument'	*/
{
	GtkType	type;
	GtkArg	error;

	g_return_val_if_fail(widget != NULL, error);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), error);
	g_return_val_if_fail(argument != NULL, error);

	for (	type = GTK_OBJECT_TYPE(widget);
		type != GTK_TYPE_INVALID;
		type = gtk_type_parent(type)	) {

		GtkArg	*args;
		guint32	*flags;
		guint	n_args;

		args = gtk_object_query_args(type, &flags, &n_args);

		if (n_args > 0) {
			gchar	**strv, *name;
			gint	i;

			/*	Assemble the name of the type of the current 
				hierarchy	*/
			strv = g_strsplit(args[0].name, ":", 1);
			name = g_strconcat(strv[0], "::", argument, NULL);
			g_strfreev(strv);

			for (i = 0; i < n_args; i++)

				if (strcmp(args[i].name, name) == 0) {
					GtkArg my_args[1];

					my_args[0].name = name;

					gtk_object_getv(GTK_OBJECT(widget), 
							1, 
							my_args	);

					return my_args[0];
				}

			g_free(name);
		}

		g_free(args);
		g_free(flags);
	}

	return error;
}

gchar* erty_color_32_to_string(	guint32 color	)
/*	Convert color to string	*/
{
	GString	*string;
	gchar	*str;

	string = g_string_new(NULL);
	g_string_sprintf(string, "%08x", color);
	str = string->str;
	g_string_free(string, FALSE);

	return str;
}

guint32 erty_color_string_to_32(	gchar *color	)
/*	Convert a string defining a color (e.g. "ffff00aa") to the 
	corresponding guint32 word	*/
{
	gint	value;
	gint	i, digits = 8;

	g_return_val_if_fail(color != NULL, 0);

	value = 0;

	for (i = 0; i < digits; i++) {
		gint	tmp;

		tmp = 0;

		if ((color[i] >= '0') & (color[i] <= '9'))
			tmp = color[i] - 48;
		else if ((color[i] >= 'a') & (color[i] <= 'f'))
			tmp = color[i] - 87;
		else
			g_assert_not_reached();

		value += tmp * (gint)pow(16, (digits - i - 1));
	}

	return value;
}

GtkWidget* erty_widget_new(	GtkType type,
				gchar *name,
				gchar *argument,
				...	)
/*	Call gtk_widget_new(), interpret values of arguments as 'factory
	defaults'; only use them if they have not been set in the GNOME
	config system previously.
	'name' is a descriptive label that will appear in the property dialog;
	it should describe the current instantiation of type rather than the
	class - so don't confuse it with the name returned by
	gtk_widget_get_name() etc	*/
{
	GtkObject	*object;
	va_list		var_args;
	GSList		*arg_list, *info_list, *args, *infos;
	gchar		*tmp;

	g_return_val_if_fail(name != NULL, NULL);

	tmp = g_strconcat("/", base_name, "/", name, "/", NULL);
	gnome_config_push_prefix(tmp);
	g_free(tmp);
	object = gtk_type_new(type);
	arg_list = info_list = NULL;
	va_start(var_args, argument);

	gtk_object_args_collect(	GTK_OBJECT_TYPE(object),
					&arg_list,
					&info_list,
					argument,
					var_args	);

	va_end(var_args);

	for (	args = arg_list, infos = info_list; 
		args != NULL;
		args = args->next, infos = infos->next	) {

		erty_config_get_default((GtkArg*)args->data);
		gtk_object_arg_set(object, args->data, infos->data);
	}

	gtk_args_collect_cleanup(arg_list, info_list);

	if (!GTK_OBJECT_CONSTRUCTED(object))
		gtk_object_default_construct(object);

	gnome_config_pop_prefix();
	gtk_object_set(object, "local_name", name, NULL);

	return GTK_WIDGET(object);
}

void erty_widget_init(	GtkWidget *widget,
			gchar *name,
			gchar *argument,
			...	)
/*	Initialize widget with a set of name/argument pairs; this function
	should be used for virtual classes that have no way of invoking 
	erty_widget_new()	*/
{
	GtkObject	*object;
	va_list		var_args;
	GSList		*arg_list, *info_list, *args, *infos;
	gchar		*tmp;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(name != NULL);
	g_return_if_fail(argument != NULL);

	tmp = g_strconcat("/", base_name, "/", name, "/", NULL);
	gnome_config_push_prefix(tmp);
	g_free(tmp);
	object = GTK_OBJECT(widget);
	arg_list = info_list = NULL;
	va_start(var_args, argument);

	gtk_object_args_collect(	GTK_OBJECT_TYPE(object),
					&arg_list,
					&info_list,
					argument,
					var_args	);

	va_end(var_args);

	for (	args = arg_list, infos = info_list; 
		args != NULL;
		args = args->next, infos = infos->next	) {

		erty_config_get_default((GtkArg*)args->data);
		gtk_object_arg_set(object, args->data, infos->data);
	}

	gtk_args_collect_cleanup(arg_list, info_list);

	if (!GTK_OBJECT_CONSTRUCTED(object))
		gtk_object_default_construct(object);

	gnome_config_pop_prefix();
	gtk_object_set(object, "local_name", name, NULL);
}

static void erty_config_get_default(	GtkArg *arg	)
/*	Check if a value for arg has been set with gnome_config before;
	if so, load it into arg, otherwise use the default already set in
	arg	*/
{
	GString	*key;
	gchar	*format, *default_format;

	g_return_if_fail(arg != NULL);
	g_return_if_fail(base_name != NULL);

	/*	These can't be saved in gnome_config anyway	*/
	g_return_if_fail(arg->type != GTK_TYPE_INVALID);
	g_return_if_fail(arg->type != GTK_TYPE_NONE);
	g_return_if_fail(arg->type != GTK_TYPE_BOXED);
	g_return_if_fail(arg->type != GTK_TYPE_POINTER);
	g_return_if_fail(arg->type != GTK_TYPE_SIGNAL);
	g_return_if_fail(arg->type != GTK_TYPE_ARGS);
	g_return_if_fail(arg->type != GTK_TYPE_CALLBACK);
	g_return_if_fail(arg->type != GTK_TYPE_C_CALLBACK);
	g_return_if_fail(arg->type != GTK_TYPE_FOREIGN);
	g_return_if_fail(arg->type != GTK_TYPE_OBJECT);

	default_format = NULL;

	switch (arg->type) {
		case GTK_TYPE_CHAR:
		case GTK_TYPE_BOOL:
		case GTK_TYPE_INT:
		case GTK_TYPE_ENUM:
		case GTK_TYPE_FLAGS:
			default_format = g_strdup("%d");
			break;
		case GTK_TYPE_UCHAR:
		case GTK_TYPE_UINT:
			default_format = g_strdup("%u");
			break;
		case GTK_TYPE_LONG:
			default_format = g_strdup("%ld");
			break;
		case GTK_TYPE_ULONG:
			default_format = g_strdup("%lu");
			break;
		case GTK_TYPE_FLOAT:
			default_format = g_strdup("%f");
			break;
		case GTK_TYPE_DOUBLE:
			default_format = g_strdup("%lf");
			break;
		case GTK_TYPE_STRING:
			default_format = g_strdup("%s");
			break;
		default:
			g_assert_not_reached();
	}

	format = g_strconcat(arg->name, "=", default_format, NULL);
	key = g_string_new(NULL);

	switch (arg->type) {
		gchar	*str;

		case GTK_TYPE_CHAR:
			g_string_sprintf(key, format, GTK_VALUE_CHAR(*arg));
			str = key->str;
			GTK_VALUE_CHAR(*arg) = gnome_config_get_int(str);
			break;
		case GTK_TYPE_UCHAR:
			g_string_sprintf(key, format, GTK_VALUE_UCHAR(*arg));
			str = key->str;
			GTK_VALUE_UCHAR(*arg) = gnome_config_get_int(str);
			break;
		case GTK_TYPE_BOOL:
			g_string_sprintf(key, format, GTK_VALUE_BOOL(*arg));
g_print("erty_config_get_default:	key == %s\n", key->str);
			str = key->str;
			GTK_VALUE_BOOL(*arg) = gnome_config_get_bool(str);
g_print("erty_config_get_default:	value == %d\n", gnome_config_get_bool(str));
			break;
		case GTK_TYPE_INT:
			g_string_sprintf(key, format, GTK_VALUE_INT(*arg));
			str = key->str;
			GTK_VALUE_INT(*arg) = gnome_config_get_int(str);
			break;
		case GTK_TYPE_UINT:
			g_string_sprintf(key, format, GTK_VALUE_UINT(*arg));
			str = key->str;
			GTK_VALUE_UINT(*arg) = gnome_config_get_int(str);
			break;
		case GTK_TYPE_LONG:
			g_string_sprintf(key, format, GTK_VALUE_LONG(*arg));
			str = key->str;
			GTK_VALUE_LONG(*arg) = gnome_config_get_int(str);
			break;
		case GTK_TYPE_ULONG:
			g_string_sprintf(key, format, GTK_VALUE_ULONG(*arg));
			str = key->str;
			GTK_VALUE_ULONG(*arg) = gnome_config_get_int(str);
			break;
		case GTK_TYPE_FLOAT:
			g_string_sprintf(key, format, GTK_VALUE_FLOAT(*arg));
			str = key->str;
			GTK_VALUE_FLOAT(*arg) = gnome_config_get_float(str);
			break;
		case GTK_TYPE_DOUBLE:
			g_string_sprintf(key, format,GTK_VALUE_DOUBLE(*arg));
			str = key->str;
			GTK_VALUE_DOUBLE(*arg) = gnome_config_get_float(str);
			break;
		case GTK_TYPE_STRING:
			g_string_sprintf(key, format,GTK_VALUE_STRING(*arg));
			str = key->str;
			GTK_VALUE_STRING(*arg) =gnome_config_get_string(str);
			break;
		case GTK_TYPE_ENUM:
			g_string_sprintf(key, format, GTK_VALUE_ENUM(*arg));
			str = key->str;
			GTK_VALUE_ENUM(*arg) = gnome_config_get_int(str);
			break;
		case GTK_TYPE_FLAGS:
			g_string_sprintf(key, format, GTK_VALUE_FLAGS(*arg));
			str = key->str;
			GTK_VALUE_FLAGS(*arg) = gnome_config_get_int(str);
			break;
		default:
			g_assert_not_reached();
	}

	g_string_free(key, FALSE);
}

void erty_arg_add_multiple(	gchar *widget_name,
				gchar *name,
				GtkType type,
				guint arg,
				...	)
/*	Add multiple arguments to GernelCanvas	*/
{
	va_list	args;
	gchar	*current_name;
	GtkType	current_type;
	guint	current_arg;

	g_return_if_fail(widget_name != NULL);
	g_return_if_fail(name != NULL);
	g_return_if_fail(type != GTK_TYPE_NONE);
	g_return_if_fail(type != GTK_TYPE_POINTER);
	g_return_if_fail(arg != 0);

	current_name = name;
	current_type = type;
	current_arg = arg;
	va_start(args, arg);

	while (current_name != NULL) {
		gchar	*str;

		str = g_strconcat(widget_name, "::", current_name, NULL);
		erty_arg_add(str, current_type, current_arg);

		if ((current_name = va_arg(args, gchar*)) != NULL) {
			current_type = va_arg(args, GtkType);
			current_arg = va_arg(args, guint);
		}
	}

	va_end(args);
}

void erty_arg_add(	gchar *name,
			GtkType type,
			guint argument	)
/*	Wrapper for gtk_object_add_arg_type()	*/
{
	g_return_if_fail(name != NULL);
	g_return_if_fail(type != GTK_TYPE_NONE);
	g_return_if_fail(type != GTK_TYPE_POINTER);
	g_return_if_fail(argument != 0);

	gtk_object_add_arg_type(	name, 
					type, 
					GTK_ARG_READWRITE | GTK_ARG_CONSTRUCT,
					argument	);
}

void erty_save(	GtkWidget *widget,
		gchar *name	)
/*	Wrapper around erty_save()	*/
{
	GtkArg	arg;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(name != NULL);

	arg = erty_arg_query(widget, name);
	erty_save_by_arg(widget, name, &arg);
}

void erty_save_by_arg(	GtkWidget *widget,
			gchar *name,
			GtkArg *arg	)
/*	Wrapper to gnome_config_set*(); not to be used outside of Liberty */
{
	gchar	*tmp, *local_name;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(name != NULL);
	g_return_if_fail(arg != NULL);
	g_return_if_fail(arg->type != GTK_TYPE_NONE);
	g_return_if_fail(arg->type != GTK_TYPE_INVALID);
	g_return_if_fail(arg->type != GTK_TYPE_BOXED);
	g_return_if_fail(arg->type != GTK_TYPE_SIGNAL);
	g_return_if_fail(arg->type != GTK_TYPE_ARGS);
	g_return_if_fail(arg->type != GTK_TYPE_CALLBACK);
	g_return_if_fail(arg->type != GTK_TYPE_C_CALLBACK);
	g_return_if_fail(arg->type != GTK_TYPE_FOREIGN);
	g_return_if_fail(arg->type != GTK_TYPE_OBJECT);
	g_return_if_fail(base_name != NULL);

	local_name = GTK_VALUE_STRING(erty_arg_query(widget, "local_name" ));
	tmp = g_strconcat("/", base_name, "/", local_name, "/", NULL);
	gnome_config_push_prefix(tmp);

	switch (arg->type) {
		gchar	*str;

		case GTK_TYPE_BOOL:
			gnome_config_set_bool(name, GTK_VALUE_BOOL(*arg));
			break;
		case GTK_TYPE_CHAR:
		case GTK_TYPE_INT:
		case GTK_TYPE_ENUM:
		case GTK_TYPE_FLAGS:
		case GTK_TYPE_UCHAR:
		case GTK_TYPE_UINT:
		case GTK_TYPE_LONG:
		case GTK_TYPE_ULONG:
			gnome_config_set_int(name, GTK_VALUE_INT(*arg));
			break;
		case GTK_TYPE_FLOAT:
		case GTK_TYPE_DOUBLE:
			gnome_config_set_float(name, GTK_VALUE_FLOAT(*arg));
			break;
		case GTK_TYPE_STRING:
		case GTK_TYPE_POINTER:
			str = GTK_VALUE_STRING(*arg);
			gnome_config_set_string(name, str);
			break;
		default:
			g_assert_not_reached();
	}

	gnome_config_pop_prefix();
	gnome_config_sync();
}

gchar* erty_get_argument_from_id(	GtkWidget *widget,
					guint arg_id	)
/*	Get the name of argument specified by arg_id, for widget	*/
{
	GtkType		type;
	GString		*string;

	g_return_val_if_fail(widget != NULL, NULL);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), NULL);

	for (	type = GTK_OBJECT_TYPE(widget);
		type != GTK_TYPE_INVALID;
		type = gtk_type_parent(type)	) {

		GtkObjectClass	*class;
		GSList		*slist;

		class = gtk_type_class(type);

		for (	slist = class->construct_args; 
			slist != NULL; 
			slist = slist->next	) {

			GtkArgInfo	*arg_info;

			arg_info = (GtkArgInfo*)(slist->data);
			if (arg_info->arg_id == arg_id) return arg_info->name;
		}
	}

	string = g_string_new(NULL);

	g_string_sprintf(
		string,
		_("Argument %d was not found in the %s class ancestry.\n"),
		arg_id,
		gtk_widget_get_name(widget)
		);

	g_error(string->str);

	return NULL;
}
