#define ERTY_TYPE_ENTRY	(erty_entry_get_type())

#define ERTY_ENTRY(obj) \
	(GTK_CHECK_CAST((obj), ERTY_TYPE_ENTRY, ErtyEntry))

#define ERTY_ENTRY_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass, ERTY_TYPE_ENTRY, ErtyEntryClass))

#define ERTY_IS_ENTRY(obj)	(GTK_CHECK_TYPE((obj), ERTY_TYPE_ENTRY))

#define ERTY_IS_ENTRY_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_ENTRY))

typedef struct _ErtyEntry		ErtyEntry;
typedef struct _ErtyEntryClass	ErtyEntryClass;

struct _ErtyEntry {
	GtkHBox	hbox;

	GtkWidget	*combo, *remove, *clear;
	gchar		*path;
	GList		*history;
};

struct _ErtyEntryClass {
	GtkHBoxClass	parent_class;

	void (*changed)	(ErtyEntry *entry);
};

GtkType		erty_entry_get_type(	void	);
GtkWidget*	erty_entry_new(	gchar *history_id	);

GtkWidget*	erty_entry_new_with_label(	gchar *history_id,
						gchar *label	);

void		erty_entry_save_config(	ErtyEntry *entry	);
gchar*		erty_entry_get_current(	ErtyEntry *entry	);

void		erty_entry_set_current(	ErtyEntry *entry,
					gchar *string	);

void		erty_entry_hide_trashcan(	ErtyEntry *entry	);
void		erty_entry_hide_pointer(	ErtyEntry *entry	);
void		erty_entry_hide_brush(	ErtyEntry *entry	);

void		erty_entry_set_visibility(	ErtyEntry *entry,
						gboolean flag	);
