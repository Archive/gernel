#define	ERTY_TYPE_CHECK_BUTTON	(erty_check_button_get_type())

#define ERTY_CHECK_BUTTON(obj) \
	(GTK_CHECK_CAST(	(obj), \
				ERTY_TYPE_CHECK_BUTTON, \
				ErtyCheckButton	))

#define	ERTY_CHECK_BUTTON_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				ERTY_TYPE_CHECK_BUTTON, \
				ErtyCheckButtonClass	))

#define	ERTY_IS_CHECK_BUTTON(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_CHECK_BUTTON))

#define	ERTY_IS_CHECK_BUTTON_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_CHECK_BUTTON))

typedef struct _ErtyCheckButton	ErtyCheckButton;
typedef struct _ErtyCheckButtonClass	ErtyCheckButtonClass;

struct _ErtyCheckButton {
	GtkCheckButton	check_button;
};

struct _ErtyCheckButtonClass {
	GtkCheckButtonClass	parent_class;

	void (*changed)		(ErtyCheckButton *check_button);
	void (*active)		(ErtyCheckButton *check_button);
	void (*inactive)	(ErtyCheckButton *check_button);
};

GtkType		erty_check_button_get_type(	void	);
GtkWidget*	erty_check_button_new(	void	);
GtkWidget*	erty_check_button_new_with_label(	gchar *label	);

gboolean	erty_check_button_get_active(
		ErtyCheckButton *check_button
		);

void		erty_check_button_set_active(	ErtyCheckButton *check_button,
						gboolean flag	);
