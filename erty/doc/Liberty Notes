Notes on how to teach an application to remember its properties using Liberty.

-Add an autoconf check for Liberty to configure.in:

AC_CHECK_LIB(
        erty, 
        erty_preferences_new,,
        AC_MSG_ERROR(Liberty was not found. Please install module gernel from GNOME CVS.), 
        -lgtk -lgnomeui -lgnome -lart_lgpl -lpopt
)

-Initialize Liberty in main(). PROGRAM is supposed to be a descriptive name of the application; it will be used as part of the title for the property dialog window.

erty_init(_(PROGRAM));

The parameter will also serve as the 'top level' path string for gnome_config_* functions; this means that properties won't be recalled when the user changes the locale he lives hin. If you consider this a bug, fix it.

-At some point in the code where a property dialog is to be fired up, set up the main Liberty window and pass all widgets to it that have user-configurable arguments. An example from the Gerk:

	preferences = erty_preferences_new();

	erty_preferences_add_from_widgets(	ERTY_PREFERENCES(preferences),
						GTK_WIDGET(applet), NULL,
						NULL	);

	gtk_widget_show(preferences);

We have only one widget here (applet).

-The widgets we pass to erty_preferences_add_from_widgets() have to have the following properties:

	a) they should make use of the GTK arg system. Everything configurable should be get'able as an argument. 
	b) in <widget-name>_new(), use erty_widget_new() to create the new object. It is a convenience function that checks for previously saved argument values and it initializes the objects with those. In case that nothing has been saved earlier the defaults as specified are used. Example from the Gerk:

        widget = erty_widget_new(       gerk_applet_get_type(),
                                        local_name,
                                        "show_buttons", TRUE,
                                        "ignore_middle", TRUE,
                                        "init_on_exit", TRUE,
                                        "wrap_around", FALSE,
                                        "permanent_check", TRUE,
                                        "emulate_mouse", FALSE,
                                        "key_button1", "8,Alt_L+semicolon",
                                        "key_button2", "8,Alt_L+apostrophe",
                                        "key_button3", "8,Alt_L+backslash",
                                        NULL    );

local_name will serve Liberty as part of the path under which to store values in the Gnome config system.
Of course, all of the arguments have to be GTK args of that object. ARG_LOCAL_NAME always has to be there. ARG_PREFERENCES should be implemented if the arguments should be customizable via the preferences dialog. The code for adding these two arguments looks like this:

        gtk_object_add_arg_type("GerkApplet::preferences",
                                GTK_TYPE_WIDGET,
                                GTK_ARG_READABLE,
                                ARG_PREFERENCES );

        gtk_object_add_arg_type("GerkApplet::local_name",
                                GTK_TYPE_STRING,
                                GTK_ARG_READWRITE,
                                ARG_LOCAL_NAME  );

For the actual, configurable arguments, Liberty provide the convenience functions erty_arg_add() and erty_arg_add_multiple(). Example:

        erty_arg_add(   "Gripwire::show_configured", 
                        GTK_TYPE_BOOL, 
                        ARG_SHOW_CONFIGURED     );

In <widget-name>_get_args, you will have to provide Liberty a function to call when trying to display that object's properties:

                case ARG_PREFERENCES:
                        widget = gerk_applet_preferences(applet);
                        GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(widget);
                        break;

In <widget-name)_preferences(), you can assemble a page to be used by Liberty for the property box dialog. Look into the Liberty reference (FIXME: not written yet) for the function names to use here. A simple example looks like this:

static GtkWidget* gripwire_databases_preferences(
                GripwireDatabases *databases
                )
/*      Assemble a page to be used in a GripwireDatabases' property box */
{
        GtkWidget       *page;

        g_return_val_if_fail(databases != NULL, NULL);
        g_return_val_if_fail(GRIPWIRE_IS_DATABASES(databases), NULL);

        page = erty_preference_page_new();
        section = erty_preference_section_new(_("Appearance"));

        erty_preference_section_add(
                        ERTY_PREFERENCE_SECTION(section),
                        GTK_WIDGET(databases),
                        ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
                        _("Show column indicating if database is configured"),
                        ARG_SHOW_CONFIGURED
                        );

        gtk_container_add(GTK_CONTAINER(page), section);

        return page;
}


