#include <erty.h>
#include <erty_radio_button.h>

static void erty_radio_button_init(	ErtyRadioButton *radio_button	);

static void erty_radio_button_class_init(
		ErtyRadioButtonClass *klass
		);

static void erty_radio_button_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void erty_radio_button_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void erty_radio_button_construct(
		ErtyRadioButton *radio_button,
		gchar *label
		);

static void erty_radio_button_changed(
		ErtyRadioButton *radio_button
		);

static GtkRadioButtonClass	*parent_class = NULL;

enum {	CHANGED,
	LAST_SIGNAL	};

enum {	ARG_0,
	ARG_DISPLAY_VALUE	};

static guint radio_button_signals[LAST_SIGNAL] = { 0 };

GtkType erty_radio_button_get_type(	void	)
/*	Return type identifier for type ErtyRadioButton	*/
{
	static GtkType	radio_button_type = 0;

	if (!radio_button_type) {

		static const GtkTypeInfo	radio_button_info = {
			"ErtyRadioButton",
			sizeof(ErtyRadioButton),
			sizeof(ErtyRadioButtonClass),
			(GtkClassInitFunc)erty_radio_button_class_init,
			(GtkObjectInitFunc)erty_radio_button_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		radio_button_type = gtk_type_unique(
				gtk_radio_button_get_type(),
				&radio_button_info
				);
	}

	return radio_button_type;
}

GtkWidget* erty_radio_button_new(	gchar *label	)
/*	Create an instance of ErtyRadioButton	*/
{
	ErtyRadioButton	*radio_button;

	g_return_val_if_fail(label != NULL, NULL);

	radio_button = gtk_type_new(erty_radio_button_get_type());
	erty_radio_button_construct(radio_button, label);

	return GTK_WIDGET(radio_button);
}

GtkWidget* erty_radio_button_new_with_dependency(	gchar *label,
							GtkWidget *button )
/*	Create a new radio button that belongs to the same group as button */
{
	GtkWidget		*radio_button;

	g_return_val_if_fail(label != NULL, NULL);
	g_return_val_if_fail(button != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_RADIO_BUTTON(button), NULL);

	radio_button = erty_radio_button_new(label);

	gtk_radio_button_set_group(	GTK_RADIO_BUTTON(radio_button),
					ERTY_RADIO_BUTTON(button)->group );

	return radio_button;
}

static void erty_radio_button_init(	ErtyRadioButton *radio_button	)
/*	Initialize the ErtyRadioButton	*/
{
	radio_button->group = NULL;
}

static void erty_radio_button_class_init(
		ErtyRadioButtonClass *klass
		)
/*	Initialize the ErtyRadioButtonClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;
	GtkButtonClass		*button_class;
	GtkToggleButtonClass	*toggle_button_class;
	GtkCheckButtonClass	*check_button_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;
	button_class = (GtkButtonClass*)klass;
	toggle_button_class = (GtkToggleButtonClass*)klass;
	check_button_class = (GtkCheckButtonClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_CHECK_BUTTON);

	gtk_object_add_arg_type(	"ErtyRadioButton::display_value",
					GTK_TYPE_BOOL,
					GTK_ARG_READWRITE,
					ARG_DISPLAY_VALUE	);

	object_class->get_arg = erty_radio_button_get_arg;
	object_class->set_arg = erty_radio_button_set_arg;

	function_offset = GTK_SIGNAL_OFFSET(ErtyRadioButtonClass, changed);

	radio_button_signals[CHANGED] =

			gtk_signal_new(	"changed",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class,
					radio_button_signals,
					LAST_SIGNAL	);

	klass->changed = NULL;
}

static void erty_radio_button_construct(
		ErtyRadioButton *radio_button,
		gchar *label
		)
/*	Construct the radio button	*/
{
	GtkWidget	*label_widget;

	g_return_if_fail(radio_button != NULL);
	g_return_if_fail(ERTY_IS_RADIO_BUTTON(radio_button));
	g_return_if_fail(label != NULL);

	label_widget = gtk_label_new(label);
	gtk_misc_set_alignment(GTK_MISC(label_widget), 0.0, 0.5);
	gtk_container_add(GTK_CONTAINER(radio_button), label_widget);
	gtk_widget_show(label_widget);

	radio_button->group =
		gtk_radio_button_group(GTK_RADIO_BUTTON(radio_button));

	gtk_signal_connect(	GTK_OBJECT(radio_button),
				"clicked",
				erty_radio_button_changed,
				NULL	);
}

static void erty_radio_button_changed(
		ErtyRadioButton *radio_button
		)
/*	Emit the 'changed' signal	*/
{
	g_return_if_fail(radio_button != NULL);
	g_return_if_fail(ERTY_IS_RADIO_BUTTON(radio_button));

	gtk_signal_emit(	GTK_OBJECT(radio_button), 
				radio_button_signals[CHANGED]	);
}

static void erty_radio_button_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument setting for this object	*/
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(ERTY_IS_RADIO_BUTTON(object));

	switch (arg_id) {
		GtkToggleButton	*toggle_button;

		case ARG_DISPLAY_VALUE:
			toggle_button = GTK_TOGGLE_BUTTON(object);

			gtk_toggle_button_set_active(
					toggle_button,
					GTK_VALUE_BOOL(*arg)
					);

			break;
		default:
			break;
	}
}

static void erty_radio_button_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument getting for this object	*/
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(ERTY_IS_RADIO_BUTTON(object));

	switch (arg_id) {
		gboolean	value;
		GtkToggleButton	*toggle_button;

		case ARG_DISPLAY_VALUE:
			toggle_button = GTK_TOGGLE_BUTTON(object);
			value = gtk_toggle_button_get_active(toggle_button);
			GTK_VALUE_BOOL(*arg) = value;
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}
