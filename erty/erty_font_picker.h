#define ERTY_TYPE_FONT_PICKER	(erty_font_picker_get_type())

#define ERTY_FONT_PICKER(obj) \
	(GTK_CHECK_CAST((obj), ERTY_TYPE_FONT_PICKER, ErtyFontPicker))

#define ERTY_FONT_PICKER_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				ERTY_TYPE_FONT_PICKER, \
				ErtyFontPickerClass	))

#define ERTY_IS_FONT_PICKER(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_FONT_PICKER))

#define	ERTY_IS_FONT_PICKER_CLASS(klass), \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_FONT_PICKER))

typedef struct _ErtyFontPicker	ErtyFontPicker;
typedef struct _ErtyFontPickerClass	ErtyFontPickerClass;

struct _ErtyFontPicker {
	ErtyPicker	picker;

	GnomeFontPicker	*selector;
	GtkWidget	*entry;
};

struct _ErtyFontPickerClass {
	ErtyPickerClass	parent_class;
};

GtkType		erty_font_picker_get_type(	void	);

GtkWidget*	erty_font_picker_new(	void	);
GtkWidget*	erty_font_picker_new_with_label(	gchar *label	);
