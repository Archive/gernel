#include <erty.h>

static void erty_picker_set_sensitive(	ErtyPicker *picker	);
static void erty_picker_no_warn(	ErtyPicker *picker	);

static void erty_picker_paint_warn(	ErtyPicker *picker,
					GdkRectangle *area	);

static gint erty_picker_expose(	GtkWidget *widget,
				GdkEventExpose *event	);

static void erty_picker_draw(	GtkWidget *widget,
				GdkRectangle *area	);

static void erty_picker_init(	ErtyPicker *picker	);
static void erty_picker_class_init(	ErtyPickerClass *picker	);

static GtkEventBoxClass	*parent_class = NULL;

enum {	CHANGED,
	LAST_SIGNAL	};

static guint picker_signals[LAST_SIGNAL] = { 0 };

GtkType erty_picker_get_type(	void	)
/*	Return type identifier for type ErtyPicker	 */
{
	static GtkType	picker_type = 0;

	if (!picker_type) {

		static const GtkTypeInfo	picker_info = {
				"ErtyPicker",
				sizeof(ErtyPicker),
				sizeof(ErtyPickerClass),
				(GtkClassInitFunc)erty_picker_class_init,
				(GtkObjectInitFunc)erty_picker_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		picker_type = gtk_type_unique(	gtk_event_box_get_type(), 
						&picker_info	);
	}

	return picker_type;
}

static void erty_picker_init(	ErtyPicker *picker	)
/*	Initialize a ErtyPicker	*/
{
	GtkWidget	*hbox;

	hbox = gtk_hbox_new(TRUE, ERTY_PICKER_SPACING);
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 1);
	picker->box = hbox;
	gtk_container_add(GTK_CONTAINER(picker), hbox);
	gtk_widget_show(hbox);

	picker->mode = ERTY_PICKER_LABEL_LAST;
	picker->label = NULL;
	picker->selector = NULL;
	picker->warn = FALSE;

	gtk_signal_connect(	GTK_OBJECT(picker),
				"state_changed",
				erty_picker_set_sensitive,
				NULL	);
}

static void erty_picker_class_init(	ErtyPickerClass *klass	)
/*	Initialize the ErtyPicker class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	parent_class = gtk_type_class(GTK_TYPE_EVENT_BOX);
	function_offset = GTK_SIGNAL_OFFSET(ErtyPickerClass, changed);

	picker_signals[CHANGED] =

			gtk_signal_new(	"changed",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(object_class, picker_signals, LAST_SIGNAL);
	widget_class->draw = erty_picker_draw;
	widget_class->expose_event = erty_picker_expose;
	klass->changed = erty_picker_no_warn;
}

void erty_picker_set_label(	ErtyPicker *picker,
				gchar *str	)
/*	Add a new label to the picker	*/
{
	GtkWidget	*label;

	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));
	g_return_if_fail(str != NULL);
	g_return_if_fail(picker->label == NULL);

	label = gtk_label_new(str);
	picker->label = label;
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_box_pack_end(GTK_BOX(picker->box), label, FALSE, FALSE, 0);
	gtk_widget_show(label);
}

void erty_picker_set_selector(	ErtyPicker *picker,
				GtkWidget *selector	)
/*	Add a selector to the picker	*/
{
	GtkWidget	*box;
	GtkBox		*hbox;

	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));
	g_return_if_fail(picker->selector == NULL);
	g_return_if_fail(selector != NULL);
	g_return_if_fail(GTK_IS_WIDGET(selector));

	hbox = GTK_BOX(picker->box);

	box = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_end(hbox, box, FALSE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(box), selector, FALSE, TRUE, 0);
	gtk_widget_show(box);
}

void erty_picker_set_mode(	ErtyPicker *picker,
				ErtyPickerMode picker_mode	)
/*	Change the label mode to picker_mode	*/
{
	GList	*list;

	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));
	g_return_if_fail(picker->mode != picker_mode);

	list = gtk_container_children(GTK_CONTAINER(picker->box));

	switch (picker_mode) {
		GtkWidget	*child;
		gint		length;
		GtkBox		*box;

		case ERTY_PICKER_LABEL_FIRST:
			box = GTK_BOX(picker->box);

			child = GTK_WIDGET(g_list_last(list)->data);
			gtk_box_set_homogeneous(box, FALSE);
			gtk_box_reorder_child(box, child, 0);
			break;
		case ERTY_PICKER_LABEL_LAST:
			box = GTK_BOX(picker->box);

			length = g_list_length(list);
			child = GTK_WIDGET(list->data);
			gtk_box_set_homogeneous(box, TRUE);
			gtk_box_reorder_child(box, child, length);
			break;
		default:
			g_assert_not_reached();
			break;
	}

	picker->mode = picker_mode;
}

void erty_picker_changed(	ErtyPicker *picker	)
/*	Emit the "changed" signal of ErtyPicker	*/
{
	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));

	gtk_signal_emit(GTK_OBJECT(picker), picker_signals[CHANGED]);
}

void erty_picker_set_warn(	ErtyPicker *picker,
				gboolean flag	)
/*	Draw a red shadow around picker to draw the user's attention	*/
{
	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));

	picker->warn = flag;
	gtk_widget_queue_resize(GTK_WIDGET(picker));
}

static void erty_picker_draw(	GtkWidget *widget,
				GdkRectangle *area	)
/*	Draw handler	*/
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(ERTY_IS_PICKER(widget));

	(*GTK_WIDGET_CLASS(parent_class)->draw)(widget, area);
	erty_picker_paint_warn(ERTY_PICKER(widget), area);
}

static gint erty_picker_expose(	GtkWidget *widget,
				GdkEventExpose *event	)
/*	Expose handler	*/
{
	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(ERTY_IS_PICKER(widget), FALSE);

	(*GTK_WIDGET_CLASS(parent_class)->expose_event)(widget, event);
	erty_picker_paint_warn(ERTY_PICKER(widget), &event->area);

	return TRUE;
}

static void erty_picker_paint_warn(	ErtyPicker *picker,
					GdkRectangle *area	)
/*	Paint a warning box around picker	*/
{
	GtkWidget	*widget;
	GdkGC		*gc;
	GdkColor	red;

	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));
	g_return_if_fail(area != NULL);

	if (picker->warn == FALSE) return;
	widget = GTK_WIDGET(picker);
	red.red = 65535;
	red.green = 0;
	red.blue = 0;

	gdk_colormap_alloc_color(widget->style->colormap, &red, FALSE, TRUE);
	gc = gdk_gc_new(widget->window);
	gdk_gc_set_foreground(gc, &red);

	gdk_draw_rectangle(	widget->window,
				gc,
				FALSE,
				0,
				0,
				widget->allocation.width - 1,
				widget->allocation.height - 1	);
}

static void erty_picker_no_warn(	ErtyPicker *picker	)
/*	Switch off the red rectangle	*/
{
	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));

	picker->warn = FALSE;
	gtk_widget_queue_resize(GTK_WIDGET(picker));
}

static void erty_picker_set_sensitive(	ErtyPicker *picker	)
/*	Check if warning should still be should with the actual state	*/
{
	g_return_if_fail(picker != NULL);
	g_return_if_fail(ERTY_IS_PICKER(picker));

	if (!GTK_WIDGET_SENSITIVE(picker)) erty_picker_no_warn(picker);
}
