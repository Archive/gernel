#include <erty.h>

static void erty_preference_page_check_complete(
		ErtyPreferencePage *preference_page,
		GtkWidget *section
		);

static void erty_preference_page_reset_complete(
		ErtyPreferencePage *preference_page
		);

static void erty_preference_page_add_section(	GtkContainer *container,
						GtkWidget *widget	);

static void erty_preference_page_init(
		ErtyPreferencePage *preference_page
		);

static void erty_preference_page_class_init(
		ErtyPreferencePageClass *preference_page
		);

static ErtyPreferencePageClass	*parent_class = NULL;

enum {	CHANGED,
	APPLY,
	SAVE,
	LAST_SIGNAL	};

static guint preference_page_signals[LAST_SIGNAL] = { 0 };

GtkType erty_preference_page_get_type(	void	)
/*	Return type identifier for type ErtyPreferencePage	*/
{
	static GtkType	preference_page_type = 0;

	if (!preference_page_type) {

		static const GtkTypeInfo	preference_page_info = {
			"ErtyPreferencePage",
			sizeof(ErtyPreferencePage),
			sizeof(ErtyPreferencePageClass),
			(GtkClassInitFunc)erty_preference_page_class_init,
			(GtkObjectInitFunc)erty_preference_page_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		preference_page_type = gtk_type_unique(	gtk_vbox_get_type(), 
							&preference_page_info );
	}

	return preference_page_type;
}

GtkWidget* erty_preference_page_new(	void	)
/*	Create an instance of ErtyPreferencePage	*/
{
	ErtyPreferencePage	*preference_page;

	preference_page = gtk_type_new(erty_preference_page_get_type());

	return GTK_WIDGET(preference_page);
}

static void erty_preference_page_init(
		ErtyPreferencePage *preference_page
		)
/*	Initialize the ErtyPreferencePage class	*/
{
	GtkBox	*box;

	box = GTK_BOX(preference_page);

	gtk_box_set_homogeneous(box, FALSE);
	gtk_box_set_spacing(box, GNOME_PAD_SMALL);
	gtk_container_set_border_width(GTK_CONTAINER(box), GNOME_PAD_SMALL);
	preference_page->complete = TRUE;
	preference_page->data = NULL;
}

static void erty_preference_page_class_init(
		ErtyPreferencePageClass *klass
		)
/*	Initialize the ErtyPreferencePage class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBoxClass		*box_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	box_class = (GtkBoxClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_VBOX);
	function_offset = GTK_SIGNAL_OFFSET(ErtyPreferencePageClass, changed);

	preference_page_signals[CHANGED] =

			gtk_signal_new(	"changed",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	function_offset = GTK_SIGNAL_OFFSET(ErtyPreferencePageClass, apply);

	preference_page_signals[APPLY] =

			gtk_signal_new(	"apply",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	function_offset = GTK_SIGNAL_OFFSET(ErtyPreferencePageClass, save);

	preference_page_signals[SAVE] =

			gtk_signal_new(	"save",
					GTK_RUN_FIRST,
					object_class->type,
					function_offset,
					gtk_marshal_NONE__NONE,
					GTK_TYPE_NONE,
					0	);

	gtk_object_class_add_signals(	object_class, 
					preference_page_signals, 
					LAST_SIGNAL	);

	container_class->add = erty_preference_page_add_section;

	klass->changed = erty_preference_page_reset_complete;
	klass->apply = NULL;
	klass->save = NULL;
}

static void erty_preference_page_add_section(	GtkContainer *container,
						GtkWidget *widget	)
/*	Add a ErtySection to preference_page	*/
{
	g_return_if_fail(container != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(container));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(widget));

	gtk_box_pack_start(GTK_BOX(container), widget, FALSE, FALSE, 0);

	gtk_signal_connect_object(	GTK_OBJECT(widget),
					"changed",
					erty_preference_page_changed,
					GTK_OBJECT(container)	);

	gtk_signal_connect_object(	GTK_OBJECT(container),
					"apply",
					erty_preference_section_apply,
					GTK_OBJECT(widget)	);

	gtk_signal_connect_object(	GTK_OBJECT(container),
					"save",
					erty_preference_section_save,
					GTK_OBJECT(widget)	);

g_print("erty_preference_page_add_section:	FIXME: erty_preference_page_check_complete() should be default handler for apply AND save; modify those signals to pass section\n");
	gtk_signal_connect(	GTK_OBJECT(container),
				"apply",
				erty_preference_page_check_complete,
				widget	);
}

static void erty_preference_page_check_complete(
		ErtyPreferencePage *preference_page,
		GtkWidget *section
		)
/*	Check if 'section' has valid input and update page accordingly	*/
{
	ErtyPreferenceSection	*preference_section;
	gboolean		flag;

	g_return_if_fail(preference_page != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page));
	g_return_if_fail(section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(section));

	preference_section = ERTY_PREFERENCE_SECTION(section);

	flag = MIN(preference_section->complete, preference_page->complete);
	preference_page->complete = flag;
}

static void erty_preference_page_reset_complete(
		ErtyPreferencePage *preference_page
		)
/*	Reset the 'complete' flag	*/
{
	g_return_if_fail(preference_page != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page));

	preference_page->complete = TRUE;
}

void erty_preference_page_changed(	ErtyPreferencePage *preference_page )
/*	Emit the "changed" signal	*/
{
	g_return_if_fail(preference_page != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page));

	gtk_signal_emit(	GTK_OBJECT(preference_page), 
				preference_page_signals[CHANGED]	);
}

void erty_preference_page_save(	ErtyPreferencePage *preference_page	)
/*	Tell subsections to save their information	*/
{
	g_return_if_fail(preference_page != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page));

	gtk_signal_emit(	GTK_OBJECT(preference_page), 
				preference_page_signals[SAVE]	);
}

void erty_preference_page_apply(	ErtyPreferencePage *preference_page )
/*	Tell subsections that it is time to apply their information	*/
{
	g_return_if_fail(preference_page != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page));

	gtk_signal_emit(	GTK_OBJECT(preference_page),
				preference_page_signals[APPLY]	);
}

void erty_preference_page_set_data(	ErtyPreferencePage *preference_page,
					gpointer data	)
/*	Set the data field of preference_page	*/
{
	g_return_if_fail(preference_page != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page));

	preference_page->data = data;
}

gpointer erty_preference_page_get_data(
		ErtyPreferencePage *preference_page
		)
/*	Get the data field of preference_page	*/
{
	g_return_val_if_fail(preference_page != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page), NULL);

	return preference_page->data;
}
