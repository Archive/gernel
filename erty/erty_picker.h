#define	ERTY_TYPE_PICKER	(erty_picker_get_type())

#define ERTY_PICKER(obj) \
	(GTK_CHECK_CAST((obj), ERTY_TYPE_PICKER, ErtyPicker))

#define ERTY_PICKER_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST((klass), ERTY_TYPE_PICKER, ErtyPickerlass))

#define ERTY_IS_PICKER(obj)	(GTK_CHECK_TYPE((obj), ERTY_TYPE_PICKER))

#define ERTY_IS_PICKER_CLASS(klass), \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_PICKER))

#define ERTY_PICKER_SPACING	GNOME_PAD_SMALL

typedef struct _ErtyPicker		ErtyPicker;
typedef struct _ErtyPickerClass	ErtyPickerClass;

typedef enum {	ERTY_PICKER_LABEL_0,
		ERTY_PICKER_LABEL_FIRST,
		ERTY_PICKER_LABEL_LAST	} ErtyPickerMode;

struct _ErtyPicker {
	GtkEventBox		event_box;

	ErtyPickerMode		mode;
	GtkWidget		*label, *box, *selector;
	gboolean		warn;
};

struct _ErtyPickerClass {
	GtkEventBoxClass	parent_class;

	void (*changed)		(ErtyPicker *picker);
};

GtkType		erty_picker_get_type(	void	);

void 		erty_picker_set_mode(	ErtyPicker *picker,
					ErtyPickerMode picker_mode	);

void		erty_picker_set_label(	ErtyPicker *picker,
					gchar *label	);

void		erty_picker_set_selector(	ErtyPicker *picker,
						GtkWidget *selector	);

void		erty_picker_changed(	ErtyPicker *picker	);

void		erty_picker_set_warn(	ErtyPicker *picker,
					gboolean flag	);
