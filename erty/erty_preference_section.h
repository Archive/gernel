#define ERTY_TYPE_PREFERENCE_SECTION	(erty_preference_section_get_type())

#define	ERTY_PREFERENCE_SECTION(obj) \
	(GTK_CHECK_CAST(	(obj), \
				ERTY_TYPE_PREFERENCE_SECTION, \
				ErtyPreferenceSection	))

#define ERTY_PREFERENCE_SECTION_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				ERTY_TYPE_PREFERENCE_SECTION, \
				ErtyPreferenceSectionClass	))

#define ERTY_IS_PREFERENCE_SECTION(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_PREFERENCE_SECTION))

#define ERTY_IS_PREFERENCE_SECTION_CLASS(klass), \
	(GTK_CHECK_CLASS_TYP((klass), ERTY_TYPE_PREFERENCE_SECTION))

enum ErtyPreferenceSectionDependency {
		ERTY_PREFERENCE_SECTION_DEPENDENCY_0,
		ERTY_PREFERENCE_SECTION_DEPENDENCY_A,
		ERTY_PREFERENCE_SECTION_DEPENDENCY_NOT_A,
		ERTY_PREFERENCE_SECTION_DEPENDENCY_XOR
		};

enum ErtyPreferenceSectionAction {
		ERTY_PREFERENCE_SECTION_ACTION_0,
		ERTY_PREFERENCE_SECTION_ACTION_APPLY,
		ERTY_PREFERENCE_SECTION_ACTION_SAVE
		};

typedef struct _ErtyPreferenceSection		ErtyPreferenceSection;
typedef struct _ErtyPreferenceSectionClass	ErtyPreferenceSectionClass;
typedef	struct _ErtyPreferenceSectionChild	ErtyPreferenceSectionChild;

enum ErtyPreferenceSectionWidget {
		ERTY_PREFERENCE_SECTION_WIDGET_0,
		ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
		ERTY_PREFERENCE_SECTION_WIDGET_COLOR_PICKER,
		ERTY_PREFERENCE_SECTION_WIDGET_FONT_PICKER,
		ERTY_PREFERENCE_SECTION_WIDGET_RADIO_BUTTON,
		ERTY_PREFERENCE_SECTION_WIDGET_KEY_BINDING,
		ERTY_PREFERENCE_SECTION_WIDGET_ENTRY
		};

struct _ErtyPreferenceSection {
	GtkFrame	frame;

	gboolean	complete;
	GtkWidget	*box;
	GList		*children;
};

struct _ErtyPreferenceSectionClass {
	GtkFrameClass	parent_class;

	void (*changed)	(ErtyPreferenceSection *preference_section);
};

struct _ErtyPreferenceSectionChild {
	GtkWidget			*widget, *operator;
	gchar				*argument;
};

GtkType		erty_preference_section_get_type(	void	);
GtkWidget*	erty_preference_section_new(	gchar *label	);

GtkWidget*	erty_preference_section_add(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		enum ErtyPreferenceSectionWidget widget_type,
		gchar *label,
		guint arg_id
		);

GtkWidget*	erty_preference_section_add_multiple(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		enum ErtyPreferenceSectionDependency dependency,
		gchar *label_a, guint arg_id_a,
		gchar *label_b, guint arg_id_b,
		enum ErtyPreferenceSectionWidget widget_type,
		gchar *label, guint arg_id,
		...
		);

void		erty_preference_section_add_dependencies(
		ErtyPreferenceSection *preference_section,
		GtkWidget *prerequisite,
		GtkWidget *dependant,
		...
		);

void		erty_preference_section_apply(
		ErtyPreferenceSection *preference_section
		);

void		erty_preference_section_save(
		ErtyPreferenceSection *preference_section
		);
