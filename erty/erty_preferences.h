#define ERTY_TYPE_PREFERENCES	(erty_preferences_get_type())

#define ERTY_PREFERENCES(obj) \
	(GTK_CHECK_CAST((obj), ERTY_TYPE_PREFERENCES, ErtyPreferences))

#define ERTY_PREFERENCES_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				ERTY_PREFERENCES, \
				ErtyPreferencesClass)	)

#define ERTY_IS_PREFERENCES(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_PREFERENCES))

#define ERTY_IS_PREFERENCES_CLASS(klass), \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_PREFERENCES))

typedef struct _ErtyPreferences	ErtyPreferences;
typedef struct _ErtyPreferencesClass	ErtyPreferencesClass;

struct _ErtyPreferences {
	GnomePropertyBox	property_box;

	GSList			*pages;
};

struct _ErtyPreferencesClass {
	GnomePropertyBoxClass	parent_class;
};

GtkType	erty_preferences_get_type(	void	);
GtkWidget* erty_preferences_new(	void	);

void	erty_preferences_add_from_widgets(	ErtyPreferences *preferences,
						GtkWidget *widget,
						gpointer data,
						...	);

void	erty_preferences_show_page(	ErtyPreferences *preferences,
					gpointer data	);
