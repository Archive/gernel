#include <erty.h>

static void erty_color_picker_set_color(
		ErtyColorPicker *color_picker,
		guint32 color
		);

static void erty_color_picker_set_color_from_string(
		ErtyColorPicker *color_picker,
		gchar *color
		);

static void erty_color_picker_init(	ErtyColorPicker *color_picker	);

static void erty_color_picker_changed(
		ErtyColorPicker *color_picker
		);

static void erty_color_picker_class_init(
		ErtyColorPicker *color_picker
		);

static GtkWidget* erty_color_picker_construct(
		ErtyColorPicker *color_picker
		);

static void erty_color_picker_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static void erty_color_picker_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	);

static gchar* erty_color_picker_get_color_string(
	ErtyColorPicker *color_picker
	);

static guint32 erty_color_picker_get_color(
	ErtyColorPicker *color_picker
	);

static ErtyPickerClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_DISPLAY_VALUE,
	ARG_COLOR	};

GtkType erty_color_picker_get_type(	void	)
/*	Return type identifier for type ErtyColorPicker	*/
{
	static GtkType	color_picker_type = 0;

	if (!color_picker_type) {

		static const GtkTypeInfo	color_picker_info = {
			"ErtyColorPicker",
			sizeof(ErtyColorPicker),
			sizeof(ErtyColorPickerClass),
			(GtkClassInitFunc)erty_color_picker_class_init,
			(GtkObjectInitFunc)erty_color_picker_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		color_picker_type = gtk_type_unique(
				erty_picker_get_type(),
				&color_picker_info
				);
	}

	return color_picker_type;
}

GtkWidget* erty_color_picker_new(	void	)
/*	Create an instance of ErtyColorPicker	*/
{
	ErtyColorPicker	*color_picker;

	color_picker = gtk_type_new(erty_color_picker_get_type());

	return erty_color_picker_construct(color_picker);
}

GtkWidget* erty_color_picker_new_with_label(	gchar	*label	)
/*	Create an instance of ErtyColorPicker with label	*/
{
	GtkWidget	*color_picker;

	g_return_val_if_fail(label != NULL, NULL);

	color_picker = erty_color_picker_new();
	erty_picker_set_label(ERTY_PICKER(color_picker), label);

	erty_picker_set_mode(	ERTY_PICKER(color_picker),
				ERTY_PICKER_LABEL_FIRST	);

	return color_picker;
}

static GtkWidget* erty_color_picker_construct(
		ErtyColorPicker *color_picker
		)
/*	Fill the hbox with some widgets	*/
{
	GtkWidget	*picker, *cp_box;
	GtkBox		*hbox;

	g_return_val_if_fail(color_picker != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_COLOR_PICKER(color_picker), NULL);

	hbox = GTK_BOX(color_picker);

	cp_box = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(hbox, cp_box, FALSE, TRUE, 0);
	gtk_widget_show(cp_box);

	picker = gnome_color_picker_new();
	color_picker->selector = GNOME_COLOR_PICKER(picker);
	erty_picker_set_selector(ERTY_PICKER(color_picker), picker);

	gtk_signal_connect_object(	GTK_OBJECT(picker),
					"color_set",
					erty_color_picker_changed,
					GTK_OBJECT(color_picker)	);

	gtk_widget_show(picker);

	return GTK_WIDGET(color_picker);
}

static void erty_color_picker_init(	ErtyColorPicker *color_picker	)
/*	Initializer a ErtyColorPicker	*/
{
	color_picker->selector = NULL;
}

static void erty_color_picker_class_init(	ErtyColorPicker *klass )
/*	Initialize the ErtyColorPicker class	*/
{
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBoxClass		*box_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	box_class = (GtkBoxClass*)klass;

	parent_class = gtk_type_class(GTK_TYPE_HBOX);

	gtk_object_add_arg_type(	"ErtyColorPicker::color", 
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE, 
					ARG_COLOR	);

	gtk_object_add_arg_type(	"ErtyColorPicker::display_value",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_DISPLAY_VALUE	);

	object_class->get_arg = erty_color_picker_get_arg;
	object_class->set_arg = erty_color_picker_set_arg;
}

static void erty_color_picker_set_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument setting for this object	*/
{
	ErtyColorPicker	*color_picker;

	color_picker = ERTY_COLOR_PICKER(object);

	switch (arg_id) {
		case ARG_COLOR:
		case ARG_DISPLAY_VALUE:

			erty_color_picker_set_color_from_string(
					color_picker, 
					GTK_VALUE_STRING(*arg)
					);

			break;
		default:
			break;
	}
}

static void erty_color_picker_get_arg(	GtkObject *object,
						GtkArg *arg,
						guint arg_id	)
/*	Implements argument getting	*/
{
	ErtyColorPicker	*color_picker;

	g_return_if_fail(object != NULL);
	g_return_if_fail(ERTY_IS_COLOR_PICKER(object));

	color_picker = ERTY_COLOR_PICKER(object);

	switch (arg_id) {
		gchar	*str;

		case ARG_DISPLAY_VALUE:
		case ARG_COLOR:

			str = erty_color_picker_get_color_string(
				color_picker
				);

			GTK_VALUE_STRING(*arg) = str;
			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void erty_color_picker_set_color(
		ErtyColorPicker *color_picker,
		guint32 color
		)
/*	Set the color of the color_picker	*/
{
	guint8	r, g, b, a;

	g_return_if_fail(color_picker != NULL);
	g_return_if_fail(ERTY_IS_COLOR_PICKER(color_picker));

	r = (color & 0xff000000) >> 24;
	g = (color & 0x00ff0000) >> 16;
	b = (color & 0x0000ff00) >> 8;
	a = color & 0x000000ff;

	gnome_color_picker_set_i8(color_picker->selector, r, g, b, a);
}

static void erty_color_picker_set_color_from_string(
		ErtyColorPicker *color_picker,
		gchar *color
		)
/*	Set the color of the color_picker, where color is a guint32 value
	encoded as a string	*/
{
	guint32	value;

	g_return_if_fail(color_picker != NULL);
	g_return_if_fail(ERTY_IS_COLOR_PICKER(color_picker));
	g_return_if_fail(color != NULL);

	value = erty_color_string_to_32(color);
	erty_color_picker_set_color(color_picker, value);
}

static guint32 erty_color_picker_get_color(
	ErtyColorPicker *color_picker
	)
/*	Get the color displayed in the color_picker	*/
{
	guint8			r, g, b, a;
	guint32			result, tmp;
	GnomeColorPicker	*picker;

	g_return_val_if_fail(color_picker != NULL, 0x00000000);
	g_return_val_if_fail(ERTY_IS_COLOR_PICKER(color_picker), 0);

	picker = GNOME_COLOR_PICKER(color_picker->selector);

	gnome_color_picker_get_i8(picker, &r, &g, &b, &a);
	tmp = 0x00000000;
	result = (tmp | r) << 24;
	result = result | ((tmp | g) << 16);
	result = result | ((tmp | b) << 8);
	result = result | a;

	return result;
}

static gchar* erty_color_picker_get_color_string(
	ErtyColorPicker *color_picker
	)
/*	Get the color displayed by color_picker as a string	*/
{
	guint32	color;

	g_return_val_if_fail(color_picker != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_COLOR_PICKER(color_picker), NULL);

	color = erty_color_picker_get_color(color_picker);

	return erty_color_32_to_string(color);
}

static void erty_color_picker_changed(
		ErtyColorPicker *color_picker
		)
/*	Emit the "changed" signal	*/
{
	g_return_if_fail(color_picker != NULL);
	g_return_if_fail(ERTY_IS_COLOR_PICKER(color_picker));

	erty_picker_changed(ERTY_PICKER(color_picker));
}
