#include <erty.h>
#include <erty_entry.h>
#include <erty_stock_menu_clear.xpm>
#include <gdk/gdkx.h>

static void erty_entry_get_arg(	GtkObject *object,
				GtkArg *arg,
				guint arg_id	);

static void erty_entry_set_arg(	GtkObject *object,
				GtkArg *arg,
				guint arg_id	);

static void erty_entry_set_label(	ErtyEntry *entry,
					gchar *label	);

static void erty_entry_clear(	ErtyEntry *entry	);
static void erty_entry_clear_sensitive(	ErtyEntry *entry	);

static GtkWidget* erty_entry_pixmap(	ErtyEntry *entry, 
					gchar *file	);

static void erty_entry_changed(	ErtyEntry *entry	);
static void erty_entry_init(	ErtyEntry *entry	);
static void erty_entry_class_init(	ErtyEntryClass *klass	);
static void erty_entry_remove_current(	ErtyEntry *entry	);
static void erty_entry_update_history(	ErtyEntry *entry	);
static const gchar** erty_entry_get_historyv(	ErtyEntry *entry );
static GList* erty_entry_get_history(	ErtyEntry *entry	);
static void erty_entry_clear_all(	ErtyEntry *entry	);

static void erty_entry_set_popdown_strings(	ErtyEntry *entry,
						GList *list	);

static gboolean erty_entry_history_contains(	ErtyEntry *entry,
						gchar *string	);

static void erty_entry_set_history(	ErtyEntry *entry,
					gchar *history_id	);
enum {	ARG_0,
	ARG_DISPLAY_VALUE	};

enum {	CHANGED,
	LAST_SIGNAL	};

static guint 		entry_signals[LAST_SIGNAL] = { 0 };
static GtkHBoxClass	*parent_class = NULL;

static void erty_entry_init(	ErtyEntry *entry	)
/*	Initialize a ErtyEntry	*/
{
	GtkBox		*box;
	GtkWidget	*combo, *button, *editable, *pixmap;
	GtkTooltips	*tooltips;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	/*	Tooltips	*/
	tooltips = gtk_tooltips_new();

	/*	The container	*/
	box = GTK_BOX(entry);
	gtk_box_set_homogeneous(box, FALSE);
	gtk_box_set_spacing(box, 0);

	/*	The combo widget	*/
	combo = gtk_combo_new();
	editable = GTK_COMBO(combo)->entry;

	gtk_signal_connect_object(	GTK_OBJECT(editable),
					"activate",
					erty_entry_update_history,
					GTK_OBJECT(entry)	);

	gtk_signal_connect_object(	GTK_OBJECT(editable),
					"activate",
					erty_entry_save_config,
					GTK_OBJECT(entry)	);

	gtk_signal_connect_object(	GTK_OBJECT(editable),
					"changed",
					erty_entry_changed,
					GTK_OBJECT(entry)	);

	entry->combo = combo;
	gtk_combo_disable_activate(GTK_COMBO(combo));
	gtk_box_pack_start(box, combo, FALSE, FALSE, 0);
	gtk_widget_show(combo);

	/*	The trash button	*/
	button = gtk_button_new();
	pixmap = gnome_stock_pixmap_widget(NULL, GNOME_STOCK_MENU_TRASH);
	gtk_container_add(GTK_CONTAINER(button), pixmap);
	gtk_widget_show(pixmap);
	gtk_tooltips_set_tip(tooltips, button, _("Remove current entry"), NULL);
	entry->remove = button;

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					erty_entry_remove_current,
					GTK_OBJECT(entry)	);

	gtk_widget_set_sensitive(button, FALSE);
	gtk_box_pack_start(box, button, FALSE, FALSE, 0);
	gtk_widget_show(button);

	/*	The clear button	*/
	button = gtk_button_new();
	pixmap = erty_entry_pixmap(entry, ERTY_STOCK_MENU_CLEAR);
	gtk_container_add(GTK_CONTAINER(button), pixmap);
	gtk_widget_show(pixmap);
	gtk_widget_set_sensitive(button, FALSE);
	entry->clear = button;
	gtk_tooltips_set_tip(tooltips, button, _("Clear current entry"), NULL);

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					erty_entry_clear,
					GTK_OBJECT(entry)	);

	gtk_box_pack_start(box, button, FALSE, FALSE, 0);
	gtk_widget_show(button);
}

static void erty_entry_class_init(	ErtyEntryClass *klass	)
/*	Initialize the ErtyEntry class	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;

	object_class = (GtkObjectClass*)klass;

	parent_class = gtk_type_class(gtk_hbox_get_type());

	function_offset = GTK_SIGNAL_OFFSET(ErtyEntryClass, changed);

	entry_signals[CHANGED] = gtk_signal_new(	"changed",
							GTK_RUN_FIRST,
							object_class->type,
							function_offset,
							gtk_marshal_NONE__NONE,
							GTK_TYPE_NONE,
							0	);

	gtk_object_class_add_signals(object_class, entry_signals, LAST_SIGNAL);

	gtk_object_add_arg_type(	"ErtyEntry::display_value",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_DISPLAY_VALUE	);

	object_class->get_arg = erty_entry_get_arg;
	object_class->set_arg = erty_entry_set_arg;
	klass->changed = erty_entry_clear_sensitive;
}

static void erty_entry_clear_sensitive(	ErtyEntry *entry	)
/*	Decide if the the 'clear' button should be sensitive	*/
{
	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	if (erty_entry_get_current(entry) != NULL)
		gtk_widget_set_sensitive(entry->clear, TRUE);
	else
		gtk_widget_set_sensitive(entry->clear, FALSE);
}

GtkType erty_entry_get_type(	void	)
/*	Return type identifier for type ErtyEntry	*/
{
	static GtkType	entry_type = 0;

	if (!entry_type) {

		static const GtkTypeInfo	entry_info = {
				"ErtyEntry",
				sizeof(ErtyEntry),
				sizeof(ErtyEntryClass),
				(GtkClassInitFunc)erty_entry_class_init,
				(GtkObjectInitFunc)erty_entry_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		entry_type = gtk_type_unique(gtk_hbox_get_type(), &entry_info);
	}

	return entry_type;
}

GtkWidget* erty_entry_new(	gchar *history_id	)
/*	Create an instance of ErtyEntry	*/
{
	ErtyEntry	*entry;

	entry = gtk_type_new(erty_entry_get_type());
	erty_entry_set_history(entry, history_id);

	return GTK_WIDGET(entry);
}

GtkWidget* erty_entry_new_with_label(	gchar *history_id,
					gchar *label	)
/*	Create an instance of ErtyEntry	*/
{
	GtkWidget	*entry;

	g_return_val_if_fail(label != NULL, NULL);

	entry = erty_entry_new(history_id);
	erty_entry_set_label(ERTY_ENTRY(entry), label);

	return entry;
}

static void erty_entry_set_label(	ErtyEntry *entry,
					gchar *label	)
/*	Put a label next to the entry	*/
{
	GtkWidget	*widget;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));
	g_return_if_fail(label != NULL);

	widget = gtk_label_new(label);
	gtk_misc_set_alignment(GTK_MISC(widget), 0, 0.5);
	gtk_container_add(GTK_CONTAINER(entry), widget);
	gtk_box_reorder_child(GTK_BOX(entry), widget, 0);
	gtk_widget_show(widget);
}

static void erty_entry_set_history(	ErtyEntry *entry,
					gchar *history_id	)
/*	Try to load the history specified by history_id into entry	*/
{
	gint	count, i;
	gchar	**values;
	GList	*list;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	list = NULL;
	entry->path = g_strconcat("/", PACKAGE, "/", history_id, NULL);

	/*	gnome_config_get_vector() sets count to '1' even if list is 
		empty. Bug in gnome-config?	*/
	gnome_config_get_vector(entry->path, &count, &values);

	/*	Workaround:	*/
	if ((count > 0) && (strlen(values[0]) > 0))

		for (i = count; i > 0; i--) 
			list = g_list_append(list, values[i - 1]);

	erty_entry_set_popdown_strings(entry, list);
	entry->history = list;
	gtk_widget_set_sensitive(entry->remove, g_list_length(list));
}

static void erty_entry_set_popdown_strings(	ErtyEntry *entry,
						GList *list	)
/*	Set the a list of strings to be the popdown menu of entry	*/
{
	GList		*strings;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	strings = list != NULL?list:g_list_append(NULL, NULL);
	gtk_combo_set_popdown_strings(GTK_COMBO(entry->combo), strings);
}

static void erty_entry_remove_current(	ErtyEntry *entry	)
/*	Remove the item currently in entry	*/
{
	GList	*element;
	gchar	*current;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	if (entry->history == NULL) {
		gtk_widget_set_sensitive(entry->remove, FALSE);
		return;
	}

	element = NULL;
	current = erty_entry_get_current(entry);

	if (current != NULL)

		element = g_list_find_custom(	entry->history, 
						current, 
						(GCompareFunc)strcmp	);

	if (element == NULL)
		/*	User trying to remove an entry that has not yet been
			added to history	*/

		return;

	entry->history = g_list_remove(entry->history, element->data);

	if (entry->history != NULL)
		erty_entry_set_popdown_strings(entry, entry->history);
	else
		erty_entry_clear_all(entry);

	erty_entry_save_config(entry);
}

static void erty_entry_clear_all(	ErtyEntry *entry	)
/*	Clear the entry completely, including history	*/
{
	GtkCombo	*combo;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));
	g_return_if_fail(entry->history == NULL);

	combo = GTK_COMBO(entry->combo);
	erty_entry_set_popdown_strings(entry, NULL);
	gtk_widget_set_sensitive(entry->remove, FALSE);
}

static void erty_entry_clear(	ErtyEntry *entry	)
/*	Clear the current text in entry, leaving it in history	*/
{
	GtkCombo	*combo;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	combo = GTK_COMBO(entry->combo);
	gtk_editable_delete_text(GTK_EDITABLE(combo->entry), 0, -1);
}

gchar* erty_entry_get_current(	ErtyEntry *entry	)
/*	Get text currently in entry	*/
{
	gchar		*string;
	GtkCombo	*combo;

	g_return_val_if_fail(entry != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_ENTRY(entry), NULL);

	combo = GTK_COMBO(entry->combo);
	string = gtk_entry_get_text(GTK_ENTRY(combo->entry));

	return strlen(string) > 0?string:NULL;
}

void	erty_entry_set_current(	ErtyEntry *entry,
				gchar *string	)
/*	Show string in entry	*/
{
	GtkCombo	*combo;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));
	g_return_if_fail(string != NULL);

	combo = GTK_COMBO(entry->combo);

	gtk_entry_set_text(GTK_ENTRY(combo->entry), string);
	erty_entry_update_history(entry);
}

static void erty_entry_update_history(	ErtyEntry *entry	)
/*	Add current text in entry to entry's history list	*/
{
	gchar	*current;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	current = erty_entry_get_current(entry);

	if (	(current != NULL) &&
		(!erty_entry_history_contains(entry, current))	) {

		GList	*list;

		list = g_list_prepend(entry->history, g_strdup(current));
		entry->history = list;
		gtk_widget_set_sensitive(entry->remove, TRUE);
		erty_entry_set_popdown_strings(entry, list);
	}
}

void erty_entry_save_config(	ErtyEntry *entry	)
/*	Save configuration of entry	*/
{
        const gchar	**historyv;
	gint		history_count;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	erty_entry_update_history(entry);
	historyv = erty_entry_get_historyv(entry);
	history_count = g_list_length(erty_entry_get_history(entry));
        gnome_config_set_vector(entry->path, history_count, historyv);
	gnome_config_sync();
}

static const gchar** erty_entry_get_historyv(	ErtyEntry *entry )
/*	Assemble an array from the strings currently displayed in entry	*/
{
	const gchar	**historyv;
	gint		count;
	GList		*history, *list;

	g_return_val_if_fail(entry != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_ENTRY(entry), NULL);

	history = erty_entry_get_history(entry);
	if (history == NULL) return NULL;
	count = g_list_length(history);
	historyv = g_new(const gchar*, count);

	for (list = history; list != NULL; list = list->next) {
		count--;
		historyv[count] = list->data;
	}

	return historyv;
}

static GList* erty_entry_get_history(	ErtyEntry *entry	)
/*	Return list of strings currently displayed in entry	*/
{
	g_return_val_if_fail(entry != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_ENTRY(entry), NULL);

	return entry->history;
}

static gboolean erty_entry_history_contains(	ErtyEntry *entry,
							gchar *string	)
/*	Check if string is in entry's history	*/
{
	GList	*list;

	g_return_val_if_fail(entry != NULL, FALSE);
	g_return_val_if_fail(ERTY_IS_ENTRY(entry), FALSE);
	g_return_val_if_fail(string != NULL, FALSE);

	for (list = erty_entry_get_history(entry); list; list = list->next)
		if (strcmp(list->data, string) == 0) return TRUE;

	return FALSE;
}

void erty_entry_hide_brush(	ErtyEntry *entry	)
/*	Hide the button with the brush	*/
{
	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	gtk_widget_hide(entry->clear);
}

void erty_entry_hide_trashcan(	ErtyEntry *entry	)
/*	Hide the button with the trashcan	*/
{
	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	gtk_widget_hide(entry->remove);
}

void erty_entry_hide_pointer(	ErtyEntry *entry	)
/*	Hide the button with the pointer	*/
{
	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	gtk_widget_hide(GTK_COMBO(entry->combo)->button);
}

void erty_entry_set_visibility(	ErtyEntry *entry,
					gboolean flag	)
/*	Wrapper to gtk_entry_set_visibility()	*/
{
	GtkCombo	*combo;

	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	combo = GTK_COMBO(entry->combo);

	gtk_entry_set_visibility(GTK_ENTRY(combo->entry), flag);
}

static void erty_entry_changed(	ErtyEntry *entry	)
/*	Emit the 'changed' signal	*/
{
	g_return_if_fail(entry != NULL);
	g_return_if_fail(ERTY_IS_ENTRY(entry));

	gtk_signal_emit(GTK_OBJECT(entry), entry_signals[CHANGED], NULL);
}

static GtkWidget* erty_entry_pixmap(	ErtyEntry *entry, 
					gchar *file	)
/*	Create a new pixmap from 'file'; note that gnome_pixmap_new*() won't 
	work as well as the GTK versions since they don't make insensitive
	images	*/
{
	gchar		*tmp;
	GtkWidget	*pixmap;
	GdkPixmap	*data;
	GdkBitmap	*mask;
	GdkWindow	*window;

	g_return_val_if_fail(entry != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_ENTRY(entry), NULL);
	g_return_val_if_fail(file != NULL, NULL);

	tmp = g_file_exists(file)?file:gnome_pixmap_file(file);

	g_assert(tmp != NULL);

	window = GDK_ROOT_PARENT();

	data = gdk_pixmap_create_from_xpm_d(	window,
						&mask,
						NULL,
						erty_stock_menu_clear_xpm );

	pixmap = gtk_pixmap_new(data, mask);

	return pixmap;
}

static void erty_entry_set_arg(	GtkObject *object,
				GtkArg *arg,
				guint arg_id	)
/*	Implementss argument setting for this object	*/
{
	switch (arg_id) {
		gchar	*str;

		case ARG_DISPLAY_VALUE:
			str = GTK_VALUE_STRING(*arg);
			erty_entry_set_current(ERTY_ENTRY(object), str);
			break;
		default:
			break;
	}
}

static void erty_entry_get_arg(	GtkObject *object,
				GtkArg *arg,
				guint arg_id	)
/*	Implements argument getting for this object	*/
{
	switch (arg_id) {
		gchar	*str;

		case ARG_DISPLAY_VALUE:
			str = erty_entry_get_current(ERTY_ENTRY(object));
			GTK_VALUE_STRING(*arg) = str;
			break;
		default:
			break;
	}
}
