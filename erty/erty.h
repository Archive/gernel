#include <config.h>
#include <gnome.h>

#include "erty_preferences.h"
#include "erty_preference_page.h"
#include "erty_preference_section.h"
#include "erty_color_picker.h"
#include "erty_font_picker.h"
#include "erty_key_picker.h"

#define ERTY_STOCK_MENU_CLEAR	"erty_stock_menu_clear.xpm"

/*	Prefix of the config path; needs to be remembered because some GNOME
	functions don't care for pushed prefixes	*/
gchar *base_name;

GtkArg erty_arg_query(	GtkWidget *widget,
			gchar *argument	);

#define erty_arg_query_by_id(obj, id) \
		erty_arg_query(obj, erty_get_argument_from_id(obj, id))

gchar* erty_color_32_to_string(	guint32 color	);
guint32 erty_color_string_to_32(	gchar *color	);

GtkWidget* erty_widget_new(	GtkType type,
				gchar *local_name,
				gchar *argument,
				...	);

void erty_widget_init(	GtkWidget *widget,
			gchar *local_name,
			gchar *argument,
			...	);

void erty_init(	gchar *name	);

void erty_arg_add_multiple(	gchar *widget_name,
				gchar *name,
				GtkType type,
				guint arg,
				...	);

void erty_arg_add(	gchar *name,
			GtkType type,
			guint argument	);

void erty_save(	GtkWidget *widget,
		gchar *name	);

void erty_save_by_arg(	GtkWidget *widget,
			gchar *name,
			GtkArg *arg	);

gchar* erty_get_argument_from_id(	GtkWidget *widget,
					guint arg_id	);
