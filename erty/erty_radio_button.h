#define ERTY_TYPE_RADIO_BUTTON	(erty_radio_button_get_type())

#define ERTY_RADIO_BUTTON(obj) \
	(GTK_CHECK_CAST(	(obj), \
				ERTY_TYPE_RADIO_BUTTON, \
				ErtyRadioButton	))

#define ERTY_RADIO_BUTTON_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass) \
				ERTY_TYPE_RADIO_BUTTON, \
				ErtyRadioButtonClass	))

#define ERTY_IS_RADIO_BUTTON(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_RADIO_BUTTON))

#define ERTY_IS_RADIO_BUTTON_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_RADIO_BUTTON))

typedef struct _ErtyRadioButton	ErtyRadioButton;
typedef struct _ErtyRadioButtonClass	ErtyRadioButtonClass;

struct _ErtyRadioButton {
	GtkRadioButton	radio_button;

	GSList		*group;
};

struct _ErtyRadioButtonClass {
	GtkRadioButtonClass	parent_class;

	void (*changed)		(ErtyRadioButton *radio_button);
};

GtkType		erty_radio_button_get_type(	void	);

GtkWidget*	erty_radio_button_new(	gchar *label	);

GtkWidget*	erty_radio_button_new_with_dependency(
		gchar *label,
		GtkWidget *dependency
		);
