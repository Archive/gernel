#define ERTY_TYPE_KEY_PICKER	(erty_key_picker_get_type())

#define ERTY_KEY_PICKER(obj) \
	(GTK_CHECK_CAST((obj), ERTY_TYPE_KEY_PICKER, ErtyKeyPicker))

#define ERTY_KEY_PICKER_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST(	(klass), \
				ERTY_TYPE_KEY_PICKER, \
				ErtyKeyPickerClass	))

#define ERTY_IS_KEY_PICKER(obj) \
	(GTK_CHECK_TYPE((obj), ERTY_TYPE_KEY_PICKER))

#define ERTY_IS_KEY_PICKER_CLASS(klass), \
	(GTK_CHECK_CLASS_TYPE((klass), ERTY_TYPE_KEY_PICKER))

typedef struct _ErtyKeyPicker		ErtyKeyPicker;
typedef struct _ErtyKeyPickerClass	ErtyKeyPickerClass;

struct _ErtyKeyPicker {
	ErtyPicker	picker;

	GtkWidget	*entry;
	GSList		*matrix, *keys;
	guint		state;
	gboolean	keyboard_grab;
};

struct _ErtyKeyPickerClass {
	ErtyPickerClass	parent_class;
};

GtkType		erty_key_picker_get_type(	void	);
GtkWidget*	erty_key_picker_new(	void	);
GtkWidget*	erty_key_picker_new_with_label(	gchar *label	);
