#include <erty.h>
#include <erty_check_button.h>
#include <erty_radio_button.h>
#include <erty_entry.h>

static void erty_preference_section_warn(
		ErtyPreferenceSection *preference_section
		);

static void erty_preference_section_mark(
		ErtyPreferenceSection *preference_section,
		GtkWidget *operator
		);

static void erty_preference_section_add_from_operator(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		GtkWidget *operator,
		guint arg_id
		);

static void erty_preference_section_sensitive(	GtkWidget *decider,
						GtkWidget *operand );

static void erty_preference_section_insensitive(	GtkWidget *decider,
							GtkWidget *operand );

static void erty_preference_section_init(
		ErtyPreferenceSection *preference_section
		);

static void erty_preference_section_class_init(
		ErtyPreferenceSectionClass *klass
		);

static void erty_preference_section_construct(
		ErtyPreferenceSection *preference_section,
		gchar *label
		);

static void erty_preference_section_changed(
		ErtyPreferenceSection *preference_section
		);

static void erty_preference_section_add_child(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		GtkWidget *operator,
		gchar *argument
		);

static ErtyPreferenceSection	*parent_class = NULL;

enum {	CHANGED,
	LAST_SIGNAL	};

static guint preference_section_signals[LAST_SIGNAL] = { 0 };

GtkType erty_preference_section_get_type(	void	)
/*	Return type identifier for type ErtyPreferenceSection	*/
{
	static GtkType	preference_section_type = 0;

	if (!preference_section_type) {

		static const GtkTypeInfo	preference_section_info = {
			"ErtyPreferenceSection",
			sizeof(ErtyPreferenceSection),
			sizeof(ErtyPreferenceSectionClass),
			(GtkClassInitFunc)erty_preference_section_class_init,
			(GtkObjectInitFunc)erty_preference_section_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL
		};

		preference_section_type = gtk_type_unique(
				gtk_frame_get_type(),
				&preference_section_info
				);
	}

	return preference_section_type;
}

GtkWidget* erty_preference_section_new(	gchar *label	)
/*	Create an instance of ErtyPreferenceSection	*/
{
	ErtyPreferenceSection	*preference_section;

	g_return_val_if_fail(label != NULL, NULL);

	preference_section = gtk_type_new(erty_preference_section_get_type());
	erty_preference_section_construct(preference_section, label);

	return GTK_WIDGET(preference_section);
}

static void erty_preference_section_construct(
		ErtyPreferenceSection *preference_section,
		gchar *label
		)
/*	Set up the preference_section	*/
{
	GtkWidget	*vbox;

	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));
	g_return_if_fail(label != NULL);

	gtk_frame_set_label(GTK_FRAME(preference_section), label);

	vbox = gtk_vbox_new(FALSE, 0);
	preference_section->box = vbox;
	gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD_SMALL);
	gtk_container_add(GTK_CONTAINER(preference_section), vbox);
	gtk_widget_show(vbox);
}

static void erty_preference_section_init(
		ErtyPreferenceSection *preference_section
		)
/*	Initialize a ErtyPreferenceSection	*/
{
	preference_section->box = NULL;
	preference_section->children = NULL;
	preference_section->complete = TRUE;
}

static void erty_preference_section_class_init(
	ErtyPreferenceSectionClass *klass
	)
/*	Initialize the ErtyPreferenceSectionClass	*/
{
	guint			function_offset;
	GtkObjectClass		*object_class;
	GtkWidgetClass		*widget_class;
	GtkContainerClass	*container_class;
	GtkBinClass		*bin_class;

	object_class = (GtkObjectClass*)klass;
	widget_class = (GtkWidgetClass*)klass;
	container_class = (GtkContainerClass*)klass;
	bin_class = (GtkBinClass*)klass;

	parent_class = gtk_type_class(ERTY_TYPE_PREFERENCE_SECTION);

	function_offset = GTK_SIGNAL_OFFSET(	ErtyPreferenceSectionClass,
						changed	);

	preference_section_signals[CHANGED] = 

		gtk_signal_new(	"changed",
				GTK_RUN_FIRST,
				object_class->type,
				function_offset,
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE,
				0	);

	gtk_object_class_add_signals(	object_class,
					preference_section_signals,
					LAST_SIGNAL	);

	klass->changed = NULL;
}

GtkWidget* erty_preference_section_add(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		enum ErtyPreferenceSectionWidget widget_type,
		gchar *label,
		guint arg_id
		)
/*	Add new widget to section;

	preference_section:	Section to add to
	widget:			Widget to operate on (set/get preferences)
	widget_type:		type of the new widget
	label:			Label to pass to new widget constructor
	argument:		Gtk Argument for querying/setting preferences */
{
	GtkWidget	*operator;

	g_return_val_if_fail(preference_section != NULL, NULL);

	g_return_val_if_fail(	ERTY_IS_PREFERENCE_SECTION(preference_section),
				NULL	);

	g_return_val_if_fail(widget != NULL, NULL);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), NULL);

	g_return_val_if_fail(	widget_type != ERTY_PREFERENCE_SECTION_WIDGET_0,
				NULL	);

	g_return_val_if_fail(label != NULL, NULL);

	operator = NULL;

	switch (widget_type) {
		gchar	*str;

		case ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON:
			operator = erty_check_button_new_with_label(label);
			break;
		case ERTY_PREFERENCE_SECTION_WIDGET_COLOR_PICKER:
			operator = erty_color_picker_new_with_label(label);
			break;
		case ERTY_PREFERENCE_SECTION_WIDGET_FONT_PICKER:
			operator = erty_font_picker_new_with_label(label);
			break;
		case ERTY_PREFERENCE_SECTION_WIDGET_RADIO_BUTTON:
			operator = erty_radio_button_new(label);
			break;
		case ERTY_PREFERENCE_SECTION_WIDGET_KEY_BINDING:
			operator = erty_key_picker_new_with_label(label);
			break;
		case ERTY_PREFERENCE_SECTION_WIDGET_ENTRY:
			str = gtk_widget_get_name(widget);
			operator = erty_entry_new_with_label(str, label);
			break;
		default:
			g_assert_not_reached();
	}

	erty_preference_section_add_from_operator(	preference_section,
							widget,
							operator,
							arg_id	);

	return operator;
}

static void erty_preference_section_add_from_operator(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		GtkWidget *operator,
		guint arg_id
		)
/*	Add 'operator' to preference_section	*/
{
	gchar	*argument;
	GtkArg	arg;

	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(operator != NULL);
	g_return_if_fail(GTK_IS_WIDGET(operator));
	g_return_if_fail(arg_id != 0);

	argument = erty_get_argument_from_id(widget, arg_id);
	arg = erty_arg_query(widget, argument);

	g_assert(arg.type != GTK_TYPE_INVALID);

	gtk_object_set(	GTK_OBJECT(operator), 
			"display_value", 
			GTK_VALUE_POINTER(arg),
			NULL	);

	gtk_signal_connect_object(	GTK_OBJECT(operator), 
					"changed", 
					erty_preference_section_changed, 
					GTK_OBJECT(preference_section)	);

	/*	A pack spacing of 1 looks smarter when drawing warning boxes
		in consecutive widgets (e.g. ErtyPickers)	*/

	gtk_box_pack_start(	GTK_BOX(preference_section->box), 
				operator, 
				FALSE, 
				FALSE, 
				1	);

	gtk_widget_show(operator);

	erty_preference_section_add_child(	preference_section,
						widget,
						operator,
						argument	);
}

static void erty_preference_section_add_child(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		GtkWidget *operator,
		gchar *argument
		)
/*	Add a new child to the section's childrens	*/
{
	ErtyPreferenceSectionChild	*child_info;
	GList				*children;

	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(operator != NULL);
	g_return_if_fail(GTK_IS_WIDGET(operator));
	g_return_if_fail(argument != NULL);

	child_info = g_new(ErtyPreferenceSectionChild, 1);
	child_info->widget = widget;
	child_info->operator = operator;
	child_info->argument = argument;

	children = preference_section->children;
	preference_section->children = g_list_append(children, child_info);
}

GtkWidget* erty_preference_section_add_multiple(
		ErtyPreferenceSection *preference_section,
		GtkWidget *widget,
		enum ErtyPreferenceSectionDependency dependency,
		gchar *label_a, guint arg_id_a,
		gchar *label_b, guint arg_id_b,
		enum ErtyPreferenceSectionWidget widget_type,
		gchar *label, guint arg_id,
		...
		)
/*	Add widgets to preference_section
	preference_section:	The ErtyPreferenceSection to add to
	widget:			The widget to work on when setting 
				preferences
	widget_type:		The type of the new objects
	dependency:		specifies how the elements depend on each
				other; DEPENDENCY_A means that all widgets
				generated from label, arg_id will only be
				changable by the user if the label_a/arg_id_a
				widget is set to true (there is no label_b/
				arg_id_b in this case). DEPENDENCY_XOR means
				that widgets generated from label/arg_id will
				only be changable if the label_b/arg_id_b
				widget is set to true; the a and b widgets
				exclude each other (xor) and are realized
				using a radio button. DEPENDENCY_NOT_A:
				preference widgets will only be changeable if
				A is set to FALSE
	label:			The label to pass to the constructor
	arg_id:			The id specifying widget's GtkArg
	...:			An alternating list of labels and arg_id's
				to configure the new objects, and to
				set preferences when requested

	- widget should emit the 'changed' signal when it changes
	- ErtyPreferenceSection finds out by itself what the current value
	of widget's arg_id is	*/
{
	va_list				args;
	gchar				*str;
	guint				id;
	GtkSignalFunc			callback;
	GtkWidget			*operator, *tmp;
	ErtyPreferenceSection		*ps;
	GtkJustification		justification;

	g_return_val_if_fail(preference_section != NULL, NULL);

	g_return_val_if_fail(	ERTY_IS_PREFERENCE_SECTION(preference_section),
				NULL	);

	g_return_val_if_fail(widget != NULL, NULL);
	g_return_val_if_fail(GTK_WIDGET(widget), NULL);

	g_return_val_if_fail(	widget_type != ERTY_PREFERENCE_SECTION_WIDGET_0,
				NULL	);

	g_return_val_if_fail(label != NULL, NULL);

	justification = GTK_JUSTIFY_LEFT;
	ps = preference_section;
	callback = NULL;
	operator = NULL;

	/*	First check if there are dependency widgets to generate	*/
	switch (dependency) {
		case ERTY_PREFERENCE_SECTION_DEPENDENCY_0:
			g_assert(label_a == NULL);
			g_assert(arg_id_a == 0);
			g_assert(label_b == NULL);
			g_assert(arg_id_b == 0);

			break;
		case ERTY_PREFERENCE_SECTION_DEPENDENCY_A:
			g_assert(label_a != NULL);
			g_assert(arg_id_a != 0);
			g_assert(label_b == NULL);
			g_assert(arg_id_b == 0);

			operator = erty_preference_section_add(
				preference_section,
				widget,
				ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
				label_a,
				arg_id_a
				);

			callback = erty_preference_section_sensitive;
			justification = GTK_JUSTIFY_RIGHT;

			break;
		case ERTY_PREFERENCE_SECTION_DEPENDENCY_NOT_A:
			g_assert(label_a != NULL);
			g_assert(arg_id_a != 0);
			g_assert(label_b == NULL);
			g_assert(arg_id_b == 0);

			operator = erty_preference_section_add(
				preference_section,
				widget,
				ERTY_PREFERENCE_SECTION_WIDGET_CHECK_BUTTON,
				label_a,
				arg_id_a
				);

			callback = erty_preference_section_insensitive;
			justification = GTK_JUSTIFY_LEFT;

			break;
		case ERTY_PREFERENCE_SECTION_DEPENDENCY_XOR:
			g_assert(label_a != NULL);
			g_assert(arg_id_a != 0);
			g_assert(label_b != NULL);
			g_assert(arg_id_b != 0);

			operator = 

				erty_preference_section_add(
				preference_section,
				widget,
				ERTY_PREFERENCE_SECTION_WIDGET_RADIO_BUTTON,
				label_a,
				arg_id_a
				);

			operator = erty_radio_button_new_with_dependency(
					label_b,
					operator
					);

			erty_preference_section_add_from_operator(
					preference_section,
					widget,
					operator,
					arg_id_b
					);

			callback = erty_preference_section_sensitive;
			justification = GTK_JUSTIFY_RIGHT;

			break;
		default:
			g_assert_not_reached();
	}

	tmp = preference_section->box;

	if (justification == GTK_JUSTIFY_RIGHT) {
		GtkWidget	*box, *alignment;

		alignment = gtk_alignment_new(1, 1, 0, 1);

		gtk_container_add(	GTK_CONTAINER(preference_section->box),
					alignment	);

		gtk_widget_show(alignment);

		box = gtk_vbox_new(TRUE, 0);
		preference_section->box = box;
		gtk_container_add(GTK_CONTAINER(alignment), box);
		gtk_widget_show(box);
	}

	str = label;
	id = arg_id;

	va_start(args, arg_id);

	do {
		GtkWidget	*operand;

		operand = erty_preference_section_add(	preference_section, 
							widget,
							widget_type,
							str, 
							id	);

		if (callback != NULL) {

			gtk_signal_connect(	GTK_OBJECT(operator),
						"changed",
						callback,
						operand	);

			(*callback)(operator, operand);
		}

		str = va_arg(args, gchar*);
		if (str != NULL) id = va_arg(args, guint);
	} while (str != NULL);

	va_end(args);
	preference_section->box = tmp;

	return operator;
}

static void erty_preference_section_sensitive(	GtkWidget *decider,
						GtkWidget *operand )
/*	Set operand's sensitivity depending on the setting (active/
	not active) of the decider	*/
{
	GtkToggleButton	*toggle_button;
	gboolean	flag;

	g_return_if_fail(decider != NULL);
	g_return_if_fail(GTK_IS_TOGGLE_BUTTON(decider));
	g_return_if_fail(operand != NULL);
	g_return_if_fail(GTK_IS_WIDGET(operand));

	toggle_button = GTK_TOGGLE_BUTTON(decider);

	flag = gtk_toggle_button_get_active(toggle_button);
	gtk_widget_set_sensitive(operand, flag);
}

static void erty_preference_section_insensitive(	GtkWidget *decider,
							GtkWidget *operand )
/*	Set operand's sensitivity depending on the setting (active/
	not active) of the decider	*/
{
	GtkToggleButton	*toggle_button;
	gboolean	flag;

	g_return_if_fail(decider != NULL);
	g_return_if_fail(GTK_IS_TOGGLE_BUTTON(decider));
	g_return_if_fail(operand != NULL);
	g_return_if_fail(GTK_IS_WIDGET(operand));

	toggle_button = GTK_TOGGLE_BUTTON(decider);

	flag = gtk_toggle_button_get_active(toggle_button);
	gtk_widget_set_sensitive(operand, !flag);
}

static void erty_preference_section_changed(
		ErtyPreferenceSection *preference_section
		)
/*	Emit the "changed" signal of ErtyPreferenceSection	*/
{
	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));

	gtk_signal_emit(	GTK_OBJECT(preference_section),
				preference_section_signals[CHANGED]	);
}

static void erty_preference_section_action(
		ErtyPreferenceSection *preference_section,
		enum ErtyPreferenceSectionAction action
		)
/*	Apply or save state of this section; application will be omitted for 
	insensitive widgets; 'mode' decides whether to apply or save values
	(may be OR'ed together)	*/
{
	GList		*list;
	gboolean	warning;

	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));
	g_return_if_fail(action != ERTY_PREFERENCE_SECTION_ACTION_0);

	warning = FALSE;

	for (list = preference_section->children; list; list = list->next) {
		ErtyPreferenceSectionChild	*child;
		GtkWidget			*widget, *operator;

		child = (ErtyPreferenceSectionChild*)list->data;
		widget = child->widget;
		operator = child->operator;

		if (GTK_WIDGET_SENSITIVE(operator)) {
			GtkArg		arguments[1];
			gchar		*name;

			name = gtk_widget_get_name(operator);

			arguments[0].name = g_strconcat(name, 
							"::display_value", 
							NULL	);

			gtk_object_getv(GTK_OBJECT(operator), 1, arguments);

			if (arguments->type == GTK_TYPE_INVALID) {

				erty_preference_section_mark(
						preference_section,
						operator
						);

				warning = TRUE;
			} else {

				if (	action == 
					ERTY_PREFERENCE_SECTION_ACTION_APPLY )

					gtk_object_set(
						GTK_OBJECT(widget), 
						child->argument, 
						GTK_VALUE_POINTER(arguments[0]),
						NULL
						);

				if (	action ==
					ERTY_PREFERENCE_SECTION_ACTION_SAVE )

					erty_save_by_arg(	widget,
								child->argument,
								&arguments[0] );

				gtk_widget_queue_resize(widget);
			}
		}
	}

	if (warning) {
		preference_section->complete = FALSE;
		erty_preference_section_warn(preference_section);
	} else
		preference_section->complete = TRUE;
}

static void erty_preference_section_mark(
		ErtyPreferenceSection *preference_section,
		GtkWidget *operator
		)
/*	Prompt the user to enter input into 'operator'	*/
{
	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));
	g_return_if_fail(operator != NULL);
	g_return_if_fail(ERTY_IS_PICKER(operator));

	erty_picker_set_warn(ERTY_PICKER(operator), TRUE);
}

static void erty_preference_section_warn(
		ErtyPreferenceSection *preference_section
		)
/*	Pop up a warning dialog	*/
{
	GtkWidget	*dialog;

	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));

	dialog = gnome_ok_dialog(_("Input is not complete.\n"));
	gtk_widget_show(dialog);
}

gboolean erty_preference_page_get_complete(
		ErtyPreferencePage *preference_page
		)
/*	Check if all input in this page is valid	*/
{
	g_return_val_if_fail(preference_page != NULL, FALSE);
	g_return_val_if_fail(ERTY_IS_PREFERENCE_PAGE(preference_page), FALSE);

	return preference_page->complete;
}

void erty_preference_section_save(
		ErtyPreferenceSection *preference_section
		)
/*	Wrapper for erty_preference_section_action()	*/
{
	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));

	erty_preference_section_action(	preference_section, 
					ERTY_PREFERENCE_SECTION_ACTION_SAVE );
}

void erty_preference_section_apply(
		ErtyPreferenceSection *preference_section
		)
/*	Wrapper for erty_preference_section_action()	*/
{
	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));

	erty_preference_section_action(	preference_section, 
					ERTY_PREFERENCE_SECTION_ACTION_APPLY );
}

void erty_preference_section_add_dependencies(
		ErtyPreferenceSection *preference_section,
		GtkWidget *prerequisite,
		GtkWidget *dependant,
		...
		)
/*	Add dependency information for 'prerequisite' and dependants:
	whenever 'prerequisite' (a toggle button) is set to FALSE, all
	'dependant's will be insensitive; whenever it is TRUE, they'll be
	sensitive	*/
{
	va_list		args;
	GtkWidget	*current_dependant;

	g_return_if_fail(preference_section != NULL);
	g_return_if_fail(ERTY_IS_PREFERENCE_SECTION(preference_section));
	g_return_if_fail(prerequisite != NULL);
	g_return_if_fail(GTK_IS_TOGGLE_BUTTON(prerequisite));
	g_return_if_fail(dependant != NULL);
	g_return_if_fail(GTK_IS_WIDGET(dependant));

	current_dependant = dependant;
	va_start(args, dependant);

	do {
		gtk_signal_connect(
			GTK_OBJECT(prerequisite),
			"changed",
			GTK_SIGNAL_FUNC(erty_preference_section_sensitive),
			current_dependant
			);

		erty_preference_section_sensitive(	prerequisite, 
							current_dependant );

		current_dependant = va_arg(args, GtkWidget*);
	} while (current_dependant != NULL);

	va_end(args);
}
