#include <erty.h>
#include <gdk/gdkx.h>
#include <erty_entry.h>

#define	STEPSIZE	30
#define ERTY_KEY_PICKER_KEY_SEPARATOR		"+"
#define	ERTY_KEY_PICKER_POINTER_BUTTON		"Pointer_button"
#define	ERTY_KEY_PICKER_STATE_SEPARATOR		","

static void erty_key_picker_set_state(	ErtyKeyPicker *key_picker,
					guint state	);

static void erty_key_picker_add_to_key_list(	ErtyKeyPicker *key_picker, 
						gchar *str	);

static void erty_key_picker_sensitive(	ErtyKeyPicker *key_picker	);
static void erty_key_picker_insensitive(	ErtyKeyPicker *key_picker );
static gboolean erty_key_picker_advance_matrix(	ErtyKeyPicker *key_picker );
static void erty_key_picker_refresh_screen(	ErtyKeyPicker *key_picker );
static void erty_key_picker_lock_screen(	ErtyKeyPicker *key_picker );
static void erty_key_picker_destroy(	GtkObject *object	);

static GdkFilterReturn erty_key_picker_set_event(
		XEvent *xevent,
		GdkEventKey *event,
		ErtyKeyPicker *key_picker
		);

static gchar* erty_key_picker_get_key(	ErtyKeyPicker *key_picker	);

static void erty_key_picker_set_key(	ErtyKeyPicker *key_picker,
					gchar *key	);

static void erty_key_picker_grab_key(	ErtyKeyPicker *key_picker	);
static void erty_key_picker_class_init(	ErtyKeyPickerClass *klass	);
static void erty_key_picker_init(	ErtyKeyPicker *key_picker	);

static void erty_key_picker_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static void erty_key_picker_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	);

static ErtyPickerClass	*parent_class = NULL;

enum {	ARG_0,
	ARG_DISPLAY_VALUE,
	ARG_KEY	};

GtkType erty_key_picker_get_type(	void	)
/*	Return type identifier for type ErtyKeyPicker	*/
{
	static GtkType	key_picker_type = 0;

	if (!key_picker_type) {

		static const GtkTypeInfo	key_picker_info = {
				"ErtyKeyPicker",
				sizeof(ErtyKeyPicker),
				sizeof(ErtyKeyPickerClass),
				(GtkClassInitFunc)erty_key_picker_class_init,
				(GtkObjectInitFunc)erty_key_picker_init,
				NULL,
				NULL,
				(GtkClassInitFunc)NULL
		};

		key_picker_type = gtk_type_unique(	erty_picker_get_type(),
							&key_picker_info );
	}

	return key_picker_type;
}

GtkWidget* erty_key_picker_new(	void	)
/*	Create an instance of ErtyKeyPicker	*/
{
	ErtyKeyPicker	*key_picker;

	key_picker = gtk_type_new(erty_key_picker_get_type());

	return GTK_WIDGET(key_picker);
}

GtkWidget* erty_key_picker_new_with_label(	gchar *label	)
/*	Create an instance of ErtyKeyPicker with label	*/
{
	GtkWidget	*key_picker;

	g_return_val_if_fail(label != NULL, NULL);

	key_picker = erty_key_picker_new();
	erty_picker_set_label(ERTY_PICKER(key_picker), label);
	erty_picker_set_mode(ERTY_PICKER(key_picker), ERTY_PICKER_LABEL_FIRST);

	return key_picker;
}

static void erty_key_picker_init(	ErtyKeyPicker *key_picker	)
/*	Initialize an ErtyKeyPicker	*/
{
	GtkWidget	*entry, *widget, *button, *box;
	GtkTooltips	*tooltips;

	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));

	widget = GTK_WIDGET(key_picker);

	key_picker->keys = NULL;
	box = gtk_hbox_new(FALSE, 0);
	erty_picker_set_selector(ERTY_PICKER(key_picker), box);
	gtk_widget_show(box);

	entry = erty_entry_new(gtk_widget_get_name(widget));
	key_picker->entry = entry;
	gtk_box_pack_start(GTK_BOX(box), entry, FALSE, FALSE, 0);
	gtk_widget_show(entry);

	button = gtk_button_new_with_label(_("Grab..."));
	tooltips = gtk_tooltips_new();

	gtk_tooltips_set_tip(	tooltips, 
				button, 
				_("Grab a key/button combination"), 
				"NULL"	);

	gtk_signal_connect_object_after(	GTK_OBJECT(button),
						"clicked",
						erty_key_picker_insensitive,
						GTK_OBJECT(key_picker)	);

	gtk_signal_connect_object(	GTK_OBJECT(button),
					"clicked",
					erty_key_picker_grab_key,
					GTK_OBJECT(key_picker)	);

	gtk_signal_connect_object(	GTK_OBJECT(entry),
					"changed",
					erty_picker_changed,
					GTK_OBJECT(key_picker)	);

	gtk_container_add(GTK_CONTAINER(box), button);
	gtk_widget_show(button);
}

static void erty_key_picker_class_init(	ErtyKeyPickerClass *klass	)
/*	Initialize the ErtyKeyPicker class	*/
{
	GtkObjectClass	*object_class;

	object_class = (GtkObjectClass*)klass;
	parent_class = gtk_type_class(ERTY_TYPE_PICKER);

	gtk_object_add_arg_type(	"ErtyKeyPicker::key",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_KEY	);

	gtk_object_add_arg_type(	"ErtyKeyPicker::display_value",
					GTK_TYPE_STRING,
					GTK_ARG_READWRITE,
					ARG_DISPLAY_VALUE	);

	object_class->get_arg = erty_key_picker_get_arg;
	object_class->set_arg = erty_key_picker_set_arg;
	object_class->destroy = erty_key_picker_destroy;
}

static void erty_key_picker_set_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting for this object	*/
{
	ErtyKeyPicker	*key_picker;

	key_picker = ERTY_KEY_PICKER(object);

	switch (arg_id) {
		gchar	*str, key[50];
		guint	state;

		case ARG_KEY:
		case ARG_DISPLAY_VALUE:
			str = GTK_VALUE_STRING(*arg);

			g_assert(strstr(str, ",") != NULL);

			sscanf(str, "%d,%s", &state, key);
			erty_key_picker_set_key(key_picker, key);
			erty_key_picker_set_state(key_picker, state);
			break;
		default:
			break;
	}
}

static void erty_key_picker_get_arg(	GtkObject *object,
					GtkArg *arg,
					guint arg_id	)
/*	Implements argument setting	*/
{
	ErtyKeyPicker	*key_picker;

	key_picker = ERTY_KEY_PICKER(object);

	switch (arg_id) {
		gchar	*str;

		case ARG_KEY:
		case ARG_DISPLAY_VALUE:
			str = erty_key_picker_get_key(key_picker);

			if (str != NULL) {
				GString	*string;

				string = g_string_new(NULL);

				g_string_sprintf(
						string, 
						"%d%s%s", 
						key_picker->state,
						ERTY_KEY_PICKER_STATE_SEPARATOR,
						str);

				GTK_VALUE_STRING(*arg)  = string->str;
				g_string_free(string, FALSE);
			} else
				arg->type = GTK_TYPE_INVALID;

			break;
		default:
			arg->type = GTK_TYPE_INVALID;
			break;
	}
}

static void erty_key_picker_set_key(	ErtyKeyPicker *key_picker,
					gchar *key	)
/*	Set the key currently displayed by key_picker	*/
{
	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));
	g_return_if_fail(key != NULL);

	erty_entry_set_current(ERTY_ENTRY(key_picker->entry), key);
}

static gchar* erty_key_picker_get_key(	ErtyKeyPicker *key_picker	)
/*	Get the key currently displayed by key_picker	*/
{
	g_return_val_if_fail(key_picker != NULL, NULL);
	g_return_val_if_fail(ERTY_IS_KEY_PICKER(key_picker), NULL);

	return erty_entry_get_current(ERTY_ENTRY(key_picker->entry));
}

static void erty_key_picker_grab_key(	ErtyKeyPicker *key_picker	)
/*	Prompt the user for a key combination	*/
{
	gboolean	success;

	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));

	success = gdk_keyboard_grab(GDK_ROOT_PARENT(), FALSE, GDK_CURRENT_TIME);

	g_assert(success == 0);

	success = gdk_pointer_grab(	GDK_ROOT_PARENT(), 
					FALSE, 
					GDK_BUTTON_RELEASE_MASK |
					GDK_BUTTON_PRESS_MASK, 
					NULL, 
					NULL,
					GDK_CURRENT_TIME	);

	g_assert(success == 0);

	key_picker->keyboard_grab = TRUE;
	erty_key_picker_lock_screen(key_picker);
	gdk_key_repeat_disable();

	gdk_window_add_filter(	GDK_ROOT_PARENT(), 
				(GdkFilterFunc)erty_key_picker_set_event, 
				key_picker	);
}

static GdkFilterReturn erty_key_picker_set_event(
		XEvent *xevent,
		GdkEventKey *event,
		ErtyKeyPicker *key_picker
		)
/*	Add 'event' to key combination	*/
{
	GdkFilterReturn	value;

	g_return_val_if_fail(key_picker != NULL, GDK_FILTER_REMOVE);
	g_return_val_if_fail(ERTY_IS_KEY_PICKER(key_picker), GDK_FILTER_REMOVE);
	g_return_val_if_fail(xevent != NULL, GDK_FILTER_REMOVE);
	g_return_val_if_fail(event != NULL, GDK_FILTER_REMOVE);

	if (event->keyval) {
		gchar	*str;

		str = gdk_keyval_name(event->keyval);
		erty_key_picker_add_to_key_list(key_picker, str);
	} 

	if (xevent->type == ButtonRelease)

		erty_key_picker_add_to_key_list(
				key_picker, 
				ERTY_KEY_PICKER_POINTER_BUTTON
				);

	if (	(	(xevent->type == KeyRelease) ||
			(xevent->type == ButtonRelease)	) && 
		(	!gdk_events_pending()	)	) {

		GSList	*list;
		GString	*str;

		str= g_string_new(NULL);

		for (	list = key_picker->keys; 
			list != NULL; 
			list = list->next	) {

			str = g_string_append(str, list->data);

			if (list->next != NULL)

				str = g_string_append(
						str, 
						ERTY_KEY_PICKER_KEY_SEPARATOR
						);
		}

		erty_key_picker_set_state(key_picker, xevent->xkey.state);
		erty_key_picker_set_key(key_picker, str->str);
		g_string_free(str, FALSE);
		g_slist_free(key_picker->keys);
		key_picker->keys = NULL;
		erty_key_picker_sensitive(key_picker);
		XUngrabServer(GDK_DISPLAY());
		erty_key_picker_refresh_screen(key_picker);
		gdk_keyboard_ungrab(GDK_CURRENT_TIME);
		gdk_pointer_ungrab(GDK_CURRENT_TIME);
		key_picker->keyboard_grab = FALSE;
		value = GDK_FILTER_REMOVE;
		gdk_key_repeat_restore();

		gdk_window_remove_filter(
				GDK_ROOT_PARENT(), 
				(GdkFilterFunc)erty_key_picker_set_event, 
				key_picker
				);
	} else
		value = GDK_FILTER_CONTINUE;

	return value;
}

static void erty_key_picker_destroy(	GtkObject *object	)
/*	Destroy handler for ErtyKeyPicker	*/
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(object));

	if (ERTY_KEY_PICKER(object)->keyboard_grab) {
		gdk_keyboard_ungrab(GDK_CURRENT_TIME);
		gdk_key_repeat_restore();

		gdk_window_remove_filter(
				GDK_ROOT_PARENT(), 
				(GdkFilterFunc)erty_key_picker_set_event, 
				object
				);
	}

	(*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}

static void erty_key_picker_lock_screen(	ErtyKeyPicker *key_picker )
/*	Disable the screen from receiving user input	*/
{
	GtkStyle		*style;
	gint			width, height, i;
	GSList			*matrix;
	GdkSubwindowMode	mode;
	GdkGC			*gc;
	GdkGCValues		values;

	const gchar	*str = _("Please enter a valid key combination.");

	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));

	XGrabServer(GDK_DISPLAY());
	style = gtk_widget_get_style(GTK_WIDGET(key_picker));
	gc = style->bg_gc[GTK_STATE_NORMAL];
	gdk_gc_get_values(gc, &values);
	mode = values.subwindow_mode;
	gdk_gc_set_subwindow(gc, GDK_INCLUDE_INFERIORS);
	gc = style->fg_gc[GTK_STATE_NORMAL];
	gdk_gc_get_values(gc, &values);
	mode = values.subwindow_mode;
	gdk_gc_set_subwindow(gc, GDK_INCLUDE_INFERIORS);
	width = gdk_screen_width();
	height = gdk_screen_height();
	matrix = NULL;

	for (i = 0; i < width; i = i + STEPSIZE) {
		gint	j;

		for (j = 0; j < height; j = j + STEPSIZE) {
			GdkPoint	*point;

			point = (GdkPoint*)g_new(GdkPoint*, 1);
			point->x = i;
			point->y = j;
			matrix = g_slist_prepend(matrix, point);
		}
	}

	key_picker->matrix = matrix;

	gtk_timeout_add(	1, 
				(GtkFunction)erty_key_picker_advance_matrix, 
				key_picker	);

	gtk_main();

	gtk_draw_string(	style,
				GDK_ROOT_PARENT(),
				GTK_STATE_NORMAL,
				GNOME_PAD,
				GNOME_PAD + gdk_string_height(style->font, str),
				str	);

	gdk_gc_set_subwindow(gc, mode);
}

static gboolean erty_key_picker_advance_matrix(	ErtyKeyPicker *key_picker )
/*	Draw a rectangle defined by key_picker's current matrix field	*/
{
	GSList		*list;
	gint		index, x, y;
	GdkPoint	*point;
	GtkAllocation	allocation, geometry;
	GtkWidget	*widget;
	GtkStyle	*style;

	g_return_val_if_fail(key_picker != NULL, FALSE);
	g_return_val_if_fail(ERTY_IS_KEY_PICKER(key_picker), FALSE);

	list = key_picker->matrix;
	widget = GTK_WIDGET(key_picker);
	style = gtk_widget_get_style(widget);
	allocation = widget->allocation;

	index = (gint)((double)g_slist_length(list) * rand() / RAND_MAX);
	point = (GdkPoint*)g_slist_nth_data(list, index);
	gdk_window_get_deskrelative_origin(widget->window, &x, &y);

	if (	(point->x + STEPSIZE > x) &&
		(point->x < allocation.width + x) &&
		(point->y + STEPSIZE > y) &&
		(point->y < allocation.height + y)	) {

		if (point->x < x) {

			if (point->y < y)

				gtk_draw_box(	style,
						GDK_ROOT_PARENT(),
						GTK_STATE_NORMAL,
						GTK_SHADOW_NONE,
						point->x,
						point->y,
						STEPSIZE,
						y - point->y	);

			else if (point->y + STEPSIZE > allocation.height + y)

				gtk_draw_box(	style,
						GDK_ROOT_PARENT(),
						GTK_STATE_NORMAL,
						GTK_SHADOW_NONE,
						point->x,
						allocation.height + y,
						STEPSIZE,
						point->y + STEPSIZE - allocation.height - y	);

			geometry.x = point->x,
			geometry.y = point->y,
			geometry.width = x - point->x,
			geometry.height = STEPSIZE;

		} else if (point->y < y) {

			if (point->x + STEPSIZE > allocation.width + x)

				gtk_draw_box(	style,
						GDK_ROOT_PARENT(),
						GTK_STATE_NORMAL,
						GTK_SHADOW_NONE,
						x + allocation.width,
						point->y,
						point->x + STEPSIZE - allocation.width - x,
						STEPSIZE	);

			geometry.x = point->x,
			geometry.y = point->y,
			geometry.width = STEPSIZE,
			geometry.height = y - point->y;

		} else if (point->x + STEPSIZE > allocation.width + x) {

			if (point->y + STEPSIZE > allocation.height + y)

				gtk_draw_box(	style,
						GDK_ROOT_PARENT(),
						GTK_STATE_NORMAL,
						GTK_SHADOW_NONE,
						point->x,
						y + allocation.height,
						STEPSIZE,
						point->y + STEPSIZE - allocation.height - y	);
			geometry.x = allocation.width + x,
			geometry.y = point->y,

			geometry.width = 	point->x + STEPSIZE - 
						(x + allocation.width),

			geometry.height = STEPSIZE;

		} else if (point->y + STEPSIZE > allocation.height + y) {

			geometry.x = point->x,
			geometry.y = allocation.height + y,
			geometry.width = STEPSIZE,

			geometry.height = 	point->y + STEPSIZE - 
						allocation.height - y;
		}
	} else {
		geometry.x = point->x;
		geometry.y = point->y;
		geometry.width = STEPSIZE;
		geometry.height = STEPSIZE;
	}

	gtk_draw_box(	style,
			GDK_ROOT_PARENT(),
			GTK_STATE_NORMAL,
			GTK_SHADOW_NONE,
			geometry.x,
			geometry.y,
			geometry.width,
			geometry.height	);

	key_picker->matrix = g_slist_remove(list, point);

	gdk_flush();

	if (key_picker->matrix == NULL) {
		gtk_main_quit();

		return FALSE;
	} else
		return TRUE;
}

static void erty_key_picker_refresh_screen(	ErtyKeyPicker *key_picker )
/*	Refresh screen	*/
{
	GdkWindowAttr	attributes;
	GdkWindow	*window;

	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));

	attributes.x = 0;
	attributes.y = 0;
	attributes.width = gdk_screen_width ();
	attributes.height = gdk_screen_height ();
	attributes.window_type = GDK_WINDOW_TOPLEVEL;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.override_redirect = TRUE;
	attributes.event_mask = 0;

	window = gdk_window_new(	NULL, 
					&attributes, 
					GDK_WA_X | GDK_WA_Y | GDK_WA_NOREDIR );

	gdk_window_show (window);
	gdk_flush ();
	gdk_window_hide (window);

/*	XSendEvent(GDK_DISPLAY(), GDK_ROOT_PARENT(), TRUE, ExposureMask, */
}

static void erty_key_picker_insensitive(	ErtyKeyPicker *key_picker )
/*	Set the key_picker insensitive	*/
{
	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));

	gtk_container_foreach(	GTK_CONTAINER(key_picker), 
				(GtkCallback)gtk_widget_set_sensitive, 
				GINT_TO_POINTER(FALSE)	);
}

static void erty_key_picker_sensitive(	ErtyKeyPicker *key_picker	)
/*	Set the key_picker to sensitive	*/
{
	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));

	gtk_container_foreach(	GTK_CONTAINER(key_picker), 
				(GtkCallback)gtk_widget_set_sensitive, 
				GINT_TO_POINTER(TRUE)	);
}

static void erty_key_picker_add_to_key_list(	ErtyKeyPicker *key_picker, 
						gchar *str	)
/*	Add str to key_picker's key_list	*/
{
	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));
	g_return_if_fail(str != NULL);

	key_picker->keys = g_slist_append(key_picker->keys, str);
}

static void erty_key_picker_set_state(	ErtyKeyPicker *key_picker,
					guint state	)
/*	Set the mask state for key_picker	*/
{
	g_return_if_fail(key_picker != NULL);
	g_return_if_fail(ERTY_IS_KEY_PICKER(key_picker));
	g_return_if_fail(state > 0);

	key_picker->state = state;
}
